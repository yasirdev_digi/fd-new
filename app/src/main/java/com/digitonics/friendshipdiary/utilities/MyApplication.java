package com.digitonics.friendshipdiary.utilities;

import android.app.Application;

import com.digitonics.friendshipdiary.R;


public class MyApplication extends Application {
    public static final String TAG = MyApplication.class
            .getSimpleName();

    private static MyApplication mInstance;

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Thread.currentThread().setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                throwable.printStackTrace();
                throw new RuntimeException(throwable);
            }
        });

        mInstance = this;

    }
}
