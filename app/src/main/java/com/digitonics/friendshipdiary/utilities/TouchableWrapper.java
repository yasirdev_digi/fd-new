package com.digitonics.friendshipdiary.utilities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.widget.FrameLayout;

/**
 * Created by fahadali on 1/16/2018.
 */

public class TouchableWrapper extends FrameLayout {

    public boolean mMapIsTouched = false;

    public TouchableWrapper(@NonNull Context context) {
        super(context);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mMapIsTouched = true;
                break;

            case MotionEvent.ACTION_UP:
                mMapIsTouched = false;
                break;
        }

        return super.onInterceptTouchEvent(ev);
    }
}
