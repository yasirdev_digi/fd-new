package com.digitonics.friendshipdiary.utilities;

/**
 * Created by syed.kamil on 4/25/2018.
 */

public class Constants {
    public static final String PREFERENCE_KEY = "prefs", APP_TAG = "tag";
    public static final int IMAGES_GALLERY = 0, IMAGES_CAMERA = 1, VIDEO_GALLERY = 2, VIDEO_CAMERA = 3, AUDIO_RECORDER = 4,GET_LOCATION_PERMISSION_REQUEST = 111, MAP_ADDRESS_RESULT_CODE = 1020, DROP_IN_REQUEST = 1150;
    public static final int SPONSER = 0, WATCHER = 1 , SUPPORTER = 2;
    public static String GOOGLE_API_KEY = "AIzaSyAquzgo847shlU-SpPXLZMgShv6EW9pQmw";
}
