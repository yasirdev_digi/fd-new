package com.digitonics.friendshipdiary.utilities;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by syed.kamil on 4/27/2018.
 */

public class OverlapDecoration extends RecyclerView.ItemDecoration {

    private final static int vertOverlap = -20;

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.set(0, 0, vertOverlap, 0);

    }
}