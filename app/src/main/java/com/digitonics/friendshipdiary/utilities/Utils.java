package com.digitonics.friendshipdiary.utilities;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.models.ContactModel;
import com.google.android.exoplayer.C;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by syed.kamil on 10/11/2017.
 */

public class Utils {

     /* RecyclerView Animations Type */
    public static final int LIST_ANIM_FROM_BOTTOM = R.anim.list_anim_from_bottom;
    public static final int LIST_ANIM_FROM_RIGHT = R.anim.list_anim_from_right;
    public static final int LIST_ANIM_FROM_TOP = R.anim.list_anim_fall_down;
    public static final int LIST_GRID_ANIM_FROM_BOTTOM = R.anim.list_grid_anim_from_bottom;
    public static final int LIST_ANIM_SCALE = R.anim.list_grid_anim_scale;
    public static final int LIST_ANIM_SCALE_RANDOM = R.anim.list_grid_anim_scale_random;

    public static String JSON = "{\n" +
            "  \"menus\": [\n" +
            "    {\n" +
            "      \"id\": \"1\",\n" +
            "      \"image\": \"icon_goal_image\",\n" +
            "      \"selected\": true\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"2\",\n" +
            "      \"image\": \"icon_newsfeed_image\",\n" +
            "      \"selected\": false\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"3\",\n" +
            "      \"image\": \"icon_friends_image\",\n" +
            "      \"selected\": false\n" +
            "    },\n" +
            "    {\n" +
            "      \"id\": \"4\",\n" +
            "      \"image\": \"icon_profile_image\",\n" +
            "      \"selected\": false\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    public static boolean isEmailValid(CharSequence email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isEmpty(String str) {

        return (str == null) || (str.length() == 0);
    }
    public static int getResId(Context context, String resName) {

        try {
            return context.getResources().getIdentifier(resName,"drawable",context.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static Dialog showCustomDialog(Context context, View layoutView) {
        Dialog dialogRating = new Dialog(context);
        dialogRating.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialogRating.setContentView(layoutView);
        dialogRating.setCanceledOnTouchOutside(true);

        dialogRating.setCancelable(true);

        try {
            dialogRating.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialogRating.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        } catch (Exception e) {
            Log.e("Test","EXCP: " + e.getMessage());
        }

        dialogRating.show();

        return dialogRating;
    }

    public static void hide(Activity activity){
        /*InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText())
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0); // hide*/
        try{
        Utils.hideSoftKeyboard(activity.getWindow().getDecorView().getRootView().findFocus());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static void hideSoftKeyboard(View v) {
        try {
            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static Spannable getColoredString(String mString, int colorId) {
        Spannable spannable = new SpannableString(mString);
        spannable.setSpan(new ForegroundColorSpan(colorId), 0, spannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    public static void animRecyclerView(final RecyclerView recyclerView, final int animType) {
        final Context context = recyclerView.getContext();

        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, animType);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    /**
     * Show Toast Message
     *
     * @param context - Activity Context
     * @param message - String message to display
     */
    public static void showToast(Context context, String message) {
        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
    }

    /**
     * @param context  - Activity Context
     * @param listener - TimeSet Listener
     * @param is24Hour - 12/24 hour format
     */
    public static void showTimePicker(Context context, TimePickerDialog.OnTimeSetListener listener, boolean is24Hour) {
        Calendar calendar = Calendar.getInstance();
        new TimePickerDialog(context, listener,
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), is24Hour).show();
    }

    /**
     * @param context  - Activity Context
     * @param listener - TimeSet Listener
     */
    public static void showDatePicker(Context context, DatePickerDialog.OnDateSetListener listener) {
        Calendar calendar = Calendar.getInstance();
        new DatePickerDialog(context, listener,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public static String getDate(Date date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        return dateFormat.format(date);
    }


    public static String get12HourTime(String time){
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(time);
            System.out.println(dateObj);
            return new SimpleDateFormat("hh:mm a").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static Date getDate(String date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static long getTime(String date){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            return dateFormat.parse(date).getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Date().getTime();
    }


    public static String getDate(String date, String format){
        DateFormat dateFormat = new SimpleDateFormat(format);
        DateFormat returnFormat = new SimpleDateFormat("dd-MMM-yyyy");

        try {
            return returnFormat.format(dateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnFormat.format(new Date());
    }

    public static String getDate(){
        DateFormat returnFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return returnFormat.format(new Date());
    }

    public static String getDate(String date, String fromFormat, String toFormat){
        DateFormat dateFormat = new SimpleDateFormat(fromFormat);
        DateFormat returnFormat = new SimpleDateFormat(toFormat);

        try {
            return returnFormat.format(dateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnFormat.format(new Date());
    }

    public static String getDateGoal(String date, String format){
        DateFormat dateFormat = new SimpleDateFormat(format);
        DateFormat returnFormat = new SimpleDateFormat("yyyy/MM/dd");

        try {
            return returnFormat.format(dateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnFormat.format(new Date());
    }
    public static String getMapURL(Double lat, Double lng){
        String name = "https://maps.googleapis.com/maps/api/staticmap?zoom=16&size=600x300&maptype=roadmap&" +
               "markers=color:red%7Clabel:D%7C"+ lat + "," + lng +"" +
               "&key=AIzaSyAquzgo847shlU-SpPXLZMgShv6EW9pQmw";

        return name;
    }
    public static void expandOrCollapseView(final View v, boolean expand) {

        if (expand) {
            v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            final int targetHeight = v.getMeasuredHeight();
            v.getLayoutParams().height = 0;
            v.setVisibility(View.VISIBLE);
            ValueAnimator valueAnimator = ValueAnimator.ofInt(targetHeight);
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    v.getLayoutParams().height = (int) animation.getAnimatedValue();
                    v.requestLayout();
                }
            });
            valueAnimator.setInterpolator(new DecelerateInterpolator());
            valueAnimator.setDuration(500);
            valueAnimator.start();
        } else {
            final int initialHeight = v.getMeasuredHeight();
            ValueAnimator valueAnimator = ValueAnimator.ofInt(initialHeight, 0);
            valueAnimator.setInterpolator(new DecelerateInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    v.getLayoutParams().height = (int) animation.getAnimatedValue();
                    v.requestLayout();
                    if ((int) animation.getAnimatedValue() == 0)
                        v.setVisibility(View.GONE);
                }
            });
            valueAnimator.setInterpolator(new DecelerateInterpolator());
            valueAnimator.setDuration(500);
            valueAnimator.start();
        }
    }

    public static Bitmap scaleBitmapAndKeepRation(Bitmap TargetBmp, int reqHeightInPixels, int reqWidthInPixels)
    {
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, TargetBmp.getWidth(), TargetBmp.getHeight()), new RectF(0, 0, reqWidthInPixels, reqHeightInPixels), Matrix.ScaleToFit.CENTER);
        Bitmap scaledBitmap = Bitmap.createBitmap(TargetBmp, 0, 0, TargetBmp.getWidth(), TargetBmp.getHeight(), m, true);
        return scaledBitmap;
    }

    public static Bitmap scaleImageKeepAspectRatio(Bitmap scaledGalleryBitmap)
    {
        int imageWidth = scaledGalleryBitmap.getWidth();
        int imageHeight = scaledGalleryBitmap.getHeight();
        int newHeight = (imageHeight * 500)/imageWidth;
        scaledGalleryBitmap = Bitmap.createScaledBitmap(scaledGalleryBitmap, 500, newHeight, false);
        return scaledGalleryBitmap;
    }

    public static Bitmap resizeBmpImage(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > ratioBitmap) {
                finalWidth = (int) ((float)maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float)maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    /**
     *
     * @param bitmap
     * @param image_absolute_path
     * @return
     * @throws IOException
     */
    public static Bitmap modifyOrientation(Bitmap bitmap, String image_absolute_path) throws IOException {
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    private static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        Bitmap convertbitmap = null;
        convertbitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return convertbitmap;
    }

    private static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    /**
     * Shows User Prompt Dialog with Default Layout
     *
     * @param context       - Activity Context
     * @param title         - Dialog title
     * @param message       - Dialog prompt message
     * @param posButtonText - Dialog positive Button Text
     * @param negButtonText - Dialog negative Button Text
     * @param posListener   - Dialog positive Button Listener
     * @param negListener   - Dialog negative Button Listener
     */
    public static void showPromptDialog(Context context, String title, String message, String posButtonText, String negButtonText, DialogInterface.OnClickListener posListener, DialogInterface.OnClickListener negListener, boolean isCancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        if (message != null)
            builder.setMessage(message);
        builder.setCancelable(isCancelable);
        if (posListener != null)
            builder.setPositiveButton(posButtonText, posListener);
        if (negListener != null)
            builder.setNegativeButton(negButtonText, negListener);
        builder.show();
    }

    public static void showPromptDialog(Context context, String title, String message, String posButtonText, String negButtonText,String neutralText, DialogInterface.OnClickListener neutralListener, DialogInterface.OnClickListener posListener, DialogInterface.OnClickListener negListener, boolean isCancelable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        if (message != null)
            builder.setMessage(message);
        builder.setCancelable(isCancelable);
        if (posListener != null)
            builder.setPositiveButton(posButtonText, posListener);
        if (negListener != null)
            builder.setNegativeButton(negButtonText, negListener);
        if (neutralListener != null )
            builder.setNeutralButton(neutralText, neutralListener);
        builder.show();
    }
    public static String getString(String str){
        return str == null ? "" : str;
    }

    public static boolean checkGPSStatus(final Context context) {

        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showGPSPromptDialog(context);
            return false;
        }
        return true;
    }
    public static void showGPSPromptDialog(final Context context) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String message = context.getResources().getString(R.string.error_gps_prompt);

        builder.setMessage(message)
                .setPositiveButton("Turn On",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                context.startActivity(new Intent(action));
                                d.dismiss();
                            }
                        })
                .setNegativeButton(context.getResources().getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                            }
                        });
        builder.create().show();
    }

    public static List<ContactModel> getContacts(Context ctx) {
        List<ContactModel> list = new ArrayList<>();
        ContentResolver contentResolver = ctx.getContentResolver();
        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor cursorInfo = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(ctx.getContentResolver(),
                            ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(id)));

                    Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(id));
                    Uri pURI = Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

                    Bitmap photo = null;
                    if (inputStream != null) {
                        photo = BitmapFactory.decodeStream(inputStream);
                    }
                    while (cursorInfo.moveToNext()) {
                        ContactModel info = new ContactModel();
                        info.id = id;
                        info.name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        info.mobileNumber = cursorInfo.getString(cursorInfo.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        info.photo = photo;
                        info.photoURI= pURI;
                        list.add(info);
                    }

                    cursorInfo.close();
                }
            }
            cursor.close();
        }
        return list;
    }

    public static void shareIntent(Context context, String number, String message)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("sms:" +  number));
        intent.putExtra("sms_body", message);
        context.startActivity(intent);
    }

    public static String getTaskLevel(String level){
        String levelName = "1";
        switch (level){
            case "1":
                levelName = "Easy";
                break;
            case "2":
                levelName = "Medium";
                break;
            case "3":
                levelName = "Hard";
                break;
        }
        return levelName;
    }

    public static String getDeviceID(Context context){
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static String getURL(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        return url;
    }

    public static int getStatusPosition(String status){
        int pos = 0;
        switch (status){
            case "active":
                pos = 0;
                break;
            case "on_break":
                pos = 1;
                break;
            case "no_pressure":
                pos = 2;
                break;
        }
        return pos;
    }

    public static String getStatus(String status){
        String pos = status;
        switch (status){
            case "active":
                pos = "Active";
                break;
            case "on_break":
                pos = "On Break";
                break;
            case "no_pressure":
                pos = "No Pressure";
                break;
        }
        return pos;
    }

    public static void playAudio(Context context, int mediaID) {
        try {
            MediaPlayer mediaPlayer=MediaPlayer.create(context, mediaID);
            mediaPlayer.start();
        } catch (Exception e) {
        }
    }

    public static String getRelativeTime(CharSequence time){
        if (time.toString().equalsIgnoreCase("0 minutes ago"))
            return "Just now";
        return time.toString();
    }

    public static boolean checkInternetConnectivity(Context context) {

        if (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_NETWORK_STATE) !=
                PackageManager.PERMISSION_GRANTED) {
            Log.e("CHECK", "Permission not granted to access Network State");

            Toast.makeText(context, "Permission not granted to access Network State...", Toast.LENGTH_LONG).show();

            return false;
        }

        ConnectivityManager connectivitymanager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo[] networkInfo = connectivitymanager.getAllNetworkInfo();

        for (NetworkInfo netInfo : networkInfo) {

            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    return true;

            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    return true;
        }
        return false;
    }

    public float getExifAngle(Context context, Uri uri) {
        try {
            ExifInterface exifInterface = getExifInterface(context, uri);
            if(exifInterface == null) {
                return -1f;
            }

            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return 90f;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return 180f;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return 270f;
                case ExifInterface.ORIENTATION_NORMAL:
                    return 0f;
                case ExifInterface.ORIENTATION_UNDEFINED:
                    return -1f;
                default:
                    return -1f;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return -1f;
        }
    }

    @Nullable
    public ExifInterface getExifInterface(Context context, Uri uri) {
        try {
            String path = uri.toString();
            if (path.startsWith("file://")) {
                return new ExifInterface(path);
            }
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (path.startsWith("content://")) {
                    InputStream inputStream = context.getContentResolver().openInputStream(uri);
                    return new ExifInterface(inputStream);
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static File getOutputImageFile(Context context) {

        File mediaFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath()
                + File.separator
                + context.getResources().getString(R.string.app_name).replace(" ","")
                + "_IMG_" + System.currentTimeMillis() + ".jpg");

        return mediaFile;
    }
}
