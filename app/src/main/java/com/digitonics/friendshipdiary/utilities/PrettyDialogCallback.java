package com.digitonics.friendshipdiary.utilities;

/**
 * Created by mJafarinejad on 8/16/2017.
 */

public interface PrettyDialogCallback {
    void onClick();
}
