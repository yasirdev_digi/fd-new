package com.digitonics.friendshipdiary.interfaces;

/**
 * Created by apple on 7/24/16.
 */
public interface UpdateRunnable {
    public void run(Object data, int pos, boolean isUpdate);
}
