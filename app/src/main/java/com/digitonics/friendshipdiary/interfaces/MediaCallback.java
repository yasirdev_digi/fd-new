package com.digitonics.friendshipdiary.interfaces;

import android.content.Intent;


public interface MediaCallback {
    public void onMediaLoad(int reqCode, Intent data);
}
