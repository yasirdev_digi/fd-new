package com.digitonics.friendshipdiary.interfaces;

/**
 * Created by apple on 7/24/16.
 */
public interface CustomRunnable {
    public void run(Object data);
}
