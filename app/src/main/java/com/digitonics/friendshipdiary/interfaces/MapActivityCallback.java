package com.digitonics.friendshipdiary.interfaces;

import android.content.Intent;

/**
 * Created by Fahad Ali Khan on 11/29/2017.
 */
public interface MapActivityCallback {
    public void onLocationFetched(int reqCode, Intent data);
}
