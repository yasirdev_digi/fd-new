package com.digitonics.friendshipdiary.interfaces;

import com.digitonics.friendshipdiary.models.CommentModel;
import com.digitonics.friendshipdiary.models.DairyModel;
import com.digitonics.friendshipdiary.models.DeliveryAddressModel;
import com.digitonics.friendshipdiary.models.Friend;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.InfoModel;
import com.digitonics.friendshipdiary.models.NewsFeedModel;
import com.digitonics.friendshipdiary.models.NotificationModel;
import com.digitonics.friendshipdiary.models.OtherGoalModel;
import com.digitonics.friendshipdiary.models.PaymentModel;
import com.digitonics.friendshipdiary.models.ProofModel;
import com.digitonics.friendshipdiary.models.TaskModel;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.models.WishModel;
import com.digitonics.friendshipdiary.webservice.WebResponse;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface WebApi {


    @FormUrlEncoded
    @POST("login")
    Call<WebResponse<UserModel>> login(@Field("email") String username, @Field("password") String password, @Field("login_type") String loginType);

    @FormUrlEncoded
    @POST("login")
    Call<WebResponse<UserModel>> login(@Field("social_id") String socailId, @Field("social_type") String socialType, @Field("login_type") String loginType, @Field("email") String email, @Field("social_username") String fnmae, @Field("image_url") String image);

    @FormUrlEncoded
    @POST("forgotPassword")
    Call<WebResponse<String>> forgotPassword(@Field("email") String email);


    @FormUrlEncoded
    @POST("registration")
    Call<WebResponse<UserModel>> register(@Field("email") String email, @Field("password") String password, @Field("first_name") String firstName, @Field("last_name") String lastName, @Field("user_type") String userType);

    @POST("alluser")
    Call<WebResponse<ArrayList<UserModel>>> allUser(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST("creategoal")
    Call<WebResponse<Object>> createGoal(@Header("Authorization") String token, @Field("name") String name, @Field("description") String description, @Field("start_date") String startDate,
                                         @Field("end_date") String endDate, @Field("goal_type") String goalType, @Field("add_friend") String addfriends);

    @GET("findAllGoal")
    Call<WebResponse<ArrayList<GoalModel>>> allGoal(@Header("Authorization") String token, @Query("type") String type, @Query("status") String status);
    @GET("findAllGoal")
    Call<WebResponse<ArrayList<GoalModel>>> allGoal(@Header("Authorization") String token, @Query("type") String type);


    @GET("goaldetail")
    Call<WebResponse<GoalModel>> goalDetail(@Header("Authorization") String token, @Query("goal_id") String goalId);

    @GET("findtaskbyid")
    Call<WebResponse<TaskModel>> findTaskById(@Header("Authorization") String token, @Query("task_id") String goalId);

    @GET("postbyid")
    Call<WebResponse<NewsFeedModel>> findPostById(@Header("Authorization") String token, @Query("post_id") String post_id);

    @GET("findtaskprovebyid")
    Call<WebResponse<ProofModel>> findProofById(@Header("Authorization") String token, @Query("prove_id") String post_id);

    @FormUrlEncoded
    @POST("updategoal")
    Call<WebResponse<Object>> updateGoal(@Header("Authorization") String token, @Field("name") String name, @Field("description") String description, @Field("start_date") String startDate,
                                         @Field("end_date") String endDate, @Field("goal_id") String goalId, @Field("add_friend") String addfriends);
    @FormUrlEncoded
    @POST("updategoal")
    Call<WebResponse<Object>> updateGoal(@Header("Authorization") String token, @Field("name") String name, @Field("description") String description, @Field("start_date") String startDate,
                                         @Field("end_date") String endDate, @Field("goal_id") String goalId);

    @FormUrlEncoded
    @POST("taskcreate")
    Call<WebResponse<Object>> createTask(@Header("Authorization") String token, @Field("name") String name, @Field("description") String description, @Field("start_date") String startDate,
                                         @Field("end_date") String endDate, @Field("task_type") String taskType, @Field("points") String points, @Field("rewards") String rewards, @Field("goal_id") String goalId);

    @FormUrlEncoded
    @POST("taskupdate")
    Call<WebResponse<Object>> updateTask(@Header("Authorization") String token, @Field("name") String name, @Field("description") String description, @Field("start_date") String startDate,
                                         @Field("end_date") String endDate, @Field("task_type") String taskType, @Field("points") String points, @Field("rewards") String rewards, @Field("id") String id);


    @FormUrlEncoded
    @POST("dairycreate")
    Call<WebResponse<Object>> createDiary(@Header("Authorization") String token, @Field("title") String name, @Field("description") String description, @Field("start_date") String startDate,
                                          @Field("time") String timne, @Field("user_created_at") String date);

    @FormUrlEncoded
    @POST("dairyupdate")
    Call<WebResponse<Object>> updateDairy(@Header("Authorization") String token, @Field("title") String name, @Field("description") String description, @Field("start_date") String startDate,
                                          @Field("time") String timne, @Field("id") String id, @Field("user_created_at") String date);

    @GET("dairylist")
    Call<WebResponse<ArrayList<DairyModel>>> allDairy(@Header("Authorization") String token);

    @Multipart
    @POST("userupdate")
    Call<WebResponse<UserModel>> userUpdate(@Header("Authorization") String token,@Part MultipartBody.Part image, @Part("first_name") RequestBody firstName, @Part("last_name") RequestBody lastName, @Part("phone_no") Long phoneNo, @Part("about") RequestBody about, @Part("social_username") RequestBody username,
                                       @Part("facebook_link") RequestBody flink, @Part("gmail_link") RequestBody glink, @Part("twitter_link") RequestBody tlink,
                                            @Part("instagram_link") RequestBody ilink, @Part MultipartBody.Part[] qualities,  @Part("is_active") RequestBody isActive);

    @GET("userdetail")
    Call<WebResponse<UserModel>> userDetail(@Header("Authorization") String token, @Query("user_id") String userId);

    @GET("followerlist")
    Call<WebResponse<ArrayList<UserModel>>> getFriends(@Header("Authorization") String token);

    @POST("follow_unfollow")
    @FormUrlEncoded
    Call<WebResponse<String>> followToggle(@Header("Authorization") String token, @Field("follow_id") String timne);

    @Multipart
    @POST("post")
    Call<WebResponse<NewsFeedModel>> post(@Header("Authorization") String token,@Part MultipartBody.Part image, @Part("post") RequestBody postText, @Part MultipartBody.Part[] friends, @Part("user_created_at") RequestBody date);

    @Multipart
    @POST("post")
    Call<WebResponse<NewsFeedModel>> post(@Header("Authorization") String token, @Part MultipartBody.Part[] image, @Part("post") RequestBody postText, @Part MultipartBody.Part[] friends,@Part("user_created_at") RequestBody date);

    @Multipart
    @POST("post")
    Call<WebResponse<NewsFeedModel>> post(@Header("Authorization") String token, @Part("post") RequestBody postText, @Part MultipartBody.Part[] friends, @Part("user_created_at") RequestBody date);

    @Multipart
    @POST("post")
    Call<WebResponse<NewsFeedModel>> post(@Header("Authorization") String token, @Part("post") RequestBody postText, @Part("latitude") Double lat, @Part("longitude") Double lng, @Part MultipartBody.Part[] friends, @Part("user_created_at") RequestBody date);


    ///Post Update

    @Multipart
    @POST("postupdate")
    Call<WebResponse<NewsFeedModel>> postUpdate(@Header("Authorization") String token,@Part MultipartBody.Part image, @Part("post") RequestBody postText, @Part("id") RequestBody id);

    @Multipart
    @POST("postupdate")
    Call<WebResponse<NewsFeedModel>> postUpdate(@Header("Authorization") String token, @Part MultipartBody.Part[] image, @Part("post") RequestBody postText, @Part("id") RequestBody id);

    @Multipart
    @POST("postupdate")
    Call<WebResponse<NewsFeedModel>> postUpdate(@Header("Authorization") String token, @Part("post") RequestBody postText, @Part("id") RequestBody id);

    @Multipart
    @POST("postupdate")
    Call<WebResponse<NewsFeedModel>> postUpdate(@Header("Authorization") String token, @Part("post") RequestBody postText, @Part("latitude") Double lat, @Part("longitude") Double lng, @Part("id") RequestBody id);


    @GET("postlist")
    Call<WebResponse<ArrayList<NewsFeedModel>>> getPosts(@Header("Authorization") String token);

    @POST("postlike")
    @FormUrlEncoded
    Call<WebResponse<String>> postLike(@Header("Authorization") String token, @Field("post_id") String postId);

    @POST("comment")
    @FormUrlEncoded
    Call<WebResponse<CommentModel>> postComment(@Header("Authorization") String token, @Field("post_id") String postId, @Field("comment") String comment, @Field("user_created_at") String date);

    @GET("findOtherGoal")
    Call<WebResponse<OtherGoalModel>> geOtherGoal(@Header("Authorization") String token);

    @Multipart
    @POST("addprove")
    Call<WebResponse<ProofModel>> addProve(@Header("Authorization") String token, @Part MultipartBody.Part image, @Part("task_id") RequestBody taskId,@Part("user_created_at") RequestBody created);

    @GET("userdetail")
    Call<WebResponse<UserModel>> getUserDetail(@Header("Authorization") String token, @Query("user_id") String userId);

    @POST("vote")
    @FormUrlEncoded
    Call<WebResponse<Object>> postVote(@Header("Authorization") String token, @Field("task_id") String task_id, @Field("task_level") String task_level);

    @POST("updategoal")
    @FormUrlEncoded
    Call<WebResponse<GoalModel>> updateGoal(@Header("Authorization") String token, @Field("goal_id") String goal_id, @Field("status") String status);

    @GET("rewards?status=completed&type=public")
    Call<WebResponse<UserModel>> getRewards(@Header("Authorization") String token);

    @GET("invested")
    Call<WebResponse<PaymentModel>> getPayments(@Header("Authorization") String token);


    @FormUrlEncoded
    @POST("wishlistcreate")
    Call<WebResponse<WishModel>> createWishList(@Header("Authorization") String token, @Field("title") String name, @Field("description") String description, @Field("product_url") String url,
                                                @Field("price") String price, @Field("goal_id") String goalId);

    @FormUrlEncoded
    @POST("wishlistupdate")
    Call<WebResponse<WishModel>> updateWishList(@Header("Authorization") String token, @Field("title") String name, @Field("description") String description, @Field("product_url") String url,
                                                @Field("price") String price, @Field("id") String Id);

    @FormUrlEncoded
    @POST("deliveryaddresscreate")
    Call<WebResponse<DeliveryAddressModel>> createDeliveryAddress(@Header("Authorization") String token, @Field("address") String address, @Field("state") String state, @Field("country") String country, @Field("zipcode") String zipcode,
                                             @Field("goal_id") String goalId);
    @FormUrlEncoded
    @POST("deliveryaddressupdate")
    Call<WebResponse<DeliveryAddressModel>> updateDeliveryAddress(@Header("Authorization") String token, @Field("address") String address, @Field("state") String state, @Field("country") String country, @Field("zipcode") String zipcode,
                                                                  @Field("id") String goalId);

    @POST("removeById")
    @FormUrlEncoded
    Call<WebResponse<Object>> deletePost(@Header("Authorization") String token, @Field("id") String postId);

    @POST("postreport")
    @FormUrlEncoded
    Call<WebResponse<Object>> postReport(@Header("Authorization") String token, @Field("id") String postId, @Field("comment") String comment, @Field("status") String status);

    @POST("provecommentcreate")
    @FormUrlEncoded
    Call<WebResponse<CommentModel>> postProveComment(@Header("Authorization") String token, @Field("prove_id") String postId, @Field("comment") String comment, @Field("user_created_at") String date);

    @POST("goalremove")
    @FormUrlEncoded
    Call<WebResponse<Object>> deleteGoal(@Header("Authorization") String token, @Field("id") String postId);

    @POST("pointshare")
    @FormUrlEncoded
    Call<WebResponse<Object>> pointShare(@Header("Authorization") String token, @Field("goal_id") String goalId,@Field("points") String points);

    @FormUrlEncoded
    @POST("userupdate")
    Call<WebResponse<Object>> changePassword(@Header("Authorization") String token,@Field("old_password") String old, @Field("password") String newpassword);

    @GET("activity/notification/list")
    Call<WebResponse<ArrayList<NotificationModel>>> getNotification(@Header("Authorization") String token);

    @POST("setdevicetoken")
    @FormUrlEncoded
    Call<WebResponse<Object>> deviceToken(@Header("Authorization") String token, @Field("udid") String udid,@Field("token") String regtoken,@Field("type") String type);

    @POST("feedback")
    @FormUrlEncoded
    Call<WebResponse<Object>> feedback(@Header("Authorization") String token, @Field("feedback") String feedback, @Field("rating") String rating);

    @POST("taskpayment")
    @FormUrlEncoded
    Call<WebResponse<Object>> taskPayment(@Header("Authorization") String token, @Field("nonce_id") String nonce, @Field("amount") String amount, @Field("task_id") String taskId);

    @POST("packageupgrade")
    @FormUrlEncoded
    Call<WebResponse<Object>> proofPayment(@Header("Authorization") String token, @Field("nonce_id") String nonce, @Field("amount") String amount, @Field("package_id") String taskId);

    @POST("logout")
    @FormUrlEncoded
    Call<WebResponse<Object>> logout(@Header("Authorization") String token, @Field("udid") String udid, @Field("device_type") String type);


    @FormUrlEncoded
    @POST("userupdate")
    Call<WebResponse<UserModel>> notificaiton(@Header("Authorization") String token, @Field("notification") String ilink);

    @GET("updategoalfriend")
    Call<WebResponse<ArrayList<UserModel>>> getFriendList(@Header("Authorization") String token,@Query("goal_id") String userId);

    @POST("dairyremove")
    @FormUrlEncoded
    Call<WebResponse<Object>> deleteDiary(@Header("Authorization") String token, @Field("id") String postId);

    @POST("postcommentremove")
    @FormUrlEncoded
    Call<WebResponse<Object>> commentRemove(@Header("Authorization") String token, @Field("id") String postId);

    @POST("provecommentremove")
    @FormUrlEncoded
    Call<WebResponse<Object>> proofRemove(@Header("Authorization") String token, @Field("id") String postId);

    @GET("pages")
    Call<WebResponse<ArrayList<InfoModel>>> getInfo(@Header("Authorization") String token, @Query("page") String page);

    @POST("goalsponorfeedback")
    @FormUrlEncoded
    Call<WebResponse<String>> postSponsor(@Header("Authorization") String token, @Field("is_agreed") String is_agreed, @Field("id") String id,@Field("friend_id") String friend_id);

    @POST("goalsponorfeedback")
    @FormUrlEncoded
    Call<WebResponse<String>> sendFeeback(@Header("Authorization") String token, @Field("feedback") String feedback, @Field("id") String id,@Field("friend_id") String friend_id);
}
