package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.activities.VideoActivity;
import com.digitonics.friendshipdiary.activities.ZoomImageActivity;
import com.digitonics.friendshipdiary.fragments.FriendsFragment;
import com.digitonics.friendshipdiary.fragments.TagFriends;
import com.digitonics.friendshipdiary.fragments.TagListFragment;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.NewsFeedModel;
import com.digitonics.friendshipdiary.utilities.RoundedCornersTransformation;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.ArrayList;


public class NewsfeedAdapter extends RecyclerView.Adapter<NewsfeedAdapter.BaseViewHolder>{

    private Context context;
    public static final String TAG_MY_TASK_FRAGMENT = "MyTaskFragment";
    private OnNewsFeedItemClickListener mListener;
    private ArrayList<NewsFeedModel> mNewsFeed;
    private CustomRunnable mRunnable;
    private EditText mPostText;
    private Button mPostButton;

    public NewsfeedAdapter(OnNewsFeedItemClickListener listener,CustomRunnable customRunnable, ArrayList<NewsFeedModel> newsFeed ){
        mListener = listener;
        mNewsFeed = newsFeed;
        mRunnable = customRunnable;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0)
            return new ViewHeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_newsfeed_header, parent, false));
        else /*if (viewType%2 != 0)*/
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_newsfeed, parent, false);/*
        else
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_newsfeed_text, parent, false);*/

        return new ViewHolder(view);
    }

    public void changeData(NewsFeedModel model){
        mNewsFeed.remove(0);
        mNewsFeed.add(0, model);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        try {
            context = holder.itemView.getContext();
            holder.itemView.setTag(position);
            NewsFeedModel newsFeedModel = mNewsFeed.get(position);

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()){
                        case R.id.media:
                            ((BaseActivity)v.getContext()).showMediaDailog();
                            break;
                        case R.id.tag_friends:
                            ((BaseActivity)context).addFragment(TagFriends.newInstance(mRunnable),"");
                            break;
                        case R.id.location:
                            if (Utils.checkGPSStatus(v.getContext()) == true) {
                                //mMapClicked = 1;
                                ((BaseActivity) v.getContext()).openMap();
                            }
                            break;
                        case R.id.post:
                            ViewHeaderHolder headerHolder = (ViewHeaderHolder) v.getTag();
                            int pos = (int)headerHolder.itemView.getTag();
                            v.setVisibility(View.GONE);
                            mListener.onPostClick(headerHolder.postET.getText().toString());
                            break;
                        case R.id.like:
                            final int position = (int)v.getTag();
                            ((BaseActivity)context).mApiBuilder.postLike(BaseActivity.USER.getAccessToken(),mNewsFeed.get(position).getId(),new CustomRunnable() {
                                @Override
                                public void run(Object data) {
                                    if (mNewsFeed.get(position).getIsLike())
                                        mNewsFeed.get(position).setIsLike("0");
                                    else
                                        mNewsFeed.get(position).setIsLike("1");
                                    notifyDataSetChanged();
                                }
                            }, context);
                            break;
                        case R.id.media_list:
                            pos = (int)v.getTag();
                            Intent mediaIntent = new Intent(Intent.ACTION_VIEW);
                            if (mNewsFeed.get(pos).getVideoUrl() != null){
                                Intent videoIntent = new Intent(v.getContext(), VideoActivity.class);
                                videoIntent.putExtra("url",mNewsFeed.get(pos).getVideoUrl());
                                v.getContext().startActivity(videoIntent);
                                /*mediaIntent .setDataAndType(Uri.parse(mNewsFeed.get(pos).getVideoUrl()), "video");
                                context.startActivity(mediaIntent);*/
                            }
                            else if (mNewsFeed.get(pos).getAudioUrl() != null){
                                Intent videoIntent = new Intent(v.getContext(), VideoActivity.class);
                                videoIntent.putExtra("url",mNewsFeed.get(pos).getAudioUrl());
                                v.getContext().startActivity(videoIntent);
                                /*mediaIntent.setDataAndType(Uri.parse(mNewsFeed.get(pos).getAudioUrl()), "audio");
                                context.startActivity(mediaIntent);*/
                            }else if (mNewsFeed.get(pos).getPhotoUrl() != null && mNewsFeed.get(pos).getPhotoUrl().size() > 0){
                                Intent videoIntent = new Intent(v.getContext(), ZoomImageActivity.class);
                                videoIntent.putExtra("image",mNewsFeed.get(pos).getPhotoUrl().get(0).get3x());
                                v.getContext().startActivity(videoIntent);
                            }else if (mNewsFeed.get(pos).getLatitude() != null && mNewsFeed.get(pos).getLongitude() != null){
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?z=12&t=m&q=" +mNewsFeed.get(pos).getLatitude() +"," + mNewsFeed.get(pos).getLongitude()));
                                context.startActivity(intent);
                            }
                            break;

                        case R.id.day:
                            pos = (int)v.getTag();
                            displayPopupWindow(v,v.getContext(),mNewsFeed.get(pos).getUserId().equalsIgnoreCase(BaseActivity.USER.getId()),pos);

                            break;
                        case R.id.friends_count:
                            pos = (int)v.getTag();
                            ((BaseActivity)v.getContext()).addFragment(TagListFragment.newInstance(mNewsFeed.get(pos).getTagFriend()));
                            break;

                    }

                }
            };
            if(position == 0){
                ViewHeaderHolder viewHeaderHolder = (ViewHeaderHolder)holder;
                viewHeaderHolder.media.setOnClickListener(listener);
                viewHeaderHolder.tagFriends.setOnClickListener(listener);
                viewHeaderHolder.location.setOnClickListener(listener);
                mPostText = viewHeaderHolder.postET;
                mPostButton = viewHeaderHolder.postBtn;
                viewHeaderHolder.postET.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if(count > 0 && !mPostText.getText().toString().trim().equalsIgnoreCase(""))
                            mPostButton.setVisibility(View.VISIBLE);
                        else
                            mPostButton.setVisibility(View.GONE);
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                viewHeaderHolder.postBtn.setOnClickListener(listener);
                viewHeaderHolder.itemView.setTag(position);
                viewHeaderHolder.postBtn.setTag(viewHeaderHolder);
                Picasso.get().load(BaseActivity.USER.getImage() != null ? BaseActivity.USER.getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(viewHeaderHolder.profileImage);
                viewHeaderHolder.viewll.setVisibility(View.VISIBLE);
                viewHeaderHolder.iconVideo.setVisibility(View.GONE);
                if (newsFeedModel.getAudioUrl() != null){
                    viewHeaderHolder.medias.setImageDrawable(context.getDrawable(R.drawable.audio));
                }else if(newsFeedModel.getVideoUrl() != null){
                    final int radius = 20;
                    final Transformation transformation = new RoundedCornersTransformation(radius, 5);
                    Picasso.get()
                            .load(newsFeedModel.getThumbnail())
                            .placeholder(R.drawable.thumbnail)
                            .error(R.drawable.thumbnail)
                            .transform(transformation)
                            .into(viewHeaderHolder.medias);

                    viewHeaderHolder.iconVideo.setVisibility(View.VISIBLE);
                }else if (newsFeedModel.getImageUrl() != null ){
                    final int radius = 20;
                    final Transformation transformation = new RoundedCornersTransformation(radius, 5);
                    Picasso.get()
                            .load(new File(newsFeedModel.getImageUrl()))
                            .placeholder(R.drawable.image_placeholder)
                            .error(R.drawable.image_placeholder)
                            .transform(transformation)
                            /*.centerCrop()
                            .resize(R.dimen._200sdp,R.dimen._125sdp)*/
                            .into(viewHeaderHolder.medias);
                    //viewHolder.media.setImageDrawable(con
                    //text.getDrawable(R.drawable.image_newsfeed));
                }else if(newsFeedModel.getLatitude() != null && newsFeedModel.getLongitude() != null){
                    final int radius = 15;
                    viewHeaderHolder.medias.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                    final Transformation transformation = new RoundedCornersTransformation(radius, 0);
                    Picasso.get().setLoggingEnabled(true);
                    Picasso.get()
                            .load(Utils.getMapURL
                                    (Double.parseDouble(newsFeedModel.getLatitude()),
                                            Double.parseDouble(newsFeedModel.getLongitude())))
                            .placeholder(R.drawable.image_placeholder)
                            .error(R.drawable.image_placeholder)
                            .transform(transformation)
                            /*.centerCrop()
                            .resize(R.dimen._200sdp,R.dimen._125sdp)*/
                            .into(viewHeaderHolder.medias);
                }else
                    viewHeaderHolder.viewll.setVisibility(View.GONE);

            }else{

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (int)view.getTag();
                        mListener.onNewsFeedClick(pos);
                    }
                });

                ViewHolder viewHolder = (ViewHolder)holder;
                viewHolder.day.setTag(position);
                viewHolder.day.setOnClickListener(listener);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(StringUtils.capitalize(newsFeedModel.getUserModel().getFirstName()));
                stringBuilder.append(" ");
                stringBuilder.append(StringUtils.capitalize(newsFeedModel.getUserModel().getLastName() != null ? newsFeedModel.getUserModel().getLastName() : ""));
                //if (newsFeedModel.getUserModel().getSocialId() == null)
                    viewHolder.name.setText(stringBuilder);
                /*else
                    viewHolder.name.setText(StringUtils.capitalize(newsFeedModel.getUserModel().getSocialUsername()));*/
                viewHolder.title.setText(newsFeedModel.getPost());
                viewHolder.time.setText(Utils.getRelativeTime(DateUtils.getRelativeTimeSpanString(Utils.getTime(newsFeedModel.getCreatedAt()))));
                viewHolder.like.setTag(position);
                viewHolder.like.setOnClickListener(listener);

                if (newsFeedModel.getIsLike())
                    viewHolder.like.setCompoundDrawablesWithIntrinsicBounds( R.drawable.like_pressed, 0, 0, 0);
                else
                    viewHolder.like.setCompoundDrawablesWithIntrinsicBounds( R.drawable.icon_like, 0, 0, 0);

                if (newsFeedModel.getComment() != null)
                    viewHolder.comment.setText(newsFeedModel.getComment().size() + " Comments");
                if (newsFeedModel.getTagFriend() != null) {
                    viewHolder.friendCount.setText(newsFeedModel.getTagFriend().size() + " Friends");
                    viewHolder.friendCount.setTag(position);
                    viewHolder.friendCount.setOnClickListener(listener);
                }

                Picasso.get().load(newsFeedModel.getUserModel().getImage() != null ? newsFeedModel.getUserModel().getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(viewHolder.profileImage);
                viewHolder.iconVideo.setVisibility(View.GONE);
                if (newsFeedModel.getAudioUrl() != null){
                    viewHolder.media.setImageDrawable(context.getDrawable(R.drawable.audio));
                    viewHolder.media.setOnClickListener(listener);
                    viewHolder.media.setTag(position);

                }else if(newsFeedModel.getVideoUrl() != null){
                    viewHolder.media.setOnClickListener(listener);
                    viewHolder.media.setTag(position);
                    final int radius = 20;
                    final Transformation transformation = new RoundedCornersTransformation(radius, 5);
                    Picasso.get()
                            .load(newsFeedModel.getThumbnail())
                            .placeholder(R.drawable.image_placeholder)
                            .error(R.drawable.image_placeholder)
                            .transform(transformation)
                            .into(viewHolder.media);
                            viewHolder.iconVideo.setVisibility(View.VISIBLE);

                }else if (newsFeedModel.getPhotoUrl() != null && newsFeedModel.getPhotoUrl().size() > 0){
                    final int radius = 20;
                    viewHolder.media.setTag(position);
                    viewHolder.media.setOnClickListener(listener);
                    final Transformation transformation = new RoundedCornersTransformation(radius, 5);
                        Picasso.get()
                            .load(newsFeedModel.getPhotoUrl().get(0).get3x())
                            .placeholder(R.drawable.image_placeholder)
                            .error(R.drawable.image_placeholder)
                            .transform(transformation)
                            /*.centerCrop()
                            .resize(R.dimen._200sdp,R.dimen._125sdp)*/
                            .into(viewHolder.media);
                    //viewHolder.media.setImageDrawable(context.getDrawable(R.drawable.image_newsfeed));
                }else if(newsFeedModel.getLatitude() != null && newsFeedModel.getLongitude() != null){
                    final int radius = 15;
                    viewHolder.media.setTag(position);
                    viewHolder.media.setOnClickListener(listener);
                    viewHolder.media.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                    final Transformation transformation = new RoundedCornersTransformation(radius, 0);
                    Picasso.get().setLoggingEnabled(true);
                    Picasso.get()
                            .load(Utils.getMapURL
                                    (Double.parseDouble(newsFeedModel.getLatitude()),
                                            Double.parseDouble(newsFeedModel.getLongitude())))
                            .placeholder(R.drawable.image_placeholder)
                            .error(R.drawable.image_placeholder)
                            .transform(transformation)
                            /*.centerCrop()
                            .resize(R.dimen._200sdp,R.dimen._125sdp)*/
                            .into(viewHolder.media);
                }else {
                    viewHolder.media.setVisibility(View.GONE);
                    viewHolder.title.setMaxLines(3);
                }


            }

            if(position == mNewsFeed.size() - 1){
                holder.bottomLeft.setVisibility(View.GONE);
                holder.bottomRight.setVisibility(View.GONE);
            }





        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mNewsFeed.size();
    }



    public static class BaseViewHolder extends RecyclerView.ViewHolder {

        ImageView bottomLeft, bottomRight;

        public BaseViewHolder(View itemView) {
            super(itemView);

            bottomLeft = (ImageView) itemView.findViewById(R.id.bottomLeft);
            bottomRight = (ImageView) itemView.findViewById(R.id.bottomRight);

        }
    }

    public static class ViewHolder extends BaseViewHolder {

        TextView name, title, time,like, comment, friendCount;
        ImageView profileImage, media, day, iconVideo;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            title = (TextView) itemView.findViewById(R.id.title);
            like = (TextView) itemView.findViewById(R.id.like);
            comment = (TextView) itemView.findViewById(R.id.comments);
            friendCount = (TextView) itemView.findViewById(R.id.friends_count);
            time = (TextView) itemView.findViewById(R.id.time);
            profileImage = (ImageView) itemView.findViewById(R.id.profile_image);
            media = (ImageView) itemView.findViewById(R.id.media_list);
            iconVideo = (ImageView) itemView.findViewById(R.id.icon_video);
            day = (ImageView) itemView.findViewById(R.id.day);
        }
    }

    public static class ViewHeaderHolder extends BaseViewHolder {

        TextView media, tagFriends, location;
        ImageView courseImage, medias;
        Button postBtn;
        EditText postET;
        ImageView profileImage,iconVideo;
        View viewll;
        public ViewHeaderHolder(View itemView) {
            super(itemView);
            media = itemView.findViewById(R.id.media);
            tagFriends = itemView.findViewById(R.id.tag_friends);
            location = itemView.findViewById(R.id.location);
            medias = (ImageView) itemView.findViewById(R.id.media_list);
            viewll = itemView.findViewById(R.id.media_list_ll);
            iconVideo = (ImageView) itemView.findViewById(R.id.icon_video);
            postBtn =  itemView.findViewById(R.id.post);
            postET =  itemView.findViewById(R.id.post_text);
            profileImage =  itemView.findViewById(R.id.profile_image);
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else {
            return position;
        }
    }

    public interface OnNewsFeedClickListener {
        public void onNewsFeedClick(int position);
    }

    public interface OnNewsFeedItemClickListener {
        public void onNewsFeedClick(int position);
        public void onPostClick(String postText);
        public void onEditClick(int post);
        public void reportPost(int post);
        public void deletePost(int post);

    }

    private void displayPopupWindow(View anchorView, Context context, boolean currentUserPost, final int pos) {

        final PopupWindow popup = new PopupWindow(context);
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                switch (v.getId()){
                    case R.id.edit:
                        mListener.onEditClick(pos);
                        break;
                    case R.id.delete:
                        mListener.deletePost(pos);
                        break;
                    case R.id.inappropriate:
                        mListener.reportPost(pos);
                        break;
                }
            }
        };

        View layout = ((BaseActivity)context).getLayoutInflater().inflate(R.layout.list_item_pop_window, null);
        TextView edit =  layout.findViewById(R.id.edit);
        TextView delete =  layout.findViewById(R.id.delete);
        TextView inapp =  layout.findViewById(R.id.inappropriate);
        if(currentUserPost){
            inapp.setVisibility(View.GONE);
        }else{
            edit.setVisibility(View.GONE);
            delete.setVisibility(View.GONE);
        }
        edit.setOnClickListener(listener);
        delete.setOnClickListener(listener);
        inapp.setOnClickListener(listener);

        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
    }

}
