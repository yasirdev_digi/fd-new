package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.fragments.ProfileFragment;
import com.digitonics.friendshipdiary.models.Friend;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class CircularImageAdapter extends RecyclerView.Adapter<CircularImageAdapter.ViewHolder>{

    private final GoalAdapter.OnGoalClickListener mListener;
    private Context context;
    ArrayList<Friend> mFriends;

    public CircularImageAdapter(GoalAdapter.OnGoalClickListener listener, ArrayList<Friend> list){
        mListener = listener;
        mFriends = list;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_circular_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            context = holder.itemView.getContext();
            holder.profileImage.setTag(position);
            holder.profileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    ((HomeActivity)v.getContext()).addFragment(ProfileFragment.newInstance(0,mFriends.get(pos).getId()));
                    ((BaseActivity)v.getContext()).mBottomLL.setVisibility(View.GONE);
                }
            });
            Picasso.get().load(mFriends.get(position).getImageUrl() != null?mFriends.get(position).getImageUrl().get2x():null).placeholder(R.drawable.user_default).error(R.drawable.icon_friends).into(holder.profileImage);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mFriends.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView profileImage;

        public ViewHolder(View itemView) {
            super(itemView);
            profileImage = itemView.findViewById(R.id.profile_image);

        }
    }
}
