package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.models.UserModel;

import java.util.ArrayList;


public class FacebookAdapter extends RecyclerView.Adapter<FacebookAdapter.ViewHolder>{

    private final NewsfeedAdapter.OnNewsFeedClickListener mListener;
    private Context context;
    ArrayList<UserModel> mFriends;

    public FacebookAdapter(NewsfeedAdapter.OnNewsFeedClickListener listener, ArrayList<UserModel> friends){
        mListener = listener;
        mFriends =  friends;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_invite_friend, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            UserModel friend = mFriends.get(position);
            context = holder.itemView.getContext();
            holder.name.setText(friend.getName());
            holder.invite.setTag(position);
            holder.invite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mFriends.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, invite;
        ImageView courseImage;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            invite = (TextView) itemView.findViewById(R.id.invite);
            /*profileImage = (ImageView) itemView.findViewById(R.id.imageView6);*/

        }
    }
}
