package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.WishModel;

import java.util.ArrayList;


public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.BaseViewHolder>{

    private Context context;
    private OnWishlistClickListener mListener;
    private ArrayList<WishModel> mWishList;
    private GoalModel mGoalModel;

    public WishListAdapter(OnWishlistClickListener listener,GoalModel goalModel){
        mListener = listener;
        mWishList = goalModel.getWishlist();
        mGoalModel = goalModel;
    }

    public void setData(WishModel model){
        mWishList.add(model);
        notifyDataSetChanged();
    }
    public void setData(WishModel model,int pos){
        mWishList.set(pos, model);
        notifyDataSetChanged();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0)
            return new ViewHeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_reward_header, parent, false));
        else
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_wish, parent, false));

    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        try {
            context = holder.itemView.getContext();
            if (position == 0){
                ViewHeaderHolder viewHeaderHolder = (ViewHeaderHolder)holder;
                viewHeaderHolder.reward.setText("$ " + (mGoalModel.getRewards() != null ? mGoalModel.getRewards( ) : "0.00" ));
            }else{

                ViewHolder viewHolder = (ViewHolder)holder;
                viewHolder.itemView.setTag(position);
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (int)view.getTag();
                        mListener.onWishlistClick(pos);
                    }
                });
                if (position == 1)
                    viewHolder.type.setText("Primary Wishlist Item :");
                else
                    viewHolder.type.setText("Secondary Wishlist Item :");

                viewHolder.title.setText(mWishList.get(position).getTitle());
                viewHolder.desc.setText(mWishList.get(position).getDescription());
                viewHolder.price.setText("$" +mWishList.get(position).getPrice());
                viewHolder.url.setTag(position);
                viewHolder.url.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }

            if(position == mWishList.size() - 1){
                holder.bottomLeft.setVisibility(View.GONE);
                holder.bottomRight.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mWishList.size();
    }



    public static class BaseViewHolder extends RecyclerView.ViewHolder {

        ImageView bottomLeft, bottomRight;

        public BaseViewHolder(View itemView) {
            super(itemView);

            bottomLeft = (ImageView) itemView.findViewById(R.id.bottomLeft);
            bottomRight = (ImageView) itemView.findViewById(R.id.bottomRight);

        }
    }

    public static class ViewHolder extends BaseViewHolder {

        TextView type, title, price, desc, url;

        public ViewHolder(View itemView) {
            super(itemView);
            type =  itemView.findViewById(R.id.item_type);
            title =  itemView.findViewById(R.id.title);
            price =  itemView.findViewById(R.id.price);
            desc =  itemView.findViewById(R.id.goal_desc);
            url =  itemView.findViewById(R.id.url);
        }
    }

    public static class ViewHeaderHolder extends BaseViewHolder {

        TextView reward;

        public ViewHeaderHolder(View itemView) {
            super(itemView);
            reward = itemView.findViewById(R.id.reward);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else {
            return position;
        }
    }

    public interface OnWishlistClickListener {
        public void onWishlistClick(int position);
    }
}
