package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.models.ContactModel;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.util.ArrayList;
import java.util.List;


public class InviteFriendAdapter extends RecyclerView.Adapter<InviteFriendAdapter.ViewHolder>{

    private final NewsfeedAdapter.OnNewsFeedClickListener mListener;
    private Context context;
    List<ContactModel> mContacts;

    public InviteFriendAdapter(NewsfeedAdapter.OnNewsFeedClickListener listener, List<ContactModel> list){
        mListener = listener;
        mContacts = list;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_invite_friend, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            context = holder.itemView.getContext();
            holder.name.setText(mContacts.get(position).name);
            holder.inviteLL.setTag(position);
            holder.inviteLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    ContactModel contactModel = mContacts.get(pos);
                    Utils.shareIntent(v.getContext(),contactModel.mobileNumber, "Hey check out Friendship Diary app at: https://play.google.com/store/apps/details?id=com.digitonics.friendshipdiary");
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, invite;
        LinearLayout inviteLL;

        public ViewHolder(View itemView) {
            super(itemView);
            invite = (TextView) itemView.findViewById(R.id.invite);
            name = (TextView) itemView.findViewById(R.id.name);
            inviteLL = itemView.findViewById(R.id.inviteLL);

        }
    }
}
