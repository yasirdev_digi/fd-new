package com.digitonics.friendshipdiary.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.util.ArrayList;


public class GoalAdapter extends RecyclerView.Adapter<GoalAdapter.ViewHolder>{

    private Context context;

    OnGoalClickListener mListener;
    ArrayList<GoalModel> mGoalList;
    int userType = -1;

    public GoalAdapter(OnGoalClickListener listener, ArrayList<GoalModel> list){
        mListener = listener;
        mGoalList = list;
    }

    public GoalAdapter(OnGoalClickListener listener, ArrayList<GoalModel> list,int userType){
        this(listener,list);
        this.userType = userType;
    }

    public void emptyData(){
        mGoalList.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_goals, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            final GoalModel goalModel =  mGoalList.get(position);
            if(position == 0){
                holder.topLeft.setVisibility(View.GONE);
                holder.topRight.setVisibility(View.GONE);
            }
            if (position == mGoalList.size() - 1){
                holder.bottomLeft.setVisibility(View.GONE);
                holder.bottomRight.setVisibility(View.GONE);
            }
            context = holder.itemView.getContext();
            holder.itemView.setTag(position);
            holder.goalFriends.setTag(position);

            if(userType == Constants.SPONSER){
                if (goalModel.getIs_agreed().equalsIgnoreCase("0")){
                    holder.ll.setVisibility(View.VISIBLE);
                    holder.day.setVisibility(View.GONE);
                    holder.accept.setTag(position);
                    holder.reject.setTag(position);
                    holder.accept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final int pos = (int) view.getTag();
                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                            builder.setTitle("Terms & Conditions")
                                    .setMessage("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. \\n\\n Are you sure you want to be a sponsor for this goal?")
                                    .setCancelable(false)
                                    .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            mListener.onGoalClick(3, mGoalList.get(pos));
                                            //((HomeActivity) getActivity()).addFragment(CreateTaskFragment.newInstance(mGoal.getId()));

                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }
                    });

                    holder.reject.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int pos = (int) view.getTag();
                            mListener.onGoalClick(4, mGoalList.get(pos));
                        }
                    });
                }else{
                    holder.day.setVisibility(View.VISIBLE);
                    holder.ll.setVisibility(View.GONE);
                }

            }else{
                holder.ll.setVisibility(View.GONE);
            }


            holder.goalName.setText(goalModel.getName());
            holder.goalDesc.setText(goalModel.getDescription());
            holder.goalStartDate.setText(Utils.getDate(goalModel.getStartDate(), "yyyy-MM-dd hh:mm:ss"));
            holder.goalEndDate.setText(Utils.getDate(goalModel.getEndDate(), "yyyy-MM-dd hh:mm:ss"));
            holder.goalType.setText(goalModel.getGoalType());
            holder.goalFriends.setText(goalModel.getFriends() != null && goalModel.getFriends().size() > 0 ? goalModel.getFriends().size() + " Friends": "0 Friends");
            holder.day.setTag(position);


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
//                    if (userType == Constants.SPONSER && goalModel.getIs_agreed().equalsIgnoreCase("0"))
//                        return;
                    mListener.onGoalClick(1, mGoalList.get(pos));
                }
            });

            holder.goalFriends.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    mListener.onGoalClick(2, mGoalList.get(pos));
                }
            });

            holder.day.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position  = (int)v.getTag();
                    displayPopupWindow(v,v.getContext(),BaseActivity.USER.getId().equalsIgnoreCase(mGoalList.get(position).getUserId()), (int) position);
                }
            });



        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mGoalList.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView goalName, goalDesc, goalStartDate,goalEndDate,goalFriends, goalType;

        ImageView topLeft, topRight, bottomLeft, bottomRight,day;

        View ll, accept, reject;

        public ViewHolder(View itemView) {
            super(itemView);
            topLeft = (ImageView) itemView.findViewById(R.id.topLeft);
            topRight = (ImageView) itemView.findViewById(R.id.topRight);
            bottomLeft = (ImageView) itemView.findViewById(R.id.bottomLeft);
            bottomRight = (ImageView) itemView.findViewById(R.id.bottomRight);

            ll = itemView.findViewById(R.id.acceptll);
            accept = itemView.findViewById(R.id.accept);
            reject = itemView.findViewById(R.id.reject);

            goalName =  itemView.findViewById(R.id.goal_name);
            goalDesc =  itemView.findViewById(R.id.goal_desc);
            goalStartDate =  itemView.findViewById(R.id.date_start);
            goalEndDate =  itemView.findViewById(R.id.date_end);
            goalFriends =  itemView.findViewById(R.id.friends_count);
            goalType =  itemView.findViewById(R.id.goal_status);
            day =  itemView.findViewById(R.id.day);

            /*imageProofDetail = (TextView) itemView.findViewById(R.id.textView);
            email = (TextView) itemView.findViewById(R.id.textView4);
            profileImage = (ImageView) itemView.findViewById(R.id.tabImageView);*/
        }
    }

    public interface OnGoalClickListener {
        public void onGoalClick(int type, GoalModel goalModel);
    }

    private void displayPopupWindow(View anchorView,final Context context, boolean currentUserPost, final int pos) {

        final PopupWindow popup = new PopupWindow(context);
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                switch (v.getId()){
                    case R.id.share:
                        popup.dismiss();
                        /*View view = ((Activity)context).getLayoutInflater().inflate(R.layout.skill_dialog, null);
                        final EditText skill = (EditText) view.findViewById(R.id.skillName);
                        skill.setHint("Please enter points");
                        skill.setInputType(InputType.TYPE_CLASS_NUMBER);
                        new AlertDialog.Builder(v.getContext(),R.style.AppTheme_DialogTheme)
                                .setTitle("Add Skills")
                                .setView(view)
                                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String string = skill.getText().toString();
                                        if (string.equalsIgnoreCase("")
                                                && Integer.parseInt(string) > 0 && Integer.parseInt(string) <= 10){
                                            notifyDataSetChanged();
                                            Utils.hideSoftKeyboard(skill);
                                        }
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Utils.hideSoftKeyboard(skill);
                                    }
                                }).create().show();*/

                        DialogInterface.OnClickListener posListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                ((BaseActivity)context).mApiBuilder.sharePoints(BaseActivity.USER.getAccessToken(),mGoalList.get(pos).getId(),new CustomRunnable() {
                                    @Override
                                    public void run(Object data) {

                                    }
                                }, context);
                            }
                        };

                        DialogInterface.OnClickListener negListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        };

                        Utils.showPromptDialog(v.getContext(), "Are you sure 10 point to that goal creater?", null, "Yes", "No",posListener, negListener, true);
                        break;
                    case R.id.delete:
                        popup.dismiss();
                        posListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                ((BaseActivity)context).mApiBuilder.deleteGoal(BaseActivity.USER.getAccessToken(),mGoalList.get(pos).getId(),new CustomRunnable() {
                                    @Override
                                    public void run(Object data) {
                                        mGoalList.remove(pos);
                                        notifyDataSetChanged();
                                    }
                                }, context);
                            }
                        };

                        negListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        };

                        Utils.showPromptDialog(v.getContext(), "Are you sure to delete the item?", null, "Yes", "No",posListener, negListener, true);


                        break;
                    case R.id.status:
                        popup.dismiss();
                        showChangeStatusDialog(context, pos);
                        break;
                }
            }
        };

        View layout = ((BaseActivity)context).getLayoutInflater().inflate(R.layout.list_item_goal_pop_window, null);
        TextView status =  layout.findViewById(R.id.status);
        TextView delete =  layout.findViewById(R.id.delete);
        TextView share =  layout.findViewById(R.id.share);
        if(currentUserPost){
            share.setVisibility(View.GONE);
        }else{
            status.setVisibility(View.GONE);
            delete.setVisibility(View.GONE);
        }
        status.setOnClickListener(listener);
        delete.setOnClickListener(listener);
        share.setOnClickListener(listener);

        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
    }

    private void showChangeStatusDialog(final Context context, int pos) {

        View view = View.inflate(context, R.layout.dialog_status, null);
        final Dialog dialog = Utils.showCustomDialog(context, view);
        final RadioGroup voteGrp = view.findViewById(R.id.vote_radio_grp);
        final TextView submitBT = (TextView) view.findViewById(R.id.submit);
        submitBT.setTag(pos);
        ImageView closeIV = (ImageView) view.findViewById(R.id.diag_dept_closeIV);
        closeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        ((RadioButton)voteGrp.getChildAt(Utils.getStatusPosition(mGoalList.get(pos).getStatus()))).setChecked(true);
        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String taskLevel = "";
                final int pos = (int)view.getTag();
                Utils.hideSoftKeyboard(submitBT);
                dialog.dismiss();

                switch (voteGrp.getCheckedRadioButtonId()){
                    case R.id.active:
                        taskLevel = "active";
                        break;
                    case R.id.on_break:
                        taskLevel = "on_break";
                        break;
                    case R.id.no_pressure:
                        taskLevel = "no_pressure";
                        break;
                }

                ((BaseActivity)context).mApiBuilder.changeStatus(BaseActivity.USER.getAccessToken(),mGoalList.get(pos).getId(), taskLevel + "",new CustomRunnable() {
                    @Override
                    public void run(Object data) {
                            GoalModel goalModel = (GoalModel)data;
                            if (goalModel != null)
                                mGoalList.get(pos).setStatus(goalModel.getStatus());
                    }
                }, context);
//                ((BaseActivity) mContext).mBaseFragment = new PastTripDetailsFragment();
//                ((PastTripDetailsFragment) ((BaseActivity) mContext).mBaseFragment).setCommunicator(1);
//                ((BaseActivity)mContext).changeFragment(((BaseActivity)mContext).mBaseFragment,true);
            }
        });

        dialog.show();


    }
}
