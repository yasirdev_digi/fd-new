package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.UserModel;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;


public class TagFriendAdapter extends RecyclerView.Adapter<TagFriendAdapter.ViewHolder>{

    private Context context;
    ArrayList<Boolean> arrayList;
    //private int mType = 0;
    private NewsfeedAdapter.OnNewsFeedClickListener mListener;
    ArrayList<UserModel> mFriends;
    public TagFriendAdapter(NewsfeedAdapter.OnNewsFeedClickListener listener, ArrayList<UserModel> friends){
        mListener = listener;
        mFriends =  friends;
    }
  /*  public FriendsAdapter(int commentTxt){
        mType = commentTxt;
    }*/

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_tag_friends, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            UserModel friend = mFriends.get(position);
            String lastName = friend.getLastName() != null ? friend.getLastName() : "";
            holder.name.setText(StringUtils.capitalize(friend.getFirstName()) + " " + StringUtils.capitalize(lastName));
            holder.email.setText(friend.getEmail());
            Picasso.get().load(friend.getImage() != null ? friend.getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(holder.profileImage);

            holder.type.setOnCheckedChangeListener(null);

            holder.type.setTag(position);
            holder.type.setChecked(friend.isChecked());

            holder.type.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    mFriends.get((int) buttonView.getTag()).setChecked(isChecked);
                    notifyDataSetChanged();

                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mFriends.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, email;
        CheckBox type;
        ImageView profileImage;
        ImageView imageView;
        ConstraintLayout ll;

        public ViewHolder(View view) {
            super(view);
            profileImage = (ImageView) view.findViewById(R.id.profile_image);
            name = (TextView) itemView.findViewById(R.id.name);
            email = (TextView) itemView.findViewById(R.id.email_id);
            type =  itemView.findViewById(R.id.type);
            /*
            ll = (ConstraintLayout) view.findViewById(R.id.constraintLL);
            email = (TextView) itemView.findViewById(R.id.email);
            profileImage = (ImageView) itemView.findViewById(R.id.profileImage);*/
        }
    }
}
