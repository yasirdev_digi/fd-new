package com.digitonics.friendshipdiary.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.fragments.CreateTaskFragment;
import com.digitonics.friendshipdiary.fragments.GoalDetailFragment;
import com.digitonics.friendshipdiary.fragments.TaskDetailFragment;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.CommentModel;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.ProofModel;
import com.digitonics.friendshipdiary.models.TaskModel;
import com.digitonics.friendshipdiary.utilities.OverlapDecoration;
import com.digitonics.friendshipdiary.utilities.Utils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;


public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.ViewHolder>{

    private Context context;
    private View.OnClickListener mListener;
    ProofAdapter.OnAddProofClickListener mProofListener;
    private ArrayList<TaskModel> mTasks;
    private boolean mAddProof;

    public TaskListAdapter(ProofAdapter.OnAddProofClickListener proofListener, View.OnClickListener listener, ArrayList<TaskModel> tasks, boolean addProve){
        mListener = listener;
        mProofListener = proofListener;
        mTasks = tasks;
        mAddProof = addProve;
    }
    public void emptyData(){
        mTasks.clear();
        notifyDataSetChanged();
    }


    public void setData(int pos, ProofModel model){
        mTasks.get(pos).getProves().add(model);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_task_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        try {
            context = holder.itemView.getContext();
            final TaskModel taskModel =  mTasks.get(position);

            /*holder.ll.setTag(1);*/

            holder.taskStatus.setText(taskModel.getStatus());
            holder.taskName.setText(taskModel.getName());
            holder.taskDesc.setText(taskModel.getDescription());
            holder.taskLevel.setText(Utils.getTaskLevel(taskModel.getTaskType()));
            holder.taskReward.setText(taskModel.getRewards());
            //holder.taskVotes.setText(taskModel.get);
            holder.taskSno.setText((position + 1)+ "");
            holder.taskVotes.setText(taskModel.getTotalVotes());

//            ArrayList<ProofModel> proofList = new ArrayList<>();
//            proofList.addAll(mTasks.get(position).getProves());

            if (mAddProof){
                holder.taskProve.setTag(position);
                holder.taskProve.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ((BaseActivity)v.getContext()).addFragment(TaskDetailFragment.newInstance(mTasks.get(pos), pos));

                    }
                });
                //proofList.add(new ProofModel());
                /*holder.view.setVisibility(View.VISIBLE);
                holder.view2.setVisibility(View.VISIBLE);
                holder.parentll.setVisibility(View.VISIBLE);*/
            }else{
                holder.taskProve.setVisibility(View.GONE);
                /*holder.view.setVisibility(View.GONE);
                holder.view2.setVisibility(View.GONE);
                holder.parentll.setVisibility(View.GONE);*/
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Created by: ");
            sb.append(StringUtils.capitalize(taskModel.getUser().getFirstName()));
            sb.append(" ");
            sb.append(StringUtils.capitalize(taskModel.getUser().getLastName() != null ? taskModel.getUser().getLastName() : ""));
            holder.createdBy.setText(sb.toString());
            if (BaseActivity.USER.getId().equalsIgnoreCase(taskModel.getUser().getId()) && !taskModel.getStatus().equalsIgnoreCase("completed"))
                holder.taskEdit.setVisibility(View.VISIBLE);
            else
                holder.taskEdit.setVisibility(View.GONE);
            holder.taskEdit.setTag(position);
            holder.taskEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int)v.getTag();
                    ((BaseActivity)v.getContext()).addFragment(CreateTaskFragment.newInstance(mTasks.get(position)));
                }
            });
            LinearLayoutManager mImagesManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
            holder.imageRV.setLayoutManager(mImagesManager);
            ProofAdapter mTaskAdapter = new ProofAdapter(mProofListener,mTasks.get(position).getProves() , position , false);

            holder.imageRV.setLayoutManager(mImagesManager);
            holder.imageRV.setAdapter(mTaskAdapter);
            holder.votesll.setTag(position);
            if(!mAddProof) {
                holder.votesll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mTasks.get((int) v.getTag()).getProves() != null && mTasks.get((int) v.getTag()).getProves().size() > 0) {
                            showVoteDialog(v.getContext(), (int) v.getTag());
                        }else
                            Utils.showToast(v.getContext(), "Vote can be done with at least one proof for task");
                    }
                });
            }
            holder.ll.setTag(position);
            holder.ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                    if(mTasks.get((int)view.getTag()).getStatus().equalsIgnoreCase("completed")) {
                        if (BaseActivity.USER.getId().equalsIgnoreCase(mTasks.get((int)view.getTag()).getUser().getId())) {
                            String clientToken = mTasks.get(pos).getClientToken();
                            Intent in = new Intent();
                            in.putExtra("amount",mTasks.get(pos).getRewards());
                            in.putExtra("taskId",mTasks.get(pos).getId());
                            in.putExtra("type","task");
                            ((BaseActivity) view.getContext()).initPayment(clientToken, in);
                        }
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mTasks.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView taskName, taskDesc, taskLevel, taskReward, taskVotes, taskSno, taskStatus, createdBy,taskProve;
        LinearLayout ll, votesll, parentll;
        ImageView taskEdit;
        View view, view2;

        RecyclerView imageRV;

        public ViewHolder(View itemView) {
            super(itemView);
            /*imageProofDetail = itemView.findViewById(R.id.imageProofDetail);*/

            taskName =  itemView.findViewById(R.id.task_name);
            taskDesc =  itemView.findViewById(R.id.task_desc);
            taskLevel =  itemView.findViewById(R.id.task_level);
            taskReward =  itemView.findViewById(R.id.reward);
            taskVotes =  itemView.findViewById(R.id.votes);
            createdBy =  itemView.findViewById(R.id.createby);
            taskSno =  itemView.findViewById(R.id.sno);
            ll = itemView.findViewById(R.id.reward_item);
            votesll = itemView.findViewById(R.id.votes_ll);
            parentll = itemView.findViewById(R.id.ll);
            view = itemView.findViewById(R.id.view);
            view2 = itemView.findViewById(R.id.view2);
            taskStatus = itemView.findViewById(R.id.task_status);
            taskEdit = itemView.findViewById(R.id.edit);
            taskProve = itemView.findViewById(R.id.textView2);
            imageRV = itemView.findViewById(R.id.image_proof);
        }
    }

    private void showVoteDialog(final Context context, int pos) {

        View view = View.inflate(context, R.layout.dialog_vote, null);
        final Dialog dialog = Utils.showCustomDialog(context, view);
        final RadioGroup voteGrp = view.findViewById(R.id.vote_radio_grp);
        final TextView submitBT = (TextView) view.findViewById(R.id.submit);
        submitBT.setTag(pos);
        ImageView closeIV = (ImageView) view.findViewById(R.id.diag_dept_closeIV);

        closeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final int pos; int taskLevel = 0;
                pos = (int)view.getTag();
                Utils.hideSoftKeyboard(submitBT);
                dialog.dismiss();

                switch (voteGrp.getCheckedRadioButtonId()){
                   case R.id.easy:
                       taskLevel = 1;
                       break;
                   case R.id.medium:
                       taskLevel = 2;
                       break;
                   case R.id.hard:
                       taskLevel = 3;
                       break;
               }

                ((BaseActivity)context).mApiBuilder.postVote(BaseActivity.USER.getAccessToken(),mTasks.get(pos).getId(), taskLevel + "",new CustomRunnable() {
                    @Override
                    public void run(Object data) {
                        try {
                            int votes = Integer.parseInt(mTasks.get(pos).getTotalVotes()) +1;
                            mTasks.get(pos).setTotalVotes(votes +"");
                            notifyDataSetChanged();
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                        //Toast.makeText(view.getContext(),"SSS",Toast.LENGTH_SHORT).show();
                    }
                }, context);
//                ((BaseActivity) mContext).mBaseFragment = new PastTripDetailsFragment();
//                ((PastTripDetailsFragment) ((BaseActivity) mContext).mBaseFragment).setCommunicator(1);
//                ((BaseActivity)mContext).changeFragment(((BaseActivity)mContext).mBaseFragment,true);
            }
        });

        dialog.show();


    }
}
