package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.DairyModel;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.util.ArrayList;

import okhttp3.internal.Util;


public class MyDiaryAdapter extends RecyclerView.Adapter<MyDiaryAdapter.ViewHolder>{

    private final OnDairylClickListener mListener;
    private final ArrayList<DairyModel> mDairyList;
    private Context context;

    public MyDiaryAdapter(MyDiaryAdapter.OnDairylClickListener listener, ArrayList<DairyModel> list){
        mListener = listener;
        mDairyList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_my_diary, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            context = holder.itemView.getContext();
            DairyModel model = mDairyList.get(position);
            holder.title.setText(model.getTitle());
            holder.date.setText(Utils.getDate(model.getStartDate()  +" "+ model.getTime(), "yyyy-MM-dd hh:mm:ss") + "," + Utils.get12HourTime(model.getTime()));
            holder.day.setText(DateUtils.getRelativeTimeSpanString(Utils.getTime(model.getStartDate() +" "+ model.getTime())));

            if(position == 0){
                holder.topLeft.setVisibility(View.GONE);
                holder.topRight.setVisibility(View.GONE);
            }
            if (position == mDairyList.size() - 1){
                holder.bottomLeft.setVisibility(View.GONE);
                holder.bottomRight.setVisibility(View.GONE);
            }
            holder.itemView.setTag(position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onDairyClick((int)v.getTag(), mDairyList.get((int)v.getTag()));
                }
            });
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View v) {
                    final int pos = (int)v.getTag();
                    DialogInterface.OnClickListener posListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ((BaseActivity)v.getContext()).mApiBuilder.deleteDiary(BaseActivity.USER.getAccessToken(),mDairyList.get(pos).getId(),new CustomRunnable() {
                                @Override
                                public void run(Object data) {
                                    mDairyList.remove(pos);
                                    notifyDataSetChanged();
                                }
                            }, context);
                        }
                    };

                    DialogInterface.OnClickListener negListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    };


                    Utils.showPromptDialog(v.getContext(), "Are you sure to delete the item?", null, "Yes", "No",posListener, negListener, true);
                    return false;
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mDairyList.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, date, day;
        ImageView topLeft, topRight, bottomLeft, bottomRight;

        public ViewHolder(View itemView) {
            super(itemView);
            title =  itemView.findViewById(R.id.goal_name);
            date =  itemView.findViewById(R.id.goal_desc);
            day =  itemView.findViewById(R.id.day);
            topLeft = (ImageView) itemView.findViewById(R.id.topLeft);
            topRight = (ImageView) itemView.findViewById(R.id.topRight);
            bottomLeft = (ImageView) itemView.findViewById(R.id.bottomLeft);
            bottomRight = (ImageView) itemView.findViewById(R.id.bottomRight);

        }
    }

    public interface OnDairylClickListener {
        public void onDairyClick(int type, DairyModel dairyModel);
    }
}
