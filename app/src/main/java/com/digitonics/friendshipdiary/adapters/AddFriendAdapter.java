package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.fragments.ProfileFragment;
import com.digitonics.friendshipdiary.models.Friend;
import com.digitonics.friendshipdiary.models.UserModel;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;


public class AddFriendAdapter extends RecyclerView.Adapter<AddFriendAdapter.ViewHolder> implements Filterable {

    private final NewsfeedAdapter.OnNewsFeedClickListener mListener;
    private Context context;
    private ArrayList<UserModel> mFriendList;
    ArrayList<UserModel> filteredList;
    private FilterList filterList;

    public AddFriendAdapter(NewsfeedAdapter.OnNewsFeedClickListener listener, ArrayList<UserModel> friend){
        mListener = listener;
        filteredList =  friend;
        mFriendList = friend;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_add_friends, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            context = holder.itemView.getContext();
            UserModel friend = filteredList.get(position);
            String lastName = friend.getLastName() != null ? friend.getLastName() : "";
            /*if (friend.getSocialId() == null)*/
                holder.name.setText(StringUtils.capitalize(friend.getFirstName()) + " " + StringUtils.capitalize(lastName));
            /*else
                holder.name.setText(StringUtils.capitalize(friend.getSocialUsername()));*/
            holder.email.setText(friend.getEmail());
            Picasso.get().load(friend.getImage() != null ? friend.getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(holder.profileImage);
            holder.roleGroup.setTag(position);
            holder.profileImage.setTag(position);
            holder.profileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    ((HomeActivity)v.getContext()).addFragment(ProfileFragment.newInstance(0,filteredList.get(pos).getId()));
                    ((BaseActivity)v.getContext()).mBottomLL.setVisibility(View.GONE);
                }
            });
            if (filteredList.get(position).getRole() != null){
                RadioButton rd = (RadioButton)holder.roleGroup.getChildAt(Integer.parseInt(filteredList.get(position).getRole()) - 1);
                rd.setChecked(true);
            }
            else
                holder.roleGroup.clearCheck();

            for(int i = 0; i < holder.roleGroup.getChildCount(); i++){
                holder.roleGroup.getChildAt(i).setTag(position);
                holder.roleGroup.getChildAt(i).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        switch (v.getId()){
                            case R.id.supporter:
                                filteredList.get(pos).setRole((1) + "");
                                break;
                            case R.id.sponser:
                                filteredList.get(pos).setRole((2) + "");
                                break;
                            case R.id.watcher:
                                filteredList.get(pos).setRole((3) + "");
                                break;
                        }
                    }
                });
            }

            holder.roleGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    /*RadioButton radioButton = radioGroup.findViewById(i);
                    int index = radioGroup.indexOfChild(radioButton);
                    int pos = (Integer) radioGroup.getTag();
                    mFriendList.get(pos).setRole((index + 1) + "");
                    mFriendList.get(pos).setRoleType(radioButton.getText().toString().toLowerCase());*/
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public Filter getFilter() {
        if(filterList == null) {
            filterList =new FilterList();
        }
        return filterList;
    }


    private class FilterList extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            //Implement filter logic
            // if edittext is null return the actual list
            if (constraint == null || constraint.length() == 0) {
                //No need for filter
                results.values = mFriendList;
                results.count = mFriendList.size();

            } else {
                //Need Filter
                // it matches the text  entered in the edittext and set the data in adapter list
                ArrayList<UserModel> filterUsers = new ArrayList<UserModel>();
                for (int i = 0; i < mFriendList.size(); i++) {
                    UserModel s = mFriendList.get(i);
                    if ((s.getFirstName()!= null && s.getFirstName().toUpperCase().trim().contains(constraint.toString().toUpperCase().trim()))
                            || (s.getLastName()!= null && s.getLastName().toUpperCase().trim().contains(constraint.toString().toUpperCase().trim()))
                            || (s.getEmail()!=null && s.getEmail().toUpperCase().trim().contains(constraint.toString().toUpperCase().trim()))) {
                        s.setOldPosition(i);
                        filterUsers.add(s);
                    }
                }
                results.values = filterUsers;
                results.count = filterUsers.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {

            //it set the data from filter to adapter list and refresh the recyclerview adapter
            filteredList = (ArrayList<UserModel>) results.values;
            notifyDataSetChanged();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final RadioGroup roleGroup;
        TextView name, email;
        ImageView profileImage;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name_tv);
            email = itemView.findViewById(R.id.email_tv);
            roleGroup = itemView.findViewById(R.id.role_type_radio_group);
            profileImage = itemView.findViewById(R.id.profile_image);

        }
    }
}
