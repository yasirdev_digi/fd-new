package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.models.Friend;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;


public class GoalFriendsAdapter extends RecyclerView.Adapter<GoalFriendsAdapter.ViewHolder>{


    private ArrayList<Friend> mFriends;

    public GoalFriendsAdapter(ArrayList<Friend> list){
        mFriends = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_friends, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {

            /*holder.ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                }
            });*/

            Friend friend = mFriends.get(position);
            String lastName = friend.getLastName() != null ? friend.getLastName() : "";
            holder.name.setText(StringUtils.capitalize(friend.getFirstName()) + " " + StringUtils.capitalize(lastName));
            holder.email.setText(friend.getEmail());
            Picasso.get().load(friend.getImageUrl() != null ? friend.getImageUrl().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(holder.profileImage);
            holder.type.setText(getRoleType(friend.getRole_id()));
            holder.more.setVisibility(View.GONE);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mFriends.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView type,name, email, roleType;
        ImageView more;
        ImageView profileImage;
        ConstraintLayout ll;

        public ViewHolder(View view) {
            super(view);
            more = (ImageView) view.findViewById(R.id.more);
            type = (TextView) itemView.findViewById(R.id.type);
            name = itemView.findViewById(R.id.name);
            email = itemView.findViewById(R.id.email_id);
            roleType = itemView.findViewById(R.id.type);
            profileImage = itemView.findViewById(R.id.profile_image);
            /*
            ll = (ConstraintLayout) view.findViewById(R.id.constraintLL);
            email = (TextView) itemView.findViewById(R.id.email);
            profileImage = (ImageView) itemView.findViewById(R.id.profileImage);*/
        }
    }
    private String getRoleType(String id){
        if (id.equalsIgnoreCase("1"))
            return "Supporter";
        else if (id.equalsIgnoreCase("2"))
            return "Sponsor";
        else if (id.equalsIgnoreCase("3"))
            return "Watcher";

        return "";
    }
}
