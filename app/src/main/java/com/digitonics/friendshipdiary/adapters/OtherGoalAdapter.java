package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;


public class OtherGoalAdapter extends RecyclerView.Adapter<OtherGoalAdapter.ViewHolder>{

    private Context context;


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_goals, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            context = holder.itemView.getContext();
            //holder.commentTxt.setText("Notification " + (++position));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return 7;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView courseTitle, courseDesc;

        public ViewHolder(View itemView) {
            super(itemView);
            courseTitle = (TextView) itemView.findViewById(R.id.goal_name);

        }
    }
}
