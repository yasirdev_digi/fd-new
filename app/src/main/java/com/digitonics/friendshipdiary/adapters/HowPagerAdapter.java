package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.fragments.ImageFragment;
import com.digitonics.friendshipdiary.fragments.OtherGoalsFragment;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.OtherGoalModel;

import java.util.ArrayList;

/**
 * Created by syed.kamil on 3/9/2018.
 */

public class HowPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private int[] arr = {R.drawable.how_image_1,R.drawable.how_image_2,R.drawable.how_image_3,R.drawable.how_image_4
                            ,R.drawable.how_image_5,R.drawable.how_image_6};

    public HowPagerAdapter(Context context, FragmentManager fm, OtherGoalModel otherGoalModel) {
        super(fm);
        mContext = context;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {

            return ImageFragment.newInstance(arr[position]);

    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return arr.length;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position

                return "";
    }

}