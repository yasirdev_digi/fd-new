package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.fragments.TagListFragment;
import com.digitonics.friendshipdiary.models.BottomModel;
import com.digitonics.friendshipdiary.models.Friend;
import com.digitonics.friendshipdiary.models.UserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ConnectionAdapter extends RecyclerView.Adapter<ConnectionAdapter.ViewHolder>{

    private final GoalAdapter.OnGoalClickListener mListener;
    private Context context;
    ArrayList<UserModel> mFriends;

    public ConnectionAdapter(GoalAdapter.OnGoalClickListener listener, ArrayList<UserModel> list){
        mListener = listener;
        mFriends = list;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_circular_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            context = holder.itemView.getContext();
            Picasso.get().load(mFriends.get(position).getImage() != null?mFriends.get(position).getImage().get2x():null).placeholder(R.drawable.user_default).error(R.drawable.icon_friends).into(holder.profileImage);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BaseActivity)v.getContext()).addFragment(TagListFragment.newInstance(mFriends));
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mFriends.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView profileImage;

        public ViewHolder(View itemView) {
            super(itemView);
            profileImage = itemView.findViewById(R.id.profile_image);

        }
    }
}
