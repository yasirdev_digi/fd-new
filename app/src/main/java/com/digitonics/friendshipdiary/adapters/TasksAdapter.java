package com.digitonics.friendshipdiary.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.fragments.CreateTaskFragment;
import com.digitonics.friendshipdiary.fragments.TaskDetailFragment;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.ProofModel;
import com.digitonics.friendshipdiary.models.TaskModel;
import com.digitonics.friendshipdiary.utilities.Utils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;


public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.ViewHolder>{

    private Context context;
    private View.OnClickListener mListener;
    ProofAdapter.OnAddProofClickListener mProofListener;
    private ArrayList<TaskModel> mTasks;
    private boolean mAddProof;

    public TasksAdapter(ArrayList<TaskModel> tasks){
        mTasks = tasks;
    }
    public void emptyData(){
        mTasks.clear();
        notifyDataSetChanged();
    }


    public void setData(int pos, ProofModel model){
        mTasks.get(pos).getProves().add(model);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_tasks, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        try {
            context = holder.itemView.getContext();
            final TaskModel taskModel =  mTasks.get(position);

            /*holder.ll.setTag(1);*/

            holder.taskStatus.setText(taskModel.getStatus());
            holder.taskName.setText(taskModel.getName());
            holder.taskDesc.setText(taskModel.getDescription());

            holder.taskSno.setText((position + 1)+ "");


            StringBuilder sb = new StringBuilder();
            sb.append("Task Created by: ");
            sb.append(StringUtils.capitalize(taskModel.getUser().getFirstName()));
            sb.append(" ");
            sb.append(taskModel.getUser().getLastName() != null ? taskModel.getUser().getLastName() : "");
            holder.createdBy.setText(sb.toString());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mTasks.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView taskName, taskDesc, taskSno, taskStatus, createdBy;

        public ViewHolder(View itemView) {
            super(itemView);

            taskName =  itemView.findViewById(R.id.task_name);
            taskDesc =  itemView.findViewById(R.id.task_desc);
            createdBy =  itemView.findViewById(R.id.createby);
            taskSno =  itemView.findViewById(R.id.sno);
            taskStatus = itemView.findViewById(R.id.task_status);
        }
    }

}
