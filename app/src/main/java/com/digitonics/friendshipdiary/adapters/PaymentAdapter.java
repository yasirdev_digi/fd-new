package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.PaymentModel;
import com.digitonics.friendshipdiary.models.TaskModel;
import com.digitonics.friendshipdiary.models.UserModel;

import java.util.ArrayList;


public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.BaseViewHolder>{

    private final PaymentModel mUserModel;
    private Context context;
    private OnCourseClickListener mListener;
    private ArrayList<TaskModel> mGoals;

    public PaymentAdapter(OnCourseClickListener listener, PaymentModel userModel){
        mListener = listener;
        mGoals = userModel.getTask();
        mUserModel = userModel;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0)
            return new ViewHeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_payment_header, parent, false));
        else
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_payment, parent, false));

    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        try {
            context = holder.itemView.getContext();
            holder.itemView.setTag(position);


            if (position == 0){
                PaymentAdapter.ViewHeaderHolder viewHeaderHolder = (PaymentAdapter.ViewHeaderHolder)holder;
                viewHeaderHolder.totalReward.setText("$ " +mUserModel.getTotal());
                //viewHeaderHolder.totalPoints.setText("" +mUserModel.getPoints());
            }else{

                ViewHolder viewHolder = (ViewHolder)holder;
                viewHolder.itemView.setTag(position);
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (int)view.getTag();
                        mListener.onCourseClick(pos);
                    }
                });
                viewHolder.goalName.setText(mGoals.get(position).getName());
                viewHolder.goalDesc.setText("Reward : $ " +mGoals.get(position).getRewards());
            }

            if(position == mGoals.size() - 1){
                holder.bottomLeft.setVisibility(View.GONE);
                holder.bottomRight.setVisibility(View.GONE);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mGoals.size();
    }



    public static class BaseViewHolder extends RecyclerView.ViewHolder {

        ImageView bottomLeft, bottomRight;

        public BaseViewHolder(View itemView) {
            super(itemView);

            bottomLeft = (ImageView) itemView.findViewById(R.id.bottomLeft);
            bottomRight = (ImageView) itemView.findViewById(R.id.bottomRight);

        }
    }

    public static class ViewHolder extends PaymentAdapter.BaseViewHolder {

        TextView goalName, goalDesc;

        public ViewHolder(View itemView) {
            super(itemView);
            goalName =  itemView.findViewById(R.id.goal_name);
            goalDesc =  itemView.findViewById(R.id.goal_desc);
        }
    }

    public static class ViewHeaderHolder extends PaymentAdapter.BaseViewHolder {

        TextView totalReward, totalPoints, location;

        public ViewHeaderHolder(View itemView) {
            super(itemView);
            totalReward = itemView.findViewById(R.id.totalReward);
            totalPoints = itemView.findViewById(R.id.totalPoints);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else {
            return position;
        }
    }

    public interface OnCourseClickListener {
        public void onCourseClick(int position);
    }
}
