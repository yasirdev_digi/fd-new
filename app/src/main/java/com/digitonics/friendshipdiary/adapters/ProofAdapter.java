package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.fragments.ImageProofDetail;
import com.digitonics.friendshipdiary.models.Friend;
import com.digitonics.friendshipdiary.models.ProofModel;
import com.digitonics.friendshipdiary.models.TaskModel;
import com.digitonics.friendshipdiary.utilities.RoundedCornersTransformation;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;


public class ProofAdapter extends RecyclerView.Adapter<ProofAdapter.ViewHolder>{

    private final ProofAdapter.OnAddProofClickListener mListener;
    private Context context;
    ArrayList<ProofModel> mProofs;
    private int mTaskPos;
    private boolean mIsAddProve;

    public ProofAdapter(OnAddProofClickListener listener, ArrayList<ProofModel> list, int taskPos, boolean isAddProve){
        mListener = listener;
        mProofs = list;
        mTaskPos = taskPos;
        mIsAddProve = isAddProve;

    }
    public void empty(){
        mProofs.clear();
        notifyDataSetChanged();
    }
    public void setData(int pos, ProofModel model){
        mProofs.add(mProofs.size() - 1, model);
        notifyDataSetChanged();
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_proof, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            context = holder.itemView.getContext();
            holder.itemView.setTag(position);
            if (mProofs.get(position).getId() == null)
                holder.proofImage.setImageDrawable(context.getDrawable(R.drawable.add_more));
            else{
                final int radius = 15;
                final Transformation transformation = new RoundedCornersTransformation(radius, 0);
                if (mProofs.get(position).getImageUrl() != null){
                    Picasso.get()
                            .load(mProofs.get(position).getImageUrl().get2x())
                            .placeholder(R.drawable.thumb_placeholder)
                            .error(R.drawable.thumb_placeholder)
                            .transform(transformation)
                            .centerCrop()
                            .resize(80,80)
                            .into(holder.proofImage);
                    holder.icon.setVisibility(View.GONE);
                }else if (mProofs.get(position).getVideo_url() != null){
                    Picasso.get()
                            .load(mProofs.get(position).getThumbnail_url())
                            .placeholder(R.drawable.thumb_placeholder)
                            .error(R.drawable.thumb_placeholder)
                            .transform(transformation)
                            .centerCrop()
                            .resize(80,80)
                            .into(holder.proofImage);
                    holder.icon.setVisibility(View.VISIBLE);
                }
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    if (pos == mProofs.size()-1 && mIsAddProve){
                        mListener.onAddProofClick(mTaskPos);
                    }else{
                        ((BaseActivity)v.getContext()).addFragment(ImageProofDetail.newInstance(mProofs.get(pos)));
                    }
                }
            });
            context = holder.itemView.getContext();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mProofs.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView proofImage, icon;

        public ViewHolder(View itemView) {
            super(itemView);
            proofImage = itemView.findViewById(R.id.add_image);
            icon = itemView.findViewById(R.id.icon);

        }
    }

    public interface OnAddProofClickListener {
        public void onAddProofClick(int pos);
    }
}
