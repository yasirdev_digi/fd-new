package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.CommentModel;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder>{

    private Context context;
    ArrayList<Boolean> arrayList;
    //private int mType = 0;
    private View.OnClickListener mListener;
    ArrayList<CommentModel> mCommentList;
    boolean mProof;
    public CommentAdapter(ArrayList<CommentModel> friends, boolean isProof, View.OnClickListener listener){
        mCommentList =  friends;
        mProof = isProof;
        mListener = listener;

    }
  /*  public FriendsAdapter(int commentTxt){
        mType = commentTxt;
    }*/

    public void setData(CommentModel data){
        mCommentList.add(0,data);
        notifyDataSetChanged();
    }

    public ArrayList<CommentModel> getData(){
        return  mCommentList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_comments, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            CommentModel commentModel = mCommentList.get(position);
            UserModel userModel = commentModel.getUserModel();

            if(userModel != null) {
                holder.commentTxt.setText(commentModel.getComment());
                holder.time.setText(Utils.getRelativeTime(DateUtils.getRelativeTimeSpanString(Utils.getTime(commentModel.getCreatedAt()))));
                Picasso.get().load(userModel.getImage() != null ? userModel.getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(holder.profileImage);
            }
            holder.itemView.setTag(position);
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View v) {
                    final int pos = (int)v.getTag();
                    if (mCommentList.get(pos).getUserModel().getId().toString().equalsIgnoreCase(BaseActivity.USER.getId())){
                        DialogInterface.OnClickListener posListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                if (!mProof) {
                                    ((BaseActivity) v.getContext()).mApiBuilder.deletePostComment(BaseActivity.USER.getAccessToken(), mCommentList.get(pos).getId(), new CustomRunnable() {
                                        @Override
                                        public void run(Object data) {
                                            mCommentList.remove(pos);
                                            notifyDataSetChanged();
                                        }
                                    }, v.getContext());
                                }else{
                                    ((BaseActivity) v.getContext()).mApiBuilder.deleteProofComment(BaseActivity.USER.getAccessToken(), mCommentList.get(pos).getId(), new CustomRunnable() {
                                        @Override
                                        public void run(Object data) {
                                            mCommentList.remove(pos);
                                            notifyDataSetChanged();
                                            mListener.onClick(v);
                                        }
                                    }, v.getContext());
                                }
                            }
                        };

                        DialogInterface.OnClickListener negListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        };


                        Utils.showPromptDialog(v.getContext(), "Are you sure to delete the item?", null, "Yes", "No",posListener, negListener, true);
                    }
                    return false;
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mCommentList.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView commentTxt, time;
        ImageView profileImage;

        public ViewHolder(View view) {
            super(view);
            profileImage = (ImageView) view.findViewById(R.id.profile_image);
            commentTxt = (TextView) itemView.findViewById(R.id.comment_text);
            time = (TextView) itemView.findViewById(R.id.time);
        }
    }
}
