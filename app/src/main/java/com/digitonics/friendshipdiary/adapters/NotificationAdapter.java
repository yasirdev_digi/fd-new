package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.models.ActivityModel;
import com.digitonics.friendshipdiary.models.NotificationModel;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder>{

    private OnNotificationClickListener mListener;
    private Context context;
    ArrayList<NotificationModel> mNotificationList;

    public NotificationAdapter(ArrayList<NotificationModel> list,OnNotificationClickListener listener){
        if (list != null && list.size() > 0)
            list.remove(0);
        mNotificationList = list;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notifcation, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            String lastName = mNotificationList.get(position).getUser().getLastName() != null ? mNotificationList.get(position).getUser().getLastName() : "";
            holder.name.setText(StringUtils.capitalize(mNotificationList.get(position).getUser().getFirstName()) + " " + StringUtils.capitalize(lastName));
            Picasso.get().load(mNotificationList.get(position).getUser().getImage() != null ? mNotificationList.get(position).getUser().getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(holder.profileImage);
            /*holder.ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (int) view.getTag();
                }
            });*/
            /*if (position % 2 == 0){
                holder.textView.append(Utils.getColoredString("and 19 other like your ", ContextCompat.getColor(holder.itemView.getContext(),R.color.light_gray)));
                holder.textView.append(Utils.getColoredString("photo", ContextCompat.getColor(holder.itemView.getContext(),R.color.colorPrimaryDark)));
            }else{*/
                holder.textView.setText(mNotificationList.get(position).getActivity().getExtras());
                holder.itemView.setTag(position);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (int) view.getTag();
                        mListener.onNotificationClick(mNotificationList.get(pos));
                    }
                });
//            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mNotificationList.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView, name;
        ImageView profileImage;
        ConstraintLayout ll;

        public ViewHolder(View view) {
            super(view);

            textView = view.findViewById(R.id.following);
            name = itemView.findViewById(R.id.name);
            profileImage = itemView.findViewById(R.id.profile_image);
            /*imageView = (ImageView) view.findViewById(R.id.imageView5);
            ll = (ConstraintLayout) view.findViewById(R.id.constraintLL);
            commentTxt = (TextView) itemView.findViewById(R.id.commentTxt);
            email = (TextView) itemView.findViewById(R.id.email);
            profileImage = (ImageView) itemView.findViewById(R.id.profileImage);*/
        }
    }

    public interface OnNotificationClickListener {
        public void onNotificationClick(NotificationModel model);
    }
}
