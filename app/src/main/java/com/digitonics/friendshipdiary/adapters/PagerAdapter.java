package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.fragments.MyGoals;
import com.digitonics.friendshipdiary.fragments.OtherGoalsFragment;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.OtherGoalModel;

import java.util.ArrayList;

/**
 * Created by syed.kamil on 3/9/2018.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {

    private Context mContext;
    private OtherGoalModel mModel;

    public PagerAdapter(Context context, FragmentManager fm, OtherGoalModel otherGoalModel) {
        super(fm);
        mContext = context;
        mModel = otherGoalModel;
    }

    @Override
    public int getItemPosition(Object object){
        return POSITION_NONE;
    }
    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return OtherGoalsFragment.newInstance(mModel.getSponsor() != null ? mModel.getSponsor(): new ArrayList<GoalModel>(), 0);
        } else if (position == 1){
            return OtherGoalsFragment.newInstance(mModel.getWatcher() != null ? mModel.getWatcher(): new ArrayList<GoalModel>(), 1);
        } else if (position == 2){
            return OtherGoalsFragment.newInstance(mModel.getSupporter() != null ? mModel.getSupporter():new ArrayList<GoalModel>(), 2);
        } else {
            return new OtherGoalsFragment();
        }

    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 3;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return mContext.getString(R.string.sponser);
            case 1:
                return mContext.getString(R.string.watcher);
            case 2:
                return mContext.getString(R.string.supporter);
            default:
                return null;
        }
    }

}