package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.UserModel;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;


public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> implements Filterable{

    private Context context;
    ArrayList<Boolean> arrayList;
    //private int mType = 0;
    private NewsfeedAdapter.OnNewsFeedClickListener mListener;
    ArrayList<UserModel> filteredList;
    ArrayList<UserModel> mFriends;
    private FilterList filterList;
    private View mImage;

    public UsersAdapter(NewsfeedAdapter.OnNewsFeedClickListener listener, ArrayList<UserModel> friends,View image){
        mListener = listener;
        filteredList =  friends;
        mFriends = friends;
        mImage = image;
    }
  /*  public FriendsAdapter(int commentTxt){
        mType = commentTxt;
    }*/

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_friends, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            UserModel friend = filteredList.get(position);
            String lastName = friend.getLastName() != null ? friend.getLastName() : "";
            holder.name.setText(StringUtils.capitalize(friend.getFirstName()) + " " + StringUtils.capitalize(lastName));
            holder.email.setText(friend.getEmail());
            Picasso.get().load(friend.getImage() != null ? friend.getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(holder.profileImage);
            if (friend.getFollowing().equalsIgnoreCase("0")){
                holder.type.setText("Follow");
            }else{
                holder.type.setText("UnFollow");
            }
            holder.type.setTag(position);
            holder.type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final int pos = (int) view.getTag();
                    ((BaseActivity)view.getContext()).mApiBuilder.follow(BaseActivity.USER.getAccessToken(), filteredList.get(pos).getId(), new CustomRunnable() {
                        @Override
                        public void run(Object data) {
                            int oldPos = filteredList.get(pos).getOldPosition();
                            if (filteredList.get(pos).getFollowing().equalsIgnoreCase("0")){
                                filteredList.get(pos).setFollowing("1");
                                mFriends.get(oldPos).setFollowing("1");
                            }else{
                                filteredList.get(pos).setFollowing("0");
                                mFriends.get(oldPos).setFollowing("0");
                            }

                            notifyDataSetChanged();
                        }
                    }, view.getContext());
                }
            });
            holder.itemView.setTag(position);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public Filter getFilter() {
        if(filterList == null) {
            filterList =new FilterList();
        }
        return filterList;
    }


    private class FilterList extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            //Implement filter logic
            // if edittext is null return the actual list
            if (constraint == null || constraint.length() == 0) {
                //No need for filter
                results.values = mFriends;
                results.count = mFriends.size();

            } else {
                //Need Filter
                // it matches the text  entered in the edittext and set the data in adapter list
                ArrayList<UserModel> filterUsers = new ArrayList<UserModel>();
                for (int i = 0; i < mFriends.size(); i++) {
                    UserModel s = mFriends.get(i);
                    if ((s.getFirstName()!= null && s.getFirstName().toUpperCase().trim().contains(constraint.toString().toUpperCase().trim())) || s.getEmail().toUpperCase().trim().contains(constraint.toString().toUpperCase().trim())) {
                        s.setOldPosition(i);
                        filterUsers.add(s);
                    }
                }
                results.values = filterUsers;
                results.count = filterUsers.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {

            //it set the data from filter to adapter list and refresh the recyclerview adapter
            filteredList = (ArrayList<UserModel>) results.values;
            notifyDataSetChanged();
            if (((ArrayList<UserModel>) results.values).size() > 0)
                mImage.setVisibility(View.GONE);
            else
                mImage.setVisibility(View.VISIBLE);
        }
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, email,type;
        ImageView profileImage;
        ImageView imageView;
        ConstraintLayout ll;

        public ViewHolder(View view) {
            super(view);
            profileImage = (ImageView) view.findViewById(R.id.profile_image);
            name = (TextView) itemView.findViewById(R.id.name);
            email = (TextView) itemView.findViewById(R.id.email_id);
            type = (TextView) itemView.findViewById(R.id.type);
            /*
            ll = (ConstraintLayout) view.findViewById(R.id.constraintLL);
            email = (TextView) itemView.findViewById(R.id.email);
            profileImage = (ImageView) itemView.findViewById(R.id.profileImage);*/
        }
    }
}
