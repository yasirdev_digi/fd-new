package com.digitonics.friendshipdiary.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.models.DairyModel;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.util.ArrayList;


public class SkillAdapter extends RecyclerView.Adapter<SkillAdapter.ViewHolder>{

    private final MyDiaryAdapter.OnDairylClickListener mListener;
    private final ArrayList<String> mSkillList;
    private Context context;
    private boolean isProfileView;

    public SkillAdapter(MyDiaryAdapter.OnDairylClickListener listener, ArrayList<String> list){
        mListener = listener;
        mSkillList = list;
        isProfileView = false;
    }

    public SkillAdapter(MyDiaryAdapter.OnDairylClickListener listener, ArrayList<String> list, boolean isProfile){
        mListener = listener;
        mSkillList = list;
        isProfileView = isProfile;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_skill, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            context = holder.itemView.getContext();
            holder.title.setText(mSkillList.get(position));
            holder.title.setTag(position);
            if(!isProfileView) {
                if (position == mSkillList.size() - 1) {
                    holder.title.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            View view = ((Activity) context).getLayoutInflater().inflate(R.layout.skill_dialog, null);
                            final EditText skill = (EditText) view.findViewById(R.id.skillName);
                            new AlertDialog.Builder(v.getContext(), R.style.AppTheme_DialogTheme)
                                    .setTitle("Add Skills")
                                    .setView(view)
                                    .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (!skill.getText().toString().trim().equalsIgnoreCase("")) {
                                                mSkillList.add(mSkillList.size() - 1, skill.getText().toString());
                                                notifyDataSetChanged();
                                                Utils.hideSoftKeyboard(skill);
                                            }
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Utils.hideSoftKeyboard(skill);
                                        }
                                    }).create().show();

                        }
                    });
                }else{
                    holder.title.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            final int pos = (int)v.getTag();
                            DialogInterface.OnClickListener posListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    View view = ((Activity) context).getLayoutInflater().inflate(R.layout.skill_dialog, null);
                                    final EditText skill = (EditText) view.findViewById(R.id.skillName);
                                    skill.setText(mSkillList.get(pos));
                                    new AlertDialog.Builder(v.getContext(), R.style.AppTheme_DialogTheme)
                                            .setTitle("Edit Skills")
                                            .setView(view)
                                            .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (!skill.getText().toString().equalsIgnoreCase("")) {
                                                        mSkillList.set(pos, skill.getText().toString());
                                                        notifyDataSetChanged();
                                                        Utils.hideSoftKeyboard(skill);
                                                    }
                                                }
                                            })
                                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Utils.hideSoftKeyboard(skill);
                                                }
                                            }).create().show();

                                }
                            };

                            DialogInterface.OnClickListener negListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mSkillList.remove(pos);
                                    notifyDataSetChanged();
                                    dialog.dismiss();
                                }
                            };

                            DialogInterface.OnClickListener neutralListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            };
                            Utils.showPromptDialog(v.getContext(), "You want to edit or delete the item?", null, "Edit", "Delete", "Cancel",neutralListener,posListener, negListener, true);

                        }
                    });
                }
            }
            /*DairyModel model = mSkillList.get(position);
            holder.title.setText(model.getTitle());
            holder.date.setText(Utils.getDate(model.getStartDate(), "yyyy-MM-dd hh:mm:ss") + "," + Utils.get12HourTime(model.getTime()));
            holder.day.setText(DateUtils.getRelativeTimeSpanString(Utils.getTime(model.getStartDate())));

            if(position == 0){
                holder.topLeft.setVisibility(View.GONE);
                holder.topRight.setVisibility(View.GONE);
            }
            if (position == mSkillList.size() - 1){
                holder.bottomLeft.setVisibility(View.GONE);
                holder.bottomRight.setVisibility(View.GONE);
            }*/

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mSkillList.size();
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            title =  itemView.findViewById(R.id.skillName);

        }
    }


}
