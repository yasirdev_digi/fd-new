package com.digitonics.friendshipdiary.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.fragments.NotificationlFragment;
import com.digitonics.friendshipdiary.fragments.TaskListFragment;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.util.ArrayList;


public class RewardAdapter extends RecyclerView.Adapter<RewardAdapter.BaseViewHolder>{

    private final UserModel mUserModel;
    private Context context;
    private OnCourseClickListener mListener;
    private ArrayList<GoalModel> mGoals;

    public RewardAdapter(OnCourseClickListener listener, UserModel userModel){
        mListener = listener;
        mGoals = userModel.getGoals();
        mUserModel = userModel;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == 0)
            return new ViewHeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_wish_header, parent, false));
        else
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_reward, parent, false));

    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        try {
            context = holder.itemView.getContext();
            holder.itemView.setTag(position);


            if (position == 0){
                RewardAdapter.ViewHeaderHolder viewHeaderHolder = (RewardAdapter.ViewHeaderHolder)holder;
                viewHeaderHolder.totalReward.setText("$ " +mUserModel.getRewards());
                viewHeaderHolder.totalPoints.setText("" +mUserModel.getPoints());
            }else{

                ViewHolder viewHolder = (ViewHolder)holder;
                viewHolder.itemView.setTag(position);
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = (int) view.getTag();
//                        if (mGoals.get(pos).getStatus().equalsIgnoreCase("completed")) {
                            mListener.onCourseClick(pos);
                        /*}else
                            Utils.showPromptDialog(view.getContext(), "", "Wish list item can be created once goal is completed!", "OK", "Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }, false);*/
                    }
                });
                viewHolder.goalName.setText(mGoals.get(position).getName());
                viewHolder.goalDesc.setText("Reward : $ " +mGoals.get(position).getRewards());
                viewHolder.status.setText(mGoals.get(position).getStatus());

                viewHolder.viewTask.setTag(position);
                viewHolder.viewTask.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        ((BaseActivity)v.getContext()).addFragment(TaskListFragment.newInstance(mGoals.get(pos)));
                    }
                });
            }

            if(position == mGoals.size() - 1){
                holder.bottomLeft.setVisibility(View.GONE);
                holder.bottomRight.setVisibility(View.GONE);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mGoals.size();
    }



    public static class BaseViewHolder extends RecyclerView.ViewHolder {

        ImageView bottomLeft, bottomRight;

        public BaseViewHolder(View itemView) {
            super(itemView);

            bottomLeft = (ImageView) itemView.findViewById(R.id.bottomLeft);
            bottomRight = (ImageView) itemView.findViewById(R.id.bottomRight);

        }
    }

    public static class ViewHolder extends RewardAdapter.BaseViewHolder {

        TextView goalName, goalDesc, status;
        ImageView viewTask;
        public ViewHolder(View itemView) {
            super(itemView);
            goalName =  itemView.findViewById(R.id.goal_name);
            goalDesc =  itemView.findViewById(R.id.goal_desc);
            status =  itemView.findViewById(R.id.status);
            viewTask =  itemView.findViewById(R.id.view_task);
        }
    }

    public static class ViewHeaderHolder extends RewardAdapter.BaseViewHolder {

        TextView totalReward, totalPoints, location;

        public ViewHeaderHolder(View itemView) {
            super(itemView);
            totalReward = itemView.findViewById(R.id.totalReward);
            totalPoints = itemView.findViewById(R.id.totalPoints);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else {
            return position;
        }
    }

    public interface OnCourseClickListener {
        public void onCourseClick(int position);
    }
}
