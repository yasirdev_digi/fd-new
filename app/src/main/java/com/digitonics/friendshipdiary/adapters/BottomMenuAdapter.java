package com.digitonics.friendshipdiary.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.models.BottomModel;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.util.List;

public class BottomMenuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<BottomModel.Menu> menus;
    private OnMenuClickListener listener;
    private Context mContext;

    public BottomMenuAdapter(List<BottomModel.Menu> menus, OnMenuClickListener listener) {
        this.menus = menus;
        this.listener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bottom_menu, parent, false);
        return new BottomHolder(view);
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {

        BottomModel.Menu item = menus.get(position);

        final BottomHolder holder = (BottomHolder) viewHolder;
        holder.ll.setTag(position);
        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hide((Activity) v.getContext());
                int pos = Integer.parseInt(v.getTag().toString());
                setSelectedMenu(pos);

            }
        });

        holder.imageIcon.setImageResource(Utils.getResId(mContext, menus.get(position).getImage()));

        holder.ll.setSelected(item.getSelected());

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return menus == null ? 0 : menus.size();
    }

    public void setSelectedMenu(int position){
        for(int i = 0;i < menus.size(); i++){
            menus.get(i).setSelected(false);
        }
        menus.get(position).setSelected(true);
        BottomModel.Menu menuItem = menus.get(position);
        listener.onMenuClick(menuItem, position);
        notifyDataSetChanged();
    }


    private class BottomHolder extends RecyclerView.ViewHolder {

        private ImageView imageIcon;
        private RelativeLayout ll;

        BottomHolder(View view) {
            super(view);

            imageIcon = (ImageView) view.findViewById(R.id.tabImageView);
            ll = (RelativeLayout) view.findViewById(R.id.itemList);
        }
    }

    public interface OnMenuClickListener {
        public void onMenuClick(BottomModel.Menu menu, int position);
    }

}