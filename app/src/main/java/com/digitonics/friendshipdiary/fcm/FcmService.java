package com.digitonics.friendshipdiary.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.CustomToastActivity;
import com.digitonics.friendshipdiary.activities.MainActivity;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;

import java.io.InputStream;
import java.net.URL;
import java.util.Map;

public class FcmService extends FirebaseMessagingService {
    private static void generateNotification(Context context, String message, String image, Intent in) {
        int icon = R.mipmap.ic_launcher;

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        String title = context.getString(R.string.app_name);
        Bitmap remotePicture = null;
        Bitmap appIcon = null;

        // Create the style object with BigPictureStyle subclass.
        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
        notiStyle.setBigContentTitle(title);
        notiStyle.setSummaryText(message);

        try {
            appIcon = BitmapFactory.decodeResource(context.getResources(), icon);
            remotePicture = BitmapFactory.decodeStream((InputStream) new URL(image).getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Add the big picture to the style.
        if (remotePicture != null)
            notiStyle.bigPicture(remotePicture);

        // This ensures that the back button follows the recommended convention for the back key.
        PendingIntent resultPendingIntent;
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself).
        try {
            if (in.getData() == null)
                stackBuilder.addParentStack(Class.forName(in.getComponent().getClassName()));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // Adds the Intent that starts the Activity to the top of the stack.
        stackBuilder.addNextIntent(in);
        resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);


        Notification noti = new NotificationCompat.Builder(context)
                .setSmallIcon(icon)
                .setAutoCancel(true)
                .setLargeIcon(appIcon)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(remotePicture == null ? null : notiStyle).build();

        noti.defaults |= Notification.DEFAULT_LIGHTS;
        noti.defaults |= Notification.DEFAULT_VIBRATE;
        noti.defaults |= Notification.DEFAULT_SOUND;

        noti.flags |= Notification.FLAG_ONLY_ALERT_ONCE;

        notificationManager.notify(0, noti);

    }

    @Override
    public void onMessageReceived(final RemoteMessage message) {
        String from = message.getFrom();
        final Map data = message.getData();
        String title =  getResources().getString(R.string.app_name);
        if (data != null) {
            String temp = "";

            if (data.size() > 0) {

                try{
                    temp = message.getNotification().getBody();
                    title = message.getNotification().getTitle();
                    //orderRefresh = data.get("newOrder").toString();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
            else
                temp = "" + getResources().getString(R.string.app_name);
            //final String image = data.get("image").toString();
            final String msg = temp;


            /* Check if message is New order is added. then we will refresh the order list using BroadcastReceiver*/
            /*if(orderRefresh.equalsIgnoreCase("1")) {
                Intent in = new Intent();
                in.putExtra("TYPE", msg);
                in.setAction("ACTION_REFRESH_ORDER_LIST");
                //sendBroadcast(in);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(in);
            }*/
            getApplication().getSharedPreferences(BaseActivity.PREFERENCE_KEY, MODE_PRIVATE).
                    edit().putBoolean(BaseActivity.NOTIFICATION_KEY,true).apply();
            Utils.playAudio(getApplicationContext(), R.raw.notification_sound);
            if (!PushNotificationUtils.isAppIsInBackground(getApplicationContext())) {
                Intent intent = new Intent(this, CustomToastActivity.class);
                String interestedID = "", senderID = "";
                try {
                    Intent in = new Intent();
                    in.putExtra("TYPE", msg);
                    in.setAction("ACTION_REFRESH_ORDER_LIST");
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(in);
                    /*interestedID = jsonObject.getString(Utils.NOTIFICATION_DATA_INTERESTEDID);
                    senderID = jsonObject.getString(Utils.NOTIFICATION_DATA_SENDERID);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }

                intent.putExtra("title", title);
                intent.putExtra("message", msg);

                /*ntent.putExtra(Utils.NOTIFICATION_TITLE, (notification.getTitle() == null ?"Alert":notification.getTitle() + "\n"));
                intent.putExtra(Utils.NOTIFICATION_MESSAGE, (notification.getBody() == null ?"":notification.getBody()));
//                    intent.putExtra(Utils.NOTIFICATION_DATA, jsonObject.toString());
                intent.putExtra(Utils.NOTIFICATION_TYPE, type);*/
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }else{
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        generateNotification(getApplicationContext(), msg, "", notificationDispatcher(getApplicationContext(), data));
                    }
                }).start();
            }
        }
    }

    private Intent notificationDispatcher(Context context, @NonNull Map bundle) {

        Intent notificationIntent = new Intent();
        notificationIntent.setClass(context, MainActivity.class);

        return notificationIntent;
    }
}
