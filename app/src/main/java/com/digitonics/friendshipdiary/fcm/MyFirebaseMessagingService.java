package com.digitonics.friendshipdiary.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by fahadali on 3/27/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
//    @Override
//    public void onMessageReceived(RemoteMessage remoteMessage) {
//        // ...
//
//        // TODO(developer): Handle FCM messages here.
//        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
//        Log.d("FCM_TAG", "From: " + remoteMessage.getFrom());
//
//        // Check if message contains a data payload.
//        if (remoteMessage.getData().size() > 0) {
//            Log.e("FCM_TAG", "Message data payload: " + remoteMessage.getData());
//
//            if (/* Check if data needs to be processed by long running job */ true) {
//                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//                scheduleJob();
//            } else {
//                // Handle message within 10 seconds
//                handleNow();
//            }
//
//        }
//
//        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            Log.e("FCM_TAG", "Message Notification Body: " + remoteMessage.getNotification().getBody());
//        }
//
//        // Also if you intend on generating your own notifications as a result of a received FCM
//        // message, here is where that should be initiated. See sendNotification method below.
//    }
//}


    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private PushNotificationUtils notificationUtils;

    Intent resultIntent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        String channelId = "Default";
        NotificationCompat.Builder builder = new  NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("test")
                .setContentText(remoteMessage.getNotification().getBody()).setAutoCancel(true).setContentIntent(pendingIntent);;
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(channel);
        }
        manager.notify(0, builder.build());

       /* Log.e("FCM_TAG", "remoteMessage.toString(): " + remoteMessage.toString());
        Log.e("FCM_TAG", "From: " + remoteMessage.getFrom());
        Log.e("FCM_TAG", "Notification getData: " + remoteMessage.getData());

        if (remoteMessage == null)
            return;


        JSONObject jsonObject = new JSONObject(remoteMessage.getData());
        if(jsonObject!=null)
            if(jsonObject.has("custom")){
                JSONObject customObject = null;
                try {

                    //Sample Response Structure of FCM notification
                    //{"custom":"{\"view\":\"JOB FOUND\",\"view_id\":133,\"rda_id\":\"2133\",\"type\":\"job_request\",\"order_id\":\"2133\"}"}

//                    jsonObject = new JSONObject(remoteMessage.getData());
                    customObject = new JSONObject(jsonObject.getString("custom"));
                } catch (Exception e) {
                    Log.e("FCM_TAG", "Exception e = "+e.getMessage());
                }

                handleNotification(remoteMessage.getNotification(), customObject);
            }*/


       /* // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e("FCM_TAG", "Notification Body: " + remoteMessage.getNotification().getBody());

            JSONObject jsonObject = null;
            JSONObject customObject = null;
            try {

                //Sample Response Structure of FCM notification
                //{"custom":"{\"view\":\"JOB FOUND\",\"view_id\":133,\"rda_id\":\"2133\",\"type\":\"job_request\",\"order_id\":\"2133\"}"}

                jsonObject = new JSONObject(remoteMessage.getData());
                customObject = new JSONObject(jsonObject.getString("custom"));
            } catch (Exception e) {
                Log.e("FCM_TAG", "Exception e = "+e.getMessage());
            }

            handleNotification(remoteMessage.getNotification(), customObject);
        }
*/
        // Check if message contains a data payload.
//        if (remoteMessage.getData().size() > 0) {
//            DebugManager.showLog("Data Payload: " + remoteMessage.getData().toString());
//            DebugManager.showLog("Title: " + remoteMessage.getNotification().getTitle().toString());
//            DebugManager.showLog("Body: " + remoteMessage.getNotification().getBody().toString());
//
//            try {
////                DebugManager.showLog("Data---"+remoteMessage.getData().get("data"));
//                handleDataMessage(remoteMessage.getData());
//
//            } catch (Exception e) {
//                DebugManager.showLog("Exception: " + e.getMessage());
//            }
//        }
    }

    private void handleNotification(final RemoteMessage.Notification notification, final JSONObject jsonObject) {

        Log.e("FCM_TAG", "******_________________________________CHECK_________________________________******");

        /*if (!PushNotificationUtils.isAppIsInBackground(getApplicationContext())) {


            Log.e("FCM_TAG", "oye----------" + notification.getTitle() + "\n" + notification.getBody());

            String type = "";

            try {
                type = jsonObject.getString(Utils.NOTIFICATION_TYPE);
            } catch (Throwable e) {
                e.printStackTrace();
            }
//            type = Utils.NOTIFICATION_TYPE_DELIVERED_RDA;
            Intent intent;
            switch (type) {
                case Utils.NOTIFICATION_TYPE_CHAT:
                    Utils.playAudio(this, R.raw.notification_sound);

                    intent = new Intent(this, CustomToastActivity.class);
                    String interestedID = "", senderID = "";
                    try {
                        interestedID = jsonObject.getString(Utils.NOTIFICATION_DATA_INTERESTEDID);
                        senderID = jsonObject.getString(Utils.NOTIFICATION_DATA_SENDERID);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    intent.putExtra(Utils.NOTIFICATION_DATA_INTERESTEDID, interestedID);
                    intent.putExtra(Utils.NOTIFICATION_DATA_SENDERID, senderID);

                    intent.putExtra(Utils.NOTIFICATION_TITLE, (notification.getTitle() == null ?"Alert":notification.getTitle() + "\n"));
                    intent.putExtra(Utils.NOTIFICATION_MESSAGE, (notification.getBody() == null ?"":notification.getBody()));
//                    intent.putExtra(Utils.NOTIFICATION_DATA, jsonObject.toString());
                    intent.putExtra(Utils.NOTIFICATION_TYPE, type);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                    break;

                case Utils.NOTIFICATION_TYPE_PICKEDUP_CUSTOMER:

                    Utils.playAudio(this, R.raw.reached_pickup_customer);

                    intent = new Intent(this, CustomToastActivity.class);
                    intent.putExtra(Utils.NOTIFICATION_TITLE, (notification.getTitle() == null ?"Alert":notification.getTitle() + "\n"));
                    intent.putExtra(Utils.NOTIFICATION_MESSAGE, (notification.getBody() == null ?"":notification.getBody()));
//                    intent.putExtra(Utils.NOTIFICATION_DATA, jsonObject.toString());
                    intent.putExtra(Utils.NOTIFICATION_TYPE, type);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                    break;

                case Utils.NOTIFICATION_TYPE_PICKEDUP_RDA:

                    Utils.playAudio(this, R.raw.notification_sound);

                    intent = new Intent(this, CustomToastActivity.class);
                    intent.putExtra(Utils.NOTIFICATION_TITLE, (notification.getTitle() == null ?"Alert":notification.getTitle() + "\n"));
                    intent.putExtra(Utils.NOTIFICATION_MESSAGE, (notification.getBody() == null ?"":notification.getBody()));
//                    intent.putExtra(Utils.NOTIFICATION_DATA, jsonObject.toString());
                    intent.putExtra(Utils.NOTIFICATION_TYPE, type);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                    break;

                case Utils.NOTIFICATION_TYPE_DELIVERED_CUSTOMER:

                    Utils.playAudio(this, R.raw.notification_sound);

                    intent = new Intent(this, CustomToastActivity.class);
                    String rdaID = "", orderID = "";
                    try {
                        rdaID = jsonObject.getString(Utils.NOTIFICATION_DATA_RDAID);
                        orderID = jsonObject.getString(Utils.NOTIFICATION_DATA_ORDERID);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    intent.putExtra(Utils.NOTIFICATION_DATA_RDAID, rdaID);
                    intent.putExtra(Utils.NOTIFICATION_DATA_ORDERID, orderID);

                    intent.putExtra(Utils.NOTIFICATION_TITLE, (notification.getTitle() == null ?"Alert":notification.getTitle() + "\n"));
                    intent.putExtra(Utils.NOTIFICATION_MESSAGE, (notification.getBody() == null ?"":notification.getBody()));
//                    intent.putExtra(Utils.NOTIFICATION_DATA, jsonObject.toString());
                    intent.putExtra(Utils.NOTIFICATION_TYPE, type);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                    break;

                case Utils.NOTIFICATION_TYPE_DELIVERED_RDA:

                    Utils.playAudio(this, R.raw.reached_dropoff_rda);
                    Singleton.getInstance().setRdaStatus(Singleton.RDA_STATUS.ONLINE);

                    intent = new Intent(this, CustomToastActivity.class);
                    intent.putExtra(Utils.NOTIFICATION_TITLE, (notification.getTitle() == null ?"Alert":notification.getTitle() + "\n"));
                    intent.putExtra(Utils.NOTIFICATION_MESSAGE, (notification.getBody() == null ?"":notification.getBody()));
//                    intent.putExtra(Utils.NOTIFICATION_DATA, jsonObject.toString());
                    intent.putExtra(Utils.NOTIFICATION_TYPE, type);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);


                    break;

                case Utils.NOTIFICATION_TYPE_JOB_ACCEPTED:
                    Utils.playAudio(this, R.raw.notification_sound);
                    intent = new Intent(this, CustomToastActivity.class);

                    String orderid = "", requestid = "", rdaName = "";
                    JSONObject rda = null;
                    try {
                        orderid = jsonObject.getString(Utils.NOTIFICATION_DATA_ORDERID);
                        requestid = jsonObject.getString(Utils.NOTIFICATION_DATA_REQUESTID);
                        rda = jsonObject.getJSONObject(Utils.NOTIFICATION_DATA_RDA);
                        rdaName = rda.getString(Utils.NOTIFICATION_DATA_RDA_FIRSTNAME) + " " +jsonObject.getString(Utils.NOTIFICATION_DATA_RDA_LASTNAME);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    intent.putExtra(Utils.NOTIFICATION_DATA_ORDERID, orderid);
                    intent.putExtra(Utils.NOTIFICATION_DATA_REQUESTID, requestid);
                    intent.putExtra(Utils.NOTIFICATION_DATA_RDA_NAME, rdaName);

                    intent.putExtra(Utils.NOTIFICATION_TITLE, (notification.getTitle() == null ?"Alert":notification.getTitle() + "\n"));
                    intent.putExtra(Utils.NOTIFICATION_MESSAGE, (notification.getBody() == null ?"":notification.getBody()));
//                    intent.putExtra(Utils.NOTIFICATION_DATA, jsonObject.toString());
                    intent.putExtra(Utils.NOTIFICATION_TYPE, type);
                    Singleton.getInstance().setRdaStatus(Singleton.RDA_STATUS.BUSY);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    break;

                case Utils.NOTIFICATION_TYPE_JOB_CANCELLED:
                    Utils.playAudio(this, R.raw.notification_sound);

                    Singleton.getInstance().setRdaStatus(Singleton.RDA_STATUS.ONLINE);

                    intent = new Intent(this, CustomToastActivity.class);
                    intent.putExtra(Utils.NOTIFICATION_TITLE, (notification.getTitle() == null ?"Alert":notification.getTitle() + "\n"));
                    intent.putExtra(Utils.NOTIFICATION_MESSAGE, (notification.getBody() == null ?"":notification.getBody()));
//                    intent.putExtra(Utils.NOTIFICATION_DATA, jsonObject.toString());
                    intent.putExtra(Utils.NOTIFICATION_TYPE, type);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    break;

                case Utils.NOTIFICATION_TYPE_RDA_RATED:
                    Utils.playAudio(this, R.raw.notification_sound);
                    intent = new Intent(this, CustomToastActivity.class);
                    intent.putExtra(Utils.NOTIFICATION_TITLE, (notification.getTitle() == null ?"Alert":notification.getTitle() + "\n"));
                    intent.putExtra(Utils.NOTIFICATION_MESSAGE, (notification.getBody() == null ?"":notification.getBody()));
//                    intent.putExtra(Utils.NOTIFICATION_DATA, jsonObject.toString());
                    intent.putExtra(Utils.NOTIFICATION_TYPE, type);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    break;

                case Utils.NOTIFICATION_TYPE_LOGOUT:
                case Utils.NOTIFICATION_TYPE_SESSION:

                    intent = new Intent(Utils.PUSH_NOTIFICATION);
                    intent.putExtra(Utils.NOTIFICATION_TITLE, (notification.getTitle() == null ?"Alert":notification.getTitle() + "\n"));
                    intent.putExtra(Utils.NOTIFICATION_MESSAGE, (notification.getBody() == null ?"":notification.getBody()));
                    intent.putExtra(Utils.NOTIFICATION_DATA, jsonObject.toString());
                    LocalBroadcastManager.getInstance(MyFirebaseMessagingService.this).sendBroadcast(intent);

                    break;

                case Utils.NOTIFICATION_TYPE_JOB_REQUEST_RECEIVED:
                    Utils.playAudio(this, R.raw.notification_sound);
                    intent = new Intent(this, CustomToastActivity.class);
                    intent.putExtra(Utils.NOTIFICATION_TITLE, (notification.getTitle() == null ?"Alert":notification.getTitle() + "\n"));
                    intent.putExtra(Utils.NOTIFICATION_MESSAGE, (notification.getBody() == null ?"":notification.getBody()));
//                    intent.putExtra(Utils.NOTIFICATION_DATA, jsonObject.toString());
                    intent.putExtra(Utils.NOTIFICATION_TYPE, type);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    break;

                default:
                    Utils.playAudio(this, R.raw.notification_sound);
                    intent = new Intent(this, CustomToastActivity.class);
                    intent.putExtra(Utils.NOTIFICATION_TITLE, (notification.getTitle() == null ?"Alert":notification.getTitle() + "\n"));
                    intent.putExtra(Utils.NOTIFICATION_MESSAGE, (notification.getBody() == null ?"":notification.getBody()));
//                    intent.putExtra(Utils.NOTIFICATION_DATA, jsonObject.toString());
                    intent.putExtra(Utils.NOTIFICATION_TYPE, type);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    break;
            }
        } else {*/
            /*notificationUtils = new PushNotificationUtils(this);
            //Utils.playAudio(this, R.raw.notification_sound);
            // app is in background, show the notification in notification tray
            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            Log.e("FCM_TAG", "******_________________________________OYE_________________________________******");

            String type = "";

            try {
                //type = jsonObject.getString(Utils.NOTIFICATION_TYPE);
            } catch (Throwable e) {
                Log.e("FCM_TAG", "Exception e = "+e.getMessage());
            }*/
            //type = Utils.NOTIFICATION_TYPE_DELIVERED_CUSTOMER;

           /* resultIntent.putExtra(Utils.FCM_MESSAGE, notification.getBody());
            resultIntent.putExtra(Utils.NOTIFICATION_TITLE, notification.getTitle());
            resultIntent.putExtra(Utils.NOTIFICATION_MESSAGE, notification.getBody());
            resultIntent.putExtra(Utils.NOTIFICATION_DATA, jsonObject.toString());

            resultIntent.putExtra(Utils.IS_NOTIFICATION, true);
            resultIntent.putExtra(Utils.NOTIFICATION_TYPE, type);*/
//            type =  Utils.NOTIFICATION_TYPE_DELIVERED_CUSTOMER;

            /*switch(type) {
                case Utils.NOTIFICATION_TYPE_DELIVERED_CUSTOMER:
                    String rdaID = "", orderID = "";
                    try {
                        rdaID = jsonObject.getString(Utils.NOTIFICATION_DATA_RDAID);
                        orderID = jsonObject.getString(Utils.NOTIFICATION_DATA_ORDERID);
                    } catch (Throwable e) {
//                        e.printStackTrace();
                        rdaID = "DUMMYRDAID";
                        orderID = "DUMMYORDERID";
                    }
                    resultIntent.putExtra(Utils.NOTIFICATION_DATA_RDAID, rdaID);
                    resultIntent.putExtra(Utils.NOTIFICATION_DATA_ORDERID, orderID);
                break;

                case Utils.NOTIFICATION_TYPE_CHAT:
                    String interestedID = "", senderID = "";
                    try {
                        interestedID = jsonObject.getString(Utils.NOTIFICATION_DATA_INTERESTEDID);
                        senderID = jsonObject.getString(Utils.NOTIFICATION_DATA_SENDERID);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    resultIntent.putExtra(Utils.NOTIFICATION_DATA_INTERESTEDID, interestedID);
                    resultIntent.putExtra(Utils.NOTIFICATION_DATA_SENDERID, senderID);
                break;
            }*/

            // check for image attachment
//            if (TextUtils.isEmpty(notification.getIcon())) {
//                showNotificationMessage(getApplicationContext(), /*/*networkClass.decodeData(*/notification.getTitle()/*)*/, /*/*networkClass.decodeData(*/notification.getBody(), /*/*networkClass.decodeData(*/notification.getBody(), resultIntent);
//            } else {
//                // image is present, show notification with image
//                showNotificationMessageWithBigImage(getApplicationContext(), /*networkClass.decodeData(*/notification.getTitle()/*)*/, /*/*networkClass.decodeData(*/notification.getBody()/*)*/, /*/*networkClass.decodeData(*/notification.getBody()/*)*/, resultIntent, notification.getIcon());
//            }
      //  }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new PushNotificationUtils(context);
        if (intent != null) {
            intent.setFlags(
//                    Intent.FLAG_ACTIVITY_NEW_TASK
//                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                     Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
//                    | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
//                    | Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP
            );

        }
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new PushNotificationUtils(context);
//        if (intent != null)
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }


}
