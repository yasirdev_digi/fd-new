package com.digitonics.friendshipdiary.fcm;

import android.content.SharedPreferences;
import android.util.Log;

import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.webservice.ApiBuilder;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static com.digitonics.friendshipdiary.activities.BaseActivity.DEVICE_TOKEN;
import static com.digitonics.friendshipdiary.activities.BaseActivity.REG_KEY;
import static com.digitonics.friendshipdiary.activities.BaseActivity.PREFERENCE_KEY;

public class ListenerService extends FirebaseInstanceIdService {

    public static final String TAG = "token";
    public SharedPreferences mPreferences;

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        mPreferences = getApplicationContext().getSharedPreferences(PREFERENCE_KEY, MODE_PRIVATE);

        mPreferences.edit().putString(DEVICE_TOKEN, refreshedToken).apply();
        if (BaseActivity.USER != null) {
            ApiBuilder.getInstance(this).deviceUpdate(BaseActivity.USER.getAccessToken(), Utils.getDeviceID(this), refreshedToken, new CustomRunnable() {
                @Override
                public void run(Object data) {
                    mPreferences.edit().putBoolean(REG_KEY, true).apply();
                }
            },this);
        }
    }
}
