package com.digitonics.friendshipdiary.fcm;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.webservice.ApiBuilder;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import static com.digitonics.friendshipdiary.activities.BaseActivity.DEVICE_TOKEN;
import static com.digitonics.friendshipdiary.activities.BaseActivity.PREFERENCE_KEY;
import static com.digitonics.friendshipdiary.activities.BaseActivity.REG_KEY;

public class RegistrationService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public RegistrationService(String name) {
        super(name);
    }

    public RegistrationService() {
        super(RegistrationService.class.getCanonicalName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("Token", token + "");

        if (BaseActivity.USER != null) {
            ApiBuilder.getInstance(this).deviceUpdate(BaseActivity.USER.getAccessToken(), Utils.getDeviceID(this), token, new CustomRunnable() {
                @Override
                public void run(Object data) {
                    getSharedPreferences(PREFERENCE_KEY, MODE_PRIVATE).edit().putBoolean(REG_KEY, true).apply();
                }
            },this);
        }
    }
}
