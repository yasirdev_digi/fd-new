package com.digitonics.friendshipdiary.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Syed Kamil Hafeez on 7/12/16.
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface OverridingBaseLayout {
    int coordinatorId();
}
