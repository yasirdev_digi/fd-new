package com.digitonics.friendshipdiary.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.activities.VideoActivity;
import com.digitonics.friendshipdiary.activities.ZoomImageActivity;
import com.digitonics.friendshipdiary.adapters.CommentAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.CommentModel;
import com.digitonics.friendshipdiary.models.ProofModel;
import com.digitonics.friendshipdiary.utilities.RoundedCornersTransformation;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;

import static com.digitonics.friendshipdiary.activities.BaseActivity.NOTIFICATION_KEY;

@IgnoreApi
public class ImageProofDetail extends BaseFragment<ProofModel> implements View.OnClickListener{


    public static final String TAG_MODULE = "InternetFailure";
    private Runnable mRunnable;
    private TextView name, time;
    private ImageView profileImage, postProfileImage, media;
    private Button postBtn;
    private EditText mCommentTxt;
    private LinearLayoutManager mTaskManager;
    private ProofModel mModel;
    private CommentAdapter mCommentAdapter;
    private RecyclerView mTasksRV;
    private boolean PENDING_HIT = false;
    private ImageView icon;

    public static ImageProofDetail newInstance(ProofModel model) {
        ImageProofDetail imageProofDetail = new ImageProofDetail();
        Bundle bundle = new Bundle();
        bundle.putParcelable("proof", model);
        imageProofDetail.setArguments(bundle);
        return imageProofDetail;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragments_image_proof, container, false);

        mTasksRV = (RecyclerView) view.findViewById(R.id.comment_rv);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        setHasOptionsMenu(true);
        ((BaseActivity)getActivity()).setTitle("Image Proof");

        name = (TextView) view.findViewById(R.id.name);
        time = (TextView) view.findViewById(R.id.time);
        profileImage = (ImageView) view.findViewById(R.id.profile_image);
        icon = (ImageView) view.findViewById(R.id.icon);
        postProfileImage = (ImageView) view.findViewById(R.id.post_profile_image);
        media = (ImageView) view.findViewById(R.id.media_item);
        //mIconMore = (ImageView) view.findViewById(R.id.day);
        postBtn = (Button) view.findViewById(R.id.post);
        mCommentTxt = (EditText) view.findViewById(R.id.comment_text);

        mTaskManager = new LinearLayoutManager(getActivity());

        mModel = getArguments().getParcelable("proof");
        callApi();
        return view;
    }

    @Override
    public void onClick(View view) {
        Utils.hide(getActivity());
        if (!mCommentTxt.getText().toString().equalsIgnoreCase("")){
            if(!PENDING_HIT) {
                PENDING_HIT = !PENDING_HIT;
                mApiBuilder.postProveComment(BaseActivity.USER.getAccessToken(), mModel.getId(), mCommentTxt.getText().toString(), new CustomRunnable() {
                    @Override
                    public void run(Object data) {
                        PENDING_HIT = !PENDING_HIT;
                        CommentModel model = (CommentModel) data;
                        mCommentAdapter.setData(model);
                        mCommentTxt.setText("");
                    }
                }, getActivity());
            }else
                ((BaseActivity)getActivity()).showSnack( "Please wait while your request completed");
        }else{
            ((BaseActivity)getActivity()).showSnack( "Please enter comment text...");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                Utils.hide(getActivity());
                getActivity().onBackPressed();
                return true;
            case R.id.notification:
                ((HomeActivity)getActivity()).addFragment(new NotificationlFragment());
                ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.GONE);
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.getProofDetail(BaseActivity.USER.getAccessToken(), mModel.getId());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_notification, menu);
        if (((BaseActivity)getActivity()).mPreferences.contains(NOTIFICATION_KEY))
            ((BaseActivity)getActivity()).getToolbar().getMenu().findItem(R.id.notification).setIcon(R.drawable.icon_noti1);
        else
            ((BaseActivity)getActivity()).getToolbar().getMenu().findItem(R.id.notification).setIcon(R.drawable.icon_noti);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void run() {
        try {

            mModel = mData;
            mCommentAdapter = new CommentAdapter(mModel.getComments() != null ? mModel.getComments(): new ArrayList<CommentModel>(), true, null);

            mTasksRV.setLayoutManager(mTaskManager);
            mTasksRV.setAdapter(mCommentAdapter);
            postBtn.setOnClickListener(this);
            final int radius = 20;
            final Transformation transformation = new RoundedCornersTransformation(radius, 5);
            String lastName = mModel.getUser().getLastName() != null ? mModel.getUser().getLastName() : "";
            name.setText(StringUtils.capitalize(mModel.getUser().getFirstName()) + " " + StringUtils.capitalize(lastName));
            time.setText(DateUtils.getRelativeTimeSpanString(Utils.getTime(mModel.getCreatedAt())));
            if (mModel.getImageUrl() != null){
                Picasso.get()
                        .load(mModel.getImageUrl() != null ? mModel.getImageUrl().get2x() : null)
                        .placeholder(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .transform(transformation)
                        /*.centerCrop()
                        .resize(R.dimen._200sdp,R.dimen._125sdp)*/
                        .into(media);
            }else if (mModel.getVideo_url() != null){
                Picasso.get()
                        .load(mModel.getVideo_url())
                        .placeholder(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .transform(transformation)
                        /*.centerCrop()
                        .resize(R.dimen._200sdp,R.dimen._125sdp)*/
                        .into(media);
                icon.setVisibility(View.VISIBLE);
            }

            //Picasso.get().load(mModel.getImageUrl() != null ? mModel.getImageUrl().get2x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(media);
            Picasso.get().load(mModel.getUser().getImage() != null ? mModel.getUser().getImage().get2x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(profileImage);
            Picasso.get().load(BaseActivity.USER.getImage()!= null ? BaseActivity.USER.getImage().get2x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(postProfileImage);

            media.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mModel.getImageUrl() != null){
                        Intent videoIntent = new Intent(v.getContext(), ZoomImageActivity.class);
                        videoIntent.putExtra("image", mModel.getImageUrl() != null ? mModel.getImageUrl().get2x() : null);
                        startActivity(videoIntent);
                    }else if (mModel.getVideo_url() != null){
                        Intent videoIntent = new Intent(v.getContext(), VideoActivity.class);
                        videoIntent.putExtra("url",mModel.getVideo_url());
                        v.getContext().startActivity(videoIntent);
                    }

                }
            });
        }catch (Exception ex){

        }
    }
}
