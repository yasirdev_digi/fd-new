package com.digitonics.friendshipdiary.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.adapters.WishListAdapter;
import com.digitonics.friendshipdiary.interfaces.UpdateRunnable;
import com.digitonics.friendshipdiary.models.DeliveryAddressModel;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.WishModel;


public class WishListFragment extends Fragment implements UpdateRunnable, WishListAdapter.OnWishlistClickListener {


    public static final String TAG_MODULE = "ModuleFragment";
    private Runnable mRunnable;
    private LinearLayoutManager mWishManager;
    private RecyclerView mWishRV;
    private GoalModel mModel;
    int index = 0;
    private WishListAdapter mWishAdapter;
    private View mNoData;


    public static WishListFragment newInstance(GoalModel goalModel) {
        WishListFragment f = new WishListFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("goal",goalModel);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wishlist, container, false);
        Bundle args = getArguments();
        if (args != null && args.containsKey("goal") && mModel == null) {
            mModel = args.getParcelable("goal");
            mModel.getWishlist().add(0, new WishModel());
        }

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        return view;
    }
    public void init(View view){
        mNoData = view.findViewById(R.id.no_data_img);
        mWishRV = (RecyclerView) view.findViewById(R.id.wish_rv);
        mWishManager = new LinearLayoutManager(getActivity());

        mWishAdapter = new WishListAdapter(this, mModel);

        mWishRV.setLayoutManager(mWishManager);
        mWishRV.setAdapter(mWishAdapter);
        if (mWishAdapter.getItemCount() == 1 ) {
            mNoData.setVisibility(View.VISIBLE);
        }else{
            mNoData.setVisibility(View.GONE);
        }
        setHasOptionsMenu(true);
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
        ((BaseActivity)getActivity()).setTitle("Wishlist");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.add:
                if (mModel.getWishlist().size() < 3) {
                    ((BaseActivity) getActivity()).addFragment(CreateWishlist.newInstance(false, mModel.getWishlist().size() == 1, mModel.getRewards(),null, mModel.getId(), this));
                }else
                    ((BaseActivity)getActivity()).showSnack("Wishlist can be only primary or secondary...");
                return true;
            case R.id.delivery:
                ((BaseActivity)getActivity()).addFragment(DeliveryAddress.newInstance(false,mModel, this));
                return true;
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (BaseActivity.USER.getId().equalsIgnoreCase(mModel.getUserId()))
            inflater.inflate(R.menu.wish_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void run(Object data, int pos,boolean isUpdate) {
        if(data instanceof DeliveryAddressModel){
            mModel.setDeliveryAddress(data);
        }else {
            if (isUpdate)
                mWishAdapter.setData((WishModel) data, pos);
            else
                mWishAdapter.setData((WishModel) data);
            if (mWishAdapter.getItemCount() == 1) {
                mNoData.setVisibility(View.VISIBLE);
            } else {
                mNoData.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onWishlistClick(int position) {
        ((BaseActivity) getActivity()).addFragment(CreateWishlist.newInstance(true,
                position == 1,
                mModel.getRewards(), mModel.getWishlist().get(position), mModel.getId(), this));
    }
}
