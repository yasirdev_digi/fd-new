package com.digitonics.friendshipdiary.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.digitonics.friendshipdiary.R;


public class InternetFailure extends Fragment implements View.OnClickListener{


    public static final String TAG_INTERNET_FAILURE = "InternetFailure";
    private Runnable mRunnable;

    public InternetFailure() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_internet_failure, container, false);
        view.findViewById(R.id.retry).setOnClickListener(this);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        return view;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.retry && mRunnable != null)
            mRunnable.run();
    }

    public void setRunnable(Runnable runnable){
        mRunnable = runnable;
    }
}
