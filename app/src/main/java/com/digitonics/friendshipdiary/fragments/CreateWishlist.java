package com.digitonics.friendshipdiary.fragments;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.UpdateRunnable;
import com.digitonics.friendshipdiary.models.WishModel;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.io.IOException;

import retrofit2.Call;

@IgnoreApi
public class CreateWishlist extends BaseFragment<WishModel> {


    public static final String TAG_FEEDBACK = "Feedback";
    private Button mFriendsBtn;
    private LinearLayoutManager mFeedbackManager;
    private Button mCreateBtn;
    private EditText mTitle, mDesc, mURL, mRewards;
    TextInputLayout mTitleIL;
    private Bundle mArgs;
    private static UpdateRunnable mRunnable;
    private WishModel mWishModel;

    public static CreateWishlist newInstance(boolean update, boolean isPrimary,String reward,WishModel wishModel, String goalId, UpdateRunnable runnable) {
        CreateWishlist f = new CreateWishlist();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        //args.putParcelable("goal", goalModel);
        args.putBoolean("update", update);
        args.putBoolean("isPrimary", isPrimary);
        args.putString("reward", reward);
        args.putString("goalId", goalId);
        args.putParcelable("wish",wishModel);
        f.setArguments(args);
        mRunnable = runnable;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_wishlist, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        return view;
    }

    public void init(View view){

        mArgs = getArguments();

        mTitleIL = view.findViewById(R.id.textInputLayout3);
        mTitle = view.findViewById(R.id.item_title);
        mDesc = view.findViewById(R.id.item_desc);
        mURL = view.findViewById(R.id.item_url);
        mRewards = view.findViewById(R.id.item_rewards);
        mCreateBtn = view.findViewById(R.id.create_btn);

        if (mArgs != null){
            mRewards.setText(mArgs.getString("reward"));
            mWishModel = mArgs.getParcelable("wish");
            if (mArgs.getBoolean("isPrimary")){
                mTitleIL.setHint("Primary Wishlist Item");
            }else
                mTitleIL.setHint("Secondary Wishlist Item");
        }
        if (mWishModel != null){
            mTitle.setText(mWishModel.getTitle());
            mDesc.setText(mWishModel.getDescription());
            mURL.setText(mWishModel.getProductUrl());
            ((BaseActivity)getActivity()).setTitle("Update Wishlist");
            mCreateBtn.setText("Update");
        }else
            ((BaseActivity)getActivity()).setTitle("Create Wishlist");


        mCreateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validateFields()){
                    Utils.hide(getActivity());
                    callApi();
                }

            }
        });
        setHasOptionsMenu(true);

        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                Utils.hide(getActivity());
                ((BaseActivity)getActivity()).onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        if (mWishModel == null)
            return mApiBuilder.createWishList(BaseActivity.USER.getAccessToken(), mTitle.getText().toString(), mDesc.getText().toString(), Utils.getURL(mURL.getText().toString()),mArgs.getString("reward"), mArgs.getString("goalId"));
        else
            return mApiBuilder.updateWishList(BaseActivity.USER.getAccessToken(), mTitle.getText().toString(), mDesc.getText().toString(), Utils.getURL(mURL.getText().toString()),mArgs.getString("reward"), mWishModel.getId());

    }

    private boolean validateFields() {


        if (mTitle.getText().toString().isEmpty() == true) {
            showSnack( "Please enter name of item");
            return false;
        }
        if (mDesc.getText().toString().isEmpty() == true) {
            showSnack( "Please enter description of item");
            return false;
        }
        if (mURL.getText().toString().isEmpty() == true) {
            showSnack( "Link could not be empty!");
            return false;
        }else if (!Patterns.WEB_URL.matcher(Utils.getURL(mURL.getText().toString())).matches()){
            ((BaseActivity)getActivity()).showSnack( "Please enter proper product url");
            return false;
        }


        return true;
    }

    @Override
    public void run() {
        mRunnable.run(mData,mArgs.getBoolean("isPrimary") ? 1 : 2, mArgs.getBoolean("update"));
        ((BaseActivity)getActivity()).onBackPressed();
        if (mWishModel == null)
            ((BaseActivity)getActivity()).showSnack("Wishlist created successfully");
        else
            ((BaseActivity)getActivity()).showSnack("Wishlist updated successfully");
    }
}
