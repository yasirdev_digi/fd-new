package com.digitonics.friendshipdiary.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.adapters.GoalAdapter;
import com.digitonics.friendshipdiary.adapters.NewsfeedAdapter;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.digitonics.friendshipdiary.utilities.TopDialog;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.widgets.RecyclerViewItemDecorator;

import java.util.ArrayList;
import java.util.List;


public class OtherGoalsFragment extends Fragment implements GoalAdapter.OnGoalClickListener{


    public static final String TAG_MY_COURSE = "MyCourse";
    private Runnable mRunnable;
    private RecyclerView mGoalRV, mAchorRV;
    private LinearLayoutManager mGoalManager, mAchorManager;
    private TopDialog mDialog;
    private ArrayList<GoalModel> goalList;
    private int userType;
    private GoalAdapter mGoalAdapter;


    public static OtherGoalsFragment newInstance(ArrayList<GoalModel> goalModels, int userType) {
        OtherGoalsFragment otherGoalsFragment =  new OtherGoalsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("list", goalModels);
        bundle.putInt("userType",userType);
        otherGoalsFragment.setArguments(bundle);
        return otherGoalsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_goals, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        return view;
    }

    public void init(View view){

        mGoalRV = (RecyclerView) view.findViewById(R.id.goals_rv);

        view.findViewById(R.id.create_goal).setVisibility(View.GONE);
        goalList = getArguments().getParcelableArrayList("list");
        if (goalList != null && goalList.size() > 0){
            view.findViewById(R.id.no_data_img).setVisibility(View.GONE);
        }else
            view.findViewById(R.id.no_data_img).setVisibility(View.VISIBLE);
        userType = getArguments().getInt("userType");
        mGoalManager = new LinearLayoutManager(getActivity());;
        mGoalRV.addItemDecoration(new RecyclerViewItemDecorator(0));
        if (userType == Constants.SPONSER){
            mGoalAdapter = new GoalAdapter(this, goalList,Constants.SPONSER);
        }else{
            mGoalAdapter = new GoalAdapter(this, goalList);
        }


        mGoalRV.setLayoutManager(mGoalManager);
        mGoalRV.setAdapter(mGoalAdapter);
        Utils.animRecyclerView(mGoalRV, Utils.LIST_ANIM_FROM_TOP);
        //setHasOptionsMenu(true);
    }


    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
    }*/

    @Override
    public void onGoalClick(int type, GoalModel goalModel) {
        goalModel.setUserType(userType);
        if (type == 1){
            OtherMainFragment.ON_DETAIL = true;
            ((HomeActivity)getActivity()).addFragment(GoalDetailFragment.newInstance(goalModel));
        }
        else if(type == 2)
            ((HomeActivity)getActivity()).addFragment(FriendsFragment.newInstance(goalModel));
        else if (type == 3){
            ((BaseActivity)getActivity()).mApiBuilder.postSponsor(BaseActivity.USER.getAccessToken(), "2", goalModel.getId(), new CustomRunnable() {
                @Override
                public void run(Object data) {
                    OtherMainFragment parentFrag = ((OtherMainFragment)OtherGoalsFragment.this.getParentFragment());
                    parentFrag.callApi();
                }
            },((BaseActivity)getActivity()));

        }
        else if (type == 4){
            ((BaseActivity)getActivity()).mApiBuilder.postSponsor(BaseActivity.USER.getAccessToken(), "1", goalModel.getId(), new CustomRunnable() {
                @Override
                public void run(Object data) {
                    OtherMainFragment parentFrag = ((OtherMainFragment)OtherGoalsFragment.this.getParentFragment());
                    parentFrag.callApi();
                }
            },((BaseActivity)getActivity()).mContext);
        }
    }
}
