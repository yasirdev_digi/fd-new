package com.digitonics.friendshipdiary.fragments;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.adapters.FriendsAdapter;
import com.digitonics.friendshipdiary.adapters.NewsfeedAdapter;
import com.digitonics.friendshipdiary.adapters.TagFriendAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.widgets.RecyclerViewItemDecorator;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;

@IgnoreApi
public class TagFriends extends BaseFragment<ArrayList<UserModel>> {


    public static final String TAG_MY_FRIEND = "MyFriend";
    private static CustomRunnable mCustomRunnable;
    private RecyclerView mFriendRV, mAchorRV;
    private LinearLayoutManager mFriendManager, mAchorManager;
    private View mNoData;

    public static TagFriends newInstance (CustomRunnable runnable){
        TagFriends tagFriends = new TagFriends();
        mCustomRunnable = runnable;
        return tagFriends;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_friends, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        callApi();
        return view;
    }

    public void init(View view){

        mNoData= view.findViewById(R.id.no_data_img);
        mFriendRV = (RecyclerView) view.findViewById(R.id.friends_rv);
        if (((BaseActivity)getActivity()).mBottomLL != null)
            ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.GONE);
        getActivity().findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
        view.findViewById(R.id.create_goal).setVisibility(View.GONE);

        mFriendManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean requestChildRectangleOnScreen(RecyclerView parent, View child, Rect rect, boolean immediate, boolean focusedChildVisible) {

                return false;
            }
        };
        mFriendRV.addItemDecoration(new RecyclerViewItemDecorator(0));

        setHasOptionsMenu(true);
        ((BaseActivity)getActivity()).setDefaultBackground();
        ((BaseActivity)getActivity()).setTitle("Tag Friends");
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.VISIBLE);
                ((BaseActivity)getActivity()).onBackPressed();
                return true;
            case R.id.search:
                //Toast.makeText(getActivity(), "Will functional in beta", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.done:
                ArrayList<UserModel> user = new ArrayList<>();
                for (int i = 0; i < mData.size(); i++){
                    if (mData.get(i).isChecked()){
                        user.add(mData.get(i));
                    }
                }
                mCustomRunnable.run(user);
                ((BaseActivity)getActivity()).onBackPressed();

                return true;
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.add_edit_profile, menu);
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.getFriends(BaseActivity.USER.getAccessToken());
    }

    @Override
    public void run(){
        /*mFriendsManager = new LinearLayoutManager(getActivity());

        AddFriendAdapter mFriendAdapter = new AddFriendAdapter(this, mData);

        mFriendsRV.setLayoutManager(mFriendsManager);
        mFriendsRV.setAdapter(mFriendAdapter);*/
        if (mData != null && mData.size() > 0 ) {
            mNoData.setVisibility(View.GONE);
            TagFriendAdapter friendsAdapter = new TagFriendAdapter(null, mData);

            mFriendRV.setLayoutManager(mFriendManager);
            mFriendRV.setAdapter(friendsAdapter);
            Utils.animRecyclerView(mFriendRV, Utils.LIST_ANIM_FROM_TOP);
            getActivity().invalidateOptionsMenu();
        }else{
            mNoData.setVisibility(View.VISIBLE);
        }
    }
}
