package com.digitonics.friendshipdiary.fragments;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.adapters.FacebookAdapter;
import com.digitonics.friendshipdiary.adapters.NewsfeedAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.widgets.RecyclerViewItemDecorator;
import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;


@IgnoreApi
public class FacebookFriends extends Fragment implements View.OnClickListener, NewsfeedAdapter.OnNewsFeedClickListener {


    public static final String TAG_MY_FRIEND = "MyFriend";
    private Runnable mRunnable;
    private RecyclerView mFriendRV, mAchorRV;
    private LinearLayoutManager mFriendManager, mAchorManager;
    private LoginButton mFacebookLoginBtn;
    private View mNoData;

    public FacebookFriends() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_facebook_friends, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        return view;
    }

    public void init(View view) {
        mNoData= view.findViewById(R.id.no_data_img);
        mFacebookLoginBtn = (LoginButton) view.findViewById(R.id.login_button);
        mFriendRV = (RecyclerView) view.findViewById(R.id.friends_rv);

        if (((BaseActivity) getActivity()).mBottomLL != null)
            ((BaseActivity) getActivity()).mBottomLL.setVisibility(View.GONE);

        setHasOptionsMenu(true);

        mFriendRV.addItemDecoration(new RecyclerViewItemDecorator(0));

        getActivity().findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
        ((BaseActivity) getActivity()).setDefaultBackground();
        ((BaseActivity) getActivity()).setTitle("Facebook Friends");
        ((BaseActivity) getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);

        mFriendManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean requestChildRectangleOnScreen(RecyclerView parent, View child, Rect rect, boolean immediate, boolean focusedChildVisible) {

                return false;
            }
        };

        try {
            AccessToken token = AccessToken.getCurrentAccessToken();

            if (token == null) {
                mFacebookLoginBtn.setVisibility(View.VISIBLE);
                mFacebookLoginBtn.registerCallback(((BaseActivity) getActivity()).mCallbackManager, new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        getFriends(loginResult.getAccessToken());
                        mFacebookLoginBtn.setVisibility(View.GONE);
                    }

                    @Override
                    public void onCancel() {
                        ((BaseActivity) getActivity()).showSnack("Cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        ((BaseActivity) getActivity()).showSnack("Error");
                    }
                });
            }else{
                getFriends(token);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        ((HomeActivity) getActivity()).addFragment(new GoalDetailFragment());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                ((BaseActivity) getActivity()).onBackPressed();
                return true;

            case R.id.search:
                Toast.makeText(getActivity(), "Search feature not implemented yet", Toast.LENGTH_SHORT).show();
                return true;
        }
        return false;
    }

    @Override
    public void onNewsFeedClick(int position) {
        ((HomeActivity) getActivity()).addFragment(new ProfileFragment());
        ((BaseActivity) getActivity()).mBottomLL.setVisibility(View.GONE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.menu_search, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void getFriends(AccessToken token){
        GraphRequest request = GraphRequest.newMyFriendsRequest(
                token, new GraphRequest.GraphJSONArrayCallback() {
                    @Override
                    public void onCompleted(JSONArray objects, GraphResponse response) {
                        Log.v("LoginActivity Response ", response.toString());
                        Type listType = new TypeToken<ArrayList<UserModel>>() {}.getType();
                        ArrayList<UserModel> userlist = new Gson().fromJson(objects.toString(), listType);
                        if (userlist != null && userlist.size() >0 ) {
                            mNoData.setVisibility(View.GONE);
                            FacebookAdapter friendsAdapter = new FacebookAdapter(FacebookFriends.this, userlist);

                            mFriendRV.setLayoutManager(mFriendManager);
                            mFriendRV.setAdapter(friendsAdapter);
                            Utils.animRecyclerView(mFriendRV, Utils.LIST_ANIM_FROM_TOP);
                        }else{
                            mNoData.setVisibility(View.VISIBLE);
                        }
                    }
                });


        request.executeAsync();
    }
}
