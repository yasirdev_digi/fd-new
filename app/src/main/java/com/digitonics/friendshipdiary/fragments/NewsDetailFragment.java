package com.digitonics.friendshipdiary.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.activities.VideoActivity;
import com.digitonics.friendshipdiary.activities.ZoomImageActivity;
import com.digitonics.friendshipdiary.adapters.CommentAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.CommentModel;
import com.digitonics.friendshipdiary.models.NewsFeedModel;
import com.digitonics.friendshipdiary.utilities.RoundedCornersTransformation;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;

import static com.digitonics.friendshipdiary.activities.BaseActivity.NOTIFICATION_KEY;


@IgnoreApi
public class NewsDetailFragment extends BaseFragment<NewsFeedModel> implements View.OnClickListener{


    public static final String TAG_MODULE = "ModuleFragment";
    private Runnable mRunnable;
    private LinearLayoutManager mTaskManager;
    private RecyclerView mTasksRV;
    private TextView name,title,like, comment, friendCount, time;
    private ImageView profileImage, postProfileImage, media;
    private NewsFeedModel mModel;
    private Button postBtn;
    private EditText mCommentTxt;
    private CommentAdapter mCommentAdapter;
    private ImageView mIconMore;
    private ImageView mIconVideo;
    private View mediaLL;
    private View mMainRL;
    private boolean PENDING_HIT = false;

    public static NewsDetailFragment newInstance(NewsFeedModel model) {

        NewsDetailFragment newsDetailFragment =  new NewsDetailFragment();

        Bundle args = new Bundle();
        args.putParcelable("mModel", model);

        newsDetailFragment.setArguments(args);

        return newsDetailFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragments_newsfeed_detail, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        callApi();
        return view;
    }

    public void init(View view){


        mTasksRV = (RecyclerView) view.findViewById(R.id.comment_rv);
        mMainRL =  view.findViewById(R.id.mainRL);

        name = (TextView) view.findViewById(R.id.name);
        title = (TextView) view.findViewById(R.id.title);
        like = (TextView) view.findViewById(R.id.like);
        comment = (TextView) view.findViewById(R.id.comments);
        friendCount = (TextView) view.findViewById(R.id.friends_count);
        time = (TextView) view.findViewById(R.id.time);
        profileImage = (ImageView) view.findViewById(R.id.profile_image);
        postProfileImage = (ImageView) view.findViewById(R.id.post_profile_image);
        media = (ImageView) view.findViewById(R.id.media_item);
        mediaLL = view.findViewById(R.id.media_item_ll);
        mIconMore = (ImageView) view.findViewById(R.id.day);
        mIconVideo = (ImageView) view.findViewById(R.id.icon_video);
        postBtn = (Button) view.findViewById(R.id.post);
        mCommentTxt = (EditText) view.findViewById(R.id.comment_text);


        mTaskManager = new LinearLayoutManager(getActivity());

        mModel = getArguments().getParcelable("mModel");

        setHasOptionsMenu(true);
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
        ((BaseActivity)getActivity()).setTitle(mModel.getPost());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                Utils.hide(getActivity());
                return true;
            case R.id.notification:
                ((HomeActivity)getActivity()).addFragment(new NotificationlFragment());
                ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.GONE);
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return  mApiBuilder.getPostDetail(BaseActivity.USER.getAccessToken(), mModel.getId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.like:
                mApiBuilder.postLike(BaseActivity.USER.getAccessToken(),mModel.getId(),new CustomRunnable() {
                    @Override
                    public void run(Object data) {
                            if (mModel.getIsLike()) {
                                mModel.setIsLike("0");
                                like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_like, 0, 0, 0);
                            }
                            else {
                                mModel.setIsLike("1");
                                like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.like_pressed, 0, 0, 0);
                            }
                    }
                }, getActivity());
                break;
            case R.id.media_item:
                Intent mediaIntent = new Intent(Intent.ACTION_VIEW);
                if (mModel.getVideoUrl() != null) {
                    Intent videoIntent = new Intent(view.getContext(), VideoActivity.class);
                    videoIntent.putExtra("url", mModel.getVideoUrl());
                    view.getContext().startActivity(videoIntent);
                } else if (mModel.getAudioUrl() != null) {
                    mediaIntent.setDataAndType(Uri.parse(mModel.getAudioUrl()), "audio/*");
                    getActivity().startActivity(mediaIntent);
                } else if (mModel.getPhotoUrl() != null && mModel.getPhotoUrl().size() > 0) {
                    Intent videoIntent = new Intent(view.getContext(), ZoomImageActivity.class);
                    videoIntent.putExtra("image", mModel.getPhotoUrl().get(0).get3x());
                    startActivity(videoIntent);
                } else if (mModel.getLatitude() != null && mModel.getLongitude() != null) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?z=12&t=m&q=" + mModel.getLatitude() + "," + mModel.getLongitude()));
                    startActivity(intent);
                }
                break;

            case R.id.post:
                Utils.hide(getActivity());
                if (!mCommentTxt.getText().toString().equalsIgnoreCase("")){
                    if(!PENDING_HIT) {
                        PENDING_HIT = !PENDING_HIT;
                        mApiBuilder.postComment(BaseActivity.USER.getAccessToken(), mModel.getId(), mCommentTxt.getText().toString(), new CustomRunnable() {
                            @Override
                            public void run(Object data) {
                                PENDING_HIT = !PENDING_HIT;
                                CommentModel model = (CommentModel) data;
                                mCommentAdapter.setData(model);
                                mCommentTxt.setText("");
                                comment.setText(mCommentAdapter.getData().size() + " Comments");
                            }
                        }, getActivity());
                    }else
                        ((BaseActivity)getActivity()).showSnack( "Please wait while your request completed");
                }else{
                    ((BaseActivity)getActivity()).showSnack( "Please enter comment text...");
                }

                break;

            case R.id.day:
                //displayPopupWindow(view, view.getContext(), mModel.getUserId().equalsIgnoreCase(BaseActivity.USER.getId()));
                break;
            case R.id.friends_count:
                ((BaseActivity)view.getContext()).addFragment(TagListFragment.newInstance(mModel.getTagFriend()));
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_notification, menu);
        if (((BaseActivity)getActivity()).mPreferences.contains(NOTIFICATION_KEY))
            ((BaseActivity)getActivity()).getToolbar().getMenu().findItem(R.id.notification).setIcon(R.drawable.icon_noti1);
        else
            ((BaseActivity)getActivity()).getToolbar().getMenu().findItem(R.id.notification).setIcon(R.drawable.icon_noti);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void displayPopupWindow(View anchorView, Context context, boolean currentUserPost) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.edit:
                        break;
                    case R.id.delete:
                        break;
                    case R.id.inappropriate:
                        break;
                }
            }
        };
        PopupWindow popup = new PopupWindow(context);
        View layout = ((BaseActivity)context).getLayoutInflater().inflate(R.layout.list_item_pop_window, null);
        TextView edit =  layout.findViewById(R.id.edit);
        TextView delete =  layout.findViewById(R.id.delete);
        TextView inapp =  layout.findViewById(R.id.inappropriate);
        if(currentUserPost){
            inapp.setVisibility(View.GONE);
        }else{
            edit.setVisibility(View.GONE);
            delete.setVisibility(View.GONE);
        }
        edit.setOnClickListener(listener);
        delete.setOnClickListener(listener);
        inapp.setOnClickListener(listener);

        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
    }

    @Override
    public void run() {
        try {
            mModel = mData;

            mCommentAdapter = new CommentAdapter(mModel.getComment() != null ? mModel.getComment() : new ArrayList<CommentModel>(), false, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    comment.setText(mCommentAdapter.getData().size() + " Comments");
                }
            });
            mTasksRV.setNestedScrollingEnabled(false);
            mTasksRV.setLayoutManager(mTaskManager);
            mTasksRV.setAdapter(mCommentAdapter);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(StringUtils.capitalize(mModel.getUserModel().getFirstName()));
            stringBuilder.append(" ");
            stringBuilder.append(StringUtils.capitalize(mModel.getUserModel().getLastName() != null ? mModel.getUserModel().getLastName() : ""));
            //if (mModel.getUserModel() == null)
                name.setText(stringBuilder.toString());
//            else
//                name.setText(mModel.getUserModel().getSocialUsername());

            title.setText(mModel.getPost());
            time.setText(Utils.getRelativeTime(DateUtils.getRelativeTimeSpanString(Utils.getTime(mModel.getCreatedAt()))));

            like.setOnClickListener(this);
            postBtn.setOnClickListener(this);
            mIconMore.setOnClickListener(this);

            if (mModel.getIsLike())
                like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.like_pressed, 0, 0, 0);
            else
                like.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_like, 0, 0, 0);

            if (mModel.getComment() != null)
                comment.setText(mModel.getComment().size() + " Comments");

            if (mModel.getTagFriend() != null) {
                friendCount.setText(mModel.getTagFriend().size() + " Friends");
                friendCount.setOnClickListener(this);
            }

            Picasso.get().load(mModel.getUserModel().getImage() != null ? mModel.getUserModel().getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(profileImage);
            Picasso.get().load(BaseActivity.USER.getImage() != null ? BaseActivity.USER.getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(postProfileImage);
            if (mModel.getAudioUrl() != null) {
                media.setImageDrawable(getActivity().getDrawable(R.drawable.audio));
                media.setOnClickListener(this);

            } else if (mModel.getVideoUrl() != null) {
                    /*media.setImageDrawable(context.getDrawable(R.drawable.audio));
                    media.setOnClickListener(listener);
                    media.setTag(position);
                    *//*Picasso.with(mContext)
                            .load(com.app.utility.Constants.BASE_URL+b.image)
                            .placeholder(R.drawable.profile)
                            .error(R.drawable.profile)
                            .transform(new RoundedTransformation(50, 4))
                            .resizeDimen(R.dimen.list_detail_image_size, R.dimen.list_detail_image_size)
                            .centerCrop()
                            .into(v.im_user);*/
                media.setOnClickListener(this);
                final int radius = 20;
                final Transformation transformation = new RoundedCornersTransformation(radius, 5);
                Picasso.get()
                        .load(mModel.getThumbnail())
                        .placeholder(R.drawable.image_placeholder)
                        .error(R.drawable.ic_check)
                        .transform(transformation)
                            /*.centerCrop()
                            .resize(R.dimen._200sdp,R.dimen._125sdp)*/
                        .into(media);
                mIconVideo.setVisibility(View.VISIBLE);

            } else if (mModel.getPhotoUrl() != null && mModel.getPhotoUrl().size() > 0) {
                final int radius = 20;

                media.setOnClickListener(this);
                final Transformation transformation = new RoundedCornersTransformation(radius, 5);
                Picasso.get()
                        .load(mModel.getPhotoUrl().get(0).get3x())
                        .placeholder(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .transform(transformation)
                            /*.centerCrop()
                            .resize(R.dimen._200sdp,R.dimen._125sdp)*/
                        .into(media);
                //media.setImageDrawable(context.getDrawable(R.drawable.image_newsfeed));
            } else if (mModel.getLatitude() != null && mModel.getLongitude() != null) {
                final int radius = 15;
                media.setOnClickListener(this);
                media.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                final Transformation transformation = new RoundedCornersTransformation(radius, 0);
                Picasso.get().setLoggingEnabled(true);
                Picasso.get()
                        .load(Utils.getMapURL
                                (Double.parseDouble(mModel.getLatitude()),
                                        Double.parseDouble(mModel.getLongitude())))
                        .placeholder(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder)
                        .transform(transformation)
                            /*.centerCrop()
                            .resize(R.dimen._200sdp,R.dimen._125sdp)*/
                        .into(media);
            } else {
                //title.setMaxLines(3);
                media.setVisibility(View.GONE);
                mediaLL.findViewById(R.id.media_item_ll).setVisibility(View.GONE);
            }

            mMainRL.setVisibility(View.VISIBLE);

        }catch (Exception ex){}
    }

    private void setSingleLine(TextView tx){
        tx.setEllipsize(TextUtils.TruncateAt.END);
        tx.setMaxLines(1);
    }
}
