package com.digitonics.friendshipdiary.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.models.DairyModel;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;

@IgnoreApi
public class CreateDiaryFragment extends BaseFragment implements View.OnClickListener {


    public static final String TAG_FEEDBACK = "Feedback";
    private Runnable mRunnable;
    private Button mCreateBtn;
    private EditText mTitle, mDescription, mDate, mTime;
    private Bundle mArgs;
    private Date mDairyDate;
    private DairyModel mDairyModel;
    private String mTime12Hr = "";

    public static CreateDiaryFragment newInstance(boolean update, DairyModel dairyModel) {
        CreateDiaryFragment f = new CreateDiaryFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("dairy", dairyModel);
        args.putBoolean("update", update);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_diary, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        return view;
    }

    public void init(View view){

        mArgs = getArguments();

        mTitle = view.findViewById(R.id.dairy_title);
        mDescription = view.findViewById(R.id.dairy_desc);
        mDate = view.findViewById(R.id.dairy_date);
        mTime = view.findViewById(R.id.dairy_time);
        mCreateBtn = view.findViewById(R.id.create_btn);

        setHasOptionsMenu(true);
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
        ((BaseActivity)getActivity()).setTitle("Create Diary");

        if (mArgs != null && mArgs.containsKey("update")) {
            mDairyModel = mArgs.getParcelable("dairy");
            mTime12Hr = mDairyModel.getTime();
            mTitle.setText(mDairyModel.getTitle());
            mDescription.setText(mDairyModel.getDescription());
            mDate.setText(Utils.getDateGoal(mDairyModel.getStartDate() + " " + mDairyModel.getTime() , "yyyy-MM-dd hh:mm:ss"));
            mTime.setText(Utils.get12HourTime(mTime12Hr));
            mCreateBtn.setText("Update");
            ((BaseActivity)getActivity()).setTitle("Update Goal");
        }

        mCreateBtn.setOnClickListener(this);
        mDate.setOnClickListener(this);
        mTime.setOnClickListener(this);



    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                Utils.hide(getActivity());
                ((BaseActivity)getActivity()).onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        if (mArgs != null && mArgs.containsKey("update"))
            return  mApiBuilder.updateDiary(BaseActivity.USER.getAccessToken(), mTitle.getText().toString(), mDescription.getText().toString(), mDate.getText().toString(), mTime12Hr,mDairyModel.getId());
        else
            return  mApiBuilder.createDiary(BaseActivity.USER.getAccessToken(), mTitle.getText().toString(), mDescription.getText().toString(), mDate.getText().toString(), mTime12Hr);
    }

    @Override
    public void run(){
        ((BaseActivity)getActivity()).onBackPressed();
        if (mArgs != null && mArgs.containsKey("update")) {
            ((BaseActivity)getActivity()).showSnack("Diary created successfully");
        }else
            ((BaseActivity)getActivity()).showSnack("Diary updated successfully");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.dairy_date:
                Utils.showDatePicker(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.YEAR, year);
                        c.set(Calendar.MONTH,month);
                        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        Date setDate = Utils.getDate(Utils.getDate(c.getTime()));
                        Date currentDate = Utils.getDate(Utils.getDate(Calendar.getInstance().getTime()));
                        if (setDate.compareTo(currentDate) < 0){
                            showSnack("Date must not be less than current date");
                        }else{
                            mDairyDate = setDate;
                            mDate.setText(Utils.getDate(c.getTime()));
                        }

                    }
                });
                break;
            case R.id.dairy_time:
                if (!mDate.getText().toString().equalsIgnoreCase("")){
                    Utils.showTimePicker(v.getContext(), new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            mTime12Hr = hourOfDay +  ":"  + minute;
                            mTime.setText(Utils.get12HourTime(mTime12Hr));
                        }
                    }, false);
                }else{
                    Utils.showToast(v.getContext(),"Please enter date first");
                }
                break;
            case R.id.create_btn:
                if (validateFields()){
                    Utils.hide(getActivity());
                    callApi();
                }
                break;
        }
    }

    private boolean validateFields() {


        if (mTitle.getText().toString().isEmpty() == true) {
            showSnack( "Please enter title of dairy");
            return false;
        }
        if (mDescription.getText().toString().isEmpty() == true) {
            showSnack( "Please enter description of goal");
            return false;
        }
        if (mDate.getText().toString().isEmpty() == true) {
            showSnack( "Date could not be empty!");
            return false;
        }
        if (mTime.getText().toString().isEmpty() == true) {
            showSnack( "Time could not be empty!");
            return false;
        }

        return true;
    }
}
