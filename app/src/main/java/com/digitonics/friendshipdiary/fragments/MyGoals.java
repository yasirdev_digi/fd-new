package com.digitonics.friendshipdiary.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.adapters.GoalAdapter;
import com.digitonics.friendshipdiary.adapters.NewsfeedAdapter;
import com.digitonics.friendshipdiary.annotations.HideLoader;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.fcm.RegistrationService;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.utilities.TopDialog;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.widgets.RecyclerViewItemDecorator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import static com.digitonics.friendshipdiary.activities.BaseActivity.DEVICE_TOKEN;
import static com.digitonics.friendshipdiary.activities.BaseActivity.NOTIFICATION_KEY;


@IgnoreApi
@HideLoader
public class MyGoals extends BaseFragment<ArrayList<GoalModel>> implements View.OnClickListener, GoalAdapter.OnGoalClickListener {


    public static final String TAG_GOAL = "MyGoal";
    private Runnable mRunnable;
    private RecyclerView mGoalRV, mAchorRV;
    private LinearLayoutManager mGoalManager, mAchorManager;
    private TopDialog mDialog;
    private Spinner typeSpinner;
    private Spinner statusSpinner;
    private View customView;
    private View mNoData;
    private GoalAdapter mGoalAdapter;

    public MyGoals() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_goals, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        callApi();
        return view;
    }

    public void init(View view){

        mGoalRV = (RecyclerView) view.findViewById(R.id.goals_rv);
        mNoData = view.findViewById(R.id.no_data_img);

        if (((BaseActivity)getActivity()).mBottomLL != null)
            ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.VISIBLE);
        view.findViewById(R.id.create_goal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).addFragment(new CreateGoalFragment());
                ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.GONE);
            }
        });


        setHasOptionsMenu(true);
        ((BaseActivity)getActivity()).setTitle("My Goals");
        ((BaseActivity)getActivity()).setToolBar(R.drawable.icon_menu);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ((BaseActivity)getActivity()).mDrawerLayout.closeDrawers();
            }
        });

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        customView = inflater.inflate(R.layout.dialog_filter, null);
        typeSpinner = (Spinner) customView.findViewById(R.id.goal_type);
        statusSpinner = (Spinner) customView.findViewById(R.id.goal_status);

        List<String> types = new ArrayList<String>();
        types.add("Public");
        types.add("Private");

        List<String> goal_status = new ArrayList<String>();
        goal_status.add("Active");
        goal_status.add("Inactive");


        ArrayAdapter<String> ada = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, types);

        // Drop down layout style - list view with radio button
        ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<String> adas = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, goal_status);

        // Drop down layout style - list view with radio button
        adas.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        typeSpinner.setAdapter(ada);
        statusSpinner.setAdapter(adas);
        customView.findViewById(R.id.apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                callApi();
                //Toast.makeText(getActivity(), "Filter applied successfully", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        ((HomeActivity)getActivity()).addFragment(new GoalDetailFragment());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                ((BaseActivity)getActivity()).mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.notification:
                ((HomeActivity)getActivity()).addFragment(new NotificationlFragment());
                ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.GONE);
                return true;
            case R.id.filter:

                mDialog = new TopDialog.Builder(getActivity())
                        .setCustomView(customView)
                        .show();
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.allGoal(BaseActivity.USER.getAccessToken(), typeSpinner.getSelectedItem().toString().toLowerCase(), statusSpinner.getSelectedItem().toString().toLowerCase());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.my_goals_menu, menu);
        if (((BaseActivity)getActivity()).mPreferences.contains(NOTIFICATION_KEY))
            ((BaseActivity)getActivity()).getToolbar().getMenu().findItem(R.id.notification).setIcon(R.drawable.icon_noti1);
        else
            ((BaseActivity)getActivity()).getToolbar().getMenu().findItem(R.id.notification).setIcon(R.drawable.icon_noti);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void run(){

        if (mData != null && mData.size() > 0 ) {
            mNoData.setVisibility(View.GONE);
            mGoalManager = new LinearLayoutManager(getActivity());

            mGoalRV.addItemDecoration(new RecyclerViewItemDecorator(0));
            mGoalAdapter = new GoalAdapter(this, mData);

            mGoalRV.setLayoutManager(mGoalManager);
            mGoalRV.setAdapter(mGoalAdapter);
            Utils.animRecyclerView(mGoalRV, Utils.LIST_ANIM_FROM_TOP);
        }else{
            if (mGoalAdapter != null) {
                mGoalAdapter.emptyData();
                ((BaseActivity)getActivity()).showSnack( "No Data Found!");
            }
            mNoData.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onGoalClick(int type, GoalModel goalModel) {
        goalModel.setUserType(-1);
        if (type == 1)
            ((HomeActivity)getActivity()).addFragment(GoalDetailFragment.newInstance(goalModel));
        else
            ((HomeActivity)getActivity()).addFragment(FriendsFragment.newInstance(goalModel));
        ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.GONE);
    }
}
