package com.digitonics.friendshipdiary.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.activities.LoginActivity;
import com.digitonics.friendshipdiary.activities.SettingActivity;
import com.digitonics.friendshipdiary.adapters.CircularImageAdapter;
import com.digitonics.friendshipdiary.adapters.GoalAdapter;
import com.digitonics.friendshipdiary.adapters.ProofAdapter;
import com.digitonics.friendshipdiary.adapters.TaskListAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.Friend;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.ProofModel;
import com.digitonics.friendshipdiary.models.TaskModel;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.digitonics.friendshipdiary.utilities.OverlapDecoration;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.facebook.login.LoginManager;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;

@IgnoreApi
public class GoalDetailFragment extends BaseFragment<GoalModel> implements View.OnClickListener, GoalAdapter.OnGoalClickListener, ProofAdapter.OnAddProofClickListener{


    public static final String TAG_MODULE = "ModuleFragment";
    private Runnable mRunnable;
    private LinearLayoutManager mTaskManager, mImagesManager;
    private RecyclerView mTasksRV, mImagesRV;
    int index = 0;
    GoalModel mGoal;
    private TextView goalName, goalDesc, goalFriends, goalStartDate, goalEndDate;
    private View mMainCL;
    private TaskListAdapter mTaskAdapter;
    private int mSelectedPos;
    private ImageView day;
    private View mNoData;
    private TextView status;
    private View mAddTask;
    private Dialog mDialog;
    private View ll,accept,reject;


    /*public static GoalDetailFragment newInstance(int index) {
        GoalDetailFragment f = new GoalDetailFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        return f;
    }*/

    public static GoalDetailFragment newInstance(GoalModel goal) {
        GoalDetailFragment f = new GoalDetailFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("goal", goal);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_goals_detail, container, false);
        Bundle args = getArguments();
       /* if (args != null && args.containsKey("index"))
            index = args.getInt("index", 0);
        else */
       if(args != null && args.containsKey("goal")){
           mGoal = args.getParcelable("goal");
        }

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        callApi();
        return view;
    }
    public void init(View view){
        mTasksRV = (RecyclerView) view.findViewById(R.id.task_lv);
        mNoData = view.findViewById(R.id.no_data_img);
        mImagesRV = (RecyclerView) view.findViewById(R.id.circular_iv);
        mTaskManager = new LinearLayoutManager(getActivity());
        mImagesManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        mImagesRV.addItemDecoration(new OverlapDecoration());
        mImagesRV.setLayoutManager(mImagesManager);
        goalName =  view.findViewById(R.id.goal_name);
        goalDesc =  view.findViewById(R.id.goal_desc);
        goalStartDate =  view.findViewById(R.id.date_start);
        goalEndDate = view.findViewById(R.id.date_end);
        goalFriends =  view.findViewById(R.id.friends_count);
        status =  view.findViewById(R.id.status);
        day =  view.findViewById(R.id.day);
        mMainCL = view.findViewById(R.id.mainCL);
        mAddTask = view.findViewById(R.id.imageView4);
        ll = view.findViewById(R.id.acceptll);
        accept = view.findViewById(R.id.accept);
        reject = view.findViewById(R.id.reject);
        ((BaseActivity)getActivity()).setDefaultBackground();


        setHasOptionsMenu(true);

        mAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mData != null) {
                    //if (mData.getTasks() != null && mData.getTasks().size() > 0) {
                        ((BaseActivity) getActivity()).addFragment(CreateTaskFragment.newInstance(mGoal.getId()));
                    //}else{

                    //}
                }
            }
        });

        day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayPopupWindow(view, view.getContext(),BaseActivity.USER.getId().equalsIgnoreCase(mGoal.getUserId()));
            }
        });
        //if (index == 1)
            //view.findViewById(R.id.imageView4).setVisibility(View.VISIBLE);
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
        ((BaseActivity)getActivity()).setTitle("");

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.add_task:
                ((BaseActivity)getActivity()).addFragment(CreateTaskFragment.newInstance(mGoal.getId()));
                return true;
            case R.id.edit:
                if (mData != null)
                    ((BaseActivity)getActivity()).addFragment(CreateGoalFragment.newInstance(true, mData));
                return true;
//            case R.id.wish:
//                if (mData != null){
//                    ((HomeActivity)getActivity()).addFragment(WishListFragment.newInstance(mData));
//                    //((BaseActivity)getActivity()).mBottomLL.setVisibility(View.GONE);
//                }
//                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {

        return mApiBuilder.getGoalDetail(BaseActivity.USER.getAccessToken(), mGoal.getId());
    }

    @Override
    public void onClick(View view) {
        int pos = (int)view.getTag();
       /* switch (pos){
            case 0:*/
                /*((HomeActivity)getActivity()).addFragment(ImageProofDetail.newInstance());*/
//                break;
            /*case 1:
                ((HomeActivity)getActivity()).addFragment(new WishListFragment());*/
               /* break;*/
//        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mGoal.getUserId()!= null && mGoal.getUserId().equalsIgnoreCase(BaseActivity.USER.getId())) {
            inflater.inflate(R.menu.menu_edit, menu);
        }
       /* else
        inflater.inflate(R.menu.menu_add_task, menu);*/
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void run(){
        if(mGoal.getUserType() == Constants.SPONSER){

            if (mData.getIs_agreed().equalsIgnoreCase("0")){
                ll.setVisibility(View.VISIBLE);
                day.setVisibility(View.GONE);
                accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //int pos = (int) view.getTag();
                        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                        builder.setTitle("Terms & Conditions")
                                .setMessage("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. \\n\\n Are you sure you want to be a sponsor for this goal?")
                                .setCancelable(false)
                                .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        onGoalClick(3, mData);
                                        //((HomeActivity) getActivity()).addFragment(CreateTaskFragment.newInstance(mGoal.getId()));

                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                });

                reject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //int pos = (int) view.getTag();
                        onGoalClick(4, mData);
                    }
                });
            }else if (mData.getIs_agreed().equalsIgnoreCase("2")){
                day.setVisibility(View.VISIBLE);
                mAddTask.setVisibility(View.VISIBLE);
                ll.setVisibility(View.GONE);
            }
        }else
            mAddTask.setVisibility(View.GONE);
        goalName.setText(mData.getName());
        goalDesc.setText(mData.getDescription());
        goalStartDate.setText(Utils.getDate(mData.getStartDate(), "yyyy-MM-dd hh:mm:ss"));
        goalEndDate.setText(Utils.getDate(mData.getEndDate(), "yyyy-MM-dd hh:mm:ss"));
        goalFriends.setText(mData.getFriends() != null && mData.getFriends().size() > 0 ? mData.getFriends().size() + " Friends": "0 Friends");
        status.setText(Utils.getStatus(mData.getStatus()));


        /*view.findViewById(R.id.circular_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).addFragment(new FriendsFragment());
            }
        });*/
        CircularImageAdapter circularImageAdapter = new CircularImageAdapter(this, (mData.getFriends() != null ? mData.getFriends(): new ArrayList<Friend>()));
        if (mData.getTasks() != null && mData.getTasks().size() > 0) {
            mNoData.setVisibility(View.GONE);
            mTaskAdapter = new TaskListAdapter(this, this,
                    (mData.getTasks() != null ? mData.getTasks() : new ArrayList<TaskModel>()),
                    mGoal.getUserType() == -1 ? true : false);

            mTasksRV.setLayoutManager(mTaskManager);
            mTasksRV.setAdapter(mTaskAdapter);
        }else{
            if (mTaskAdapter != null)
                mTaskAdapter.emptyData();
            mNoData.setVisibility(View.VISIBLE);
        }

        mImagesRV.setAdapter(circularImageAdapter);
        if ((mData.getTasks() != null && mData.getTasks().size() == 10) || mData.getStatus().equalsIgnoreCase("completed")) {
            mAddTask.setVisibility(View.GONE);
        }

       /* mImagesRV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //((HomeActivity)getActivity()).addFragment(FriendsFragment.newInstance(goalModel));
            }
        });*/

        ((BaseActivity)getActivity()).setTitle(mData.getName());
        mMainCL.setVisibility(View.VISIBLE);
        if (mData.getStatus().equalsIgnoreCase("completed") && mData.getFeedback() == null){
            openFeedbackDialog(getActivity());
        }

    }

    @Override
    public void onGoalClick(int type, GoalModel goalModel) {
        if (type == 3){
            ((BaseActivity)getActivity()).mApiBuilder.postSponsor(BaseActivity.USER.getAccessToken(), "2", goalModel.getId(), new CustomRunnable() {
                @Override
                public void run(Object data) {
                    ll.setVisibility(View.GONE);
                    day.setVisibility(View.VISIBLE);
                    mAddTask.setVisibility(View.VISIBLE);
                }
            },((BaseActivity)getActivity()));

        }
        else if (type == 4){
            ((BaseActivity)getActivity()).mApiBuilder.postSponsor(BaseActivity.USER.getAccessToken(), "1", goalModel.getId(), new CustomRunnable() {
                @Override
                public void run(Object data) {
                    ll.setVisibility(View.GONE);
                }
            },((BaseActivity)getActivity()).mContext);
        }
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(OtherMainFragment.class.toString());
        if (fragment != null && fragment instanceof OtherMainFragment){
            OtherMainFragment parentFrag = ((OtherMainFragment)fragment);
            parentFrag.callApi();
        }


    }

    @Override
    public void onAddProofClick(int pos) {
        mSelectedPos = pos;
        ((BaseActivity)getActivity()).showMediaPicker();
    }

    public void onMediaLoad(int reqCode, Intent data) {
        switch (reqCode) {
            case Constants.IMAGES_GALLERY:
            case Constants.IMAGES_CAMERA:
                Log.e("TESTY", "mMediaPath = " + ((BaseActivity)mContext).mMediaPath);
                Bitmap bmp = null;
                try {
                   /* bmp = BitmapFactory.decodeFile(((BaseActivity)mContext).mMediaPath);
                    bmp = Utils.scaleImageKeepAspectRatio(bmp);
                    bmp = Utils.modifyOrientation(bmp,((BaseActivity)mContext).mMediaPath);
                    mProfPicIV.setImageBitmap(bmp);*/
                   mApiBuilder.addProve(BaseActivity.USER.getAccessToken(), ((BaseActivity) mContext).mMediaPath, mData.getTasks().get(mSelectedPos).getId(), new CustomRunnable() {
                       @Override
                       public void run(Object data) {
                           Log.e("TESTY", "mMediaPath = " + data);
                           mTaskAdapter.setData(mSelectedPos, (ProofModel) data);
                       }
                   }, getActivity());
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                Picasso.with(mContext)
//                        .load(Uri.fromFile())
//                        .into(mAddDocIV);
                break;

            case Constants.VIDEO_CAMERA:
            case Constants.VIDEO_GALLERY:
                Log.e("TESTY", "mMediaPath = " + ((BaseActivity)mContext).mMediaPath);
                break;
        }
    }

    private void displayPopupWindow(View anchorView, final Context context, boolean currentUserPost) {

        final PopupWindow popup = new PopupWindow(context);
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
                switch (v.getId()){
                    case R.id.share:
                        popup.dismiss();
                        /*View view = ((Activity)context).getLayoutInflater().inflate(R.layout.skill_dialog, null);
                        final EditText skill = (EditText) view.findViewById(R.id.skillName);
                        skill.setHint("Please enter points");
                        skill.setInputType(InputType.TYPE_CLASS_NUMBER);
                        new AlertDialog.Builder(v.getContext(),R.style.AppTheme_DialogTheme)
                                .setTitle("Add Skills")
                                .setView(view)
                                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String string = skill.getText().toString();
                                        if (string.equalsIgnoreCase("")
                                                && Integer.parseInt(string) > 0 && Integer.parseInt(string) <= 10){
                                            notifyDataSetChanged();
                                            Utils.hideSoftKeyboard(skill);
                                        }
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Utils.hideSoftKeyboard(skill);
                                    }
                                }).create().show();*/

                        DialogInterface.OnClickListener posListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                ((BaseActivity)context).mApiBuilder.sharePoints(BaseActivity.USER.getAccessToken(),mGoal.getId(),new CustomRunnable() {
                                    @Override
                                    public void run(Object data) {

                                    }
                                }, context);
                            }
                        };

                        DialogInterface.OnClickListener negListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        };

                        Utils.showPromptDialog(v.getContext(), "Are you sure to share 10 point to that goal creater?", null, "Yes", "No",posListener, negListener, true);
                        break;
                    case R.id.delete:
                        popup.dismiss();
                        posListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                ((BaseActivity)context).mApiBuilder.deleteGoal(BaseActivity.USER.getAccessToken(),mGoal.getId(),new CustomRunnable() {
                                    @Override
                                    public void run(Object data) {
                                        getActivity().onBackPressed();
                                    }
                                }, context);
                            }
                        };

                        negListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        };

                        Utils.showPromptDialog(v.getContext(), "Are you sure to delete the item?", null, "Yes", "No",posListener, negListener, true);


                        break;
                    case R.id.status:
                        popup.dismiss();
                        showChangeStatusDialog(context);
                        break;
                }
            }
        };

        View layout = ((BaseActivity)context).getLayoutInflater().inflate(R.layout.list_item_goal_pop_window, null);
        TextView status =  layout.findViewById(R.id.status);
        TextView delete =  layout.findViewById(R.id.delete);
        TextView share =  layout.findViewById(R.id.share);
        if(currentUserPost){
            share.setVisibility(View.GONE);
        }else{
            status.setVisibility(View.GONE);
            delete.setVisibility(View.GONE);
        }
        status.setOnClickListener(listener);
        delete.setOnClickListener(listener);
        share.setOnClickListener(listener);

        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
    }

    private void showChangeStatusDialog(final Context context) {

        View view = View.inflate(context, R.layout.dialog_status, null);
        final Dialog dialog = Utils.showCustomDialog(context, view);
        final RadioGroup voteGrp = view.findViewById(R.id.vote_radio_grp);
        final TextView submitBT = (TextView) view.findViewById(R.id.submit);
        //submitBT.setTag(pos);
        ((RadioButton)voteGrp.getChildAt(Utils.getStatusPosition(mGoal.getStatus()))).setChecked(true);
        ImageView closeIV = (ImageView) view.findViewById(R.id.diag_dept_closeIV);

        closeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String taskLevel = "";
                Utils.hideSoftKeyboard(submitBT);
                dialog.dismiss();

                switch (voteGrp.getCheckedRadioButtonId()){
                    case R.id.active:
                        taskLevel = "active";
                        break;
                    case R.id.on_break:
                        taskLevel = "on_break";
                        break;
                    case R.id.no_pressure:
                        taskLevel = "no_pressure";
                        break;
                }

                ((BaseActivity)context).mApiBuilder.changeStatus(BaseActivity.USER.getAccessToken(),mGoal.getId(), taskLevel + "",new CustomRunnable() {
                    @Override
                    public void run(Object data) {
                        GoalModel goalModel = (GoalModel)data;
                        if (goalModel != null) {
                            mGoal.setStatus(goalModel.getStatus());
                            status.setText(Utils.getStatus(goalModel.getStatus()));
                        }
                    }
                }, context);
//                ((BaseActivity) mContext).mBaseFragment = new PastTripDetailsFragment();
//                ((PastTripDetailsFragment) ((BaseActivity) mContext).mBaseFragment).setCommunicator(1);
//                ((BaseActivity)mContext).changeFragment(((BaseActivity)mContext).mBaseFragment,true);
            }
        });

        dialog.show();


    }

    private void openFeedbackDialog(final Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_feedback, null);

        final EditText feedbackET = (EditText) view.findViewById(R.id.diag_feedback_commentsET);
        final RatingBar rateRB = (RatingBar) view.findViewById(R.id.diag_feedback_rateRB);
        rateRB.setVisibility(View.GONE);

        Button submitBT = (Button) view.findViewById(R.id.diag_feedback_submitBT);

        ImageView closeIV = (ImageView) view.findViewById(R.id.diag_reject_closeIV);

        closeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hide(getActivity());
//                showSnack( mContext.getResources().getString(R.string.request_has_been_accepted));

                /*((BaseActivity) mContext).mApiBuilder.feedbackRDA((getIntent().hasExtra(Utils.NOTIFICATION_DATA_ORDERID) == true ? getIntent().getExtras().getString(Utils.NOTIFICATION_DATA_ORDERID) : ""), (getIntent().hasExtra(Utils.NOTIFICATION_DATA_RDAID) == true ? getIntent().getExtras().getString(Utils.NOTIFICATION_DATA_RDAID) : ""), feedbackET.getText().toString().trim().toString(), (int) rateRB.getRating(), new CustomRunnable() {
                    @Override
                    public void run(Object data) {
                        mDialog.dismiss();
                        finish();
                    }
                });*/
                if (!feedbackET.getText().toString().equalsIgnoreCase("")){
                    mDialog.dismiss();
                    ((BaseActivity)view.getContext()).mApiBuilder.sendFeedback(BaseActivity.USER.getAccessToken(), feedbackET.getText().toString(), "" + mData.getId(),BaseActivity.USER.getId(),new CustomRunnable() {
                        @Override
                        public void run(Object data) {

                        }
                    }, view.getContext());
                }else{
                    showSnack("Feedback text cannot be empty!");
                }
            }
        });

        mDialog = Utils.showCustomDialog(context, view);
    }
}
