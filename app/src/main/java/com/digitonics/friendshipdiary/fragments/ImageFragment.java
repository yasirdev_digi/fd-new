package com.digitonics.friendshipdiary.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.LoginActivity;
import com.digitonics.friendshipdiary.utilities.Utils;


public class ImageFragment extends Fragment implements View.OnClickListener{


    public static final String TAG_MY_COURSE = "MyCourse";

    public static ImageFragment  newInstance(int id){
        ImageFragment otherGoalsFragment =  new ImageFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("id",id);
        otherGoalsFragment.setArguments(bundle);
        return otherGoalsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.list_item_how, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        return view;
    }

    public void init(View view){

        //getActivity().findViewById(R.id.toolbar).setVisibility(View.GONE);
        view.findViewById(R.id.skip).setTag(getArguments().getInt("id"));
        view.findViewById(R.id.skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((int)v.getTag()) != R.drawable.how_image_6) {
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                }
            }
        });
        ((ImageView)view.findViewById(R.id.how_works)).setImageResource(getArguments().getInt("id"));
        if(getArguments().getInt("id") == R.drawable.how_image_6) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                }
            }, 3000);
        }
        //view.findViewById(R.id.menu).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.menu:
                Utils.hide(getActivity());
                ((BaseActivity)getActivity()).onBackPressed();
                break;
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                return true;
        }
        return false;
    }




}
