package com.digitonics.friendshipdiary.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.Friend;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;


@IgnoreApi
public class CreateGoalFragment extends BaseFragment implements View.OnClickListener, CustomRunnable{


    public static final String TAG_FEEDBACK = "Feedback";
    private Runnable mRunnable;
    private Button mFriendsBtn;
    private LinearLayoutManager mFeedbackManager;
    private Button mCreateBtn;
    private EditText mStartDateTV, mEndDateTV;
    private Date mStartDate;
    private EditText mDescription, mGoalName;
    private Spinner mGoalType;
    private String mFriends = "";
    private TextView mFriendRole;
    private String name;
    private GoalModel mGoalModel;
    private Bundle mArgs;
    private View mAddFriendsLL;

    public static CreateGoalFragment newInstance(boolean update, GoalModel goalModel) {
        CreateGoalFragment f = new CreateGoalFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("goal", goalModel);
        args.putBoolean("update", update);
        f.setArguments(args);
        return f;
    }

    public CreateGoalFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_goal, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        return view;
    }

    public void init(View view){

        mFriendsBtn = view.findViewById(R.id.add_friends);
        mGoalName = view.findViewById(R.id.goal_name);
        mDescription = view.findViewById(R.id.goal_desc);
        mStartDateTV = view.findViewById(R.id.start_date);
        mEndDateTV = view.findViewById(R.id.end_date);
        mCreateBtn = view.findViewById(R.id.create_btn);
        mGoalType = view.findViewById(R.id.goal_type);
        mAddFriendsLL = view.findViewById(R.id.add_friends_ll);
        mFriendRole = view.findViewById(R.id.add_friends_tv);

        mGoalType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1)
                    mAddFriendsLL.setVisibility(View.GONE);
                else
                    mAddFriendsLL.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mArgs = getArguments();
        if (mArgs != null && mArgs.containsKey("update")) {
            view.findViewById(R.id.textInputLayout9).setVisibility(View.GONE);
            mGoalModel = mArgs.getParcelable("goal");

            mGoalName.setText(mGoalModel.getName());
            mDescription.setText(mGoalModel.getDescription());
            mStartDateTV.setText(Utils.getDateGoal(mGoalModel.getStartDate(), "yyyy-MM-dd hh:mm:ss"));
            mEndDateTV.setText(Utils.getDateGoal(mGoalModel.getEndDate(), "yyyy-MM-dd hh:mm:ss"));
            String friendNames = "";
            if (mGoalModel.getFriends() != null) {
                for (int i = 0; i < mGoalModel.getFriends().size(); i++) {
                    friendNames += mGoalModel.getFriends().get(i).getFirstName() + ",";
                }
                mFriendRole.setText(friendNames);
            }
            if (mGoalModel.getGoalType().equalsIgnoreCase("private")){
                mAddFriendsLL.setVisibility(View.GONE);
            }
            mCreateBtn.setText("Update");
            ((BaseActivity)getActivity()).setTitle("Edit Goal");
        }else
            ((BaseActivity)getActivity()).setTitle("Create New Goal");

        List<String> types = new ArrayList<String>();
        types.add("Public");
        types.add("Private");
        if (mGoalModel != null && name != null && !name.equalsIgnoreCase(""))
            mFriendRole.setText(mFriendRole.getText().toString() + "" + name);
        else if (name != null && !name.equalsIgnoreCase(""))
            mFriendRole.setText(name.substring(0,name.lastIndexOf(",")));

        ArrayAdapter<String> ada = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, types);
        ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mCreateBtn.setOnClickListener(this);
        mFriendsBtn.setOnClickListener(this);
        mStartDateTV.setOnClickListener(this);
        mEndDateTV.setOnClickListener(this);

        mGoalType.setAdapter(ada);

        setHasOptionsMenu(true);


        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                Utils.hide(getActivity());
                ((BaseActivity)getActivity()).onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        if (mArgs != null && mArgs.containsKey("update")) {
            /*if (mGoalModel.getGoalType().equalsIgnoreCase("private"))*/
                //return mApiBuilder.updateGoal(BaseActivity.USER.getAccessToken(), mGoalName.getText().toString(), mDescription.getText().toString(), mStartDateTV.getText().toString(), mEndDateTV.getText().toString(), mGoalModel.getId());
            /*else*/
                return mApiBuilder.updateGoal(BaseActivity.USER.getAccessToken(), mGoalName.getText().toString(), mDescription.getText().toString(), mStartDateTV.getText().toString(), mEndDateTV.getText().toString(), mGoalModel.getId(), mFriends.equalsIgnoreCase("")? null:mFriends);
        }
        else
            return  mApiBuilder.createGoal(BaseActivity.USER.getAccessToken(), mGoalName.getText().toString(), mDescription.getText().toString(), mStartDateTV.getText().toString(), mEndDateTV.getText().toString(),
                    mGoalType.getSelectedItem().toString().toLowerCase(),mFriends.equalsIgnoreCase("") ? null : mFriends);
    }

    @Override
    public void run(){
        ((BaseActivity)getActivity()).onBackPressed();
        if (mArgs != null && mArgs.containsKey("update")) {
            ((BaseActivity)getActivity()).showSnack("Goal updated successfully");
        }else
            ((BaseActivity)getActivity()).showSnack("Goal created successfully");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.start_date:
                if (mGoalModel == null) {
                    Utils.showDatePicker(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            Calendar c = Calendar.getInstance();
                            c.set(Calendar.YEAR, year);
                            c.set(Calendar.MONTH, month);
                            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            Date setDate = Utils.getDate(Utils.getDate(c.getTime()));
                            Date currentDate = Utils.getDate(Utils.getDate(Calendar.getInstance().getTime()));
                            if (setDate.compareTo(currentDate) < 0) {
                                showSnack( "You can not select previous date");
                            } else {
                                mStartDate = setDate;
                                mStartDateTV.setText(Utils.getDate(c.getTime()));
                            }

                        }
                    });
                }else
                    ((BaseActivity)getActivity()).showSnack("Date can not be changed!");
                break;
            case R.id.end_date:
                if (mGoalModel == null) {
                    if (!mStartDateTV.getText().toString().equalsIgnoreCase("")) {
                        Utils.showDatePicker(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                Calendar c = Calendar.getInstance();
                                c.set(Calendar.YEAR, year);
                                c.set(Calendar.MONTH, month);
                                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                Date setDate = Utils.getDate(Utils.getDate(c.getTime()));
                                if (setDate.compareTo(mStartDate) >= 0)
                                    mEndDateTV.setText(Utils.getDate(c.getTime()));
                                else
                                    ((BaseActivity)getActivity()).showSnack("End date must be greater than start date");
                            }
                        });
                    } else {
                        ((BaseActivity)getActivity()).showSnack("Please enter start date first");
                    }
                }else
                    ((BaseActivity)getActivity()).showSnack("Date can not be changed!");
                break;
            case R.id.add_friends:
//                if (mArgs != null && mArgs.containsKey("update")) {
//                    ((BaseActivity)getActivity()).showSnack("Friends can not be changed");
//                }else {
                    Utils.hide(getActivity());
                    ((HomeActivity) getActivity()).addFragment(AddFriendFragment.newInstance(this, mGoalModel));
//                }
                break;
            case R.id.create_btn:
                if (validateFields()){
                    Utils.hide(getActivity());
                    callApi();
                }
                break;
        }
    }

    private boolean validateFields() {


        if (mGoalName.getText().toString().isEmpty() == true) {
            showSnack( "Please enter name of goal");
            return false;
        }
        if (mDescription.getText().toString().isEmpty() == true) {
            showSnack( "Please enter description of goal");
            return false;
        }
        if (mStartDateTV.getText().toString().isEmpty() == true) {
            showSnack( "Start date could not be empty!");
            return false;
        }
        if (mEndDateTV.getText().toString().isEmpty() == true) {
            showSnack( "End date could not be empty!");
            return false;
        }
        if (getArguments() == null) {
            if (mFriends.isEmpty() == true && mGoalType.getSelectedItemPosition() != 1) {
                showSnack( "Add Friends could not be empty!");
                return false;
            }
        }

        return true;
    }

    @Override
    public void run(Object data) {
        ArrayList<UserModel> list = (ArrayList<UserModel>) data;
        Gson gson = new Gson();
        name = "";
        ArrayList<UserModel> arrayList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++){
            if (list.get(i).getRole() != null){
                arrayList.add(list.get(i));
                name += list.get(i).getFirstName() + ",";
            }
        }
        if (arrayList.size() > 0) {
            mFriends = gson.toJson(arrayList);
            ((BaseActivity)getActivity()).showSnack("Member added successfully");
        }else
            ((BaseActivity)getActivity()).showSnack("No member selected");

    }
}
