package com.digitonics.friendshipdiary.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.models.TaskModel;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;


@IgnoreApi
public class CreateTaskFragment extends BaseFragment implements View.OnClickListener{


    public static final String TAG_FEEDBACK = "Feedback";
    private Runnable mRunnable;
    private Button mFriendsBtn;
    private LinearLayoutManager mFeedbackManager;
    private View mCreateBtn;
    private EditText mTaskName, mDescription, mStartDateTV, mEndDateTV, mPoints, mRewards;
    private Spinner mLevelSpinner;
    private Date mStartDate;
    private String mGoalId;
    private TaskModel mTaskModel;

    public static CreateTaskFragment newInstance(String goalId) {
        CreateTaskFragment f = new CreateTaskFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putString("mGoalId", goalId);
        f.setArguments(args);
        return f;
    }

    public static CreateTaskFragment newInstance(TaskModel taskModel) {
        CreateTaskFragment f = new CreateTaskFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("task", taskModel);
        f.setArguments(args);
        return f;
    }

    public CreateTaskFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_task, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        return view;
    }

    public void init(View view){

        mTaskName = view.findViewById(R.id.goal_name);
        mDescription = view.findViewById(R.id.goal_desc);
        mStartDateTV = view.findViewById(R.id.date_start);
        mEndDateTV = view.findViewById(R.id.date_end);
        mPoints = view.findViewById(R.id.points);
        mRewards = view.findViewById(R.id.reward);
        mCreateBtn = view.findViewById(R.id.create_btn);


        mCreateBtn.setOnClickListener(this);
        mStartDateTV.setOnClickListener(this);
        mEndDateTV.setOnClickListener(this);

        mLevelSpinner = (Spinner) view.findViewById(R.id.level_spinner);

        List<String> types = new ArrayList<String>();
        types.add("Easy");
        types.add("Medium");
        types.add("Hard");

        ArrayAdapter<String> ada = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, types);
        ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        mLevelSpinner.setAdapter(ada);
        setHasOptionsMenu(true);
        Bundle args = getArguments();
        if(args != null && args.containsKey("mGoalId")){
            mGoalId = args.getString("mGoalId", "");
        }
        if(args != null && args.containsKey("task")){
            mTaskModel = args.getParcelable("task");
            mTaskName.setText(mTaskModel.getName());
            mDescription.setText(mTaskModel.getDescription());
            mStartDateTV.setText(Utils.getDateGoal(mTaskModel.getStartDate(), "yyyy-MM-dd hh:mm:ss"));
            mEndDateTV.setText(Utils.getDateGoal(mTaskModel.getEndDate(), "yyyy-MM-dd hh:mm:ss"));
            mRewards.setText(mTaskModel.getRewards());
            mRewards.setEnabled(false);
            mLevelSpinner.setSelection(Integer.parseInt(mTaskModel.getTaskType())-1);
            mLevelSpinner.setClickable(false);
            mLevelSpinner.setFocusable(false);
            mLevelSpinner.setFocusableInTouchMode(false);
            view.findViewById(R.id.textInputLayout9).setVisibility(View.GONE);
            ((BaseActivity)getActivity()).setTitle("Update Task");
            ((Button)mCreateBtn).setText("Update");
        }else
            ((BaseActivity)getActivity()).setTitle("Create New Task");
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                Utils.hide(getActivity());
                ((BaseActivity)getActivity()).onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        if (mTaskModel != null) {
            return mApiBuilder.updateTask(BaseActivity.USER.getAccessToken(), mTaskName.getText().toString(),
                    mDescription.getText().toString(), mStartDateTV.getText().toString(), mEndDateTV.getText().toString(),
                    mLevelSpinner.getSelectedItemPosition() + 1 + "", "10", mRewards.getText().toString(), mTaskModel.getId());
        }else{
            return mApiBuilder.createTask(BaseActivity.USER.getAccessToken(), mTaskName.getText().toString(),
                    mDescription.getText().toString(), mStartDateTV.getText().toString(), mEndDateTV.getText().toString(),
                    mLevelSpinner.getSelectedItemPosition()+1 +"","10",mRewards.getText().toString(),mGoalId);
        }
    }

    private boolean validateFields() {


        if (mTaskName.getText().toString().isEmpty() == true) {
            showSnack( "Please enter name of task");
            return false;
        }
        if (mDescription.getText().toString().isEmpty() == true) {
            showSnack( "Please enter description of goal");
            return false;
        }
        if (mStartDateTV.getText().toString().isEmpty() == true) {
            showSnack( "Start date could not be empty!");
            return false;
        }
        if (mEndDateTV.getText().toString().isEmpty() == true) {
            showSnack( "End date could not be empty!");
            return false;
        }
        /*if (mPoints.getText().toString().isEmpty() == true) {
            showSnack( "Points could not be empty!");
            return false;
        }*/
        if (mRewards.getText().toString().isEmpty() == true) {
            showSnack( "Rewards could not be empty!");
            return false;
        }
        /*if (getArguments() == null) {
            if (mFriends.isEmpty() == true) {
                showSnack( "Add Friends could not be empty!");
                return false;
            }
        }*/

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.date_start:
                if (mTaskModel == null) {
                    Utils.showDatePicker(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            Calendar c = Calendar.getInstance();
                            c.set(Calendar.YEAR, year);
                            c.set(Calendar.MONTH, month);
                            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            Date setDate = Utils.getDate(Utils.getDate(c.getTime()));
                            Date currentDate = Utils.getDate(Utils.getDate(Calendar.getInstance().getTime()));
                            if (setDate.compareTo(currentDate) < 0) {
                                showSnack( "You can not select the previous date");
                            } else {
                                mStartDate = setDate;
                                mStartDateTV.setText(Utils.getDate(c.getTime()));
                            }

                        }
                    });
                }else
                    ((BaseActivity)getActivity()).showSnack("Date can not be changed!");
                break;
            case R.id.date_end:
                if (mTaskModel == null) {
                    if (!mStartDateTV.getText().toString().equalsIgnoreCase("")) {
                        Utils.showDatePicker(v.getContext(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                Calendar c = Calendar.getInstance();
                                c.set(Calendar.YEAR, year);
                                c.set(Calendar.MONTH, month);
                                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                Date setDate = Utils.getDate(Utils.getDate(c.getTime()));
                                if (setDate.compareTo(mStartDate) >= 0)
                                    mEndDateTV.setText(Utils.getDate(c.getTime()));
                                else
                                    showSnack( "End date must be greater than start date");
                            }
                        });
                    } else {
                        Utils.showToast(v.getContext(), "Please enter start date first");
                    }
                }else
                    ((BaseActivity)getActivity()).showSnack("Date can not be changed!");
                break;
            case R.id.create_btn:
                if (validateFields()){
                    Utils.hide(getActivity());
                    if (mGoalId != null)
                        callApi();
                    else if (mTaskModel != null){
                        callApi();
                    }
                }
                break;
        }
    }

    @Override
    public void run(){
        ((BaseActivity)getActivity()).onBackPressed();
        if (mTaskModel != null) {
            ((BaseActivity) getActivity()).showSnack("Task updated successfully");
        }else{
            ((BaseActivity) getActivity()).showSnack("Task created successfully");
        }
    }
}
