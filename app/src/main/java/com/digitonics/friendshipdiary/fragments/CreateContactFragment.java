package com.digitonics.friendshipdiary.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;


public class CreateContactFragment extends Fragment implements  View.OnClickListener{


    public static final String TAG_FEEDBACK = "Feedback";
    private Runnable mRunnable;
    private View mMyConctacts;
    private LinearLayoutManager mFeedbackManager;

    public CreateContactFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_contact, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        return view;
    }

    public void init(View view){
        view.findViewById(R.id.my_contacts_ll).setOnClickListener(this);
        view.findViewById(R.id.search_contacts_ll).setOnClickListener(this);
        view.findViewById(R.id.facebook_ll).setOnClickListener(this);
        setHasOptionsMenu(true);
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
        ((BaseActivity)getActivity()).setTitle("Add New Friends");

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                ((BaseActivity)getActivity()).onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.my_contacts_ll:
                ((HomeActivity)getActivity()).addFragment(new MyContactFragment());
                break;

            case R.id.search_contacts_ll:
                ((HomeActivity)getActivity()).addFragment(new SearchFriends());
                break;

            case R.id.facebook_ll:
                ((HomeActivity)getActivity()).addFragment(new FacebookFriends());
                break;
        }
    }
}
