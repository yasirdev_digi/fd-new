package com.digitonics.friendshipdiary.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.adapters.NewsfeedAdapter;
import com.digitonics.friendshipdiary.annotations.HideLoader;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.NewsFeedModel;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.squareup.picasso.Picasso;
import com.vincent.videocompressor.VideoCompress;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;

import static com.digitonics.friendshipdiary.activities.BaseActivity.NOTIFICATION_KEY;


@IgnoreApi
@HideLoader
public class NewsfeedFragment extends BaseFragment<ArrayList<NewsFeedModel>> implements NewsfeedAdapter.OnNewsFeedItemClickListener, CustomRunnable {


    public static final String TAG_NEWSFEED = "newsfeed";
    private Runnable mRunnable;
    private RecyclerView mNewsfeedRV;
    private LinearLayoutManager mNewsfeedManager;
    private String mImagePath, mVideoPath, mAudioPath;
    private ArrayList<UserModel> tagFriends = new ArrayList<>();
    private Double mLongitude, mLatitude;
    private NewsfeedAdapter mServiceAdapter;
    private View mNoData;
    private boolean isEdit = false;

    public static NewsfeedFragment newInstacne(){
        NewsfeedFragment newsfeedFragment = new NewsfeedFragment();
        return newsfeedFragment;
    }

/*    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_newsfeed, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.VISIBLE);
        init(view);
        callApi();
        return view;
    }

    public void init(View view){
        mNoData = view.findViewById(R.id.no_data_img);
        mNewsfeedRV = (RecyclerView) view.findViewById(R.id.newsfeed_rv);
        mNewsfeedManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean requestChildRectangleOnScreen(RecyclerView parent, View child, Rect rect, boolean immediate, boolean focusedChildVisible) {

                return false;
            }
        };

        setHasOptionsMenu(true);
        ((BaseActivity)getActivity()).setToolBar(R.drawable.icon_menu);
        ((BaseActivity)getActivity()).setTitle("Newsfeed");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.content);
                if (fragment != null) {
                    if (((BaseFragment) fragment) instanceof NewsfeedFragment) {
                        ((BaseActivity) getActivity()).mDrawerLayout.openDrawer(GravityCompat.START);
                    }else if (((BaseFragment) fragment) instanceof TagFriends) {
                        ((BaseActivity) getActivity()).onBackPressed();
                        ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.VISIBLE);
                        ((BaseActivity)getActivity()).setToolBar(R.drawable.icon_menu);
                        ((BaseActivity)getActivity()).setTitle("Newsfeed");
                        getActivity().invalidateOptionsMenu();
                    }
                }
                return true;
            case R.id.notification:
                ((HomeActivity)getActivity()).addFragment(new NotificationlFragment());
                ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.GONE);
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.getPosts(BaseActivity.USER.getAccessToken());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_notification, menu);
        if (((BaseActivity)getActivity()).mPreferences.contains(NOTIFICATION_KEY))
            ((BaseActivity)getActivity()).getToolbar().getMenu().findItem(R.id.notification).setIcon(R.drawable.icon_noti1);
        else
            ((BaseActivity)getActivity()).getToolbar().getMenu().findItem(R.id.notification).setIcon(R.drawable.icon_noti);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void onMediaLoad(int reqCode, Intent data) {
        switch (reqCode) {
            case Constants.IMAGES_GALLERY:
            case Constants.IMAGES_CAMERA:
                mAudioPath = null;
                mVideoPath = null;
                mLatitude = null;
                mLongitude = null;
                Log.e("TESTY", "mMediaPath = " + ((BaseActivity)getActivity()).mMediaPath);
                Bitmap bmp = null;
                try {
                    bmp = BitmapFactory.decodeFile(((BaseActivity)getActivity()).mMediaPath);
                    bmp = Utils.scaleImageKeepAspectRatio(bmp);
                    bmp = Utils.modifyOrientation(bmp,((BaseActivity)getActivity()).mMediaPath);
//                    mProfPicIV.setImageBitmap(bmp);
                    mImagePath = ((BaseActivity)mContext).mMediaPath;
                    NewsFeedModel model = new NewsFeedModel();
                    model.setLatitude(null);
                    model.setLongitude(null);
                    model.setAudioUrl(null);
                    model.setImageUrl(mImagePath);
                    model.setVideoUrl(null);
                    if (!isEdit)
                        mServiceAdapter.changeData(model);
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                Picasso.with(mContext)
//                        .load(Uri.fromFile())
//                        .into(mAddDocIV);
                break;

            case Constants.VIDEO_CAMERA:
            case Constants.VIDEO_GALLERY:
                if (((BaseActivity)mContext).mMediaPath != null){
                    View dialogView = getLayoutInflater().inflate(R.layout.progress_bar,null);
                    mProgressDialog = Utils.showCustomDialog(getActivity(),dialogView);
                    mProgressDialog.setCancelable(false);
                    final File f = new File(getActivity().getExternalFilesDir(null).toString() + "_" + new Date().getTime() + ".mp4");
                    VideoCompress.compressVideoLow(((BaseActivity)mContext).mMediaPath, f.getAbsolutePath(), new VideoCompress.CompressListener() {
                        @Override
                        public void onStart() {
                            //Start Compress
                        }

                        @Override
                        public void onSuccess() {
                            mProgressDialog.dismiss();
                            mVideoPath = f.getAbsolutePath();
                            mAudioPath = null;
                            mImagePath = null;
                            mLatitude = null;
                            mLongitude = null;
                            NewsFeedModel model = new NewsFeedModel();
                            model.setLatitude(null);
                            model.setLongitude(null);
                            model.setAudioUrl(null);
                            model.setImageUrl(null);
                            model.setVideoUrl(mVideoPath);
                            if (!isEdit)
                                mServiceAdapter.changeData(model);
                        }

                        @Override
                        public void onFail() {
                            mProgressDialog.dismiss();
                        }

                        @Override
                        public void onProgress(float percent) {
                            //Progress
                        }
                    });

                }else{
                    showSnack("Google drive photos cannot be selected!");
                }

                Log.e("TESTY", "mMediaPath = " + ((BaseActivity)getActivity()).mMediaPath);

                break;
            case Constants.AUDIO_RECORDER:
                mVideoPath = null;
                mImagePath = null;
                mLatitude = null;
                mLongitude = null;
                Log.e("TESTY", "mMediaPath = " + ((BaseActivity)getActivity()).mMediaPath);
                mAudioPath = ((BaseActivity)mContext).mMediaPath;
                NewsFeedModel model = new NewsFeedModel();
                model.setLatitude(null);
                model.setLongitude(null);
                model.setAudioUrl(mAudioPath);
                model.setImageUrl(null);
                model.setVideoUrl(null);
                if (!isEdit)
                    mServiceAdapter.changeData(model);
                break;
        }
    }

    @Override
    public void onNewsFeedClick(int position) {
        int pos = position-1;
        ((HomeActivity)getActivity()).addFragment(NewsDetailFragment.newInstance(mData.get(pos)));
        ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.GONE);
    }

    @Override
    public void onPostClick(String postText) {
        if (!postText.equalsIgnoreCase("")) {
            mApiBuilder.post(mImagePath, mVideoPath, mAudioPath, postText, tagFriends, mLatitude, mLongitude, BaseActivity.USER.getAccessToken(), new CustomRunnable() {
                @Override
                public void run(Object data) {
                    mAudioPath = null;
                    mVideoPath = null;
                    tagFriends.clear();
                    mImagePath = null;
                    mLatitude = null;
                    mLongitude = null;
                    callApi();

                }
            }, getActivity());
        }else
            ((BaseActivity)getActivity()).showSnack("Please enter post text");//Toast.makeText(getActivity(), "Please enter post text", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEditClick(int position) {
        isEdit = true;
        final int pos = position -1;
        View itemView = View.inflate(getActivity(), R.layout.dialog_newsfeed_header, null);
        final Dialog dialog = Utils.showCustomDialog(getActivity(), itemView);
        TextView media = itemView.findViewById(R.id.media);
        TextView location = itemView.findViewById(R.id.location);
        Button postBtn =  itemView.findViewById(R.id.post);
        ImageView close =  itemView.findViewById(R.id.close);
        final EditText postET =  itemView.findViewById(R.id.post_text);
        ImageView profileImage =  itemView.findViewById(R.id.profile_image);
        Picasso.get().load(mData.get(pos).getUserModel().getImage() != null ? mData.get(pos).getUserModel().getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(profileImage);

        postET.setText(mData.get(pos).getPost());

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.media:
                        ((BaseActivity)v.getContext()).showMediaDailog();
                        break;
                    case R.id.location:
                        if (Utils.checkGPSStatus(v.getContext()) == true) {
                            //mMapClicked = 1;
                            ((BaseActivity) v.getContext()).openMap();
                        }
                        break;
                    case R.id.post:
                        dialog.dismiss();
                        mApiBuilder.postUpdate(mImagePath, mVideoPath, mAudioPath, postET.getText().toString(), mData.get(pos).getId(),mLatitude, mLongitude, BaseActivity.USER.getAccessToken(), new CustomRunnable() {
                            @Override
                            public void run(Object data) {
                                mAudioPath = null;
                                mVideoPath = null;
                                mImagePath = null;
                                mLatitude = null;
                                mLongitude = null;
                                callApi();
                                isEdit = false;

                            }
                        }, getActivity());
                        break;
                    case R.id.close:
                        isEdit =false;
                        mAudioPath = null;
                        mVideoPath = null;
                        mImagePath = null;
                        mLatitude = null;
                        mLongitude = null;
                        dialog.dismiss();
                        break;
                }

            }
        };
        close.setOnClickListener(listener);
        media.setOnClickListener(listener);
        location.setOnClickListener(listener);
        postBtn.setOnClickListener(listener);

    }

    @Override
    public void reportPost(int position) {
        final int pos = position -1;
        mApiBuilder.reportPost( BaseActivity.USER.getAccessToken(), mData.get(pos).getId(),"", "violent", new CustomRunnable() {
            @Override
            public void run(Object data) {
            }
        },getActivity());
    }

    @Override
    public void deletePost(final int position) {
        final int pos = position -1;
        mApiBuilder.deletePost( BaseActivity.USER.getAccessToken(), mData.get(pos).getId(), new CustomRunnable() {
            @Override
            public void run(Object data) {
                mData.remove(pos);
                ArrayList<NewsFeedModel> feedModels = new ArrayList<>();
                feedModels.add(new NewsFeedModel());
                feedModels.addAll(mData);

                mServiceAdapter = new NewsfeedAdapter(NewsfeedFragment.this, NewsfeedFragment.this,feedModels);
                mNewsfeedRV.setLayoutManager(mNewsfeedManager);
                mNewsfeedRV.setAdapter(mServiceAdapter);

                Utils.animRecyclerView(mNewsfeedRV, Utils.LIST_ANIM_FROM_TOP);
            }
        },getActivity());
    }

    @Override
    public void run() {
        if (((BaseActivity)getActivity()).mPreferences.contains(NOTIFICATION_KEY))
            ((BaseActivity)getActivity()).getToolbar().getMenu().findItem(R.id.notification).setIcon(R.drawable.icon_noti1);
        else
            ((BaseActivity)getActivity()).getToolbar().getMenu().findItem(R.id.notification).setIcon(R.drawable.icon_noti);

        if (mData != null && mData.size() > 0 ) {
            mNoData.setVisibility(View.GONE);
        }else
            mNoData.setVisibility(View.VISIBLE);
        ArrayList<NewsFeedModel> feedModels = new ArrayList<>();
        feedModels.add(new NewsFeedModel());
        feedModels.addAll(mData);

        mServiceAdapter = new NewsfeedAdapter(this, this,feedModels);
        mNewsfeedRV.setLayoutManager(mNewsfeedManager);
        mNewsfeedRV.setAdapter(mServiceAdapter);

        Utils.animRecyclerView(mNewsfeedRV, Utils.LIST_ANIM_FROM_TOP);
    }

    @Override
    public void run(Object data) {

        tagFriends = (ArrayList<UserModel>) data;
        if (tagFriends.size() > 0){
            //Toast.makeText(getActivity(), "Friends added successfully", Toast.LENGTH_SHORT).show();
        }else
            ((BaseActivity)getActivity()).showSnack("Please add friends to perform this action!");
        ((BaseActivity)getActivity()).setToolBar(R.drawable.icon_menu);
        ((BaseActivity)getActivity()).setTitle("Newsfeed");
        ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.VISIBLE);
        getActivity().invalidateOptionsMenu();

    }

    public void onLocationFetched(int reqCode, Intent intent) {
//        switch (reqCode) {
//            case Constants.MAP_ADDRESS_RESULT_CODE:
        if (intent != null) {
            mAudioPath = null;
            mVideoPath = null;
            mImagePath = null;
            Log.e(Constants.APP_TAG, "------------------------------------INSIDE MAP_ADDRESS_RESULT_CODE------------------------------------");

            String location = intent.getStringExtra("maps_geoLocation") + "";
/*
            if (mMapClicked == 1) {*/
                mLatitude = Double.parseDouble(location.substring(0, location.indexOf(",")));
                mLongitude = Double.parseDouble(location.substring(location.indexOf(",") + 1));
                NewsFeedModel model = new NewsFeedModel();
                model.setLatitude(location.substring(0, location.indexOf(",")));
                model.setLongitude(location.substring(location.indexOf(",") + 1));
                model.setAudioUrl(null);
                model.setImageUrl(null);
                model.setVideoUrl(null);
                if (!isEdit)
                    mServiceAdapter.changeData(model);
//                mPickupAddrET.setText(intent.getStringExtra("maps_address") + "");
////                        mPickupAddrET.setText("16090 City Walk, Sugar Land, TX 77479");
//            }
//            if (mMapClicked == 2) {
//                mDeliveryLatitude = location.substring(0, location.indexOf(","));
//                mDeliveryLongitude = location.substring(location.indexOf(",") + 1);
//
//                mDeliveryAddrET.setText(intent.getStringExtra("maps_address") + "");
////                        mDeliveryAddrET.setText("6330 E Riverpark Dr, Sugar Land, TX 77479");
//            }


            String address = intent.getStringExtra("maps_address") + "";

        }
    }
}
