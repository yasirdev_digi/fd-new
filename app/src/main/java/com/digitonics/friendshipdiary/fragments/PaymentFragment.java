package com.digitonics.friendshipdiary.fragments;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.adapters.PaymentAdapter;
import com.digitonics.friendshipdiary.adapters.RewardAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.PaymentModel;
import com.digitonics.friendshipdiary.models.TaskModel;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.io.IOException;

import retrofit2.Call;

@IgnoreApi
public class PaymentFragment extends BaseFragment<PaymentModel> implements View.OnClickListener, PaymentAdapter.OnCourseClickListener{


    public static final String TAG_MY_COURSE = "MyCourse";
    private Runnable mRunnable;
    private RecyclerView mWishRV;
    private LinearLayoutManager mWishManager;
    private ImageView mNoData;

    public PaymentFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wishlist, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        callApi();
        return view;
    }

    public void init(View view){
        mNoData= (ImageView)view.findViewById(R.id.no_data_img);
        mNoData.setImageResource(R.drawable.payment_empty);
        mWishRV = (RecyclerView) view.findViewById(R.id.wish_rv);
        mWishManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean requestChildRectangleOnScreen(RecyclerView parent, View child, Rect rect, boolean immediate, boolean focusedChildVisible) {

                return false;
            }
        };

        mWishRV.setLayoutManager(mWishManager);

        ((BaseActivity)getActivity()).setTitle("Payment Record");
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
        setHasOptionsMenu(true);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ((BaseActivity)getActivity()).mDrawerLayout.closeDrawers();
            }
        });
    }

    @Override
    public void onClick(View view) {
        //((HomeActivity)getActivity()).addFragment(new GoalDetailFragment());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                ((BaseActivity)getActivity()).onBackPressed();
                return true;

        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.getPayment(BaseActivity.USER.getAccessToken());
    }

    @Override
    public void onCourseClick(int position) {
        /*if (position > 0)
            ((HomeActivity)getActivity()).addFragment(WishListFragment.newInstance(mData.getGoals().get(position)));
*/
        ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.GONE);
    }

    @Override
    public void run() {
        if (mData!= null && mData.getTask() != null && mData.getTask().size() > 0) {
            mNoData.setVisibility(View.GONE);
            mData.getTask().add(0, new TaskModel());
            PaymentAdapter mServiceAdapter = new PaymentAdapter(this, mData);


            mWishRV.setAdapter(mServiceAdapter);
            Utils.animRecyclerView(mWishRV, Utils.LIST_ANIM_FROM_TOP);
        }else{
            mNoData.setVisibility(View.VISIBLE);
        }
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.my_goals_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }*/
}
