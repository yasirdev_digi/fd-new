package com.digitonics.friendshipdiary.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.interfaces.UpdateRunnable;
import com.digitonics.friendshipdiary.models.DeliveryAddressModel;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.IOException;

import retrofit2.Call;


@IgnoreApi
public class DeliveryAddress extends BaseFragment<DeliveryAddressModel> {


    public static final String TAG_FEEDBACK = "Feedback";
    private Button mFriendsBtn;
    private LinearLayoutManager mFeedbackManager;
    private View mCreateBtn;
    private EditText mAddress, mState, mCountry, mZipCode;
    private Bundle mArgs;
    private GoalModel goalModel;
    private DeliveryAddressModel deliveryAddress;
    private static UpdateRunnable mRunnable;

    public static DeliveryAddress newInstance(boolean update, GoalModel goal, UpdateRunnable runnable) {
        DeliveryAddress f = new DeliveryAddress();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        //args.putParcelable("goal", goalModel);
        args.putBoolean("update", update);
        args.putParcelable("goal", goal);
        f.setArguments(args);
        mRunnable = runnable;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delivery_address, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        return view;
    }

    public void init(View view){
        mArgs = getArguments();

        mAddress = view.findViewById(R.id.address);
        mState = view.findViewById(R.id.state);
        mCountry = view.findViewById(R.id.country);
        mZipCode = view.findViewById(R.id.zipcode);
        mCreateBtn = view.findViewById(R.id.create_btn);
        mCreateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateFields()){
                    Utils.hide(getActivity());
                    callApi();
                }
            }
        });
        goalModel = mArgs.getParcelable("goal");
        if (goalModel != null && !(goalModel.getDeliveryAddress() instanceof Boolean)){
            Gson gson = new Gson();
            JsonObject jsonObject = gson.toJsonTree(goalModel.getDeliveryAddress()).getAsJsonObject();
            deliveryAddress = gson.fromJson(jsonObject,DeliveryAddressModel.class);
            mAddress.setText(deliveryAddress.getAddress());
            mState.setText(deliveryAddress.getState());
            mCountry.setText(deliveryAddress.getCountry());
            mZipCode.setText(deliveryAddress.getZipcode());
        }
        setHasOptionsMenu(true);
        ((BaseActivity)getActivity()).setTitle("Delivery Address");
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                Utils.hide(getActivity());
                ((BaseActivity)getActivity()).onBackPressed();
                return true;
        }
        return false;
    }


    @Override
    protected Call<?> getCall() throws IOException {
        if (goalModel != null && !(goalModel.getDeliveryAddress() instanceof Boolean))
            return mApiBuilder.updateAddress(BaseActivity.USER.getAccessToken(), mAddress.getText().toString(),mState.getText().toString(), mCountry.getText().toString(),mZipCode.getText().toString(), deliveryAddress.getId());
        else
            return mApiBuilder.createAddress(BaseActivity.USER.getAccessToken(), mAddress.getText().toString(),mState.getText().toString(), mCountry.getText().toString(),mZipCode.getText().toString(), goalModel.getId());
    }

    private boolean validateFields() {


        if (mAddress.getText().toString().isEmpty() == true) {
            showSnack( "Please enter address");
            return false;
        }
        if (mState.getText().toString().isEmpty() == true) {
            showSnack( "Please enter state");
            return false;
        }
        if (mCountry.getText().toString().isEmpty() == true) {
            showSnack( "Please enter country");
            return false;
        }
        if (mZipCode.getText().toString().isEmpty() == true) {
            showSnack( "Please enter zipcode");
            return false;
        }


        return true;
    }

    @Override
    public void run() {
        ((BaseActivity)getActivity()).onBackPressed();
        mRunnable.run(mData,-1,false);
        ((BaseActivity)getActivity()).showSnack("Delivery Address saved successfully");
    }
}
