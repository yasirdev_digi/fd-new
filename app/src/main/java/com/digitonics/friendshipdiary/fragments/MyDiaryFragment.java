package com.digitonics.friendshipdiary.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.adapters.GoalAdapter;
import com.digitonics.friendshipdiary.adapters.MyDiaryAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.models.DairyModel;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.widgets.RecyclerViewItemDecorator;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;

@IgnoreApi
public class MyDiaryFragment extends BaseFragment<ArrayList<DairyModel>> implements MyDiaryAdapter.OnDairylClickListener{


    private LinearLayoutManager mDiaryManager;
    private RecyclerView mDiaryRV;
    private View mNoData;

    public MyDiaryFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_diary, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        callApi();
        return view;
    }

    public void init(View view){

        view.findViewById(R.id.create_diary).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).addFragment(new CreateDiaryFragment());
                ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.GONE);
            }
        });
        mDiaryRV = (RecyclerView) view.findViewById(R.id.my_diary_rv);
        mNoData = view.findViewById(R.id.no_data_img);

        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
        ((BaseActivity)getActivity()).setTitle("My Diary");
        setHasOptionsMenu(true);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ((BaseActivity)getActivity()).mDrawerLayout.closeDrawers();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                ((BaseActivity)getActivity()).onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.allDairy(BaseActivity.USER.getAccessToken());
    }

    @Override
    public void run(){
        if (mData != null && mData.size() > 0 ) {
            mNoData.setVisibility(View.GONE);

            mDiaryRV.addItemDecoration(new RecyclerViewItemDecorator(0));
            mDiaryManager = new LinearLayoutManager(getActivity());

            MyDiaryAdapter mDiaryAdapter = new MyDiaryAdapter(this, mData);

            mDiaryRV.setLayoutManager(mDiaryManager);
            mDiaryRV.setAdapter(mDiaryAdapter);
            Utils.animRecyclerView(mDiaryRV, Utils.LIST_ANIM_FROM_TOP);
        }else{
            mNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDairyClick(int type, DairyModel dairyModel) {
            ((HomeActivity)getActivity()).addFragment(CreateDiaryFragment.newInstance(true,dairyModel));
    }
}
