package com.digitonics.friendshipdiary.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.adapters.NewsfeedAdapter;
import com.digitonics.friendshipdiary.adapters.AddFriendAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.Friend;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;

@IgnoreApi
public class AddFriendFragment extends BaseFragment<ArrayList<UserModel>> implements NewsfeedAdapter.OnNewsFeedClickListener {


    public static final String TAG_MY_TASK_FRAGMENT = "MyTaskFragment";
    private static CustomRunnable mRunnable;
    private RecyclerView mFriendsRV;
    private LinearLayoutManager mFriendsManager;
    private EditText mSearchEdit;
    private AddFriendAdapter mFriendAdapter;
    private GoalModel goalModel;

    public static AddFriendFragment newInstance (CustomRunnable runnable, GoalModel goal){
        AddFriendFragment addFriendFragment = new AddFriendFragment();
        mRunnable = runnable;
        Bundle bundle = new Bundle();
        bundle.putParcelable("goal",goal);
        addFriendFragment.setArguments(bundle);
        return addFriendFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_friends, container, false);
        init(view);
        return view;
    }

    public void init(View view){
        mFriendsRV = (RecyclerView) view.findViewById(R.id.add_friends_rv);
        mSearchEdit = (EditText) view.findViewById(R.id.search_edit_text);
        ((BaseActivity)getActivity()).setDefaultBackground();
        goalModel = getArguments().getParcelable("goal");

            setHasOptionsMenu(true);
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
        ((BaseActivity)getActivity()).setTitle("Add Members");
        callApi();

    }

    @Override
    public void onNewsFeedClick(int position) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                Utils.hideSoftKeyboard(mSearchEdit);
                ((BaseActivity)getActivity()).onBackPressed();
                return true;
            case R.id.search:
                mSearchEdit.setVisibility(mSearchEdit.getVisibility() == View.GONE ? View.VISIBLE :View.GONE );
                if(mSearchEdit.getVisibility() == View.GONE)
                    Utils.hideSoftKeyboard(mSearchEdit);
                return true;
            case R.id.done:
                mRunnable.run(mData);
                ((BaseActivity)getActivity()).onBackPressed();

                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        if (goalModel == null)
            return mApiBuilder.allUser(BaseActivity.USER.getAccessToken());
        else
            return mApiBuilder.getGoalFriends(BaseActivity.USER.getAccessToken(),goalModel.getId());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_friend_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void run(){
        mFriendsManager = new LinearLayoutManager(getActivity());

        mFriendAdapter = new AddFriendAdapter(this, mData);

        mFriendsRV.setLayoutManager(mFriendsManager);
        mFriendsRV.setAdapter(mFriendAdapter);

        mSearchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mFriendAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}


