package com.digitonics.friendshipdiary.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.adapters.FriendsAdapter;
import com.digitonics.friendshipdiary.adapters.GoalFriendsAdapter;
import com.digitonics.friendshipdiary.models.Friend;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.widgets.RecyclerViewItemDecorator;

import java.util.ArrayList;


public class FriendsFragment extends Fragment{


    public static final String TAG_MY_COURSE = "MyCourse";
    private Runnable mRunnable;
    private RecyclerView mFriendRV, mAchorRV;
    private LinearLayoutManager mFriendManager, mAchorManager;
    private View mBottomLL;
    private Bundle mArgs;
    private GoalModel mGoalModel;

    public static FriendsFragment newInstance(GoalModel goalModel) {
        FriendsFragment f = new FriendsFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("goal", goalModel);
        f.setArguments(args);
        return f;
    }

    public FriendsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_friends, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        return view;
    }

    public void init(View view){

        mFriendRV = (RecyclerView) view.findViewById(R.id.friends_rv);

        mFriendManager = new LinearLayoutManager(getActivity());;
        mFriendRV.addItemDecoration(new RecyclerViewItemDecorator(0));

        setHasOptionsMenu(true);

        mArgs = getArguments();
        if (mArgs != null) {
            mGoalModel = mArgs.getParcelable("goal");
            if (mGoalModel != null){
                GoalFriendsAdapter mGoalAdapter = new GoalFriendsAdapter((mGoalModel.getFriends() != null ? mGoalModel.getFriends(): new ArrayList<Friend>()));
                mFriendRV.setLayoutManager(mFriendManager);
                mFriendRV.setAdapter(mGoalAdapter);
            }
        }
        ((BaseActivity)getActivity()).setTitle("Friends");
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.search:
                Toast.makeText(getActivity(), "Search feature not implemented yet", Toast.LENGTH_SHORT).show();
                return true;
        }
        return false;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.menu_search, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

}
