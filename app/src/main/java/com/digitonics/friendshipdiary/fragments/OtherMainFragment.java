package com.digitonics.friendshipdiary.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.adapters.NewsfeedAdapter;
import com.digitonics.friendshipdiary.adapters.PagerAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.models.OtherGoalModel;
import com.digitonics.friendshipdiary.utilities.TopDialog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import static com.digitonics.friendshipdiary.activities.BaseActivity.NOTIFICATION_KEY;

@IgnoreApi
public class OtherMainFragment extends BaseFragment<OtherGoalModel> {


    public static final String TAG_MY_COURSE = "MyCourse";
    private Runnable mRunnable;
    private RecyclerView mGoalRV, mAchorRV;
    private LinearLayoutManager mGoalManager, mAchorManager;
    private PagerAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TopDialog mDialog;


    private Spinner typeSpinner;
    private Spinner statusSpinner;
    private View customView;
    private View mNoData;
    public static boolean ON_DETAIL = false;
    public OtherMainFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_other_main, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        callApi();
        return view;
    }

    public void init(View view){

        mNoData = view.findViewById(R.id.no_data_img);

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);

        ((BaseActivity)getActivity()).setTitle("Goal Member");
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                ((BaseActivity)getActivity()).mDrawerLayout.closeDrawers();
            }
        });

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        customView = inflater.inflate(R.layout.dialog_filter, null);
        typeSpinner = (Spinner) customView.findViewById(R.id.goal_type);
        statusSpinner = (Spinner) customView.findViewById(R.id.goal_status);

        List<String> types = new ArrayList<String>();
        types.add("Public");
        types.add("Private");

        List<String> goal_status = new ArrayList<String>();
        goal_status.add("Active");
        goal_status.add("Inactive");


        ArrayAdapter<String> ada = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, types);

        // Drop down layout style - list view with radio button
        ada.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<String> adas = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, goal_status);

        // Drop down layout style - list view with radio button
        adas.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        typeSpinner.setAdapter(ada);
        statusSpinner.setAdapter(adas);
        customView.findViewById(R.id.apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                callApi();
                showSnack("Filter applied successfully");
            }
        });

        setHasOptionsMenu(true);
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.getOtherGoals(BaseActivity.USER.getAccessToken());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                ((BaseActivity)getActivity()).onBackPressed();
                return true;
            case R.id.notification:
                ((BaseActivity)getActivity()).addFragment(new NotificationlFragment());
                return true;
            case R.id.filter:

                return true;
        }
        return false;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.other_goals_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void run() {
        if (((BaseActivity)getActivity()).mPreferences.contains(NOTIFICATION_KEY))
            ((BaseActivity)getActivity()).getToolbar().getMenu().findItem(R.id.notification).setIcon(R.drawable.icon_noti1);
        else
            ((BaseActivity)getActivity()).getToolbar().getMenu().findItem(R.id.notification).setIcon(R.drawable.icon_noti);

        if (mData != null
                && (mData.getSponsor() != null
                ||  mData.getSupporter() != null
                || mData.getWatcher() != null)) {
            if (ON_DETAIL == true){
                ON_DETAIL = false;
                callApi();
            }
            adapter = new PagerAdapter(getActivity(), getChildFragmentManager(), mData);
            viewPager.setAdapter(adapter);
            adapter.instantiateItem(viewPager,0);
            tabLayout.setupWithViewPager(viewPager);
            mNoData.setVisibility(View.GONE);
        }else
            mNoData.setVisibility(View.VISIBLE);
    }
}
