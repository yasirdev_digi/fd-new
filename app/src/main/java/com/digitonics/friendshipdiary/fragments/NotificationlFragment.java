package com.digitonics.friendshipdiary.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.adapters.NewsfeedAdapter;
import com.digitonics.friendshipdiary.adapters.NotificationAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.NotificationModel;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;

@IgnoreApi
public class NotificationlFragment extends BaseFragment<ArrayList<NotificationModel>> implements View.OnClickListener, NotificationAdapter.OnNotificationClickListener {


    private LinearLayoutManager mNotificationManager;
    private RecyclerView mTasksRV;
    private RecyclerView mNotificationRV;
    private TextView textView, name;
    private ImageView profileImage;
    private View mNoData;
    private View mNotificationView, firstView;
    private NotificationModel baseModel = null;

    public NotificationlFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);


        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        callApi();
        return view;
    }

    public void init(View view) {
        mNoData = view.findViewById(R.id.no_data_img);
        mNotificationView = view.findViewById(R.id.notifcation_view);
        /*mTasksRV = (RecyclerView) view.findViewById(R.id.task_lv);
        mTaskManager = new LinearLayoutManager(getActivity());

        TaskListAdapter mTaskAdapter = new TaskListAdapter(this);

        mTasksRV.setLayoutManager(mTaskManager);
        mTasksRV.setAdapter(mTaskAdapter);*/

        textView = view.findViewById(R.id.following);

        mNotificationRV = (RecyclerView) view.findViewById(R.id.earlier_notifications);
        mNotificationManager = new LinearLayoutManager(getActivity());
        textView = view.findViewById(R.id.following);
        name = view.findViewById(R.id.name);
        profileImage = view.findViewById(R.id.profile_image);
        firstView = view.findViewById(R.id.goal_name);
        /*textView.append(Utils.getColoredString("Commented on your ",ContextCompat.getColor(getActivity(),R.color.light_gray)));
        textView.append(Utils.getColoredString("post", ContextCompat.getColor(getActivity(),R.color.colorPrimaryDark)));
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity)getActivity()).addFragment(new NewsDetailFragment());
            }
        });*/
        /*mNotificationRV = (RecyclerView) view.findViewById(R.id.earlier_notifications);
        mNotificationManager = new LinearLayoutManager(getActivity());

        NotificationAdapter mAdapter = new NotificationAdapter(new ArrayList<NotificationModel>());

        mNotificationRV.setLayoutManager(mNotificationManager);
        mNotificationRV.setAdapter(mAdapter);*/
        setHasOptionsMenu(true);
        ((BaseActivity) getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
        ((BaseActivity) getActivity()).setTitle("Notifications");
        ((BaseActivity) getActivity()).mPreferences.edit().remove(BaseActivity.NOTIFICATION_KEY).apply();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.getNotification(BaseActivity.USER.getAccessToken());
    }

    @Override
    public void onClick(View view) {
        ((HomeActivity) getActivity()).addFragment(new ImageProofDetail());
    }

    @Override
    public void run() {
        if (mData != null && mData.size() > 0) {
            mNotificationView.setVisibility(View.VISIBLE);
            if (mData != null && mData.size() > 0) {
                baseModel = mData.get(0);
                String lastName = mData.get(0).getUser().getLastName() != null ? mData.get(0).getUser().getLastName() : "";
                name.setText(StringUtils.capitalize(mData.get(0).getUser().getFirstName()) + " " + StringUtils.capitalize(lastName));
                Picasso.get().load(mData.get(0).getUser().getImage() != null ? mData.get(0).getUser().getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(profileImage);
                textView.setText(mData.get(0).getActivity().getExtras());
                firstView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onNotificationClick(baseModel);
                    }
                });
            }
            NotificationAdapter mAdapter = new NotificationAdapter(mData, this);

            mNotificationRV.setLayoutManager(mNotificationManager);
            mNotificationRV.setAdapter(mAdapter);
        } else {
            mNoData.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onNotificationClick(NotificationModel model) {
        if (model.getActivity().getAction().equalsIgnoreCase("sponsor")){
            GoalModel goalModel = new GoalModel();
            goalModel.setId(model.getActivity().getObjectId());
            goalModel.setUserType(Constants.SPONSER);
            ((HomeActivity)getActivity()).addFragment(GoalDetailFragment.newInstance(goalModel));
        }
    }
}
