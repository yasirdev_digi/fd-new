package com.digitonics.friendshipdiary.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;


public class EmailFragment extends DialogFragment implements DialogInterface.OnClickListener {


    public static final String FORGOT_PASSWORD_FRAGMENT_TAG = "forgotPasswordFragment";
    private Context mContext;
    private EditText mEmail;
    private static CustomRunnable mRunnable;


    public static EmailFragment newInstance(CustomRunnable runnable) {
        EmailFragment notificationDialog = new EmailFragment();
        mRunnable = runnable;
        return notificationDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_email_dialog, null);
        mEmail = (EditText) view.findViewById(R.id.email);
        mContext = getActivity();
        return new AlertDialog.Builder(getActivity(),R.style.AppTheme_DialogTheme)
                .setTitle(getString(R.string.forgot_password))
                .setView(view)
                .setPositiveButton(getString(R.string.send),this)
                .setNegativeButton(getString(R.string.cancel),this)
                .create();
    }

    @Override
    public void onClick(DialogInterface dialog, int i) {
        switch (i) {
            case DialogInterface.BUTTON_POSITIVE:
                getDialog().dismiss();
                /*Utils.hide(getActivity());
                if (!Utils.isEmpty(mEmail.getText().toString())) {
                    if (Utils.isEmailValid(mEmail.getText().toString())) {
                        ((BaseActivity) getActivity()).mApiBuilder.forgotPassword(new ForgotModel(mEmail.getText().toString()), mRunnable, getActivity());
                    }
                }
                else
                    Toast.makeText(getActivity(),getString(R.string.enter_email_address),Toast.LENGTH_SHORT).show();
*/
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                getDialog().dismiss();
                break;
        }
    }
}
