package com.digitonics.friendshipdiary.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.adapters.CircularImageAdapter;
import com.digitonics.friendshipdiary.adapters.GoalAdapter;
import com.digitonics.friendshipdiary.adapters.ProofAdapter;
import com.digitonics.friendshipdiary.adapters.TaskListAdapter;
import com.digitonics.friendshipdiary.adapters.TasksAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.Friend;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.ProofModel;
import com.digitonics.friendshipdiary.models.TaskModel;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.digitonics.friendshipdiary.utilities.OverlapDecoration;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;

@IgnoreApi
public class TaskListFragment extends BaseFragment<GoalModel> implements View.OnClickListener{


    public static final String TAG_MODULE = "ModuleFragment";
    private Runnable mRunnable;
    private LinearLayoutManager mTaskManager;
    private RecyclerView mTasksRV;
    int index = 0;
    GoalModel mGoal;
    private View mMainCL;
    private TasksAdapter mTaskAdapter;


    /*public static GoalDetailFragment newInstance(int index) {
        GoalDetailFragment f = new GoalDetailFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        return f;
    }*/

    public static TaskListFragment newInstance(GoalModel goal) {
        TaskListFragment f = new TaskListFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("goal", goal);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_task_list, container, false);
        Bundle args = getArguments();
       /* if (args != null && args.containsKey("index"))
            index = args.getInt("index", 0);
        else */
       if(args != null && args.containsKey("goal")){
           mGoal = args.getParcelable("goal");
        }

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        //callApi();
        return view;
    }
    public void init(View view){
        mTasksRV = (RecyclerView) view.findViewById(R.id.task_lv);

        mTaskManager = new LinearLayoutManager(getActivity());

        mMainCL = view.findViewById(R.id.mainCL);
        ((BaseActivity)getActivity()).setDefaultBackground();

        setHasOptionsMenu(true);


        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
        ((BaseActivity)getActivity()).setTitle("");

            if (mGoal != null) {
                mTaskAdapter = new TasksAdapter(mGoal.getTasks() != null ? mGoal.getTasks() : new ArrayList<TaskModel>());

                mTasksRV.setLayoutManager(mTaskManager);
                mTasksRV.setAdapter(mTaskAdapter);

                mMainCL.setVisibility(View.VISIBLE);
            }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.add_task:
                ((BaseActivity)getActivity()).addFragment(CreateTaskFragment.newInstance(mGoal.getId()));
                return true;
            case R.id.edit:
                if (mData != null)
                    ((BaseActivity)getActivity()).addFragment(CreateGoalFragment.newInstance(true, mData));
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {

        return mApiBuilder.getGoalDetail(BaseActivity.USER.getAccessToken(), mGoal.getId());
    }

    @Override
    public void onClick(View view) {
        int pos = (int)view.getTag();
       /* switch (pos){
            case 0:*/
                /*((HomeActivity)getActivity()).addFragment(ImageProofDetail.newInstance());*/
//                break;
            /*case 1:
                ((HomeActivity)getActivity()).addFragment(new WishListFragment());*/
               /* break;*/
//        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
       /* if (mGoal.getUserId().equalsIgnoreCase(BaseActivity.USER.getId())) {
            inflater.inflate(R.menu.menu_edit, menu);
        }*/
       /* else
        inflater.inflate(R.menu.menu_add_task, menu);*/
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void run(){

    }
}
