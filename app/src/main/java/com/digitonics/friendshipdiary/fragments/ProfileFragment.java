package com.digitonics.friendshipdiary.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.activities.ZoomImageActivity;
import com.digitonics.friendshipdiary.adapters.CircularImageAdapter;
import com.digitonics.friendshipdiary.adapters.ConnectionAdapter;
import com.digitonics.friendshipdiary.adapters.GoalAdapter;
import com.digitonics.friendshipdiary.adapters.NewsfeedAdapter;
import com.digitonics.friendshipdiary.adapters.SkillAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.Friend;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.OverlapDecoration;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.widgets.RecyclerViewItemDecorator;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;

@IgnoreApi
public class ProfileFragment extends BaseFragment<UserModel> implements View.OnClickListener{


    public static final String TAG_MY_COURSE = "MyCourse";
    int index = 0;
    private ImageView mProfPicIV;
    private Button mFollow;
    private TextView mUsername,mEmail, mAbout;
    private LinearLayoutManager mImagesManager;
    private RecyclerView mFriendRV;
    private RecyclerView mSkillRV;
    private FlexboxLayoutManager mSkillManager;
    private SkillAdapter mSkillAdapter;
    private ArrayList<String> skills;
    private String userId;
    private TextView mConnection;
    private Intent intent;
    private View mNoSkills;

    public ProfileFragment() {}

    public static ProfileFragment newInstance(int index, String userId) {
        ProfileFragment f = new ProfileFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putInt("index", index);
        args.putString("userId", userId);
        f.setArguments(args);
        return f;
    }
    public static ProfileFragment newInstance(int index, String userId, String follow) {
        ProfileFragment f = new ProfileFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putInt("index", index);
        args.putString("userId", userId);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        callApi();
        return view;
    }

    public void init(View view){
        Bundle args = getArguments();
        if (args != null && args.containsKey("index"))
            index = args.getInt("index", 0);

        mProfPicIV = view.findViewById(R.id.profile_image);
        mFollow = view.findViewById(R.id.follow);
        mUsername = view.findViewById(R.id.username);
        mConnection = view.findViewById(R.id.connection);
        mNoSkills = view.findViewById(R.id.skill_no);
        mEmail = view.findViewById(R.id.email);
        mAbout = view.findViewById(R.id.about_desc);
        mFriendRV = view.findViewById(R.id.friends_rv);
        mImagesManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        mFriendRV.setLayoutManager(mImagesManager);
        view.findViewById(R.id.facebookLink).setOnClickListener(this);
        view.findViewById(R.id.googleLink).setOnClickListener(this);
        view.findViewById(R.id.twitterLink).setOnClickListener(this);
        view.findViewById(R.id.instaLink).setOnClickListener(this);
        mSkillRV = (RecyclerView) view.findViewById(R.id.skill_rv);

        if (BaseActivity.USER != null && index == 1){
            userId = BaseActivity.USER.getId();
            /*mFacebookET.setText(Utils.getString(BaseActivity.USER.getFacebookLink()));
            mTwitterET.setText(Utils.getString(BaseActivity.USER.getTwitterLink()));
            mGoogleET.setText(Utils.getString(BaseActivity.USER.getGmailLink()));
            mInstaET.setText(Utils.getString(BaseActivity.USER.getInstagramLink()));*/


        }else
            userId = args.getString("userId", "");


        mSkillManager = new FlexboxLayoutManager(getActivity());
        mSkillManager.setFlexDirection(FlexDirection.ROW);
        mSkillManager.setFlexWrap(FlexWrap.WRAP);
        mSkillManager.setJustifyContent(JustifyContent.FLEX_START);
        mSkillManager.setAlignItems(AlignItems.FLEX_START);




        ((BaseActivity)getActivity()).setToolBackground();
        if (((BaseActivity)getActivity()).mBottomLL != null && index == 1){
            mFollow.setVisibility(View.GONE);
            ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.VISIBLE);
            ((BaseActivity)getActivity()).setToolBar(R.drawable.icon_menu_white);
        }
        else if (((BaseActivity)getActivity()).mBottomLL != null && index == 0) {
            mFollow.setVisibility(View.VISIBLE);
            ((BaseActivity) getActivity()).mBottomLL.setVisibility(View.GONE);
            ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_back_white_24dp);
        }
        ((BaseActivity)getActivity()).setTitle("Profile");

        setHasOptionsMenu(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                if (index == 1)
                    ((BaseActivity)getActivity()).mDrawerLayout.openDrawer(GravityCompat.START);
                else
                    ((BaseActivity)getActivity()).onBackPressed();
                return true;

            case R.id.edit:
                ((HomeActivity)getActivity()).addFragment(new EditProfileFragment());
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.getUserDetail(BaseActivity.USER.getAccessToken(), userId);
    }
    @Override
    public void run(){
        //((BaseActivity)getActivity()).saveUser(mData);
        ConnectionAdapter circularImageAdapter = new ConnectionAdapter(null, (mData != null ? mData.getFollower(): new ArrayList<UserModel>()));
        Picasso.get().load(mData.getImage() != null ? mData.getImage().get1x() : null).error(R.drawable.user_default).placeholder(R.drawable.user_default).into(mProfPicIV);
        mUsername.setText(StringUtils.capitalize(Utils.getString(mData.getFirstName())) + " " + StringUtils.capitalize(Utils.getString(mData.getLastName())));
        mEmail.setText(Utils.getString(mData.getSocialUsername()));
        mAbout.setText(Utils.getString(mData.getAbout()));
        skills = mData.getQuality();

        if (skills == null)
            skills = new ArrayList<String>();
        if (mData.getFollower().size() > 0)
            mConnection.setText(mData.getFollower().size() + " Connections");
        else
            mConnection.setText("No Connections");

        if (mData.getIsFollow() != null) {
            if (mData.getIsFollow().equalsIgnoreCase("0"))
                mFollow.setText("Follow");
            else
                mFollow.setText("Unfollow");
        }else if (mData.getFollower() != null){
            boolean isFollowing = false;
            for(int i = 0; i < mData.getFollower().size(); i++){
                if (BaseActivity.USER.getId().equalsIgnoreCase(mData.getFollower().get(i).getId())) {
                    isFollowing = true;
                    break;
                }
            }
            if (isFollowing)
                mFollow.setText("Unfollow");
            else
                mFollow.setText("Follow");
        }

        mFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mApiBuilder.follow(BaseActivity.USER.getAccessToken(), mData.getId(), new CustomRunnable() {
                    @Override
                    public void run(Object data) {
                        if (mData.getIsFollow().equalsIgnoreCase("0"))
                            mFollow.setText("Unfollow");
                        else
                            mFollow.setText("Follow");
                    }
                }, getActivity());
            }
        });

        mProfPicIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent videoIntent = new Intent(v.getContext(), ZoomImageActivity.class);
                videoIntent.putExtra("image", mData.getImage() != null ? mData.getImage().get3x() : null);
                startActivity(videoIntent);
            }
        });
        if (skills.size() == 0)
            mNoSkills.setVisibility(View.VISIBLE);
        else
            mNoSkills.setVisibility(View.GONE);
        mSkillAdapter = new SkillAdapter(null, skills, true);
        mSkillRV.setLayoutManager(mSkillManager);
        mSkillRV.setAdapter(mSkillAdapter);
        mFriendRV.setAdapter(circularImageAdapter);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (index == 1)
            inflater.inflate(R.menu.menu_edit_white, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.facebookLink:
                if (mData != null && mData.getFacebookLink() != null && !mData.getFacebookLink().equalsIgnoreCase("")){
                    try {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mData.getFacebookLink()));
                        startActivity(intent);
                    }catch (Exception ex){
                        ex.printStackTrace();
                        ((BaseActivity)getActivity()).showSnack( "No link found");
                    }
                }else{
                    ((BaseActivity)getActivity()).showSnack( "No link found");
                }
            break;
            case R.id.googleLink:
                if (mData != null && mData.getGmailLink() != null && !mData.getGmailLink().equalsIgnoreCase("")){
                    try {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mData.getGmailLink()));
                        startActivity(intent);
                    }catch (Exception ex){
                        ex.printStackTrace();
                        ((BaseActivity)getActivity()).showSnack( "No link found");
                    }
                }else{
                    ((BaseActivity)getActivity()).showSnack( "No link found");
                }
                break;
            case R.id.twitterLink:
                if (mData != null && mData.getTwitterLink() != null && !mData.getTwitterLink().equalsIgnoreCase("")){
                    try {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mData.getTwitterLink()));
                        startActivity(intent);
                    }catch (Exception ex){
                        ex.printStackTrace();
                        ((BaseActivity)getActivity()).showSnack( "No link found");
                    }
                }else{
                    ((BaseActivity)getActivity()).showSnack( "No link found");
                }
                break;
            case R.id.instaLink:
                if (mData != null && mData.getInstagramLink() != null && !mData.getInstagramLink().equalsIgnoreCase("")){
                    try {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mData.getInstagramLink()));
                        startActivity(intent);
                    }catch (Exception ex){
                        ex.printStackTrace();
                        ((BaseActivity)getActivity()).showSnack( "No link found");
                    }
                }else{
                    ((BaseActivity)getActivity()).showSnack( "No link found");
                }
                break;
        }
    }

    public void updateUI(){
        ((BaseActivity)getActivity()).setToolBackground();
        mFollow.setVisibility(View.GONE);
        ((BaseActivity)getActivity()).mBottomLL.setVisibility(View.VISIBLE);
        ((BaseActivity)getActivity()).setToolBar(R.drawable.icon_menu_white);
    }
}
