package com.digitonics.friendshipdiary.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.adapters.InviteFriendAdapter;
import com.digitonics.friendshipdiary.adapters.NewsfeedAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.models.ContactModel;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

@IgnoreApi
public class MyContactFragment extends BaseFragment implements NewsfeedAdapter.OnNewsFeedClickListener {


    public static final String TAG_MY_TASK_FRAGMENT = "MyTaskFragment";
    private Runnable mRunnable;
    private RecyclerView mContactRV, mInviteRV;
    private LinearLayoutManager mFriendsManager, mInviteManager;
    private List<ContactModel> mList;

    public MyContactFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_contacts, container, false);
        init(view);
        return view;
    }

    public void init(View view){
       /* mContactRV = (RecyclerView) view.findViewById(R.id.my_contacts);
        mFriendsManager = new LinearLayoutManager(getActivity());

        ContactAdapter mFriendAdapter = new ContactAdapter(this);

        mContactRV.setLayoutManager(mFriendsManager);
        mContactRV.setAdapter(mFriendAdapter);*/
        View dialogView = getLayoutInflater().inflate(R.layout.progress_bar,null);
        mProgressDialog = Utils.showCustomDialog(getActivity(),dialogView);
        mProgressDialog.setCancelable(false);

        new Thread(new Runnable() {
            @Override
            public void run() {
                mList = Utils.getContacts(getActivity());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgressDialog.dismiss();
                        InviteFriendAdapter mInviteAdapter = new InviteFriendAdapter(null, mList);

                        mInviteRV.setLayoutManager(mInviteManager);
                        mInviteRV.setAdapter(mInviteAdapter);
                    }
                });

            }
        }).start();

        mInviteRV = (RecyclerView) view.findViewById(R.id.invite_friends);
        mInviteManager = new LinearLayoutManager(getActivity());



        setHasOptionsMenu(true);
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
        ((BaseActivity)getActivity()).setTitle("My Contacts");

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                ((BaseActivity)getActivity()).onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return null;
    }

    @Override
    public void onNewsFeedClick(int position) {
    }

    @Override
    public void run() {

    }
}
