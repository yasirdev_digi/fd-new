package com.digitonics.friendshipdiary.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.adapters.GoalFriendsAdapter;
import com.digitonics.friendshipdiary.adapters.SkillAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.models.Friend;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.widgets.RecyclerViewItemDecorator;
import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;

@IgnoreApi
public class EditProfileFragment extends BaseFragment<UserModel> implements View.OnClickListener{


    public static final String TAG_MODULE = "ModuleFragment";
    private Runnable mRunnable;
    private LinearLayoutManager mTaskManager;
    private RecyclerView mTasksRV;
    private RecyclerView mSkillRV;
    private FlexboxLayoutManager mSkillManager;
    private ImageView mProfPicIV;
    private EditText mFNameET, mLNameET, mUsername, mPhoneET, mAboutET, mFacebookET, mTwitterET, mGoogleET, mInstaET;
    private String mProfile = "";
    private SkillAdapter mSkillAdapter;
    private ArrayList<String> skills;
    private EditText mEmailET;

    public EditProfileFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);


        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        return view;
    }
    public void init(View view){

        mProfPicIV = view.findViewById(R.id.profile_image);
        mEmailET = (EditText) view.findViewById(R.id.email);
        mLNameET = (EditText) view.findViewById(R.id.lname);
        mFNameET = (EditText) view.findViewById(R.id.fname);
        mUsername = (EditText) view.findViewById(R.id.username);
        mPhoneET = (EditText) view.findViewById(R.id.phone);
        mAboutET = (EditText) view.findViewById(R.id.about);
        mFacebookET = (EditText) view.findViewById(R.id.facebook_url);
        mTwitterET = (EditText) view.findViewById(R.id.twitter_url);
        mGoogleET = (EditText) view.findViewById(R.id.google_url);
        mInstaET = (EditText) view.findViewById(R.id.instagram_url);
        mSkillRV = (RecyclerView) view.findViewById(R.id.skill_rv);

        if (BaseActivity.USER != null){
            Picasso.get().load(BaseActivity.USER.getImage()!=null?BaseActivity.USER.getImage().get1x():null).placeholder(R.drawable.user_default).into(mProfPicIV);
            mEmailET.setText(Utils.getString(BaseActivity.USER.getEmail()));
            mFNameET.setText(Utils.getString(BaseActivity.USER.getFirstName()));
            mLNameET.setText(Utils.getString(BaseActivity.USER.getLastName()));
            mUsername.setText(Utils.getString(BaseActivity.USER.getSocialUsername()));
            mAboutET.setText(Utils.getString(BaseActivity.USER.getAbout()));
            mPhoneET.setText(Utils.getString(BaseActivity.USER.getPhoneNo()));
            mFacebookET.setText(Utils.getString(BaseActivity.USER.getFacebookLink()));
            mTwitterET.setText(Utils.getString(BaseActivity.USER.getTwitterLink()));
            mGoogleET.setText(Utils.getString(BaseActivity.USER.getGmailLink()));
            mInstaET.setText(Utils.getString(BaseActivity.USER.getInstagramLink()));
            skills = new ArrayList<>(BaseActivity.USER.getQuality());
        }

        mSkillManager = new FlexboxLayoutManager(getActivity());
        mSkillManager.setFlexDirection(FlexDirection.ROW);
        mSkillManager.setFlexWrap(FlexWrap.WRAP);
        mSkillManager.setJustifyContent(JustifyContent.FLEX_START);
        mSkillManager.setAlignItems(AlignItems.FLEX_START);

        if (skills == null)
            skills = new ArrayList<String>();
        skills.add("+ Add");
        mSkillAdapter = new SkillAdapter(null, skills);
        mSkillRV.setLayoutManager(mSkillManager);
        mSkillRV.setAdapter(mSkillAdapter);

        mProfPicIV.setOnClickListener(this);

        ((BaseActivity)getActivity()).setToolBackgroundProfile();
        ((BaseActivity) getActivity()).mBottomLL.setVisibility(View.GONE);
        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);

        TextWatcher textWatcher = new TextWatcher() {
            String text;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.toString().startsWith("https://") && editable.length() == 1){
                    editable.insert(0, "https://");
                }else if(editable.toString().length() <= 8)
                    editable.clear();

            }
        };

        mFacebookET.addTextChangedListener(textWatcher);
        mTwitterET.addTextChangedListener(textWatcher);
        mGoogleET.addTextChangedListener(textWatcher);
        mInstaET.addTextChangedListener(textWatcher);
        ((BaseActivity)getActivity()).setTitle("Edit Profile");
        setHasOptionsMenu(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                Utils.hide(getActivity());
                getActivity().onBackPressed();
                return true;

            case R.id.done:
                if (validateFields() == true) {
                    Utils.hide(getActivity());
                    skills.remove(skills.size()-1);
                    callApi();

                }

                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.userUpdate(mProfile,mFNameET.getText().toString(),mLNameET.getText().toString(),Long.parseLong(mPhoneET.getText().toString()),mAboutET.getText().toString(),
                mUsername.getText().toString(),mFacebookET.getText().toString(),mTwitterET.getText().toString(),mGoogleET.getText().toString(),mInstaET.getText().toString(),
                skills,BaseActivity.USER.getAccessToken());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.add_edit_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private boolean validateFields() {

        if (mFNameET.getText().toString().isEmpty() == true) {
            ((BaseActivity)getActivity()).showSnack( "Please enter first name");
            return false;
        }
        if (mLNameET.getText().toString().isEmpty() == true) {
            ((BaseActivity)getActivity()).showSnack( "Please enter last name");
            return false;
        }
        if (mUsername.getText().toString().isEmpty() == true) {
            ((BaseActivity)getActivity()).showSnack( "Please enter username");
            return false;
        }
        if (mPhoneET.getText().toString().isEmpty() == true) {
            ((BaseActivity)getActivity()).showSnack("Please enter phone no.");
            return false;
        }else if (mPhoneET.getText().toString().length() < 11 || mPhoneET.getText().toString().length() > 14) {
            ((BaseActivity)getActivity()).showSnack("Phone no must be greater than 11 and less 14");
            return false;
        }
        if (mAboutET.getText().toString().isEmpty() == true) {
            ((BaseActivity)getActivity()).showSnack("Please enter about text");
            return false;
        }

        /*if (mProfile.isEmpty() == true) {
            ((BaseActivity)getActivity()).showSnack( "Please select profile picture");
            return false;
        }*/
        if (!mFacebookET.getText().toString().equalsIgnoreCase("")){
            if (!Patterns.WEB_URL.matcher(Utils.getURL(mFacebookET.getText().toString())).matches()){
                ((BaseActivity)getActivity()).showSnack("Please enter proper facebook url");
                return false;
            }

        }
        if (!mGoogleET.getText().toString().equalsIgnoreCase("")){
            if (!Patterns.WEB_URL.matcher(Utils.getURL(mGoogleET.getText().toString())).matches()){
                ((BaseActivity)getActivity()).showSnack("Please enter proper google url");
                return false;
            }

        }
        if (!mInstaET.getText().toString().equalsIgnoreCase("")){
            if (!Patterns.WEB_URL.matcher(Utils.getURL(mInstaET.getText().toString())).matches()){
                ((BaseActivity)getActivity()).showSnack("Please enter proper instagram url");
                return false;
            }

        }
        if (!mTwitterET.getText().toString().equalsIgnoreCase("")){
            if (!Patterns.WEB_URL.matcher(Utils.getURL(mTwitterET.getText().toString())).matches()){
                ((BaseActivity)getActivity()).showSnack("Please enter proper twitter url");
                return false;
            }

        }
        if (skills.size() == 1) {
            ((BaseActivity) getActivity()).showSnack("Please enter skills");
            return false;
        }


        return true;
    }

    public void onMediaLoad(int reqCode, Intent data) {
        switch (reqCode) {
            case Constants.IMAGES_GALLERY:
            case Constants.IMAGES_CAMERA:
                Log.e("TESTY", "mMediaPath = " + ((BaseActivity)mContext).mMediaPath);
                Bitmap bmp = null;
                try {
                    bmp = BitmapFactory.decodeFile(((BaseActivity)mContext).mMediaPath);
                    bmp = Utils.scaleImageKeepAspectRatio(bmp);
                    bmp = Utils.modifyOrientation(bmp,((BaseActivity)mContext).mMediaPath);
                    mProfPicIV.setImageBitmap(bmp);
                    mProfile = ((BaseActivity)mContext).mMediaPath;
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                Picasso.with(mContext)
//                        .load(Uri.fromFile())
//                        .into(mAddDocIV);
                break;

            case Constants.VIDEO_CAMERA:
            case Constants.VIDEO_GALLERY:
                Log.e("TESTY", "mMediaPath = " + ((BaseActivity)mContext).mMediaPath);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_image:
                ((BaseActivity) mContext).showMediaPicker();
                break;
        }
    }

    @Override
    public void run() {
        super.run();
        ((BaseActivity)getActivity()).saveUser(mData);
        BaseActivity.USER = mData;
        getActivity().onBackPressed();
        ((BaseActivity)getActivity()).showSnack("Profile updated successfully");
    }
}
