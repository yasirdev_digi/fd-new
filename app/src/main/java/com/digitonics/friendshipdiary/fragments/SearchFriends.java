package com.digitonics.friendshipdiary.fragments;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.adapters.FriendsAdapter;
import com.digitonics.friendshipdiary.adapters.NewsfeedAdapter;
import com.digitonics.friendshipdiary.adapters.UsersAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.widgets.RecyclerViewItemDecorator;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;


@IgnoreApi
public class SearchFriends extends BaseFragment<ArrayList<UserModel>> implements View.OnClickListener, NewsfeedAdapter.OnNewsFeedClickListener {


    public static final String TAG_MY_FRIEND = "MyFriend";
    private Runnable mRunnable;
    private RecyclerView mFriendRV, mAchorRV;
    private LinearLayoutManager mFriendManager, mAchorManager;
    private EditText mSearchEdit;
    private View mNoData;

    public SearchFriends() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_friends, container, false);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        callApi();
        return view;
    }

    public void init(View view) {

        mFriendRV = (RecyclerView) view.findViewById(R.id.friends_rv);
        mSearchEdit = (EditText) view.findViewById(R.id.search_edit_text);
        mNoData = view.findViewById(R.id.no_data_img);


        if (((BaseActivity) getActivity()).mBottomLL != null)
            ((BaseActivity) getActivity()).mBottomLL.setVisibility(View.GONE);
        getActivity().findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
        view.findViewById(R.id.create_goal).setVisibility(View.GONE);

        mFriendManager = new LinearLayoutManager(getActivity()) {
            @Override
            public boolean requestChildRectangleOnScreen(RecyclerView parent, View child, Rect rect, boolean immediate, boolean focusedChildVisible) {

                return false;
            }
        };
        mFriendRV.addItemDecoration(new RecyclerViewItemDecorator(0));

        setHasOptionsMenu(true);
        ((BaseActivity) getActivity()).setDefaultBackground();
        ((BaseActivity) getActivity()).setTitle("Search Friends");
        ((BaseActivity) getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);


    }

    @Override
    public void onClick(View view) {
        ((HomeActivity) getActivity()).addFragment(new GoalDetailFragment());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                ((BaseActivity) getActivity()).onBackPressed();
                Utils.hideSoftKeyboard(mSearchEdit);
                return true;

            case R.id.search:
                mSearchEdit.setVisibility(mSearchEdit.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                if (mSearchEdit.getVisibility() == View.GONE)
                    Utils.hideSoftKeyboard(mSearchEdit);
                /*Toast.makeText(getActivity(), "Search feature not implemented yet", Toast.LENGTH_SHORT).show();*/
                return true;
        }
        return false;
    }

    @Override
    public void onNewsFeedClick(int position) {
        ((HomeActivity) getActivity()).addFragment(new ProfileFragment());
        ((BaseActivity) getActivity()).mBottomLL.setVisibility(View.GONE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.allUser(BaseActivity.USER.getAccessToken());
    }

    @Override
    public void run() {
        /*mFriendsManager = new LinearLayoutManager(getActivity());

        AddFriendAdapter mFriendAdapter = new AddFriendAdapter(this, mData);

        mFriendsRV.setLayoutManager(mFriendsManager);
        mFriendsRV.setAdapter(mFriendAdapter);*/
        if (mData != null && mData.size() > 0) {
            mNoData.setVisibility(View.GONE);
            final UsersAdapter friendsAdapter = new UsersAdapter(this, mData, mNoData);

            mFriendRV.setLayoutManager(mFriendManager);
            mFriendRV.setAdapter(friendsAdapter);
            mSearchEdit.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    friendsAdapter.getFilter().filter(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            Utils.animRecyclerView(mFriendRV, Utils.LIST_ANIM_FROM_TOP);
        } else {
            mNoData.setVisibility(View.VISIBLE);
        }
    }
}
