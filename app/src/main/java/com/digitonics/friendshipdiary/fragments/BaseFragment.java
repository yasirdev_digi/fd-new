package com.digitonics.friendshipdiary.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.annotations.HideLoader;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.webservice.ApiBuilder;
import com.digitonics.friendshipdiary.webservice.WebResponse;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Syed Kamil Hafeez on 10/16/2017.
 */
public abstract class BaseFragment<T> extends Fragment implements Callback,Runnable{

//    public static final String PREFERENCE_KEY = "prefs";

    public String mFromTime;
    public String mToTime;
    public ApiBuilder mApiBuilder;
    protected T mData;
    protected WebResponse<T> mBody;
    protected Dialog mProgressDialog;
    protected View mBaseView;
    protected NavigationView mNavigationView;
    protected CoordinatorLayout mParentCoordinatorLayout;
    protected Context mContext;
    protected SharedPreferences mPreferences;
    private Call<?> mCall;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mPreferences = mContext.getSharedPreferences(BaseActivity.PREFERENCE_KEY, MODE_PRIVATE);
        mApiBuilder = ApiBuilder.getInstance(mContext);
        if(((BaseActivity)getActivity()).mUserImg != null && BaseActivity.USER != null){
            Picasso.get().load(BaseActivity.USER.getImage() != null ? BaseActivity.USER.getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(((BaseActivity)getActivity()).mUserImg);
            if (((BaseActivity)getActivity()).mUserText != null)
                ((BaseActivity)getActivity()).mUserText.setText(StringUtils.capitalize(BaseActivity.USER.getFirstName()) + " " + StringUtils.capitalize(BaseActivity.USER.getLastName()));
        }
        if (!this.getClass().isAnnotationPresent(IgnoreApi.class))
            callApi();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home: {

                ((BaseActivity)getActivity()).mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            }
        }
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }
        }
    }


    public void callApi() {

        dismiss();
        if (!this.getClass().isAnnotationPresent(HideLoader.class)) {


            View view = getLayoutInflater().inflate(R.layout.progress_bar,null);
            mProgressDialog = Utils.showCustomDialog(getActivity(),view);
            mProgressDialog.setCancelable(false);
        }
        try {
            if (mCall != null && mCall.isExecuted())
                mCall.cancel();

            mCall = getCall();
            mCall.enqueue(this);

        } catch (IOException ex) {
            ex.printStackTrace();
            ((BaseActivity)mContext).onFailure(null, ex);
        }
    }


    protected void dismiss() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    public void showSnack(String message) {
        /*Snackbar snackbar = Snackbar.make(mParentCoordinatorLayout, message, Snackbar.LENGTH_SHORT);
        snackbar.show();*/
        ((BaseActivity)getActivity()).showSnack(message);
    }

    protected String getMessage() {
        return mContext.getResources().getString(R.string.loading);
    }

    public void onStart() {
        super.onStart();
    }

    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    protected abstract Call<?> getCall() throws IOException;
    @Override
    public void onResponse(Call call, Response response) {
        try {
            Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag(InternetFailure.TAG_INTERNET_FAILURE);

            if (fragment != null) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
            }
            /*mBody = (WebResponse<T>) response.body();

            if (mBody.getStatus() != 0) {
                mData = mBody.getData();
                run();
            } else
                showSnack(mBody.getMessage());*/
            if (response.isSuccessful()) {
                mBody = (WebResponse<T>) response.body();
                if (mBody.getResponse().getData() != null)
                    mData = (T) mBody.getResponse().getData();
                run();
            } else {
                Converter<ResponseBody, WebResponse> converter = mApiBuilder.mRetrofit.responseBodyConverter(WebResponse.class, new Annotation[0]);
                WebResponse error = converter.convert(response.errorBody());
                if (error.getError().getMessage() instanceof ArrayList){
                    ArrayList message = (ArrayList)error.getError().getMessage();
                    if (message.size() > 0)
                        showSnack(message.get(0).toString());
                }else
                    showSnack(error.getError().getMessage().toString());

            }
            dismiss();
        } catch (Exception ex) {
            onFailure(null, ex);
        }

    }

    @Override
    public void onFailure(Call call, Throwable t) {
        if (t instanceof UnknownHostException || t instanceof SocketTimeoutException) {
            Fragment fragment = null;
            if (getActivity().getSupportFragmentManager() == null){
                 fragment = getActivity().getSupportFragmentManager().findFragmentByTag(InternetFailure.TAG_INTERNET_FAILURE);
            }

            if (fragment == null) {
                InternetFailure failureFragment = new InternetFailure();
                failureFragment.setRunnable(new Runnable() {
                    @Override
                    public void run() {
                        callApi();
                    }
                });
                getActivity().getSupportFragmentManager().beginTransaction().add(android.R.id.content, failureFragment, InternetFailure.TAG_INTERNET_FAILURE).
                        commitAllowingStateLoss();
            } else {
                /*if (customOnFailure() != null) {
                    customOnFailure().run();
                } else*/
                    showSnack(getString(R.string.no_internet));
            }
        }
        //else
        //    showSnack(getString(R.string.exception));
        dismiss();
    }

    @Override
    public void run() {

    }
}
