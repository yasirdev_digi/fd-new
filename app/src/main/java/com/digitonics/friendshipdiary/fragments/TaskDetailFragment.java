package com.digitonics.friendshipdiary.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.activities.HomeActivity;
import com.digitonics.friendshipdiary.adapters.CircularImageAdapter;
import com.digitonics.friendshipdiary.adapters.GoalAdapter;
import com.digitonics.friendshipdiary.adapters.ProofAdapter;
import com.digitonics.friendshipdiary.adapters.TaskListAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.models.Friend;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.NewsFeedModel;
import com.digitonics.friendshipdiary.models.ProofModel;
import com.digitonics.friendshipdiary.models.TaskModel;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.digitonics.friendshipdiary.utilities.OverlapDecoration;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.vincent.videocompressor.VideoCompress;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;

@IgnoreApi
public class TaskDetailFragment extends BaseFragment<TaskModel> implements  ProofAdapter.OnAddProofClickListener{


    public static final String TAG_MODULE = "ModuleFragment";
    TaskModel mTaskModel;
    private View mMainCL;
    private ProofAdapter mTaskAdapter;
    private int mSelectedPos;
    private TextView taskName, taskDesc, taskLevel, taskReward, taskVotes, taskSno, taskStatus, createdBy,taskProve;
    LinearLayout ll, votesll;
    ImageView taskEdit;
    RecyclerView imageRV;
    private int mPos;



    public static TaskDetailFragment newInstance(TaskModel task, int pos) {
        TaskDetailFragment f = new TaskDetailFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putParcelable("task", task);
        args.putInt("pos", pos);
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_task_detail, container, false);

       /* if (args != null && args.containsKey("index"))
            index = args.getInt("index", 0);
        else */


        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        init(view);
        callApi();
        return view;
    }
    public void init(View view){
        taskName =  view.findViewById(R.id.task_name);
        taskDesc =  view.findViewById(R.id.task_desc);
        taskLevel =  view.findViewById(R.id.task_level);
        taskReward =  view.findViewById(R.id.reward);
        taskVotes =  view.findViewById(R.id.votes);
        createdBy =  view.findViewById(R.id.created_by);
        taskSno =  view.findViewById(R.id.sno);
        ll = view.findViewById(R.id.reward_item);
        votesll = view.findViewById(R.id.votes_ll);
        taskStatus = view.findViewById(R.id.task_status);
        taskEdit = view.findViewById(R.id.edit);
        taskProve = view.findViewById(R.id.textView2);
        imageRV = view.findViewById(R.id.image_proof);
        mMainCL = view.findViewById(R.id.mainCL);
        ((BaseActivity)getActivity()).setDefaultBackground();

        setHasOptionsMenu(true);

        ((BaseActivity)getActivity()).setToolBar(R.drawable.ic_arrow_back_white_24dp);
        ((BaseActivity)getActivity()).setTitle("Task Detail");

        try {
            Bundle args = getArguments();
            if(args != null && args.containsKey("task")){
                mTaskModel = args.getParcelable("task");
                mPos = args.getInt("pos");
            }

            /*taskStatus.setText(mTaskModel.getStatus());
            taskName.setText(mTaskModel.getName());
            taskDesc.setText(mTaskModel.getDescription());
            taskLevel.setText(Utils.getTaskLevel(mTaskModel.getTaskType()));
            taskReward.setText(mTaskModel.getRewards());
            taskSno.setText((mPos + 1)+ "");
            taskVotes.setText(mTaskModel.getTotalVotes());

//            ArrayList<ProofModel> proofList = new ArrayList<>();
//            proofList.addAll(mTasks.get(position).getProves());


            StringBuilder sb = new StringBuilder();
            sb.append("Task Created by: ");
            sb.append(mTaskModel.getUser().getFirstName() + " " + mTaskModel.getUser().getLastName() != null ? mTaskModel.getUser().getLastName() : "");
            createdBy.setText(sb.toString());

            if (BaseActivity.USER.getId().equalsIgnoreCase(mTaskModel.getUser().getId()))
                taskEdit.setVisibility(View.VISIBLE);
            else
                taskEdit.setVisibility(View.GONE);

            taskEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int)v.getTag();
                    ((BaseActivity)v.getContext()).addFragment(CreateTaskFragment.newInstance(mTaskModel));
                }
            });

            LinearLayoutManager mImagesManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
            imageRV.setLayoutManager(mImagesManager);
            ArrayList<ProofModel> newArr= new ArrayList();
            newArr.addAll(mTaskModel.getProves());
            newArr.add(new ProofModel());
            mTaskAdapter = new ProofAdapter(this,newArr , mPos , true);

            imageRV.setLayoutManager(mImagesManager);
            imageRV.setAdapter(mTaskAdapter);*/


        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    protected Call<?> getCall() throws IOException {

        return mApiBuilder.getTaskDetail(BaseActivity.USER.getAccessToken(), mTaskModel.getId());
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onAddProofClick(int pos) {
        mSelectedPos = pos;
        ((BaseActivity)getActivity()).showImageVideoDialog();
    }

    public void onMediaLoad(int reqCode, Intent data) {
        switch (reqCode) {
            case Constants.IMAGES_GALLERY:
            case Constants.IMAGES_CAMERA:
                Log.e("TESTY", "mMediaPath = " + ((BaseActivity)mContext).mMediaPath);
                Bitmap bmp = null;
                try {
                   /* bmp = BitmapFactory.decodeFile(((BaseActivity)mContext).mMediaPath);
                    bmp = Utils.scaleImageKeepAspectRatio(bmp);
                    bmp = Utils.modifyOrientation(bmp,((BaseActivity)mContext).mMediaPath);
                    mProfPicIV.setImageBitmap(bmp);*/
                   mApiBuilder.addProve(BaseActivity.USER.getAccessToken(), ((BaseActivity) mContext).mMediaPath, mTaskModel.getId(), new CustomRunnable() {
                       @Override
                       public void run(Object data) {
                           Log.e("TESTY", "mMediaPath = " + data);
                           if (data != null && ((ProofModel)data).getId() != null) {
                               mTaskAdapter.setData(mSelectedPos, (ProofModel) data);
                           }else if(data != null){
                               showPackageDialog(((ProofModel)data).getToken());
                           }
                       }
                   }, getActivity());
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                Picasso.with(mContext)
//                        .load(Uri.fromFile())
//                        .into(mAddDocIV);
                break;

            case Constants.VIDEO_CAMERA:
            case Constants.VIDEO_GALLERY:
                if (((BaseActivity)mContext).mMediaPath != null) {
                    View dialogView = getLayoutInflater().inflate(R.layout.progress_bar, null);
                    mProgressDialog = Utils.showCustomDialog(getActivity(), dialogView);
                    mProgressDialog.setCancelable(false);
                    final File f = new File(getActivity().getExternalFilesDir(null).toString() + "_" + new Date().getTime() + ".mp4");
                    VideoCompress.compressVideoLow(((BaseActivity) mContext).mMediaPath, f.getAbsolutePath(), new VideoCompress.CompressListener() {
                        @Override
                        public void onStart() {
                            //Start Compress
                        }

                        @Override
                        public void onSuccess() {
                            mProgressDialog.dismiss();
                            try {
                                mApiBuilder.addVideoProve(BaseActivity.USER.getAccessToken(), f.getAbsolutePath(), mTaskModel.getId(), new CustomRunnable() {
                                    @Override
                                    public void run(Object data) {
                                        Log.e("TESTY", "mMediaPath = " + data);
                                        if (data != null && ((ProofModel) data).getId() != null) {
                                            mTaskAdapter.setData(mSelectedPos, (ProofModel) data);
                                        } else if (data != null) {
                                            showPackageDialog(((ProofModel) data).getToken());
                                        }
                                    }
                                }, getActivity());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFail() {
                            mProgressDialog.dismiss();
                        }

                        @Override
                        public void onProgress(float percent) {
                            //Progress
                        }
                    });
                }else{
                    showSnack("Google drive photos cannot be selected!");
                }
                break;
        }
    }

    @Override
    public void run() {
        try {

            mTaskModel = mData;
            taskStatus.setText(mTaskModel.getStatus());
            taskName.setText(mTaskModel.getName());
            taskDesc.setText(mTaskModel.getDescription());
            taskLevel.setText(Utils.getTaskLevel(mTaskModel.getTaskType()));
            taskReward.setText(mTaskModel.getRewards());
            taskSno.setText((mPos + 1)+ "");
            taskVotes.setText(mTaskModel.getTotalVotes());

//            ArrayList<ProofModel> proofList = new ArrayList<>();
//            proofList.addAll(mTasks.get(position).getProves());


            StringBuilder sb = new StringBuilder();
            sb.append("Task Created by: ");
            sb.append(StringUtils.capitalize(mTaskModel.getUser().getFirstName()) + " " + StringUtils.capitalize(mTaskModel.getUser().getLastName() != null ? mTaskModel.getUser().getLastName() : ""));
            createdBy.setText(sb.toString());

            if (BaseActivity.USER.getId().equalsIgnoreCase(mTaskModel.getUser().getId()))
                taskEdit.setVisibility(View.VISIBLE);
            else
                taskEdit.setVisibility(View.GONE);

            taskEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int)v.getTag();
                    ((BaseActivity)v.getContext()).addFragment(CreateTaskFragment.newInstance(mTaskModel));
                }
            });

            LinearLayoutManager mImagesManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
            imageRV.setLayoutManager(mImagesManager);
            ArrayList<ProofModel> newArr= new ArrayList();
            newArr.addAll(mTaskModel.getProves());
            newArr.add(new ProofModel());
            mTaskAdapter = new ProofAdapter(this,newArr , mPos , true);

            imageRV.setLayoutManager(mImagesManager);
            imageRV.setAdapter(mTaskAdapter);
            mMainCL.setVisibility(View.VISIBLE);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void showPackageDialog(final String token) {

        View view = View.inflate(getActivity(), R.layout.dialog_package, null);
        final Dialog dialog = Utils.showCustomDialog(getActivity(), view);
        final RadioGroup voteGrp = view.findViewById(R.id.vote_radio_grp);
        final TextView submitBT = (TextView) view.findViewById(R.id.submit);
        //submitBT.setTag(pos);
        ImageView closeIV = (ImageView) view.findViewById(R.id.diag_dept_closeIV);

        closeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos, taskLevel = 0;
                String amount = "3.99";
                Utils.hideSoftKeyboard(submitBT);
                dialog.dismiss();

                switch (voteGrp.getCheckedRadioButtonId()){
                    case R.id.easy:
                        taskLevel = 1;
                        amount = "3.99";
                        break;
                    case R.id.medium:
                        taskLevel = 2;
                        amount = "7.99";
                        break;
                    case R.id.hard:
                        taskLevel = 3;
                        amount = "9.99";
                        break;
                }
                Intent in = new Intent();
                in.putExtra("type", "proof");
                in.putExtra("amount", amount);
                in.putExtra("package_id", taskLevel  + "");
                ((BaseActivity)getActivity()).initPayment(token,in);

            }
        });

        dialog.show();


    }
}
