package com.digitonics.friendshipdiary.models;

/**
 * Created by syed.kamil on 4/12/2018.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserModel implements Parcelable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("parent_id")
    @Expose
    private String parentId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("access_token")
    @Expose
    private String accessToken;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("login_at")
    @Expose
    private String loginAt;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("is_active")
    @Expose
    private String isActive;
    @SerializedName("remember_token")
    @Expose
    private String rememberToken;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("notification")
    @Expose
    private String notification;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("join_date")
    @Expose
    private String joinDate;
    @SerializedName("social_id")
    @Expose
    private String socialId;
    @SerializedName("social_username")
    @Expose
    private String socialUsername;
    @SerializedName("image_url")
    @Expose
    private Image image;
    @SerializedName("feedback")
    @Expose
    private List<String> feedback = null;
    @SerializedName("proof")
    @Expose
    private Proof proof;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("qualities")
    @Expose
    private String qualities;
    @SerializedName("facebook_link")
    @Expose
    private String facebookLink;
    @SerializedName("twitter_link")
    @Expose
    private String twitterLink;
    @SerializedName("gmail_link")
    @Expose
    private String gmailLink;
    @SerializedName("social_image_url")
    @Expose
    private String socialImageUrl;
    @SerializedName("instagram_link")
    @Expose
    private String instagramLink;
    @SerializedName("quality")
    @Expose
    private ArrayList<String> quality = null;
    @SerializedName("goals")
    @Expose
    private ArrayList<GoalModel> goals = null;
    @SerializedName("rewards")
    @Expose
    private String rewards;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("following")
    @Expose
    private String following;
    @SerializedName("is_follow")
    @Expose
    private String isFollow;
    @SerializedName("follow")
    @Expose
    private ArrayList<UserModel> follower= null;

    private boolean isChecked;

    private String role_id;

    private String roleType;

    private String role;
    private int oldPosition;


    public UserModel() {
    }


    protected UserModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        parentId = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        password = in.readString();
        accessToken = in.readString();
        code = in.readString();
        loginAt = in.readString();
        phoneNo = in.readString();
        address = in.readString();
        isActive = in.readString();
        rememberToken = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        deletedAt = in.readString();
        notification = in.readString();
        userType = in.readString();
        joinDate = in.readString();
        socialId = in.readString();
        socialUsername = in.readString();
        feedback = in.createStringArrayList();
        about = in.readString();
        qualities = in.readString();
        facebookLink = in.readString();
        twitterLink = in.readString();
        gmailLink = in.readString();
        socialImageUrl = in.readString();
        instagramLink = in.readString();
        quality = in.createStringArrayList();
        goals = in.createTypedArrayList(GoalModel.CREATOR);
        rewards = in.readString();
        points = in.readString();
        following = in.readString();
        isFollow = in.readString();
        follower = in.createTypedArrayList(UserModel.CREATOR);
        isChecked = in.readByte() != 0;
        role_id = in.readString();
        roleType = in.readString();
        role = in.readString();
        oldPosition = in.readInt();
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLoginAt() {
        return loginAt;
    }

    public void setLoginAt(String loginAt) {
        this.loginAt = loginAt;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public List<String> getFeedback() {
        return feedback;
    }

    public void setFeedback(List<String> feedback) {
        this.feedback = feedback;
    }

    public Proof getProof() {
        return proof;
    }

    public void setProof(Proof proof) {
        this.proof = proof;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getSocialUsername() {
        return socialUsername;
    }

    public void setSocialUsername(String socialUsername) {
        this.socialUsername = socialUsername;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getQualities() {
        return qualities;
    }

    public void setQualities(String qualities) {
        this.qualities = qualities;
    }


    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getGmailLink() {
        return gmailLink;
    }

    public void setGmailLink(String gmailLink) {
        this.gmailLink = gmailLink;
    }

    public String getSocialImageUrl() {
        return socialImageUrl;
    }

    public void setSocialImageUrl(String socialImageUrl) {
        this.socialImageUrl = socialImageUrl;
    }

    public String getInstagramLink() {
        return instagramLink;
    }

    public void setInstagramLink(String instagramLink) {
        this.instagramLink = instagramLink;
    }

    public ArrayList<String> getQuality() {
        return quality;
    }

    public void setQuality(ArrayList<String> quality) {
        this.quality = quality;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public ArrayList<GoalModel> getGoals() {
        return goals;
    }

    public void setGoals(ArrayList<GoalModel> goals) {
        this.goals = goals;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    public ArrayList<UserModel> getFollower() {
        return follower;
    }

    public void setFollower(ArrayList<UserModel> follower) {
        this.follower = follower;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }

    public int getOldPosition() {
        return oldPosition;
    }

    public void setOldPosition(int oldPosition) {
        this.oldPosition = oldPosition;
    }

    public String getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(String isFollow) {
        this.isFollow = isFollow;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(parentId);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(accessToken);
        dest.writeString(code);
        dest.writeString(loginAt);
        dest.writeString(phoneNo);
        dest.writeString(address);
        dest.writeString(isActive);
        dest.writeString(rememberToken);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(deletedAt);
        dest.writeString(notification);
        dest.writeString(userType);
        dest.writeString(joinDate);
        dest.writeString(socialId);
        dest.writeString(socialUsername);
        dest.writeStringList(feedback);
        dest.writeString(about);
        dest.writeString(qualities);
        dest.writeString(facebookLink);
        dest.writeString(twitterLink);
        dest.writeString(gmailLink);
        dest.writeString(socialImageUrl);
        dest.writeString(instagramLink);
        dest.writeStringList(quality);
        dest.writeTypedList(goals);
        dest.writeString(rewards);
        dest.writeString(points);
        dest.writeString(following);
        dest.writeString(isFollow);
        dest.writeTypedList(follower);
        dest.writeByte((byte) (isChecked ? 1 : 0));
        dest.writeString(role_id);
        dest.writeString(roleType);
        dest.writeString(role);
        dest.writeInt(oldPosition);
    }

    public class Image {

        @SerializedName("1x")
        @Expose
        private String image1x;
        @SerializedName("2x")
        @Expose
        private String image2x;
        @SerializedName("3x")
        @Expose
        private String image3x;

        public String get1x() {
            return image1x;
        }

        public void set1x(String image1x) {
            this.image1x = image1x;
        }

        public String get2x() {
            return image2x;
        }

        public void set2x(String image2x) {
            this.image2x = image2x;
        }

        public String get3x() {
            return image3x;
        }

        public void set3x(String image3x) {
            this.image3x = image3x;
        }

    }

    public class Proof {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("licence_proof")
        @Expose
        private String licenceProof;
        @SerializedName("security_no")
        @Expose
        private String securityNo;
        @SerializedName("driver_licence")
        @Expose
        private String driverLicence;
        @SerializedName("licence_issued_on")
        @Expose
        private String licenceIssuedOn;
        @SerializedName("licence_expired_on")
        @Expose
        private String licenceExpiredOn;
        @SerializedName("vehicle_proof_id")
        @Expose
        private String vehicleProofId;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("vehicle_proof")
        @Expose
        private VehicleProof vehicleProof;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getLicenceProof() {
            return licenceProof;
        }

        public void setLicenceProof(String licenceProof) {
            this.licenceProof = licenceProof;
        }

        public String getSecurityNo() {
            return securityNo;
        }

        public void setSecurityNo(String securityNo) {
            this.securityNo = securityNo;
        }

        public String getDriverLicence() {
            return driverLicence;
        }

        public void setDriverLicence(String driverLicence) {
            this.driverLicence = driverLicence;
        }

        public String getLicenceIssuedOn() {
            return licenceIssuedOn;
        }

        public void setLicenceIssuedOn(String licenceIssuedOn) {
            this.licenceIssuedOn = licenceIssuedOn;
        }

        public String getLicenceExpiredOn() {
            return licenceExpiredOn;
        }

        public void setLicenceExpiredOn(String licenceExpiredOn) {
            this.licenceExpiredOn = licenceExpiredOn;
        }

        public String getVehicleProofId() {
            return vehicleProofId;
        }

        public void setVehicleProofId(String vehicleProofId) {
            this.vehicleProofId = vehicleProofId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public VehicleProof getVehicleProof() {
            return vehicleProof;
        }

        public void setVehicleProof(VehicleProof vehicleProof) {
            this.vehicleProof = vehicleProof;
        }

    }

    public class VehicleProof {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("vehicle_type")
        @Expose
        private String vehicleType;
        @SerializedName("vehicle_registration_no")
        @Expose
        private String vehicleRegistrationNo;
        @SerializedName("vehicle_model_no")
        @Expose
        private String vehicleModelNo;
        @SerializedName("vehicle_color")
        @Expose
        private String vehicleColor;
        @SerializedName("vehicle_issue_date")
        @Expose
        private String vehicleIssueDate;
        @SerializedName("vehicle_registration_proof")
        @Expose
        private String vehicleRegistrationProof;
        @SerializedName("vehicle_permit_no")
        @Expose
        private String vehiclePermitNo;
        @SerializedName("vehicle_permit_proof")
        @Expose
        private String vehiclePermitProof;
        @SerializedName("vehicle_permit_issue_date")
        @Expose
        private String vehiclePermitIssueDate;
        @SerializedName("vehicle_permit_last_date")
        @Expose
        private String vehiclePermitLastDate;
        @SerializedName("vehicle_insurance_no")
        @Expose
        private String vehicleInsuranceNo;
        @SerializedName("vehicle_insurance_proof")
        @Expose
        private String vehicleInsuranceProof;
        @SerializedName("vehicle_insurance_issue_date")
        @Expose
        private String vehicleInsuranceIssueDate;
        @SerializedName("vehicle_insurance_last_date")
        @Expose
        private String vehicleInsuranceLastDate;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("size_of_tailor")
        @Expose
        private String sizeOfTailor;
        @SerializedName("moving_year")
        @Expose
        private String movingYear;
        @SerializedName("moving_make")
        @Expose
        private String movingMake;
        @SerializedName("moving_model_no")
        @Expose
        private String movingModelNo;
        @SerializedName("moving_vin")
        @Expose
        private String movingVin;
        @SerializedName("vehicle")
        @Expose
        private String vehicle;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVehicleType() {
            return vehicleType;
        }

        public void setVehicleType(String vehicleType) {
            this.vehicleType = vehicleType;
        }

        public String getVehicleRegistrationNo() {
            return vehicleRegistrationNo;
        }

        public void setVehicleRegistrationNo(String vehicleRegistrationNo) {
            this.vehicleRegistrationNo = vehicleRegistrationNo;
        }

        public String getVehicleModelNo() {
            return vehicleModelNo;
        }

        public void setVehicleModelNo(String vehicleModelNo) {
            this.vehicleModelNo = vehicleModelNo;
        }

        public String getVehicleColor() {
            return vehicleColor;
        }

        public void setVehicleColor(String vehicleColor) {
            this.vehicleColor = vehicleColor;
        }

        public String getVehicleIssueDate() {
            return vehicleIssueDate;
        }

        public void setVehicleIssueDate(String vehicleIssueDate) {
            this.vehicleIssueDate = vehicleIssueDate;
        }

        public String getVehicleRegistrationProof() {
            return vehicleRegistrationProof;
        }

        public void setVehicleRegistrationProof(String vehicleRegistrationProof) {
            this.vehicleRegistrationProof = vehicleRegistrationProof;
        }

        public String getVehiclePermitNo() {
            return vehiclePermitNo;
        }

        public void setVehiclePermitNo(String vehiclePermitNo) {
            this.vehiclePermitNo = vehiclePermitNo;
        }

        public String getVehiclePermitProof() {
            return vehiclePermitProof;
        }

        public void setVehiclePermitProof(String vehiclePermitProof) {
            this.vehiclePermitProof = vehiclePermitProof;
        }

        public String getVehiclePermitIssueDate() {
            return vehiclePermitIssueDate;
        }

        public void setVehiclePermitIssueDate(String vehiclePermitIssueDate) {
            this.vehiclePermitIssueDate = vehiclePermitIssueDate;
        }

        public String getVehiclePermitLastDate() {
            return vehiclePermitLastDate;
        }

        public void setVehiclePermitLastDate(String vehiclePermitLastDate) {
            this.vehiclePermitLastDate = vehiclePermitLastDate;
        }

        public String getVehicleInsuranceNo() {
            return vehicleInsuranceNo;
        }

        public void setVehicleInsuranceNo(String vehicleInsuranceNo) {
            this.vehicleInsuranceNo = vehicleInsuranceNo;
        }

        public String getVehicleInsuranceProof() {
            return vehicleInsuranceProof;
        }

        public void setVehicleInsuranceProof(String vehicleInsuranceProof) {
            this.vehicleInsuranceProof = vehicleInsuranceProof;
        }

        public String getVehicleInsuranceIssueDate() {
            return vehicleInsuranceIssueDate;
        }

        public void setVehicleInsuranceIssueDate(String vehicleInsuranceIssueDate) {
            this.vehicleInsuranceIssueDate = vehicleInsuranceIssueDate;
        }

        public String getVehicleInsuranceLastDate() {
            return vehicleInsuranceLastDate;
        }

        public void setVehicleInsuranceLastDate(String vehicleInsuranceLastDate) {
            this.vehicleInsuranceLastDate = vehicleInsuranceLastDate;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getSizeOfTailor() {
            return sizeOfTailor;
        }

        public void setSizeOfTailor(String sizeOfTailor) {
            this.sizeOfTailor = sizeOfTailor;
        }

        public String getMovingYear() {
            return movingYear;
        }

        public void setMovingYear(String movingYear) {
            this.movingYear = movingYear;
        }

        public String getMovingMake() {
            return movingMake;
        }

        public void setMovingMake(String movingMake) {
            this.movingMake = movingMake;
        }

        public String getMovingModelNo() {
            return movingModelNo;
        }

        public void setMovingModelNo(String movingModelNo) {
            this.movingModelNo = movingModelNo;
        }

        public String getMovingVin() {
            return movingVin;
        }

        public void setMovingVin(String movingVin) {
            this.movingVin = movingVin;
        }

        public String getVehicle() {
            return vehicle;
        }

        public void setVehicle(String vehicle) {
            this.vehicle = vehicle;
        }

    }


}
