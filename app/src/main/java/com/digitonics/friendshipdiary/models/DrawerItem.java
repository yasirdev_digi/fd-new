package com.digitonics.friendshipdiary.models;

/**
 * Created by apple on 10/22/17.
 */

public class DrawerItem {
    private int icon;
    private String title;
    public int getIcon() {
        return icon;
    }
    public void setIcon(int icon) {
        this.icon = icon;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
}
