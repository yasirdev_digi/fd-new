package com.digitonics.friendshipdiary.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by syed.kamil on 6/4/2018.
 */

public class PaymentModel {

    @SerializedName("task")
    @Expose
    private ArrayList<TaskModel> task = null;
    @SerializedName("total")
    @Expose
    private String total;

    public ArrayList<TaskModel> getTask() {
        return task;
    }

    public void setTask(ArrayList<TaskModel> task) {
        this.task = task;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

}