package com.digitonics.friendshipdiary.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by syed.kamil on 5/23/2018.
 */


public class OtherGoalModel implements Parcelable{

    @SerializedName("sponsor")
    @Expose
    private ArrayList<GoalModel> sponsor = null;
    @SerializedName("watcher")
    @Expose
    private ArrayList<GoalModel> watcher = null;
    @SerializedName("supporter")
    @Expose
    private ArrayList<GoalModel> supporter = null;


    protected OtherGoalModel(Parcel in) {
        sponsor = in.createTypedArrayList(GoalModel.CREATOR);
        watcher = in.createTypedArrayList(GoalModel.CREATOR);
        supporter = in.createTypedArrayList(GoalModel.CREATOR);
    }

    public static final Creator<OtherGoalModel> CREATOR = new Creator<OtherGoalModel>() {
        @Override
        public OtherGoalModel createFromParcel(Parcel in) {
            return new OtherGoalModel(in);
        }

        @Override
        public OtherGoalModel[] newArray(int size) {
            return new OtherGoalModel[size];
        }
    };

    public ArrayList<GoalModel> getSponsor() {
        return sponsor;
    }

    public void setSponsor(ArrayList<GoalModel> sponsor) {
        this.sponsor = sponsor;
    }

    public ArrayList<GoalModel> getWatcher() {
        return watcher;
    }

    public void setWatcher(ArrayList<GoalModel> watcher) {
        this.watcher = watcher;
    }

    public ArrayList<GoalModel> getSupporter() {
        return supporter;
    }

    public void setSupporter(ArrayList<GoalModel> supporter) {
        this.supporter = supporter;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(sponsor);
        dest.writeTypedList(watcher);
        dest.writeTypedList(supporter);
    }
}