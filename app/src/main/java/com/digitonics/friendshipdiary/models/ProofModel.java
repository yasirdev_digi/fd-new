package com.digitonics.friendshipdiary.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by syed.kamil on 5/23/2018.
 */

public class ProofModel implements Parcelable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("task_id")
    @Expose
    private Integer taskId;
    @SerializedName("image_url")
    @Expose
    private ImageUrl imageUrl;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("thumbnail_url")
    @Expose
    private String thumbnail_url;
    @SerializedName("video_url")
    @Expose
    private String video_url;
    @SerializedName("user")
    @Expose
    private UserModel user;
    @SerializedName("comments")
    @Expose
    private ArrayList<CommentModel> comments = null;
    public ProofModel(){}

    protected ProofModel(Parcel in) {
        id = in.readString();
        if (in.readByte() == 0) {
            userId = null;
        } else {
            userId = in.readInt();
        }
        if (in.readByte() == 0) {
            taskId = null;
        } else {
            taskId = in.readInt();
        }
        createdAt = in.readString();
        updatedAt = in.readString();
        setToken(in.readString());
        user = in.readParcelable(UserModel.class.getClassLoader());
        comments = in.createTypedArrayList(CommentModel.CREATOR);
    }

    public static final Creator<ProofModel> CREATOR = new Creator<ProofModel>() {
        @Override
        public ProofModel createFromParcel(Parcel in) {
            return new ProofModel(in);
        }

        @Override
        public ProofModel[] newArray(int size) {
            return new ProofModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;}

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public ImageUrl getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(ImageUrl imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public ArrayList<CommentModel> getComments() {
        return comments;
    }

    public void setComments(ArrayList<CommentModel> comments) {
        this.comments = comments;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        if (userId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(userId);
        }
        if (taskId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(taskId);
        }
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(getToken());
        dest.writeParcelable(user, flags);
        dest.writeTypedList(comments);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public class ImageUrl {

        @SerializedName("1x")
        @Expose
        private String _1x;
        @SerializedName("2x")
        @Expose
        private String _2x;
        @SerializedName("3x")
        @Expose
        private String _3x;

        public String get1x() {
            return _1x;
        }

        public void set1x(String _1x) {
            this._1x = _1x;
        }

        public String get2x() {
            return _2x;
        }

        public void set2x(String _2x) {
            this._2x = _2x;
        }

        public String get3x() {
            return _3x;
        }

        public void set3x(String _3x) {
            this._3x = _3x;
        }
    }
}

