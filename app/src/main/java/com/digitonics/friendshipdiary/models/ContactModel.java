package com.digitonics.friendshipdiary.models;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by kamil on 5/26/18.
 */

public class ContactModel {
    public String id;
    public String name;
    public String mobileNumber;
    public Bitmap photo;
    public Uri photoURI;
}
