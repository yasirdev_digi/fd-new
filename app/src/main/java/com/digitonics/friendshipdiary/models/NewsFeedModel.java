package com.digitonics.friendshipdiary.models;

/**
 * Created by kamil on 5/20/18.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NewsFeedModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("post")
    @Expose
    private String post;
    @SerializedName("photo_url")
    @Expose
    private List<PhotoUrl> photoUrl = null;
    @SerializedName("is_liked")
    @Expose
    private String isLike;
    @SerializedName("tag_friends")
    @Expose
    private ArrayList<UserModel> tagFriend;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("video_url")
    @Expose
    private String videoUrl;
    @SerializedName("audio_url")
    @Expose
    private String audioUrl;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("total_posts")
    @Expose
    private String totalPosts;
    @SerializedName("comment")
    @Expose
    private ArrayList<CommentModel> comment = null;

    @SerializedName("user")
    @Expose
    private UserModel userModel;

    private String imageUrl;

    public NewsFeedModel(){}

    protected NewsFeedModel(Parcel in) {
        id = in.readString();
        userId = in.readString();
        post = in.readString();
        isLike = in.readString();
        tagFriend = in.createTypedArrayList(UserModel.CREATOR);
        longitude = in.readString();
        latitude = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        deletedAt = in.readString();
        videoUrl = in.readString();
        audioUrl = in.readString();
        thumbnail = in.readString();
        totalPosts = in.readString();
        comment = in.createTypedArrayList(CommentModel.CREATOR);
        userModel = in.readParcelable(UserModel.class.getClassLoader());
        imageUrl = in.readString();
    }

    public static final Creator<NewsFeedModel> CREATOR = new Creator<NewsFeedModel>() {
        @Override
        public NewsFeedModel createFromParcel(Parcel in) {
            return new NewsFeedModel(in);
        }

        @Override
        public NewsFeedModel[] newArray(int size) {
            return new NewsFeedModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public List<PhotoUrl> getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(List<PhotoUrl> photoUrl) {
        this.photoUrl = photoUrl;
    }

    public boolean getIsLike() {
        return isLike.equalsIgnoreCase("0")?false:true;
    }

    public void setIsLike(String isLike) {
        this.isLike = isLike;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getTotalPosts() {
        return totalPosts;
    }

    public void setTotalPosts(String totalPosts) {
        this.totalPosts = totalPosts;
    }

    public ArrayList<CommentModel> getComment() {
        return comment;
    }

    public void setComment(ArrayList<CommentModel> comment) {
        this.comment = comment;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public ArrayList<UserModel> getTagFriend() {
        return tagFriend;
    }

    public void setTagFriend(ArrayList<UserModel> tagFriend) {
        this.tagFriend = tagFriend;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(userId);
        dest.writeString(post);
        dest.writeString(isLike);
        dest.writeTypedList(tagFriend);
        dest.writeString(longitude);
        dest.writeString(latitude);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(deletedAt);
        dest.writeString(videoUrl);
        dest.writeString(audioUrl);
        dest.writeString(thumbnail);
        dest.writeString(totalPosts);
        dest.writeTypedList(comment);
        dest.writeParcelable(userModel, flags);
        dest.writeString(imageUrl);
    }


    public class PhotoUrl {

        @SerializedName("1x")
        @Expose
        private String _1x;
        @SerializedName("2x")
        @Expose
        private String _2x;
        @SerializedName("3x")
        @Expose
        private String _3x;

        public String get1x() {
            return _1x;
        }

        public void set1x(String _1x) {
            this._1x = _1x;
        }

        public String get2x() {
            return _2x;
        }

        public void set2x(String _2x) {
            this._2x = _2x;
        }

        public String get3x() {
            return _3x;
        }

        public void set3x(String _3x) {
            this._3x = _3x;
        }

    }
    public class TotalPosts {

        @SerializedName("total")
        @Expose
        private String total;

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

    }

}

