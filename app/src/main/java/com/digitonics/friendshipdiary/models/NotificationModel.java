package com.digitonics.friendshipdiary.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by syed.kamil on 5/27/2018.
 */

public class NotificationModel {
    @SerializedName("user")
    @Expose
    private UserModel user;
    @SerializedName("activity")
    @Expose
    private ActivityModel activity;

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public ActivityModel getActivity() {
        return activity;
    }

    public void setActivity(ActivityModel activity) {
        this.activity = activity;
    }
}
