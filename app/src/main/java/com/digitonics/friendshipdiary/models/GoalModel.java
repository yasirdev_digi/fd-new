package com.digitonics.friendshipdiary.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by syed.kamil on 4/27/2018.
 */

public class GoalModel implements Parcelable,Cloneable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("goal_type")
    @Expose
    private String goalType;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("friends")
    @Expose
    private ArrayList<Friend> friends = null;

    @SerializedName("task")
    @Expose
    private ArrayList<TaskModel> tasks = null;
    @SerializedName("deliveryaddress")
    @Expose
    private Object deliveryAddress;
    @SerializedName("rewards")
    @Expose
    private String rewards;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("is_agreed")
    @Expose
    private String is_agreed;
    @SerializedName("feedback")
    @Expose
    private String feedback;
    @SerializedName("wishlist")
    @Expose
    private ArrayList<WishModel> wishlist = null;

    private int userType;

    public GoalModel() {}

    protected GoalModel(Parcel in) {
        id = in.readString();
        userId = in.readString();
        name = in.readString();
        description = in.readString();
        startDate = in.readString();
        endDate = in.readString();
        goalType = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        deletedAt = in.readString();
        tasks = in.createTypedArrayList(TaskModel.CREATOR);
        rewards = in.readString();
        status = in.readString();
        points = in.readString();
        wishlist = in.createTypedArrayList(WishModel.CREATOR);
        userType = in.readInt();
    }

    public static final Creator<GoalModel> CREATOR = new Creator<GoalModel>() {
        @Override
        public GoalModel createFromParcel(Parcel in) {
            return new GoalModel(in);
        }

        @Override
        public GoalModel[] newArray(int size) {
            return new GoalModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getGoalType() {
        return goalType;
    }

    public void setGoalType(String goalType) {
        this.goalType = goalType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public ArrayList<Friend> getFriends() {
        return friends;
    }

    public void setFriends(ArrayList<Friend> friends) {
        this.friends = friends;
    }

    public ArrayList<TaskModel> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<TaskModel> tasks) {
        this.tasks = tasks;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }


    public Object getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(Object deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getRewards() {
        return rewards;
    }

    public void setRewards(String rewards) {
        this.rewards = rewards;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public ArrayList<WishModel> getWishlist() {
        return wishlist;
    }

    public void setWishlist(ArrayList<WishModel> wishlist) {
        this.wishlist = wishlist;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(userId);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(startDate);
        dest.writeString(endDate);
        dest.writeString(goalType);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeString(deletedAt);
        dest.writeTypedList(tasks);
        dest.writeString(rewards);
        dest.writeString(status);
        dest.writeString(points);
        dest.writeTypedList(wishlist);
        dest.writeInt(userType);
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getIs_agreed() {
        return is_agreed;
    }

    public void setIs_agreed(String is_agreed) {
        this.is_agreed = is_agreed;
    }
}