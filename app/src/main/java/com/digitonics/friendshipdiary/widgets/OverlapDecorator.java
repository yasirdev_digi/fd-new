package com.digitonics.friendshipdiary.widgets;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class OverlapDecorator extends RecyclerView.ItemDecoration {


    private final static int vertOverlap = -20;

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.set(0, 0, vertOverlap, 0);
    }
}