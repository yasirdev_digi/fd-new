package com.digitonics.friendshipdiary.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.adapters.PlaceAutocompleteAdapter;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.digitonics.friendshipdiary.utilities.MyApplication;
import com.digitonics.friendshipdiary.utilities.TouchableWrapper;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, View.OnClickListener, LocationListener, View.OnTouchListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, TextWatcher {

    final int ZOOM_VALUE = 18;
    final int PLACE_PICKER_REQUEST = 1811;

    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));

    private GoogleMap mMap;
    private View mMapView;

    boolean mIsSearched = false;

    /**
     * GeoDataClient wraps our service connection to Google Play services and provides access
     * to the Google Places API for Android.
     */
    protected GeoDataClient mGeoDataClient;

    GoogleApiClient mGoogleApiClient;

    Context mContext;

    FrameLayout mParentViewFL;

    TouchableWrapper mTouchView;

    Marker marker;

//    Circle circle;

    LocationManager locationManager;

    Criteria criteria;

    String provider, mUserAddress, geoLocation;

    Location location;

    LatLng myPosition;

    AutoCompleteTextView mAutoCompleteTextView;

//    private TextView mPlaceDetailsText;

    private PlaceAutocompleteAdapter mAdapter;

    Button mDoneBT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Construct a GeoDataClient for the Google Places API for Android.
        mGeoDataClient = Places.getGeoDataClient(this, null);
        setContentView(R.layout.activity_maps);


        Log.e("TEST", "---------------In map Activity---------------");

//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, GET_LOCATION_PERMISSION_REQUEST); // 1 is a integer which will return the result in onRequestPermissionsResult
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
        initUI();
    }

    private void initUI() {

        mContext = this;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapsactivity_map);
        mMapView = mapFragment.getView();
        mapFragment.getMapAsync(this);

        mParentViewFL = (FrameLayout) findViewById(R.id.mapsactivity_parentFL);
        mParentViewFL.setOnTouchListener(this);
//        mTouchView = new TouchableWrapper(this);
//        mTouchView.addView(mParentViewFL);


        mDoneBT = (Button) findViewById(R.id.mapsactivity_doneBT);
        mDoneBT.setOnClickListener(this);

        findViewById(R.id.search_close_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAutoCompleteTextView.setText("");
                v.setVisibility(View.GONE);

                mUserAddress = "";

                ColorStateList csl = new ColorStateList(new int[][]{new int[0]}, new int[]{getResources().getColor(R.color.gray_dark_2)});

                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                    mDoneBT.setBackgroundTintList(csl);
                } else {
                    mDoneBT.getBackground().setColorFilter(getResources().getColor(R.color.gray_dark_2), PorterDuff.Mode.MULTIPLY);
                }

            }
        });

        // Retrieve the AutoCompleteTextView that will display Place suggestions.
        mAutoCompleteTextView = (AutoCompleteTextView)
                findViewById(R.id.map_search);

        // Register a listener that receives callbacks when a suggestion has been selected
        mAutoCompleteTextView.setOnItemClickListener(mAutocompleteClickListener);
        mAutoCompleteTextView.addTextChangedListener(this);

        // Retrieve the TextViews that will display details and attributions of the selected place.
//        mPlaceDetailsText = (TextView) findViewById(R.id.map_searched_address);


        // Set up the adapter that will retrieve suggestions from the Places Geo Data Client.
        mAdapter = new PlaceAutocompleteAdapter(this, mGeoDataClient, BOUNDS_GREATER_SYDNEY, null);
        mAutoCompleteTextView.setAdapter(mAdapter);

    }


    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data Client
     * to retrieve more details about the place.
     *
     * @see GeoDataClient#getPlaceById(String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i(MyApplication.TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data Client to retrieve a Place object with
             additional details about the place.
              */
            Task<PlaceBufferResponse> placeResult = mGeoDataClient.getPlaceById(placeId);
            placeResult.addOnCompleteListener(mUpdatePlaceDetailsCallback);

//            Toast.makeText(getApplicationContext(), "Clicked: " + primaryText,
//                    Toast.LENGTH_SHORT).show();

            Log.i(MyApplication.TAG, "Called getPlaceById to get Place details for " + placeId);
        }
    };

    /**
     * Callback for results from a Places Geo Data Client query that shows the first place result in
     * the details view on screen.
     */
    private OnCompleteListener<PlaceBufferResponse> mUpdatePlaceDetailsCallback
            = new OnCompleteListener<PlaceBufferResponse>() {
        @Override
        public void onComplete(Task<PlaceBufferResponse> task) {
            try {
                PlaceBufferResponse places = task.getResult();

                // Get the Place object from the buffer.
                final Place place = places.get(0);

                LatLng searchedLatLng = place.getLatLng();

                CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(searchedLatLng, ZOOM_VALUE);
                mMap.animateCamera(yourLocation);

                mIsSearched = true;
                mUserAddress = place.getName().toString() + ", " + place.getAddress().toString();

                onMapClick(searchedLatLng);

                Utils.hide(MapsActivity.this);

                // Format details of the place for display and show it in a TextView.
//                mPlaceDetailsText.setText(formatPlaceDetails(getResources(), place.getName(),
//                        /*place.getId()*/"", place.getAddress(), /*place.getPhoneNumber()*/"",
//                        /*place.getWebsiteUri()*/Uri.EMPTY));

//                mAutoCompleteTextView.setText(formatPlaceDetails(getResources(), place.getName(),
//                        /*place.getId()*/"", place.getAddress(), /*place.getPhoneNumber()*/"",
//                        /*place.getWebsiteUri()*/Uri.EMPTY));

                mAutoCompleteTextView.setText(place.getAddress()+"");

                // Display the third party attributions if set.
                final CharSequence thirdPartyAttribution = places.getAttributions();
//                if (thirdPartyAttribution == null) {
//                    mPlaceDetailsAttribution.setVisibility(View.GONE);
//                } else {
//                    mPlaceDetailsAttribution.setVisibility(View.VISIBLE);
//                    mPlaceDetailsAttribution.setText(
//                            Html.fromHtml(thirdPartyAttribution.toString()));
//                }

                Log.i(MyApplication.TAG, "Place details received: " + place.getName());

                places.release();
            } catch (Throwable e) {
                // Request did not complete successfully
                Log.e(MyApplication.TAG, "Place query did not complete.", e);
                return;
            }
        }
    };

    private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri) {
        Log.e(MyApplication.TAG, res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        // Add a marker in Sydney and move the camera
        Log.e(Constants.APP_TAG, "Before onMapReady");

        // For showing a move to my location button
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);

        criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, true);
        location = locationManager.getLastKnownLocation(provider);


        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();

            myPosition = new LatLng(latitude, longitude);

            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(myPosition, ZOOM_VALUE);
            mMap.animateCamera(yourLocation);
        }

        if (mMapView != null &&
                mMapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mMapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 30, 30);
        }


        mMap.setOnMapClickListener(this);

    }

    @Override
    public void onMapClick(LatLng latLng) {

        if (!Utils.checkInternetConnectivity(mContext)) {
            Toast.makeText(mContext, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            return;
        }

        if (marker != null /*&& circle != null*/) {
            marker.remove();
//            circle.remove();
        }

        Utils.hide(MapsActivity.this);

        geoLocation = latLng.latitude + "," + latLng.longitude;

        String urlValue = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + geoLocation + "&key=" + Constants.GOOGLE_API_KEY;


        FetchUrl fetchUrl = new FetchUrl(this, latLng);
        fetchUrl.execute(urlValue);

//        Geocoder geocoder;
//        List<Address> addresses = null;
//        geocoder = new Geocoder(this, Locale.getDefault());
//
//        Log.e(Constants.APP_TAG, "latLng.latitude = " + latLng.latitude + "\nlatLng.longitude = " + latLng.longitude);
//        try {
//            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
//        } catch (Exception e) {
//            Log.e(Constants.APP_TAG, "Exception in GetAddress from Coordinates: " + e.getMessage());
//        }
//
//
//        if (addresses != null && addresses.isEmpty() == false) {
//
////            mUserAddress = (addresses.get(0).getAddressLine(0) == null ? "" : addresses.get(0).getAddressLine(0).toString() + ", ")
////                    + (addresses.get(0).getLocality() == null ? "" : addresses.get(0).getLocality().toString() + ", ")
////                    + (addresses.get(0).getCountryName() == null ? "" : addresses.get(0).getCountryName().toString()) + ".";
////            mUserAddress = (addresses.get(0).getAddressLine(0) == null ? "" : addresses.get(0).getAddressLine(0).toString() + "")
////                    + "";
//
//
//            Log.e(Constants.APP_TAG, "addresses.get(0).getAddressLine(0) = " + addresses.get(0).getAddressLine(0) + "\n" +
//                    "addresses.get(0).getLocality() = " + addresses.get(0).getLocality() + "\n" +
//                    "addresses.get(0).getAdminArea() = " + addresses.get(0).getAdminArea() + "\n" +
//                    "addresses.get(0).getCountryName() = " + addresses.get(0).getCountryName() + "\n" +
//                    "addresses.get(0).getPostalCode() = " + addresses.get(0).getPostalCode() + "\n" +
//                    "addresses.get(0).getCountryCode() = " + addresses.get(0).getCountryCode() + "\n" +
//                    "addresses.get(0).getPhone() = " + addresses.get(0).getPhone() + "\n" +
//                    "addresses.get(0).getPremises() = " + addresses.get(0).getPremises() + "\n" +
//                    "addresses.get(0).getSubAdminArea() = " + addresses.get(0).getSubAdminArea() + "\n" +
//                    "addresses.get(0).getSubLocality() = " + addresses.get(0).getSubLocality() + "\n" +
//                    "addresses.get(0).getFeatureName() = " + addresses.get(0).getFeatureName()
//            );
//
//
//            // Format details of the place for display and show it in a TextView.
//
//            if (mIsSearched != true) {
//
//                mUserAddress = (addresses.get(0).getFeatureName() == null || addresses.get(0).getFeatureName().toString().isEmpty() ? "" : addresses.get(0).getFeatureName().toString() + ", ") +
//                        (addresses.get(0).getSubLocality() == null || addresses.get(0).getSubLocality().toString().isEmpty() ? "" : addresses.get(0).getSubLocality().toString() + ", ") +
//                        (addresses.get(0).getSubAdminArea() == null || addresses.get(0).getSubAdminArea().toString().isEmpty() ? "" : addresses.get(0).getSubAdminArea().toString() + ", ") +
//                        (addresses.get(0).getAdminArea() == null || addresses.get(0).getAdminArea().toString().isEmpty() ? "" : addresses.get(0).getAdminArea().toString() + ", ") +
//                        (addresses.get(0).getCountryName() == null || addresses.get(0).getCountryName().toString().isEmpty() ? "" : addresses.get(0).getCountryName().toString());
//
//                mUserAddress = mUserAddress.replaceAll("null", "");
////                mUserAddress = mUserAddress.replaceAll(null, "");
//
//                mPlaceDetailsText.setText(formatPlaceDetails(getResources(), (addresses.get(0).getFeatureName().toString().isEmpty() ? addresses.get(0).getPremises().toString() : addresses.get(0).getFeatureName().toString()),
//                        /*place.getId()*/"",
//                        (addresses.get(0).getSubLocality() == null || addresses.get(0).getSubLocality().toString().isEmpty() ? "" : addresses.get(0).getSubLocality().toString() + ", ") +
//                                (addresses.get(0).getSubAdminArea() == null || addresses.get(0).getSubAdminArea().toString().isEmpty() ? "" : addresses.get(0).getSubAdminArea().toString() + ", ") +
//                                (addresses.get(0).getAdminArea() == null || addresses.get(0).getAdminArea().toString().isEmpty() ? "" : addresses.get(0).getAdminArea().toString() + ", ") +
//                                (addresses.get(0).getCountryName() == null || addresses.get(0).getCountryName().toString().isEmpty() ? "" : addresses.get(0).getCountryName().toString() + ", "),
//                        /*place.getPhoneNumber()*/"",
//                        /*place.getWebsiteUri()*/Uri.EMPTY));
//            } else {
//                mIsSearched = false;
//            }
//
//
//            marker = mMap.addMarker(new MarkerOptions()
//                            .position(latLng)
//                            .title(getResources().getString(R.string.location))
//                            .snippet(mUserAddress)
////                .rotation((float) -15.0)
////                            .icon(BitmapDescriptorFactory.defaultMarker(185))
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker))
//            );
//
//
//            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_VALUE));
////            circle = mMap.addCircle(new CircleOptions()
////                    .center(latLng)
////                    .radius(10)
////                    .strokeColor(getResources().getColor(R.color.orange)));
//
////            mDoneBT.setEnabled(true);
//
//            ColorStateList csl = new ColorStateList(new int[][]{new int[0]}, new int[]{getResources().getColor(R.color.colorAccent)});
//
//            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
//                mDoneBT.setBackgroundTintList(csl);
////                DebugManager.showLog("in if");
//            } else {
////                DebugManager.showLog("in else");
////                mDoneBT.setBackgroundColor(getResources().getColor(R.color.colorAccent));
//                mDoneBT.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.MULTIPLY);//setBackgroundColor(getResources().getColor(R.color.colorAccent));
//            }
//
//        } else {
//            Utils.showToast(this, getResources().getString(R.string.cannot_fetch_your_location_at_the_moment));
//        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case Constants.GET_LOCATION_PERMISSION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.e(Constants.APP_TAG, "" + getResources().getString(R.string.location_permission_has_been_granted));

                    locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                    criteria = new Criteria();
                    Log.e(Constants.APP_TAG, "\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++CHECk \n\n");

                    try {
                        Log.e("TEST", "\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++try k ANDAR \n\n");
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);
                        mMap.setMyLocationEnabled(true);
                        provider = locationManager.getBestProvider(criteria, true);
                        location = locationManager.getLastKnownLocation(provider);
                    } catch (SecurityException e) {
                        Log.e(Constants.APP_TAG, "Security Exception: " + e.getMessage());
                    }
                    Log.e("TEST", "\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++AFTER TRYCATCH\n\n");

                    if (location != null) {
                        double latitude = location.getLatitude();
                        double longitude = location.getLongitude();

                        myPosition = new LatLng(latitude, longitude);

                        Log.e("TEST", "\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++GYA ANDR\n\n");

                        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(myPosition, ZOOM_VALUE);
                        mMap.animateCamera(yourLocation);
                    } else
                        Log.e("TEST", "\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++location is null\n\n");

                    mMap.setOnMapClickListener(this);

                } else {
                    // show user that permission was denied. inactive the location based feature or force user to close the app
                    finish();
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mapsactivity_doneBT:

                if (mUserAddress != null && mUserAddress != "") {
                    Intent intent = getIntent();
//                Intent intent = new Intent(MapsActivity.this, RequestNewFragment.class);

                    Log.e(Constants.APP_TAG, "mUserAddress - " + mUserAddress);

                    intent.putExtra("maps_address", mUserAddress);
                    intent.putExtra("maps_geoLocation", geoLocation);
                    setResult(MapsActivity.RESULT_OK, intent);

                    finish();
                } else {
                    Toast.makeText(mContext, getResources().getString(R.string.tap_on_map_to_select_location), Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        myPosition = new LatLng(latitude, longitude);

        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(myPosition, ZOOM_VALUE);
        mMap.animateCamera(yourLocation);

        try {
            locationManager.removeUpdates(this);
        } catch (SecurityException e) {

        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Utils.hide(this);
        return false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() > 0) {
            findViewById(R.id.search_close_btn).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.search_close_btn).setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        Context mContext;
        String mErrorMessage = "";
        LatLng mLatLng;
        ProgressDialog mProgressDialog;

        public FetchUrl(Context context, LatLng latLng) {
            mContext = context;
            mLatLng = latLng;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(mContext);

            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setCancelable(false);

            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                mErrorMessage = "";
            } catch (Exception e) {
                Log.e(MyApplication.TAG, "Exception = " + e.getMessage());
                mErrorMessage = e.getMessage();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (mErrorMessage != "") {
//                Toast.makeText(mContext, "Message from FU DIBG = " + mErrorMessage, Toast.LENGTH_SHORT).show();
            }

            if (result != "") {
                String address = "";
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray resultsArray = jsonObject.getJSONArray("results");
                    JSONObject jsonObject1 = (JSONObject) resultsArray.get(0);
                    address = jsonObject1.getString("formatted_address");


                } catch (Throwable e) {

                }
                if (address != null && address.isEmpty() == false) {

                    // Format details of the place for display and show it in a TextView.

                    if (mIsSearched != true) {

                        mUserAddress = address;
//                        mPlaceDetailsText.setText(mUserAddress);
                        mAutoCompleteTextView.setText(mUserAddress);
                    } else {
                        mIsSearched = false;
                    }


                    marker = mMap.addMarker(new MarkerOptions()
                                    .position(mLatLng)
                                    .title(getResources().getString(R.string.location))
                                    .snippet(mUserAddress)
//                .rotation((float) -15.0)
//                            .icon(BitmapDescriptorFactory.defaultMarker(185))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker))
                    );


                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, ZOOM_VALUE));
                    ColorStateList csl = new ColorStateList(new int[][]{new int[0]}, new int[]{getResources().getColor(R.color.colorAccent)});

                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                        mDoneBT.setBackgroundTintList(csl);
                    } else {
                        mDoneBT.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.MULTIPLY);//setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    }

                } else {
                    Utils.showToast(mContext, getResources().getString(R.string.cannot_fetch_your_location_at_the_moment));
                }
            } else {
//                Toast.makeText(mContext, "FU Result Empty...", Toast.LENGTH_SHORT).show();
            }

            mProgressDialog.dismiss();

        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            br.close();

        } catch (Throwable e) {
//            Toast.makeText(mContext, "BHAND IN downloadUrl", Toast.LENGTH_SHORT).show();
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
}