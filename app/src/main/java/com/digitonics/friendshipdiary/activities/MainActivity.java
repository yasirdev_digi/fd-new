package com.digitonics.friendshipdiary.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;

import java.io.IOException;
import java.lang.reflect.Type;

import retrofit2.Call;

import static com.digitonics.friendshipdiary.activities.BaseActivity.PREFERENCE_KEY;
import static com.digitonics.friendshipdiary.activities.BaseActivity.USER_KEY;

@IgnoreApi
public class MainActivity extends BaseActivity {

    private Handler mHandler;
    private static final int SPLASH_TIME = 3 * 1000;

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected View getParentView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.activity_main,parent,false);
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return null;
    }

    @Override
    protected Runnable customOnFailure() {
        return null;
    }

    @Override
    protected void init() {
        mHandler = new Handler();

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = null;
                if(mPreferences.contains("first")) {
                    if (mPreferences.contains(USER_KEY)) {
                        if (BaseActivity.USER.getIsActive().equalsIgnoreCase("3")) {
                            intent = new Intent(MainActivity.this, HomeActivity.class);
                        }else if(BaseActivity.USER.getIsActive().equalsIgnoreCase("2")) {
                            intent = new Intent(MainActivity.this, EditProfileActivity.class);
                        }
                    } else
                        intent = new Intent(MainActivity.this, LoginActivity.class);
                }else {
                    mPreferences.edit().putBoolean("first", true).apply();
                    intent = new Intent(MainActivity.this, HowWorksActivity.class);
                }

                startActivity(intent);
                MainActivity.this.finish();
            }
        }, SPLASH_TIME);
    }

    @Override
    protected Type getClassType() {
        return null;
    }

    @Override
    public void run() {

    }
}
