package com.digitonics.friendshipdiary.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.annotations.OverridingBaseLayout;
import com.digitonics.friendshipdiary.fragments.BaseFragment;
import com.digitonics.friendshipdiary.fragments.EditProfileFragment;
import com.digitonics.friendshipdiary.fragments.GoalDetailFragment;
import com.digitonics.friendshipdiary.fragments.ImageProofDetail;
import com.digitonics.friendshipdiary.fragments.MyGoals;
import com.digitonics.friendshipdiary.fragments.NewsDetailFragment;
import com.digitonics.friendshipdiary.fragments.NewsfeedFragment;
import com.digitonics.friendshipdiary.fragments.OtherMainFragment;
import com.digitonics.friendshipdiary.fragments.TaskDetailFragment;
import com.digitonics.friendshipdiary.widgets.NoScrollRecycler;
import com.digitonics.friendshipdiary.widgets.RecyclerViewItemDecorator;

import java.io.IOException;
import java.lang.reflect.Type;

import retrofit2.Call;

@IgnoreApi
@OverridingBaseLayout(coordinatorId = R.id.coordinatorLayout)
public class HomeActivity extends BaseActivity {

    private NoScrollRecycler rvBottomMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getSupportFragmentManager().beginTransaction().add(R.id.leftFrame, new NavigationDrawer(), NavigationDrawer.TAG_NAVIGATION_DRAWER).
                commitAllowingStateLoss();*/
    }

    @Override
    protected View getParentView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.activity_home, parent, false);
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return null;
    }

    @Override
    protected Runnable customOnFailure() {
        return null;
    }

    @Override
    protected void init() {
        int sd = Math.round(getResources().getDimension(R.dimen._1sdp));
        rvBottomMenu = (NoScrollRecycler) findViewById(R.id.rvBottomMenu);
        rvBottomMenu.addItemDecoration(new RecyclerViewItemDecorator(sd));
        rvBottomMenu.setLayoutManager(new GridLayoutManager(this, 4));
        rvBottomMenu.setAdapter(bottomMenuAdapter);
        onMenuClick(model.getMenus().get(0),0);
        LocalBroadcastManager.getInstance(this).registerReceiver(customBroadcastReceiver, new IntentFilter("ACTION_REFRESH_ORDER_LIST"));
    }

    @Override
    protected Type getClassType() {
        return null;
    }

    @Override
    public void run() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(customBroadcastReceiver);
    }

    BroadcastReceiver customBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context con, Intent intent) {

            //String type = intent.getStringExtra("refresh");  //get the type of message from MyGcmListenerService 1 - lock or 0 -Unlock
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
            if (fragment != null) {
                if (((BaseFragment) fragment) instanceof GoalDetailFragment){
                    GoalDetailFragment frag = (GoalDetailFragment) ((BaseFragment) fragment);
                    frag.callApi();
                }
                else if (((BaseFragment) fragment) instanceof MyGoals){
                    MyGoals frag = (MyGoals) ((BaseFragment) fragment);
                    frag.callApi();
                }
                else if (((BaseFragment) fragment) instanceof OtherMainFragment){
                    OtherMainFragment frag = (OtherMainFragment) ((BaseFragment) fragment);
                    frag.callApi();
                }
                else if (((BaseFragment) fragment) instanceof NewsfeedFragment){
                    NewsfeedFragment frag = (NewsfeedFragment) ((BaseFragment) fragment);
                    frag.callApi();
                }
                else if (((BaseFragment) fragment) instanceof NewsDetailFragment){
                    NewsDetailFragment frag = (NewsDetailFragment) ((BaseFragment) fragment);
                    frag.callApi();
                }
                else if (((BaseFragment) fragment) instanceof ImageProofDetail){
                    ImageProofDetail frag = (ImageProofDetail) ((BaseFragment) fragment);
                    frag.callApi();
                }else if (((BaseFragment) fragment) instanceof TaskDetailFragment){
                    TaskDetailFragment frag = (TaskDetailFragment) ((BaseFragment) fragment);
                    frag.callApi();
                }

            }
            Log.e("Test", "Inside onReceive... Calling API now");

        }
    };
}
