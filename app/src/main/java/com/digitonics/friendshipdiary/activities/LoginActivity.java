package com.digitonics.friendshipdiary.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.annotations.HideLoader;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.webservice.WebResponse;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.AccountService;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Arrays;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Converter;

@IgnoreApi
@HideLoader
public class LoginActivity extends BaseActivity<UserModel> implements View.OnClickListener{

    private LoginButton mFacebookLoginBtn;
    private TwitterLoginButton mTwitterLogin;
    private SignInButton mGoogleLogin;
    private EditText mEmail, mPassword;
    private String mSocialType, mSocialId, mSocialEmail, mSocailFName, mSocialImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected View getParentView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.activity_login, parent, false);
    }

    @Override
    protected Call<?> getCall() throws IOException {
        if (mSocialId == null)
            return  mApiBuilder.login(mEmail.getText().toString(),mPassword.getText().toString(),"normal");
        else
            return  mApiBuilder.login(mSocialEmail,mSocialType,"social", mSocialId, mSocailFName, mSocialImage);
    }

    @Override
    protected Runnable customOnFailure() {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    Converter<ResponseBody, WebResponse> converter = mApiBuilder.mRetrofit.responseBodyConverter(WebResponse.class, new Annotation[0]);
                    WebResponse error = converter.convert(mResponse.errorBody());
                    showSnack(getMessage(error.getError().getMessage()));
                    mSocialId = null;
                    LoginManager.getInstance().logOut();

                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        };
    }

    @Override
    protected void init() {
        findViewById(R.id.loginBtn).setOnClickListener(this);
        findViewById(R.id.signup).setOnClickListener(this);
        findViewById(R.id.facebook).setOnClickListener(this);
        findViewById(R.id.twitter).setOnClickListener(this);
        findViewById(R.id.google).setOnClickListener(this);
        findViewById(R.id.forgotPassword).setOnClickListener(this);
        mFacebookLoginBtn = (LoginButton) findViewById(R.id.login_button);
        mTwitterLogin = findViewById(R.id.twitter_o);
        mEmail = findViewById(R.id.username);
        mPassword = findViewById(R.id.password);
        mGoogleLogin = findViewById(R.id.google_o);
        mFacebookLoginBtn.setReadPermissions(Arrays.asList(EMAIL));

        mFacebookLoginBtn.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            public ProfileTracker mProfileTracker;
            @Override
            public void onSuccess(LoginResult loginResult) {
                //showSnack("Success");
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                Log.v("LoginActivity Response ", response.toString());

                                try {
                                    mSocialEmail = object.getString("email");
                                    mSocialType = "facebook";
                                    if(Profile.getCurrentProfile() == null) {
                                        mProfileTracker = new ProfileTracker() {
                                            @Override
                                            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                                                //socialLogin(currentProfile, email);
                                                mSocialId = currentProfile.getId();
                                                mSocailFName = currentProfile.getFirstName();
                                                mProfileTracker.stopTracking();
                                                callApi();
                                            }
                                        };
                                        // no need to call startTracking() on mProfileTracker
                                        // because it is called by its constructor, internally.
                                    }
                                    else {
                                        Profile profile = Profile.getCurrentProfile();
                                        //socialLogin(profile, email)
                                        mSocialId = profile.getId();
                                        mSocailFName = profile.getFirstName();
                                        mSocialImage = "https://graph.facebook.com/" +profile.getId() + "/picture";
                                        callApi();
                                        Log.v("facebook - profile", profile.getFirstName());
                                    }



                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                //showSnack("Cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                //showSnack("Error");
            }
        });

        mTwitterLogin.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls

                mSocialId = result.data.getId() + "";
                mSocialType = "twitter";
                final TwitterSession twitterSession = result.data;
                TwitterAuthClient twitterAuthClient =  new TwitterAuthClient();

                new TwitterApiClient(twitterSession).getAccountService().
                        verifyCredentials(true,false, true)
                        .enqueue(new Callback<User>() {
                            @Override
                            public void success(Result<User> result) {
                                User user = result.data;
                                mSocialEmail = user.email;
                                mSocailFName = user.name;
                                mSocialImage = user.profileImageUrl;
                                callApi();
                            }

                            @Override
                            public void failure(TwitterException exception) {

                            }
                        });
                /*twitterAuthClient.requestEmail(twitterSession, new Callback<String>() {
                    @Override
                    public void success(Result<String> emailResult) {
                        mSocialEmail = emailResult.data;
                        callApi();
                    }

                    @Override
                    public void failure(TwitterException e) {
                        Toast.makeText(LoginActivity.this, "Twiiter Error", Toast.LENGTH_SHORT).show();
                    }
                });*/

                //Toast.makeText(LoginActivity.this, "Twiiter Success", Toast.LENGTH_SHORT).show();
                //urlParams.add(new BasicNameValuePair("name", result.data.getUserName()));

                //urlParams.add(new BasicNameValuePair("email", result.data.getUserName() + "@twitter.com"));

                //urlParams.add(new BasicNameValuePair("sid", result.data.getId() + ""));
            }

            @Override
            public void failure(TwitterException exception) {
                showSnack("Twitter Error");
            }
        });
    }

    @Override
    protected Type getClassType() {
        return null;
    }

    @Override
    public void onClick(View view) {
        Intent in ;
        switch (view.getId()){
            case R.id.loginBtn:

                Utils.hideSoftKeyboard(findViewById(R.id.username));
                Utils.hideSoftKeyboard(findViewById(R.id.password));
                if (validateFields()){
                    callApi();
                }

                break;
            case R.id.signup:
                in = new Intent(view.getContext(), SignupActivity.class);
                startActivity(in);
                break;
            case R.id.forgotPassword:
                in = new Intent(view.getContext(), ResetActivity.class);
                startActivity(in);
                break;
            case R.id.facebook:
                mSocialType = "facebook";
                mFacebookLoginBtn.performClick();
                break;
            case R.id.twitter:
                mSocialType = "twitter";
                mTwitterLogin.performClick();
                break;
            case R.id.google:
                mSocialType = "gmail";
                //mGoogleLogin.performClick();
                signIn();
                break;
        }
    }

    @Override
    public void run() {
        saveUser(mData);
        Intent in;
        if (mData.getIsActive().equalsIgnoreCase("3")) {
            in = new Intent(this, HomeActivity.class);
        }else {
            in = new Intent(this, EditProfileActivity.class);
        }

        startActivity(in);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0001) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Log.i("add","enteres");
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }else if (requestCode == 140) {
            mTwitterLogin.onActivityResult(requestCode, resultCode, data);
        }
        else{
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            mSocialEmail = account.getEmail();
            mSocialId = account.getId();
            mSocialType = "gmail";
            mSocialImage = account.getPhotoUrl() != null ?account.getPhotoUrl().toString() : null;
            mSocailFName = account.getGivenName();
            callApi();

        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("TAG", "signInResult:failed code=" + e.getStatusCode());
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, 0001);
    }

    private boolean validateFields() {

        // Check if email is empty or not.
        if (TextUtils.isEmpty(mEmail.getText().toString())) {
            //showSnack( "Please enter email address!");
            showSnack("Please enter email address!");
            return false;
        } else if (!Utils.isEmailValid(mEmail.getText().toString())) {
            showSnack("Please enter valid email address!");
            return false;
        }

        // Check if password is empty or not.
        if (mPassword.getText().toString().isEmpty() == true) {
            showSnack("Please enter password");
            return false;
        }

        return true;
    }
}
