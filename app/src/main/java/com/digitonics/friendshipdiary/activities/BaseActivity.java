package com.digitonics.friendshipdiary.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.adapters.BottomMenuAdapter;
import com.digitonics.friendshipdiary.annotations.HideLoader;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.annotations.OverridingBaseLayout;
import com.digitonics.friendshipdiary.fcm.RegistrationService;
import com.digitonics.friendshipdiary.fragments.BaseFragment;
import com.digitonics.friendshipdiary.fragments.EditProfileFragment;
import com.digitonics.friendshipdiary.fragments.GoalDetailFragment;
import com.digitonics.friendshipdiary.fragments.InternetFailure;
import com.digitonics.friendshipdiary.fragments.MyDiaryFragment;
import com.digitonics.friendshipdiary.fragments.MyFriends;
import com.digitonics.friendshipdiary.fragments.MyGoals;
import com.digitonics.friendshipdiary.fragments.NewsfeedFragment;
import com.digitonics.friendshipdiary.fragments.OtherMainFragment;
import com.digitonics.friendshipdiary.fragments.PaymentFragment;
import com.digitonics.friendshipdiary.fragments.ProfileFragment;
import com.digitonics.friendshipdiary.fragments.RewardFragment;
import com.digitonics.friendshipdiary.fragments.TaskDetailFragment;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.interfaces.MapActivityCallback;
import com.digitonics.friendshipdiary.interfaces.MediaCallback;
import com.digitonics.friendshipdiary.models.BottomModel;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.digitonics.friendshipdiary.webservice.ApiBuilder;
import com.digitonics.friendshipdiary.webservice.WebResponse;
import com.digitonics.friendshipdiary.widgets.AdvanceDrawerLayout;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.FirebaseApp;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.RECORD_AUDIO;
import static com.digitonics.friendshipdiary.fragments.CreateGoalFragment.TAG_FEEDBACK;
import static com.digitonics.friendshipdiary.fragments.MyFriends.TAG_MY_FRIEND;
import static com.digitonics.friendshipdiary.fragments.MyGoals.TAG_GOAL;
import static com.digitonics.friendshipdiary.utilities.Constants.AUDIO_RECORDER;

public abstract class BaseActivity<T> extends AppCompatActivity implements BottomMenuAdapter.OnMenuClickListener,MapActivityCallback, MediaCallback,Callback, Runnable, NavigationView.OnNavigationItemSelectedListener, View.OnClickListener{

    public static final String PREFERENCE_KEY = "prefs";
    public static final String EMAIL = "email";
    public static final String DEVICE_TOKEN = "token";
    public static final String PLATFORM = "android";
    public static final String NOTIFICATION_KEY = "notifi";
    public static final String REG_KEY = "id";
    public static final String USER_KEY = "user";
    public static final String USER_NAME_KEY = "name";
    public static final String USER_IMAGE_KEY = "image";
    public static final int RUNTIME_PERMISSIONS_COUNT = 4;
    private static final long LOCATION_REFRESH_TIME = 5 * 60 * 1000;
    private static final float LOCATION_REFRESH_DISTANCE = 0f;
    static final int REQUEST_VIDEO_CAPTURE = 100;
    AlertDialog dialog;
    public String mFromTime;
    public String mToTime;
    public ApiBuilder mApiBuilder;
    protected T mData;
    protected WebResponse<T> mBody;
    protected Response mResponse;
    protected Dialog mProgressDialog;
    protected View mBaseView;
    private Intent mExtraIntent = null;
    public DrawerLayout mDrawerLayout;
    protected NavigationView mNavigationView;
    protected CoordinatorLayout mParentCoordinatorLayout;
    protected Location mLastLocation;
    public Context mContext;
    public SharedPreferences mPreferences;
    protected Integer mUserId;
    private Call<?> mCall;
    private Intent mIntent;
    private LocationManager mLocationManager;
    public ImageView mUserImg;
    public TextView mUserText, mUserEmail;
    private Activity mActivity;
    protected BottomModel model;
    protected BottomMenuAdapter bottomMenuAdapter;
    public View mBottomLL;
    public CallbackManager mCallbackManager;
    protected GoogleSignInClient mGoogleSignInClient;
    public static UserModel USER;
    public String mMediaPath;
    private MediaRecorder mediaRecorder;
    private String mAudioPath;
    private Uri fileUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        mActivity = this;
        mContext = this;
        mApiBuilder = ApiBuilder.getInstance(this);
        mPreferences = getSharedPreferences(PREFERENCE_KEY, MODE_PRIVATE);

        Gson gson = new Gson();
        model = gson.fromJson(Utils.JSON, BottomModel.class);
        bottomMenuAdapter = new BottomMenuAdapter(model.getMenus(), this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        grantPermission();

        mCallbackManager = CallbackManager.Factory.create();

        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(getString(R.string.twitter_CONSUMER_KEY), getString(R.string.twitter_CONSUMER_SECRET)))
                .debug(true)
                .build();
        Twitter.initialize(config);
        if (USER == null) {
            if (mPreferences.contains(USER_KEY))
                USER = gson.fromJson(mPreferences.getString(USER_KEY, ""), UserModel.class);
        }



        if(!mPreferences.getBoolean(REG_KEY,false))
            startService(new Intent(this, RegistrationService.class));

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        if (this.getClass().isAnnotationPresent(OverridingBaseLayout.class)) {
            setContentView(getParentView(getLayoutInflater(), null));
            mParentCoordinatorLayout = (CoordinatorLayout) findViewById(this.getClass().getAnnotation(OverridingBaseLayout.class).coordinatorId());
            initNavDrawer(mParentCoordinatorLayout.getId());
            View view = mNavigationView.getHeaderView(0);
            mUserImg = (ImageView) view.findViewById(R.id.profile_image);
            mUserText = (TextView) view.findViewById(R.id.nav_header_nameTV);
            mUserEmail = (TextView) view.findViewById(R.id.email_id);
            if (USER != null) {
                //Picasso.get().load(USER).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(mUserImg);
                //if (USER.getSocialId() == null)
                    mUserText.setText(StringUtils.capitalize(USER.getFirstName()) + " " + StringUtils.capitalize(USER.getLastName()));
                /*else
                    mUserText.setText(USER.getSocialUsername());*/
                mUserEmail.setText(USER.getEmail());

                Picasso.get().load(USER.getImage() != null ? USER.getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(mUserImg);

            }
            setToolBar(R.drawable.icon_menu);

        } else {
            setContentView(R.layout.activity_base);
            mParentCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
            ViewGroup parent = (ViewGroup) findViewById(R.id.baseLayout);
            parent.addView(getParentView(getLayoutInflater(), parent));
            mBaseView = findViewById(R.id.baseLayout);
        }
        init();
        mBottomLL = findViewById(R.id.bottomLL);
        if (!this.getClass().isAnnotationPresent(IgnoreApi.class))
            callApi();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the task you need to do.

                } else {

                    // permission denied, boo! Disable the functionality that depends on this permission.
                }
                return;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

/*        switch (itemId) {
            case android.R.id.home: {
                //mDrawerLayout.openDrawer(GravityCompat.START);
                return false;
            }
        }*/
        return false;
    }

    public void callApi() {

        dismiss();
        if (!this.getClass().isAnnotationPresent(HideLoader.class)) {

            View view = getLayoutInflater().inflate(R.layout.progress_bar,null);
            mProgressDialog = Utils.showCustomDialog( mContext,view);
            mProgressDialog.setCancelable(false);

        }
        try {
            if (mCall != null && mCall.isExecuted())
                mCall.cancel();

            mCall = getCall();
            mCall.enqueue(this);

        } catch (IOException ex) {
            ex.printStackTrace();
            onFailure(null, ex);
        }

    }


    @Override
    public void onResponse(Call call, Response response) {
        try {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(InternetFailure.TAG_INTERNET_FAILURE);

            if (fragment != null) {
                getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
            }
            mResponse = response;
            if (response.isSuccessful()) {
                mBody = (WebResponse<T>) response.body();
                if (mBody.getResponse().getData() != null)
                    mData = (T) mBody.getResponse().getData();
                /*if (this.getClass().isAnnotationPresent(ShowMessage.class)
                        && mBody.getResponse().getMessage().size() > 0) {
                    showSnack(mBody.getResponse().getMessage().get(0).toString());/
                }*/
                run();
            } else {
                if(customOnFailure() == null) {
                    Converter<ResponseBody, WebResponse> converter = mApiBuilder.mRetrofit.responseBodyConverter(WebResponse.class, new Annotation[0]);
                    WebResponse error = converter.convert(response.errorBody());
                    showSnack(getMessage(error.getError().getMessage()));
                }else{
                    customOnFailure().run();
                }
            }
            dismiss();
        }catch (Exception ex){
            onFailure(null,ex);
        }

    }

    @Override
    public void onFailure(Call call, Throwable t) {
        if (t instanceof UnknownHostException || t instanceof SocketTimeoutException) {

            Fragment fragment = getSupportFragmentManager().findFragmentByTag(InternetFailure.TAG_INTERNET_FAILURE);
            if (fragment == null) {
                InternetFailure failureFragment = new InternetFailure();
                failureFragment.setRunnable(new Runnable() {
                    @Override
                    public void run() {
                        callApi();
                    }
                });
                getSupportFragmentManager().beginTransaction().add(android.R.id.content, failureFragment, InternetFailure.TAG_INTERNET_FAILURE).
                        commitAllowingStateLoss();
            } else {
                if (customOnFailure() != null) {
                    customOnFailure().run();
                } else
                    showSnack(getString(R.string.no_internet));
            }
        }
        //else
        //    showSnack(getString(R.string.exception));
        dismiss();
    }

    protected void dismiss() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    @Override
    public void onLocationFetched(int reqCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (fragment != null) {
            if (((BaseFragment) fragment) instanceof NewsfeedFragment) {
                NewsfeedFragment frag = (NewsfeedFragment) ((BaseFragment) fragment);
                frag.onLocationFetched(reqCode, data);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        //mDrawerLayout.closeDrawers();
        setDefaultBackground();
        switch (itemId) {
            case R.id.my_goals:
                Utils.hide(this);
                if(bottomMenuAdapter != null)
                    bottomMenuAdapter.setSelectedMenu(0);
                break;
            case R.id.other_goals:
                addFragment(new OtherMainFragment());
                if (mBottomLL != null)
                    mBottomLL.setVisibility(View.GONE);
                break;
            case R.id.my_diary:
                addFragment(new MyDiaryFragment());
                if (mBottomLL != null)
                    mBottomLL.setVisibility(View.GONE);
                break;
            case R.id.rewards:
                addFragment(new RewardFragment());
                if (mBottomLL != null)
                    mBottomLL.setVisibility(View.GONE);
                break;

            case R.id.payment:
                addFragment(new PaymentFragment());
                if (mBottomLL != null)
                    mBottomLL.setVisibility(View.GONE);
                break;
            case R.id.about_app:
                Utils.hide(this);
                mDrawerLayout.closeDrawer(Gravity.LEFT);
                mIntent = new Intent(this, AboutActivity.class);
                startActivity(mIntent);
                break;
            case R.id.terms:
                Utils.hide(this);
                mDrawerLayout.closeDrawer(Gravity.LEFT);
                mIntent = new Intent(this, AboutActivity.class);
                mIntent.putExtra("name",getString(R.string.terms));
                startActivity(mIntent);
                break;
            case R.id.setting:
                mIntent = new Intent(this, SettingActivity.class);
                startActivity(mIntent);
                break;

            case R.id.logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure you want to logout?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                mApiBuilder.logout(BaseActivity.USER.getAccessToken(), new CustomRunnable() {
                                    @Override
                                    public void run(Object data) {

                                    }
                                }, mContext);
                                Intent in = new Intent(mContext, LoginActivity.class);
                                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(in);
                                mPreferences.edit().clear().apply();
                                getSharedPreferences(PREFERENCE_KEY,MODE_PRIVATE).edit().putBoolean("first", true).apply();
                                USER = null;
                                try{
                                    LoginManager.getInstance().logOut();
                                }catch (Exception ex){
                                    ex.printStackTrace();
                                }

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                break;
            /*case R.id.courses:
                Utils.hide(this);
                if(bottomMenuAdapter != null)
                    bottomMenuAdapter.setSelectedMenu(0);
                break;
            case R.id.notification:
                mIntent = new Intent(this, NotificationActivity.class);
                startActivity(mIntent);
                break;
            case R.id.setting:
                mIntent = new Intent(this, SettingActivity.class);
                startActivity(mIntent);
                break;
            case R.id.logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure you want to logout?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent in = new Intent(mContext, LoginActivity.class);
                                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(in);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

                break;*/
        }
        return false;
    }

    public void setToolBar(int drawableId) {
        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tb);

        getSupportActionBar().setHomeAsUpIndicator(drawableId);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
    }
    public void setToolBar() {
        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tb);
        getSupportActionBar().setTitle("");
    }

    public void setToolBackground() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView toolbarTile = toolbar.findViewById(R.id.toolbar_title);
        toolbarTile.setTextColor(getResources().getColor(R.color.white));
        AppBarLayout tb = findViewById(R.id.appbar);
        tb.setElevation(0);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.jugar));
    }

    public void setToolBackgroundProfile() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView toolbarTile = toolbar.findViewById(R.id.toolbar_title);
        toolbarTile.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        AppBarLayout tb = findViewById(R.id.appbar);
        tb.setElevation(0);
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.bgtop));
    }

    public void setDefaultBackground() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView toolbarTile = toolbar.findViewById(R.id.toolbar_title);
        toolbarTile.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        AppBarLayout tb = findViewById(R.id.appbar);
        if (tb != null) {
            tb.setElevation(10);
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.white)));
        }
    }

    public Toolbar getToolbar() {
        return  findViewById(R.id.toolbar);
    }

    public void setTitle(String title) {
        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
        TextView textView = (TextView) tb.findViewById(R.id.toolbar_title);
        textView.setText(title);
    }

    public void showSnack(String message) {
        Snackbar snackbar = Snackbar.make(mParentCoordinatorLayout, message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected String getMessage() {
        return "Loading...";
    }


    protected void onStart() {
        super.onStart();
    }

    protected void onStop() {
        super.onStop();
    }

    private void grantPermission(){
        ArrayList<String> permissions = new ArrayList<String>();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
                permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.CAMERA);
            }
            if (ContextCompat.checkSelfPermission(this,
                    RECORD_AUDIO)
                    != PackageManager.PERMISSION_GRANTED) {
                permissions.add(RECORD_AUDIO);
            }
            if (ContextCompat.checkSelfPermission(this,
                    READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {
                permissions.add(READ_CONTACTS);
            }


            if (permissions.size() > 0)
                ActivityCompat.requestPermissions(this, permissions.toArray(new String[permissions.size()]), 1);

        }
    }

    public void saveUser(UserModel user){
        Gson gson = new Gson();
        if(mPreferences != null) {
            mPreferences.edit().putString(BaseActivity.USER_KEY, gson.toJson(user)).apply();
        }
    }

    @Override
    public void onMenuClick(BottomModel.Menu menu, int pos) {
        findViewById(R.id.toolbar).setVisibility(View.VISIBLE);
        setDefaultBackground();
        switch (pos){
            case 0:
                //changeFragment(NewsfeedFragment.newInstacne(), NewsfeedFragment.TAG_NEWSFEED);
                changeFragment(new MyGoals(), TAG_GOAL);
                break;
            case 1:
                changeFragment(NewsfeedFragment.newInstacne(), NewsfeedFragment.TAG_NEWSFEED);
                break;
            case 2:
                changeFragment(new MyFriends(), TAG_MY_FRIEND);
                break;
            case 3:
/*                findViewById(R.id.appbar).setVisibility(View.GONE);
                findViewById(R.id.toolbar).setVisibility(View.GONE);*/
                changeFragment(ProfileFragment.newInstance(1,BaseActivity.USER.getId()), TAG_FEEDBACK);
                break;
        }

    }

    public void changeFragment(Fragment fragment, String tag){
        FragmentManager fragmentManager = getSupportFragmentManager();
        if(fragmentManager != null) {
            fragmentManager.popBackStack(null, android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        //transaction.addToBackStack(null);
        transaction.setCustomAnimations(R.animator.fade_in, R.animator.fade_out, R.animator.fade_in, R.animator.fade_out);
        transaction.replace(R.id.content, fragment).commitAllowingStateLoss();
    }

    public void addFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.animator.slide_in_right, R.animator.slide_out_left, R.animator.slide_in_left, R.animator.slide_out_right);
        transaction.addToBackStack(null);
        transaction.replace(R.id.content, fragment,fragment.getClass().toString()).commitAllowingStateLoss();

    }

    public void addFragment(Fragment fragment, String name){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.animator.slide_in_right, R.animator.slide_out_left, R.animator.slide_in_left, R.animator.slide_out_right);
        transaction.addToBackStack(null);
        transaction.add(R.id.content, fragment).commitAllowingStateLoss();
    }

    @Override
    public void onClick(View view) {
        Intent in = new Intent(view.getContext(), SignupActivity.class);
        in.putExtra("name", true);
        view.getContext().startActivity(in);
    }

    private void initNavDrawer(int containerID) {
        switch (containerID) {

            case R.id.coordinatorLayout:
                mDrawerLayout = (AdvanceDrawerLayout) findViewById(R.id.drawer_layout);
                ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                        this, mDrawerLayout, (Toolbar) findViewById(R.id.toolbar), R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                mDrawerLayout.addDrawerListener(toggle);
                toggle.syncState();

                mNavigationView = (NavigationView) findViewById(R.id.navigation);
                mNavigationView.setNavigationItemSelectedListener(this);

                ((AdvanceDrawerLayout) mDrawerLayout).setViewScale(Gravity.START, 0.65f);
                ((AdvanceDrawerLayout) mDrawerLayout).setViewElevation(Gravity.START, getResources().getDimension(R.dimen.navigation_drawer_elevation));

                ((AdvanceDrawerLayout) mDrawerLayout).useCustomBehavior(Gravity.START); //assign custom behavior for "Left" drawer

                ((AdvanceDrawerLayout) mDrawerLayout).setViewScrimColor(Gravity.START, Color.TRANSPARENT);//set drawer overlay coloe (color)


                Log.e("TEST", "Custom");
                break;
        }


    }

    public void setDrawerEnabled(boolean enabled) {
        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        mDrawerLayout.setDrawerLockMode(lockMode);
    }

    protected String getMessage(Object object){
        if (object instanceof  ArrayList){
            ArrayList<String> message = (ArrayList<String>) object;
            if (message.size() > 0)
                return message.get(0).toString();
        }else
            return object.toString();

        return  "Something went wrong";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == RESULT_OK) {

            switch (requestCode) {
                case Constants.IMAGES_GALLERY:
                case Constants.IMAGES_CAMERA:
                case Constants.VIDEO_GALLERY:
                case Constants.VIDEO_CAMERA:
                case REQUEST_VIDEO_CAPTURE:
                    onMediaLoad(requestCode, intent);
                    break;
                case Constants.MAP_ADDRESS_RESULT_CODE:

                    onLocationFetched(requestCode, intent);
                    break;

                case Constants.DROP_IN_REQUEST:
                    DropInResult dropInResult = intent.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                    String paymentMethodNonce = dropInResult.getPaymentMethodNonce().getNonce();
                    //Log.e(MyApplication.TAG, "Payment Nonce = " + paymentMethodNonce);
//                    Toast.makeText(mContext, "Payment Nonce = "+ paymentMethodNonce, Toast.LENGTH_SHORT).show();
                    String type = dropInResult.getPaymentMethodNonce().getTypeLabel();
                    if (type.toString().toLowerCase().contains("paypal"))
                        type = "Paypal";
                    else {
                        type = "CC";
                    }
                    if (mExtraIntent != null){
                        if (mExtraIntent.getStringExtra("type").equalsIgnoreCase("task")) {
                            mApiBuilder.taskPayment(BaseActivity.USER.getAccessToken(),paymentMethodNonce, mExtraIntent.getStringExtra("amount"), mExtraIntent.getStringExtra("taskId"),new CustomRunnable() {
                                @Override
                                public void run(Object data) {

                                }
                            }, this);
                        }
                        else if (mExtraIntent.getStringExtra("type").equalsIgnoreCase("proof")){
                            mApiBuilder.proofPayment(BaseActivity.USER.getAccessToken(),paymentMethodNonce, mExtraIntent.getStringExtra("amount"), mExtraIntent.getStringExtra("package_id"),new CustomRunnable() {
                                @Override
                                public void run(Object data) {

                                }
                            }, this);
                        }
                    }

                    /*if (mBaseFragment instanceof RequestsCustomerFragment)
                        ((RequestsCustomerFragment) mBaseFragment).onPaymentLoad(paymentMethodNonce);

                    if (mBaseFragment instanceof PendingOrdersFragment)
                        ((PendingOrdersFragment) mBaseFragment).onPaymentLoad(paymentMethodNonce);*/

                    break;

            }

            if (FacebookSdk.isFacebookRequestCode(requestCode)) {
                mCallbackManager.onActivityResult(requestCode, resultCode, intent);
            }

        }
    }

    @Override
    public void onMediaLoad(int reqCode, Intent data) {
        switch (reqCode) {
            case Constants.IMAGES_GALLERY:
                if (data != null) {

                    // Get the Image from data
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    mMediaPath = cursor.getString(columnIndex);
                    if (mMediaPath == null){
                        showSnack("Google drive photos cannot be selected!");
                    }

                    cursor.close();

                }
                break;

            case Constants.IMAGES_CAMERA:

                //if (data != null) {
                    try {
                    Bitmap bm = BitmapFactory.decodeFile(getRealPathFromURIPath(fileUri,this));
                    bm = Utils.scaleImageKeepAspectRatio(bm);
                    bm = Utils.modifyOrientation(bm,getRealPathFromURIPath(fileUri,this));

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
                    byte[] b = baos.toByteArray();

                    //Date d = new Date();
                    //CharSequence s = DateFormat.format("yyMMdd_hhmmss", d.getTime());
                    File f = new File(getRealPathFromURIPath(fileUri,this));
                        if (f.exists())
                            f.delete();
                        f.createNewFile();
                        FileOutputStream fo = new FileOutputStream(f);
                        fo.write(b);

                        fo.close();
                        mMediaPath = f.getAbsolutePath();
                    } catch (Exception e) {
                    }

                //}
                break;

            case Constants.VIDEO_GALLERY:

                if (data != null) {
//                // Get the Video from data
                    Uri selectedVideo = data.getData();
                    String[] filePathColumn = {MediaStore.Video.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedVideo, filePathColumn, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    mMediaPath = cursor.getString(columnIndex);
                    // Set the Video Thumb in ImageView Previewing the Media

                    cursor.close();
                }
                break;

            case Constants.VIDEO_CAMERA:

                if (data != null) {
                    Cursor cursor = getContentResolver().query(data.getData(), null, null, null, null);

                    if (cursor == null) {
                        // Source is Dropbox or other similar local file path
                        mMediaPath = data.getData().getPath();
                    } else {
                        if (cursor.moveToFirst()) {
                            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                            mMediaPath = cursor.getString(idx);

                        }
                        cursor.close();
                    }
                }
                break;
            case AUDIO_RECORDER:
                mMediaPath = mAudioPath;
                break;
        }
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (fragment != null) {
            if (((BaseFragment) fragment) instanceof EditProfileFragment ){
                EditProfileFragment frag = (EditProfileFragment) ((BaseFragment) fragment);
                frag.onMediaLoad(reqCode,data);
            }
            else if (((BaseFragment) fragment) instanceof NewsfeedFragment ){
                NewsfeedFragment frag = (NewsfeedFragment) ((BaseFragment) fragment);
                frag.onMediaLoad(reqCode,data);
            }
            else if (((BaseFragment) fragment) instanceof GoalDetailFragment){
                GoalDetailFragment frag = (GoalDetailFragment) ((BaseFragment) fragment);
                frag.onMediaLoad(reqCode,data);
            }
            else if (((BaseFragment) fragment) instanceof TaskDetailFragment){
                TaskDetailFragment frag = (TaskDetailFragment) ((BaseFragment) fragment);
                frag.onMediaLoad(reqCode,data);
            }
        }
    }

    public void showMediaPicker() {

        DialogInterface.OnClickListener posListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri = Uri.fromFile(Utils.getOutputImageFile(BaseActivity.this));

                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(intent, Constants.IMAGES_CAMERA);
            }
        };

        DialogInterface.OnClickListener negListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.select_picture)), Constants.IMAGES_GALLERY);
            }
        };

        Utils.showPromptDialog(mContext, getResources().getString(R.string.select_picture_mode), null, getResources().getString(R.string.camera), getResources().getString(R.string.gallery), posListener, negListener, true);
    }

    public void showVideoMediaPicker() {

        DialogInterface.OnClickListener posListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                    takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
                    startActivityForResult(takeVideoIntent, Constants.VIDEO_CAMERA);
                }
            }
        };

        DialogInterface.OnClickListener negListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("video/*");
                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
                startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.select_picture)), Constants.VIDEO_GALLERY);
            }
        };

        Utils.showPromptDialog(mContext, getResources().getString(R.string.select_video_mode), null, getResources().getString(R.string.camera), getResources().getString(R.string.gallery), posListener, negListener, true);
    }

    public void showMediaDailog() {

        DialogInterface.OnClickListener posListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showMediaPicker();
            }
        };

        DialogInterface.OnClickListener negListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//                if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
//                    //takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 60);
//                    startActivityForResult(takeVideoIntent, Constants.VIDEO_CAMERA);
//                }
                showVideoMediaPicker();
            }
        };

        DialogInterface.OnClickListener neutralListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showAudioDialog();
            }
        };

        Utils.showPromptDialog(mContext, "Select media mode", null, "Take Image", "Take Video", "Take Audio",neutralListener,posListener, negListener, true);
    }

    public void showImageVideoDialog() {

        DialogInterface.OnClickListener posListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showMediaPicker();
            }
        };

        DialogInterface.OnClickListener negListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//                if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
//                    takeVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
//                    startActivityForResult(takeVideoIntent, Constants.VIDEO_CAMERA);
//                }
                showVideoMediaPicker();
            }
        };



        Utils.showPromptDialog(mContext, "Select media mode", null, "Take Image", "Take Video",posListener, negListener, true);
    }

    public void initPayment(String token, Intent intent) {
        mExtraIntent = intent;
        DropInRequest dropInRequest = new DropInRequest()
                .clientToken(token);
        startActivityForResult(dropInRequest.getIntent(mContext), Constants.DROP_IN_REQUEST);
    }

    public void showAudioDialog(){


        View view = getLayoutInflater().inflate(R.layout.record_audio, null);
        final ImageView recordBtn = (ImageView) view.findViewById(R.id.record);
        final ImageView closeBtn = (ImageView) view.findViewById(R.id.close);

        mAudioPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "AudioRecording_" + new Date().getTime() + ".3gp";

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (new File(mAudioPath).exists()) {
                    if (mediaRecorder != null) {
                        recordBtn.setSelected(false);
                        mediaRecorder.stop();
                        mediaRecorder.release();
                        mediaRecorder = null;
                    }
                    onMediaLoad(AUDIO_RECORDER, new Intent());
                }
            }
        });

        recordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!recordBtn.isSelected()){
                    File fileRoot = new File(mAudioPath);
                    if (fileRoot.exists())
                        fileRoot.delete();
                    recordBtn.setSelected(true);
                    try {
                        startRecording(recordBtn);
                        mediaRecorder.prepare();
                        mediaRecorder.start();
                        showSnack( "Recording Started");
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    recordBtn.setSelected(false);
                    mediaRecorder.stop();
                    mediaRecorder.release();
                    mediaRecorder = null;
                    /*MediaPlayer mediaPlayer = new MediaPlayer();

                    try {
                        mediaPlayer.setDataSource(mAudioPath);
                        mediaPlayer.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    mediaPlayer.start();*/
                    showSnack( "Recording Completed");
                }
            }
        });
        dialog = new AlertDialog.Builder(this,R.style.AppTheme_DialogTheme)
                .setView(view).create();
        dialog.setCancelable(false);
        dialog.show();
    }


    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void startRecording(final ImageView recordBtn){
        mediaRecorder=new MediaRecorder();

        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);

        mediaRecorder.setOutputFile(mAudioPath);
        mediaRecorder.setMaxDuration(60*1000);
        mediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
            @Override
            public void onInfo(MediaRecorder mr, int what, int extra) {
                if (recordBtn.isSelected())
                    recordBtn.performClick();
            }
        });
    }

    public String showMessage(Object msg){
        if (msg != null) {
            if (msg instanceof ArrayList) {
                ArrayList<String> message = (ArrayList<String>) msg;
                if (message.size() > 0)
                    return message.get(0).toString();
            } else {
                return msg.toString();
            }
        }
        return "";
    }

    public void openMap() {
        Intent intent = new Intent(mContext, MapsActivity.class);

        startActivityForResult(intent, Constants.MAP_ADDRESS_RESULT_CODE);
    }

    @Override
    public void onResume(){
        super.onResume();
        invalidateOptionsMenu();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (fragment != null) {
            try {
                if (((BaseFragment) fragment) instanceof ProfileFragment) {
                    ProfileFragment profileFragment = (ProfileFragment) fragment;
                    profileFragment.updateUI();
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    protected abstract View getParentView(LayoutInflater inflater, ViewGroup parent);

    protected abstract Call<?> getCall() throws IOException;

    protected abstract Runnable customOnFailure();

    protected abstract void init();

    protected abstract Type getClassType();

}
