package com.digitonics.friendshipdiary.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;


import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.OnFragmentInteractionListener;
import com.digitonics.friendshipdiary.utilities.PrettyDialog;
import com.digitonics.friendshipdiary.utilities.PrettyDialogCallback;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.io.IOException;
import java.lang.reflect.Type;

import retrofit2.Call;

@IgnoreApi
public class CustomToastActivity extends BaseActivity implements OnFragmentInteractionListener, View.OnClickListener {

    Thread thread;
    PrettyDialog mNotificationDialog;
    Dialog mDialog;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected View getParentView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.layout_toast_custom, parent, false);
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return null;
    }

    @Override
    protected Runnable customOnFailure() {
        return null;
    }

    @Override
    protected void init() {

        mContext = this;




//        TextView textView = ((TextView) findViewById(R.id.textView_Toast));
//        textView.setText(getIntent().getExtras().getString(Utils.NOTIFICATION_TITLE) +
//                getIntent().getExtras().getString(Utils.NOTIFICATION_MESSAGE));
//        textView.setOnClickListener(this);


            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(5000);
                    } catch (Throwable e) {

                    } finally {
                        finish();
                    }
                }
            });
            thread.start();


        mNotificationDialog = new PrettyDialog(this);
        mNotificationDialog
                .setTitle(getIntent().getExtras().getString("title"))
                .setTitleColor(R.color.colorAccent)
                .setIconTint(R.color.colorAccent)
                .setMessage(getIntent().getExtras().getString("message"))
                .setMessageColor(R.color.pdlg_color_black)
//                .setTypeface(Typeface.createFromAsset(getResources().getAssets(),"myfont.ttf"))
                .setAnimationEnabled(true)
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorAccent)
                .setDialogCallback(new PrettyDialogCallback() {
                    @Override
                    public void onClick() {
                        Intent intent;

                        /*switch (getIntent().getStringExtra(Utils.NOTIFICATION_TYPE)) {
                            case Utils.NOTIFICATION_TYPE_CHAT:

                                intent = new Intent(CustomToastActivity.this, PopUpActivity.class);

                                intent.putExtra(Utils.NOTIFICATION_DATA_INTERESTEDID, getIntent().getStringExtra(Utils.NOTIFICATION_DATA_INTERESTEDID));
                                intent.putExtra(Utils.NOTIFICATION_DATA_SENDERID, getIntent().getStringExtra(Utils.NOTIFICATION_DATA_SENDERID));

                                intent.putExtra(Utils.NOTIFICATION_TITLE, getIntent().getStringExtra(Utils.NOTIFICATION_TITLE));
                                intent.putExtra(Utils.NOTIFICATION_MESSAGE, getIntent().getStringExtra(Utils.NOTIFICATION_MESSAGE));

                                intent.putExtra(Utils.NOTIFICATION_TYPE, getIntent().getStringExtra(Utils.NOTIFICATION_TYPE));
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                                startActivity(intent);
                                finish();
                                break;
                        }*/

                    }
                })
                .setCancelable(false);
        mNotificationDialog.show();

    }

    @Override
    protected Type getClassType() {
        return null;
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (thread != null && thread.isAlive() == true) {
            thread.interrupt();
        }

        if (mNotificationDialog != null && mNotificationDialog.isShowing())
            mNotificationDialog.dismiss();
    }

    @Override
    public void onClick(View view) {

//        Intent intent = new Intent(Utils.PUSH_NOTIFICATION);
//        intent.putExtras(getIntent().getExtras());
//        LocalBroadcastManager.getInstance(CustomToastActivity.this).sendBroadcast(intent);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void run() {

    }
}
