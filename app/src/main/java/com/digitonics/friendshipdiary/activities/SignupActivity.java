package com.digitonics.friendshipdiary.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.io.IOException;
import java.lang.reflect.Type;

import retrofit2.Call;

@IgnoreApi
public class SignupActivity extends BaseActivity<UserModel> implements View.OnClickListener {

    private EditText mFirstName, mLastName, mEmail, mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected View getParentView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.signup, parent, false);
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.register(mEmail.getText().toString(), mPassword.getText().toString(), mFirstName.getText().toString(), mLastName.getText().toString());
    }

    @Override
    protected Runnable customOnFailure() {
        return null;
    }

    @Override
    protected void init() {
       /* if(getIntent().hasExtra("name")){
            ((Button)findViewById(R.id.createBtn)).setText("Update Account");
            findViewById(R.id.alreadySign).setVisibility(View.GONE);
            findViewById(R.id.profileImage).setVisibility(View.GONE);
            findViewById(R.id.profileImageNew).setVisibility(View.VISIBLE);
            findViewById(R.id.back).setVisibility(View.VISIBLE);
        }*/
        mFirstName = findViewById(R.id.firstName);
        mLastName = findViewById(R.id.lastName);
        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.password);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hide(SignupActivity.this);
                onBackPressed();
            }
        });

        findViewById(R.id.createBtn).setOnClickListener(this);
        findViewById(R.id.alreadySign).setOnClickListener(this);
    }

    @Override
    protected Type getClassType() {
        return null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.createBtn:
                if (validateFields())
                {
                    Utils.hide(this);
                    callApi();
                }
                break;
            case R.id.alreadySign:
                Utils.hide(this);
                onBackPressed();
                break;
        }
    }

    @Override
    public void run() {
        saveUser(mData);
        Intent in ;
        if (mData.getIsActive().equalsIgnoreCase("3")) {
            in = new Intent(this, HomeActivity.class);
        }else {
            in = new Intent(this, EditProfileActivity.class);
        }
        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(in);
        showSnack(getMessage(mBody.getResponse().getMessage()));
    }

    private boolean validateFields() {

        if (mFirstName.getText().toString().isEmpty() == true) {
            showSnack( "Please enter first name");
            return false;
        }
        if (mLastName.getText().toString().isEmpty() == true) {
            showSnack( "Please enter last name");
            return false;
        }

        if (TextUtils.isEmpty(mEmail.getText().toString())) {
            showSnack( "Please enter email address!");
            return false;
        } else if (!Utils.isEmailValid(mEmail.getText().toString())) {
            showSnack( "Please enter valid email address!");
            return false;
        }

        // Check if password is empty or not.
        if (mPassword.getText().toString().isEmpty() == true) {
            showSnack( "Please enter password");
            return false;
        } else if (mPassword.getText().toString().length() < 8 || mPassword.getText().toString().length() > 20) {
            showSnack( "Password should be in between 8-20 characters long");
            return false;
        }


        return true;
    }
}
