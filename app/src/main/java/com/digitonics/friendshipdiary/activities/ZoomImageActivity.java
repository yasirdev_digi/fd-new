package com.digitonics.friendshipdiary.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.utilities.Utils;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import uk.co.senab.photoview.PhotoViewAttacher;


public class ZoomImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.layout_fullscreen_image);

        ImageView imageView_fullscreen = (ImageView) findViewById(R.id.fullscreen);

        if (getIntent().getBooleanExtra("isBitmap", false) == true) {
            Bitmap bmp = BitmapFactory.decodeFile(getIntent().getStringExtra("image").toString());
            bmp = Utils.scaleImageKeepAspectRatio(bmp);
            try {
                bmp = Utils.modifyOrientation(bmp, getIntent().getStringExtra("image").toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            Glide.with(this)
                    .asBitmap()
                    .load(bmp)
                    .listener(new RequestListener<Bitmap>() {

                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                            findViewById(R.id.fullscreen_bg).setVisibility(View.GONE);
                            return false;
                        }

                    })
//                    .apply(RequestOptions.placeholderOf(R.drawable.default_image))
//                    .apply(RequestOptions.noTransformation()/*.placeholderOf(R.drawable.default_image)*/.override(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).dontTransform())
                    .into(imageView_fullscreen);
        } else {
            Glide.with(this)
                    .load(getIntent().getStringExtra("image"))
                    .apply(RequestOptions.noTransformation()/*.placeholderOf(R.drawable.default_image)*/.override(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).dontTransform())
                    .listener(new RequestListener<Drawable>() {

                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            findViewById(R.id.fullscreen_bg).setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(imageView_fullscreen);

//            Picasso.with(this).load(getIntent().getStringExtra(Utils.KEY_IMAGE_RECEIPT)).placeholder(R.drawable.default_image).into(imageView_fullscreen);
        }

        PhotoViewAttacher pAttacher;
        pAttacher = new PhotoViewAttacher(imageView_fullscreen);
//        pAttacher.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
//            @Override
//            public void onPhotoTap(View view, float x, float y) {
//                finish();
//            }
//        });
        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.custom_dialog_anim_fade_in, R.anim.custom_dialog_anim_fade_out);
            }
        });
        pAttacher.update();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.custom_dialog_anim_fade_in, R.anim.custom_dialog_anim_fade_out);
    }
}
