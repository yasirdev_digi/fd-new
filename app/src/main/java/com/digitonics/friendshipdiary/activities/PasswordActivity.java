package com.digitonics.friendshipdiary.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.io.IOException;
import java.lang.reflect.Type;

import retrofit2.Call;

@IgnoreApi
public class PasswordActivity extends BaseActivity<Object> {

    private Dialog mDialogOrderDetail;
    private EditText mPassCurrentET,mPassNewET, mPassConfirmET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected View getParentView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.activity_change_password, parent, false);
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.changePassword(BaseActivity.USER.getAccessToken(),mPassCurrentET.getText().toString(), mPassNewET.getText().toString());
    }

    @Override
    protected Runnable customOnFailure() {
        return new Runnable() {
            @Override
            public void run() {
                showSnack("Old password is incorrect!");
            }
        };
    }

    @Override
    protected void init() {

        mPassCurrentET = (EditText) findViewById(R.id.old_password);
        mPassNewET = (EditText) findViewById(R.id.newPassword);
        mPassConfirmET = (EditText) findViewById(R.id.confirmPassword);
        setTitle("Change Password");
        setToolBar(R.drawable.ic_arrow_back_white_24dp);
        findViewById(R.id.create_btn).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                try {
                    if (validateFields() == true) {
                        callApi();
                    }
                }
                catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        });

    }

    @Override
    protected Type getClassType() {
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void run() {
        finish();
        showSnack("Password changed successfully");
    }

    private boolean validateFields() {

        // Check if Old password is empty or not.
        if (mPassCurrentET.getText().toString().isEmpty() == true) {
            showSnack( getResources().getString(R.string.error_invalid_password_current));
            return false;
        }

        // Check if New password is empty or not.
        if (mPassNewET.getText().toString().isEmpty() == true) {
            showSnack( getResources().getString(R.string.error_invalid_password_new));
            return false;
        }

        // Check if Confirm password is empty or not.
        if (mPassConfirmET.getText().toString().isEmpty() == true) {
            showSnack( getResources().getString(R.string.error_invalid_password_confirm));
            return false;
        }

        // Check if new password length is within specified range.
        if (mPassNewET.getText().toString().length() < 8 || mPassNewET.getText().toString().length() > 20) {
            showSnack( getResources().getString(R.string.error_invalid_password_length));
            return false;
        }

        // Check if new password is same as old password or not.
        if (mPassNewET.getText().toString().equals(mPassCurrentET.getText().toString()) == true) {
            showSnack( getResources().getString(R.string.error_invalid_password_new_current));
            return false;
        }

        // Check if new password is same as confirm password or not.
        if (mPassNewET.getText().toString().equals(mPassConfirmET.getText().toString()) == true) {
            return true;
        } else {
            showSnack( getResources().getString(R.string.error_invalid_password_not_match));
            return false;
        }

    }


}
