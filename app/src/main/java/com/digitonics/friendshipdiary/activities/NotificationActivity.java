package com.digitonics.friendshipdiary.activities;

import android.app.Dialog;
import android.app.Notification;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.adapters.NotificationAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.fragments.GoalDetailFragment;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.NotificationModel;
import com.digitonics.friendshipdiary.utilities.Constants;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;


public class NotificationActivity extends BaseActivity<ArrayList<NotificationModel>> implements  NotificationAdapter.OnNotificationClickListener {

    private LinearLayoutManager mNotificationManager;
    private RecyclerView mTasksRV;
    private RecyclerView mNotificationRV;
    private TextView textView, name;
    private ImageView profileImage;
    private View mNoData;
    private View mNotificationView, firstView;
    private NotificationModel baseModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected View getParentView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.activity_other_goals, parent, false);
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.getNotification(BaseActivity.USER.getAccessToken());
    }

    @Override
    protected Runnable customOnFailure() {
        return null;
    }

    @Override
    protected void init() {
        mNoData = findViewById(R.id.no_data_img);
        mNotificationView = findViewById(R.id.notifcation_view);
        mNotificationRV = (RecyclerView) findViewById(R.id.earlier_notifications);
        mNotificationRV = (RecyclerView) findViewById(R.id.earlier_notifications);
        mNotificationManager = new LinearLayoutManager(this);
        textView = findViewById(R.id.following);
        name = findViewById(R.id.name);
        firstView = findViewById(R.id.goal_name);
        profileImage = findViewById(R.id.profile_image);
        setToolBar(R.drawable.ic_arrow_back_white_24dp);
        setTitle("Notifications");
    }

    @Override
    protected Type getClassType() {
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void run() {
        mPreferences.edit().remove(BaseActivity.NOTIFICATION_KEY).apply();
        if (mData != null && mData.size() > 0 ) {
            mNotificationView.setVisibility(View.VISIBLE);
            if (mData != null && mData.size() > 0){
                baseModel = mData.get(0);
                String lastName = mData.get(0).getUser().getLastName() != null ? mData.get(0).getUser().getLastName() : "";
                name.setText(StringUtils.capitalize(mData.get(0).getUser().getFirstName()) + " " + StringUtils.capitalize(lastName));
                Picasso.get().load(mData.get(0).getUser().getImage() != null ? mData.get(0).getUser().getImage().get1x() : null).placeholder(R.drawable.user_default).error(R.drawable.user_default).into(profileImage);
                textView.setText(mData.get(0).getActivity().getExtras());
                firstView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onNotificationClick(baseModel);
                    }
                });
            }
            NotificationAdapter mAdapter = new NotificationAdapter(mData, this);

            mNotificationRV.setLayoutManager(mNotificationManager);
            mNotificationRV.setAdapter(mAdapter);
        }else{
            mNoData.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setTitle("Notifications");
    }

    @Override
    public void onNotificationClick(NotificationModel model) {
        if (model.getActivity().getAction().equalsIgnoreCase("sponsor")){
            GoalModel goalModel = new GoalModel();
            goalModel.setId(model.getActivity().getObjectId());
            goalModel.setUserType(Constants.SPONSER);
            addFragment(GoalDetailFragment.newInstance(goalModel));

        }

    }
}
