package com.digitonics.friendshipdiary.activities;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.models.InfoModel;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import retrofit2.Call;

public class AboutActivity extends BaseActivity<ArrayList<InfoModel>> {

    private LinearLayoutManager mInstitutesManager;
    private RecyclerView mInstitutes;
    private Dialog mDialogOrderDetail;
    boolean isTerms = false;
    private WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected View getParentView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.activity_about_us, parent, false);
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.getInfo(BaseActivity.USER.getAccessToken(), isTerms == true ? "termsandconditions" : "aboutus");
    }

    @Override
    protected Runnable customOnFailure() {
        return null;
    }

    @Override
    protected void init() {
        if (getIntent().hasExtra("name")) {
            isTerms = true;
            setTitle(getIntent().getStringExtra("name"));
        } else
            setTitle("About Us");
        setToolBar(R.drawable.ic_arrow_back_white_24dp);
        webview = (WebView)this.findViewById(R.id.heading_1);
        webview.getSettings().setJavaScriptEnabled(true);


    }

    @Override
    protected Type getClassType() {
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void run() {
        String content = "";
        if (mData != null && mData.size() > 0) {
            content = mData.get(0).getContent();
            webview.loadData(content, "text/html; charset=utf-8", "UTF-8");
        }
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ((TextView) findViewById(R.id.heading_1)).setText(Html.fromHtml(
                    content, Html.FROM_HTML_MODE_COMPACT));
        } else {
            ((TextView) findViewById(R.id.heading_1)).setText(Html.fromHtml(content));
        }*/
    }
}
