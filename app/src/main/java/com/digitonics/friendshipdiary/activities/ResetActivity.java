package com.digitonics.friendshipdiary.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.io.IOException;
import java.lang.reflect.Type;

import retrofit2.Call;

@IgnoreApi
public class ResetActivity extends BaseActivity {

    private Dialog mDialogOrderDetail;
    private EditText mEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected View getParentView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.reset, parent, false);
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return mApiBuilder.forgotPassword(mEmail.getText().toString());
    }

    @Override
    protected Runnable customOnFailure() {
        return null;
    }

    @Override
    protected void init() {


        findViewById(R.id.reseBtn).setOnClickListener(this);
        findViewById(R.id.signup).setOnClickListener(this);

        mEmail = findViewById(R.id.username);
    }

    @Override
    protected Type getClassType() {
        return null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.reseBtn:
                if (TextUtils.isEmpty(mEmail.getText().toString())) {
                    showSnack("Please enter email address");
                } else if (Utils.isEmailValid(mEmail.getText().toString())) {
                    callApi();
                } else {
                    showSnack( "Please enter valid email address");
                }
                break;
            case R.id.signup:
                Utils.hide(this);
                onBackPressed();
                break;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void run() {
        showSnack( getString(R.string.an_email_has_been_send_to_your_email_address));
        Utils.hide(this);
        onBackPressed();
    }
}
