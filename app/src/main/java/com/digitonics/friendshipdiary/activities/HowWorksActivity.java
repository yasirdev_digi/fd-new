package com.digitonics.friendshipdiary.activities;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.adapters.HowPagerAdapter;
import com.digitonics.friendshipdiary.adapters.PagerAdapter;

public class HowWorksActivity extends AppCompatActivity {

    private HowPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_works);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new HowPagerAdapter(this, getSupportFragmentManager(), null);
        viewPager.setAdapter(adapter);

    }
}
