package com.digitonics.friendshipdiary.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.adapters.NotificationAdapter;
import com.digitonics.friendshipdiary.annotations.IgnoreApi;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Set;

import retrofit2.Call;

@IgnoreApi
public class SettingActivity extends BaseActivity {

    private LinearLayoutManager mInstitutesManager;
    private RecyclerView mInstitutes;
    private Dialog mDialogOrderDetail;
    private Dialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected View getParentView(LayoutInflater inflater, ViewGroup parent) {
        return inflater.inflate(R.layout.activity_settings, parent, false);
    }

    @Override
    protected Call<?> getCall() throws IOException {
        return null;
    }

    @Override
    protected Runnable customOnFailure() {
        return null;
    }

    @Override
    protected void init() {

        findViewById(R.id.notifcation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), NotificationActivity.class));
            }
        });

        findViewById(R.id.change_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (BaseActivity.USER.getSocialId() == null) {
                    startActivity(new Intent(view.getContext(), PasswordActivity.class));
                }else
                    Utils.showToast(SettingActivity.this, "Changed password is disabled for social login user!");

            }
        });

        findViewById(R.id.feedback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFeedbackDialog(view.getContext());
            }
        });

        if (mPreferences.contains("notification")){
            ((SwitchCompat)findViewById(R.id.switch_compat)).setChecked(mPreferences.getBoolean("notification", true));
        }
        ((SwitchCompat)findViewById(R.id.switch_compat)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked)
            {
                String status;
                if (isChecked) {
                    status = "1";
                }
                else {
                    status = "0";
                }
                mApiBuilder.notification(BaseActivity.USER.getAccessToken(), status, new CustomRunnable() {
                    @Override
                    public void run(Object data) {
                        mPreferences.edit().putBoolean("notification", isChecked).apply();
                    }
                }, SettingActivity.this);

            }
        });



        /*findViewById(R.id.about_diary).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), AboutActivity.class));
            }
        });*/
        findViewById(R.id.sign_out).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setMessage("Are you sure you want to logout?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent in = new Intent(mContext, LoginActivity.class);
                                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(in);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        /*findViewById(R.id.about).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), AboutActivity.class));
            }
        });
        findViewById(R.id.help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Client will provide the functionality.", Toast.LENGTH_SHORT).show();
            }
        });
        findViewById(R.id.faq).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Client will provide the functionality.", Toast.LENGTH_SHORT).show();
            }
        });*/
        setToolBar(R.drawable.ic_arrow_back_white_24dp);
        setTitle("Settings");

    }

    @Override
    protected Type getClassType() {
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void run() {

    }

    private void openFeedbackDialog(final Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_feedback, null);

        final EditText feedbackET = (EditText) view.findViewById(R.id.diag_feedback_commentsET);
        final RatingBar rateRB = (RatingBar) view.findViewById(R.id.diag_feedback_rateRB);

        Button submitBT = (Button) view.findViewById(R.id.diag_feedback_submitBT);

        ImageView closeIV = (ImageView) view.findViewById(R.id.diag_reject_closeIV);

        closeIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.hide(SettingActivity.this);
//                showSnack( mContext.getResources().getString(R.string.request_has_been_accepted));

                /*((BaseActivity) mContext).mApiBuilder.feedbackRDA((getIntent().hasExtra(Utils.NOTIFICATION_DATA_ORDERID) == true ? getIntent().getExtras().getString(Utils.NOTIFICATION_DATA_ORDERID) : ""), (getIntent().hasExtra(Utils.NOTIFICATION_DATA_RDAID) == true ? getIntent().getExtras().getString(Utils.NOTIFICATION_DATA_RDAID) : ""), feedbackET.getText().toString().trim().toString(), (int) rateRB.getRating(), new CustomRunnable() {
                    @Override
                    public void run(Object data) {
                        mDialog.dismiss();
                        finish();
                    }
                });*/
                mDialog.dismiss();
                ((BaseActivity)view.getContext()).mApiBuilder.sendFeedback(BaseActivity.USER.getAccessToken(), feedbackET.getText().toString(), "" + rateRB.getRating(),new CustomRunnable() {
                    @Override
                    public void run(Object data) {

                    }
                }, view.getContext());
            }
        });

        mDialog = Utils.showCustomDialog(context, view);
    }
}
