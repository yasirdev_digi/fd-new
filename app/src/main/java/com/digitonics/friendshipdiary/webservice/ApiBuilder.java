package com.digitonics.friendshipdiary.webservice;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.interfaces.CustomRunnable;
import com.digitonics.friendshipdiary.interfaces.WebApi;
import com.digitonics.friendshipdiary.models.CommentModel;
import com.digitonics.friendshipdiary.models.GoalModel;
import com.digitonics.friendshipdiary.models.ProofModel;
import com.digitonics.friendshipdiary.models.UserModel;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Dispatcher;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Syed Kamil Hafeez on 10/16/2017.
 */
public class ApiBuilder {

    private static final String BASE_URL = "https://www.fnds.us/api/";
    private static final String KEY_ID = "id";
    private static final String KEY_USER_ID = "userid";
    private static final boolean SHOW_PROGRESS = true;
    private static final boolean HIDE_PROGRESS = false;
    private final OkHttpClient mClient;
    public Retrofit mRetrofit;

    private WebApi mCallApi;

    private static ApiBuilder mInstance = null;

    public static ApiBuilder getInstance(Context context) {
        if (mInstance == null)
            mInstance = new ApiBuilder(context);
        return mInstance;
    }

    private Context mContext;
    private SharedPreferences mPreferences;

    private ApiBuilder(Context context) {

        mContext = context;
        mPreferences = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequests(3);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        mClient = new OkHttpClient.Builder().dispatcher(dispatcher).addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {

                Request request = chain.request().newBuilder()
                        .build();
                Log.e("Request:",request.toString());
//                okhttp3.Response response = chain.proceed(request);
//                Log.e("Response:",response.body().string());

                return chain.proceed(request);
            }
        }).readTimeout(60*3, TimeUnit.SECONDS).build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(mClient)
                .build();

        mCallApi = mRetrofit.create(WebApi.class);
    }

    public OkHttpClient getClient(){
        return mClient;
    }

    public Call<?> login(String username, String password, String loginType) {
        return mCallApi.login(username, password, loginType);
    }

    public Call<?> login(String email, String socialType, String loginType, String socialId, String fName, String image) {
        return mCallApi.login(socialId, socialType, loginType,email, fName, image);
    }

    public Call<?> forgotPassword(String email) {
        return mCallApi.forgotPassword(email);
    }

    public Call<?> register( String email, String password, String firstname, String lastname) {

        return mCallApi.register(email, password, firstname, lastname, "user");
    }


    public Call<?> allUser( String token) {

        return mCallApi.allUser("Bearer " + token);
    }

    public Call<?> allGoal( String token, String type, String status) {
        if (status.equalsIgnoreCase("active"))
            return mCallApi.allGoal("Bearer " + token, type);
        else
            return mCallApi.allGoal("Bearer " + token, type, status);
    }

    public Call<?> createGoal( String token, String name , String desc, String startDate, String endDate,String goalType, String friends) {

        return mCallApi.createGoal("Bearer " + token, name, desc, startDate, endDate, goalType, friends);
    }

    public Call<?> updateGoal( String token, String name , String desc, String startDate, String endDate, String goalId, String friends) {

        return mCallApi.updateGoal("Bearer " + token, name, desc, startDate, endDate, goalId, friends);
    }

    public Call<?> updateGoal( String token, String name , String desc, String startDate, String endDate, String goalId) {

        return mCallApi.updateGoal("Bearer " + token, name, desc, startDate, endDate, goalId);
    }

    public Call<?> getGoalDetail( String token, String goalId) {

        return mCallApi.goalDetail("Bearer " + token, goalId);
    }

    public Call<?> getTaskDetail( String token, String taskId) {

        return mCallApi.findTaskById("Bearer " + token, taskId);
    }

    public Call<?> createTask( String token, String name , String desc, String startDate, String endDate,String levelType, String points, String rewards, String goalId) {

        return mCallApi.createTask("Bearer " + token, name, desc, startDate, endDate, levelType, points, rewards, goalId);
    }

    public Call<?> updateTask( String token, String name , String desc, String startDate, String endDate,String levelType, String points, String rewards, String taskId) {

        return mCallApi.updateTask("Bearer " + token, name, desc, startDate, endDate, levelType, points, rewards, taskId);
    }

    public Call<?> createDiary(String token, String name , String desc, String startDate, String time) {

        return mCallApi.createDiary("Bearer " + token, name, desc, startDate, time,Utils.getDate());
    }

    public Call<?> updateDiary(String token, String name , String desc, String startDate, String time, String id) {

        return mCallApi.updateDairy("Bearer " + token, name, desc, startDate, time,id, Utils.getDate());
    }

    public Call<?> allDairy( String token) {

        return mCallApi.allDairy("Bearer " + token);
    }

    public Call<?> userUpdate(String imagePath, String firstname, String lastname, long phone, String about, String username, String facebookLink, String twitterLink, String googleLink, String instaLink, ArrayList<String> skill, String token) {
        MultipartBody.Part profPicPart = null;
        MultipartBody.Part[] skillParts = new MultipartBody.Part[skill.size()];
        for (int i = 0; i < skill.size(); i++) {
                skillParts[i] = MultipartBody.Part.createFormData("quality[]", skill.get(i));
        }
        if (imagePath != null && !imagePath.equalsIgnoreCase("")) {
            File profPic = new File(imagePath);
            profPicPart = MultipartBody.Part.createFormData("image", profPic.getName(), RequestBody.create(MediaType.parse("image/*"), profPic));
        }

        RequestBody fnBody = RequestBody.create(MediaType.parse("text/plain"), firstname);
        RequestBody lnBody = RequestBody.create(MediaType.parse("text/plain"), lastname);
        RequestBody aBody = RequestBody.create(MediaType.parse("text/plain"), about);
        RequestBody uBody = RequestBody.create(MediaType.parse("text/plain"), username);
        RequestBody fBody = RequestBody.create(MediaType.parse("text/plain"), facebookLink);
        RequestBody tBody = RequestBody.create(MediaType.parse("text/plain"), twitterLink);
        RequestBody gBody = RequestBody.create(MediaType.parse("text/plain"), googleLink);
        RequestBody iBody = RequestBody.create(MediaType.parse("text/plain"), instaLink);
        RequestBody isActive = RequestBody.create(MediaType.parse("text/plain"), "3");

        return mCallApi.userUpdate("Bearer " + token,profPicPart, fnBody, lnBody, phone, aBody, uBody,fBody,gBody,tBody,iBody,skillParts, isActive);
    }

    public Call<?> getUserDetail( String token, String userId) {

        return mCallApi.getUserDetail("Bearer " + token, userId);
    }

    public Call<?> getFriends( String token) {

        return mCallApi.getFriends("Bearer " + token);
    }

    public void follow(String token, String userId, final CustomRunnable runnable, final Context context) {
        mCallApi.followToggle("Bearer " +token, userId).enqueue(new CustomApiCall<String>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                //((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void notification(String token, String notification, final CustomRunnable runnable, final Context context) {
        mCallApi.notificaiton("Bearer " +token, notification).enqueue(new CustomApiCall<String>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void post(String imagePath, String videoPath, String audioPath, String postText, ArrayList<UserModel> users,Double lat, Double lng , String token,final CustomRunnable runnable, final Context context) {

        MultipartBody.Part[] image = null;
        MultipartBody.Part videoURL = null, audioURL = null;
        MultipartBody.Part[] userParts = new MultipartBody.Part[users.size()];
        RequestBody fnBody = RequestBody.create(MediaType.parse("text/plain"), postText);
        RequestBody dateBody = RequestBody.create(MediaType.parse("text/plain"), Utils.getDate());

        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).isChecked())
                userParts[i] = MultipartBody.Part.createFormData("tag_friend[]", users.get(i).getId());
        }

        if (imagePath != null && !imagePath.equalsIgnoreCase("")){

            image = new MultipartBody.Part[1];

            File file;
            for (int i = 0; i < 1; i++) {
                file = new File(imagePath);
                if (file.exists())
                    image[i] = MultipartBody.Part.createFormData("photo_url_image[]", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
            }

            File postImage = new File(imagePath);
            MultipartBody.Part profPicPart = MultipartBody.Part.createFormData("photo_url_image", postImage.getName(), RequestBody.create(MediaType.parse("image/*"), postImage));

        } else if(videoPath != null && !videoPath.equalsIgnoreCase("")){

            File postVideo = new File(videoPath);
            videoURL = MultipartBody.Part.createFormData("video_url_file", postVideo.getName(), RequestBody.create(MediaType.parse("video/*"), postVideo));

        } else if(audioPath != null && !audioPath.equalsIgnoreCase("")){

            File audio = new File(audioPath);
            audioURL = MultipartBody.Part.createFormData("audio_url_file", audio.getName(), RequestBody.create(MediaType.parse("audio/*"), audio));
        }

        CustomApiCall customApiCall = new CustomApiCall<String>(context, SHOW_PROGRESS) {
            @Override
            public void run() {

                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));

            }
        };

        if (image != null)
            mCallApi.post("Bearer " + token, image, fnBody, userParts,dateBody).enqueue(customApiCall);
        else if(videoURL != null){
            mCallApi.post("Bearer " + token, videoURL, fnBody, userParts,dateBody).enqueue(customApiCall);
        }
        else if (audioURL != null) {
            mCallApi.post("Bearer " + token, audioURL, fnBody, userParts,dateBody).enqueue(customApiCall);
        }else if(lat != null && lng != null){
            mCallApi.post("Bearer " + token, fnBody,lat,lng, userParts,dateBody).enqueue(customApiCall);
        }
        else
            mCallApi.post("Bearer " + token, fnBody, userParts,dateBody).enqueue(customApiCall);
    }
    public void postUpdate(String imagePath, String videoPath, String audioPath, String postText, String  postId,Double lat, Double lng ,  String token,final CustomRunnable runnable, final Context context) {

        MultipartBody.Part[] image = null;
        MultipartBody.Part videoURL = null, audioURL = null;

        RequestBody fnBody = RequestBody.create(MediaType.parse("text/plain"), postText);
        RequestBody idBody = RequestBody.create(MediaType.parse("text/plain"), postId);


        if (imagePath != null && !imagePath.equalsIgnoreCase("")){

            image = new MultipartBody.Part[1];

            File file;
            for (int i = 0; i < 1; i++) {
                file = new File(imagePath);
                if (file.exists())
                    image[i] = MultipartBody.Part.createFormData("photo_url_image[]", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
            }

            File postImage = new File(imagePath);
            MultipartBody.Part profPicPart = MultipartBody.Part.createFormData("photo_url_image", postImage.getName(), RequestBody.create(MediaType.parse("image/*"), postImage));

        } else if(videoPath != null && !videoPath.equalsIgnoreCase("")){

            File postVideo = new File(videoPath);
            videoURL = MultipartBody.Part.createFormData("video_url_file", postVideo.getName(), RequestBody.create(MediaType.parse("video/*"), postVideo));

        } else if(audioPath != null && !audioPath.equalsIgnoreCase("")){

            File audio = new File(audioPath);
            audioURL = MultipartBody.Part.createFormData("audio_url_file", audio.getName(), RequestBody.create(MediaType.parse("audio/*"), audio));
        }

        CustomApiCall customApiCall = new CustomApiCall<String>(context, SHOW_PROGRESS) {
            @Override
            public void run() {

                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));

            }
        };

        if (image != null)
            mCallApi.postUpdate("Bearer " +token, image, fnBody, idBody).enqueue(customApiCall);
        else if(videoURL != null){
            mCallApi.postUpdate("Bearer " +token, videoURL, fnBody, idBody).enqueue(customApiCall);
        }
        else if (audioURL != null) {
            mCallApi.postUpdate("Bearer " + token, audioURL, fnBody, idBody).enqueue(customApiCall);
        }else if(lat != null && lng != null){
            mCallApi.postUpdate("Bearer " +token, fnBody,lat,lng, idBody).enqueue(customApiCall);
        }
        else
            mCallApi.postUpdate("Bearer " +token, fnBody, idBody).enqueue(customApiCall);
    }

    public Call<?> getPosts( String token) {

        return mCallApi.getPosts("Bearer " + token);
    }

    public void postLike(String token, String postId, final CustomRunnable runnable, final Context context) {
        mCallApi.postLike("Bearer " +token, postId).enqueue(new CustomApiCall<String>(context, HIDE_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                //((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void postComment(String token, String postId, String postText,final CustomRunnable runnable, final Context context) {
        mCallApi.postComment("Bearer " +token, postId, postText, Utils.getDate()).enqueue(new CustomApiCall<CommentModel>(context, HIDE_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                //((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public Call<?> getOtherGoals( String token) {

        return mCallApi.geOtherGoal("Bearer " + token);
    }

    public void addProve(String token, String imagePath, String taskId,final CustomRunnable runnable, final Context context) {

        File imagePic = new File(imagePath);

        MultipartBody.Part imagePart = MultipartBody.Part.createFormData("image[]", imagePic .getName(), RequestBody.create(MediaType.parse("image/*"), imagePic));

        RequestBody taskBody = RequestBody.create(MediaType.parse("text/plain"), taskId);
        RequestBody dateBody = RequestBody.create(MediaType.parse("text/plain"), Utils.getDate());

        mCallApi.addProve("Bearer " +token, imagePart, taskBody,dateBody).enqueue(new CustomApiCall<ProofModel>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void addVideoProve(String token, String videoPath, String taskId,final CustomRunnable runnable, final Context context) {

        File postVideo = new File(videoPath);
        MultipartBody.Part videoURL = MultipartBody.Part.createFormData("video_url_file", postVideo.getName(), RequestBody.create(MediaType.parse("video/*"), postVideo));


        RequestBody taskBody = RequestBody.create(MediaType.parse("text/plain"), taskId);
        RequestBody dateBody = RequestBody.create(MediaType.parse("text/plain"), Utils.getDate());

        mCallApi.addProve("Bearer " +token, videoURL, taskBody,dateBody).enqueue(new CustomApiCall<ProofModel>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void postVote(String token, String taskId, String taskLevel,final CustomRunnable runnable, final Context context) {
        mCallApi.postVote("Bearer " +token, taskId, taskLevel).enqueue(new CustomApiCall<CommentModel>(context, HIDE_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void postSponsor(String token, String is_agreed, String id,final CustomRunnable runnable, final Context context) {
        mCallApi.postSponsor("Bearer " +token, is_agreed, id,BaseActivity.USER.getId()).enqueue(new CustomApiCall<CommentModel>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public Call<?> getRewards( String token) {

        return mCallApi.getRewards("Bearer " + token);
    }


    public Call<?> createWishList( String token, String name , String desc, String url, String price, String goalId) {

        return mCallApi.createWishList("Bearer " + token, name, desc, url, price, goalId);
    }

    public Call<?> updateWishList( String token, String name , String desc, String url, String price, String id) {

        return mCallApi.updateWishList("Bearer " + token, name, desc, url, price, id);
    }

    public Call<?> createAddress( String token, String address, String state,String country, String zipcode, String goalId) {

        return mCallApi.createDeliveryAddress("Bearer " + token, address, state, country, zipcode, goalId);
    }

    public Call<?> updateAddress( String token, String address, String state,String country, String zipcode, String goalId) {

        return mCallApi.updateDeliveryAddress("Bearer " + token, address, state, country, zipcode, goalId);
    }


    public void deletePost(String token, String postId, final CustomRunnable runnable, final Context context) {
        mCallApi.deletePost("Bearer " +token, postId).enqueue(new CustomApiCall<String>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void reportPost(String token, String postId, String comment, String status, final CustomRunnable runnable, final Context context) {
        mCallApi.postReport("Bearer " +token, postId, comment, status).enqueue(new CustomApiCall<String>(context, HIDE_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage("Thank you for reporting the post."));
            }
        });
    }
    public void postProveComment(String token, String proveId, String postText,final CustomRunnable runnable, final Context context) {
        mCallApi.postProveComment("Bearer " +token, proveId, postText, Utils.getDate()).enqueue(new CustomApiCall<CommentModel>(context, HIDE_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                //Utils.showToast(context,((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void changeStatus(String token, String goalId, String status,final CustomRunnable runnable, final Context context) {
        mCallApi.updateGoal("Bearer " +token, goalId, status).enqueue(new CustomApiCall<GoalModel>(context, HIDE_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack( "Status changed successfully");
            }
        });
    }

    public void deleteGoal(String token, String postId, final CustomRunnable runnable, final Context context) {
        mCallApi.deleteGoal("Bearer " +token, postId).enqueue(new CustomApiCall<String>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void sharePoints(String token, String goalId, final CustomRunnable runnable, final Context context) {
        mCallApi.pointShare("Bearer " +token, goalId, "10").enqueue(new CustomApiCall<String>(context, HIDE_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack("Points shared successfully");
            }
        });
    }

    public Call<?> changePassword(String token, String oldPassword, String newPassword) {
        return mCallApi.changePassword("Bearer " + token, oldPassword, newPassword);
    }

    public Call<?> getNotification( String token) {

        return mCallApi.getNotification("Bearer " + token);
    }

    public void deviceUpdate(String token, String udid, String regToken, final CustomRunnable runnable, final Context context) {
        mCallApi.deviceToken("Bearer " +token, Utils.getDeviceID(context), regToken, "android").enqueue(new CustomApiCall<String>(context, HIDE_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                //((BaseActivity)context).showMessage(mBody.getResponse().getMessage());
            }
        });
    }

    public void sendFeedback(String token, String feedback, String rating, final CustomRunnable runnable, final Context context) {
        mCallApi.feedback("Bearer " +token, feedback, rating).enqueue(new CustomApiCall<String>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void sendFeedback(String token, String feedback, String id,String userId,final CustomRunnable runnable, final Context context) {
        mCallApi.sendFeeback("Bearer " +token, feedback, id,userId).enqueue(new CustomApiCall<CommentModel>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void taskPayment(String token, String nonce,String amount,String taskId, final CustomRunnable runnable, final Context context) {
        mCallApi.taskPayment("Bearer " +token, nonce, amount, taskId).enqueue(new CustomApiCall<String>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void proofPayment(String token, String nonce,String amount,String packageId, final CustomRunnable runnable, final Context context) {
        mCallApi.proofPayment("Bearer " +token, nonce, amount, packageId).enqueue(new CustomApiCall<String>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void logout(String token, final CustomRunnable runnable, final Context context) {
        mCallApi.logout("Bearer " +token, Utils.getDeviceID(context),"android").enqueue(new CustomApiCall<String>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                //Utils.showToast(context,((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public Call<?> getGoalFriends( String token, String goalId) {

        return mCallApi.getFriendList("Bearer " + token, goalId);
    }

    public Call<?> getPayment( String token) {

        return mCallApi.getPayments("Bearer " + token);
    }

    public void deleteDiary(String token, String postId, final CustomRunnable runnable, final Context context) {
        mCallApi.deleteDiary("Bearer " +token, postId).enqueue(new CustomApiCall<String>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public Call<?> getPostDetail( String token, String taskId) {

        return mCallApi.findPostById("Bearer " + token, taskId);
    }

    public Call<?> getProofDetail( String token, String taskId) {

        return mCallApi.findProofById("Bearer " + token, taskId);
    }

    public void deletePostComment(String token, String postId, final CustomRunnable runnable, final Context context) {
        mCallApi.commentRemove("Bearer " +token, postId).enqueue(new CustomApiCall<String>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public void deleteProofComment(String token, String postId, final CustomRunnable runnable, final Context context) {
        mCallApi.proofRemove("Bearer " +token, postId).enqueue(new CustomApiCall<String>(context, SHOW_PROGRESS) {
            @Override
            public void run() {
                runnable.run(mData);
                ((BaseActivity)context).showSnack(((BaseActivity)context).showMessage(mBody.getResponse().getMessage()));
            }
        });
    }

    public Call<?> getInfo( String token,String query) {

        return mCallApi.getInfo("Bearer " + token,query);
    }

}
