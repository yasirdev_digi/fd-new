package com.digitonics.friendshipdiary.webservice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class WebResponse<T> {
    @SerializedName("error")
    @Expose
    private DataResponse error;
    @SerializedName("response")
    @Expose
    private DataResponse<T> response;

    public DataResponse getError() {
        return error;
    }

    public void setError(DataResponse error) {
        this.error = error;
    }

    public DataResponse getResponse() {
        return response;
    }

    public void setResponse(DataResponse response) {
        this.response = response;
    }

}