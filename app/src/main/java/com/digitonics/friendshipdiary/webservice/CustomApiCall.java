package com.digitonics.friendshipdiary.webservice;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import com.digitonics.friendshipdiary.R;
import com.digitonics.friendshipdiary.activities.BaseActivity;
import com.digitonics.friendshipdiary.fragments.InternetFailure;
import com.digitonics.friendshipdiary.utilities.Utils;

import java.lang.annotation.Annotation;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by Syed Kamil Hafeez on 10/16/2017.
 */
public abstract class CustomApiCall<T> implements Callback, Runnable {

    private Context mContext;
    private Dialog mProgressDialog;
    protected WebResponse<T> mBody;
    protected T mData;

    public CustomApiCall(Context context){
        mContext = context;
    }

    public CustomApiCall(Context context, boolean showProgressDialog){
        mContext = context;
        if (showProgressDialog){
            try {
                View view = ((Activity) mContext).getLayoutInflater().inflate(R.layout.progress_bar, null);
                mProgressDialog = Utils.showCustomDialog(mContext, view);
                mProgressDialog.setCancelable(false);
            }catch (Exception ex){
                ex.printStackTrace();
            }

        }
    }

    @Override
    public void onResponse(Call call, Response response) {
        if (mContext instanceof BaseActivity)
            mData = null;


        try {
            if (response.isSuccessful()) {
                mBody = (WebResponse<T>) response.body();
                if (mBody.getResponse() != null)
                    mData = (T) mBody.getResponse().getData();
                run();
            } else {
                Converter<ResponseBody, WebResponse> converter = ((BaseActivity) mContext).mApiBuilder.mRetrofit.responseBodyConverter(WebResponse.class, new Annotation[0]);
                WebResponse error = converter.convert(response.errorBody());
                if (error.getError().getMessage() instanceof ArrayList){
                    ArrayList<String> message = (ArrayList<String>) error.getError().getMessage();
                    if (message.size() > 0)
                        ((BaseActivity) mContext).showSnack(message.get(0).toString());
                }else
                    ((BaseActivity) mContext).showSnack(error.getError().toString());
            }

            dismiss();
        } catch (Exception ex) {
            onFailure(null, ex);
        }
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        dismiss();
        Log.e("Exception",t.getMessage());
        try {
            if (t instanceof UnknownHostException || t instanceof SocketTimeoutException) {
                ((BaseActivity) mContext).showSnack("No internet connection!");
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    protected void dismiss() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
}
